#include "types.h"
#include "stat.h"
#include "user.h"
#include "fs.h"
#include "fcntl.h"

char buf[512];

void fileInFileOut(int fdSrc, int fdDst) {
    int n;
    while((n = read(fdSrc, buf, sizeof(buf))) > 0) {
        if (write(fdDst, buf, n) != n) {
            printf(1, "cat: write error\n");
            exit();
        }
    }
    if(n < 0) {
        printf(1, "cat: read error\n");
        exit();
    }
}

void stdinpInFileOut(int fdDst) { 
    int n;
    n = read(0, buf, sizeof(buf));
    if (write(fdDst, buf, n) != n) {
        printf(1, "cpt: write error\n");
        exit();
    }
}

int main(int argc, char *argv[]) {
    if (argc == 2) {
        int fdDst = 0;
        unlink(argv[1]);
        fdDst = open(argv[1], 1 | O_CREATE);
        stdinpInFileOut(fdDst);
    }
    else if (argc == 3) {
        int fdSrc, fdDst;
        if ((fdSrc = open(argv[1], 0)) < 0){
            printf(1, "cpt: cannot open %s\n", argv[1]);
            exit();
        }
        unlink(argv[2]);
        fdDst = open(argv[2], 1 | O_CREATE);
        fileInFileOut(fdSrc, fdDst);
        close(fdSrc);
        close(fdDst);
    }
    exit();
}