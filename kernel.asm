
kernel:     file format elf32-i386


Disassembly of section .text:

80100000 <multiboot_header>:
80100000:	02 b0 ad 1b 00 00    	add    0x1bad(%eax),%dh
80100006:	00 00                	add    %al,(%eax)
80100008:	fe 4f 52             	decb   0x52(%edi)
8010000b:	e4                   	.byte 0xe4

8010000c <entry>:
8010000c:	0f 20 e0             	mov    %cr4,%eax
8010000f:	83 c8 10             	or     $0x10,%eax
80100012:	0f 22 e0             	mov    %eax,%cr4
80100015:	b8 00 90 10 00       	mov    $0x109000,%eax
8010001a:	0f 22 d8             	mov    %eax,%cr3
8010001d:	0f 20 c0             	mov    %cr0,%eax
80100020:	0d 00 00 01 80       	or     $0x80010000,%eax
80100025:	0f 22 c0             	mov    %eax,%cr0
80100028:	bc c0 b5 10 80       	mov    $0x8010b5c0,%esp
8010002d:	b8 80 31 10 80       	mov    $0x80103180,%eax
80100032:	ff e0                	jmp    *%eax
80100034:	66 90                	xchg   %ax,%ax
80100036:	66 90                	xchg   %ax,%ax
80100038:	66 90                	xchg   %ax,%ax
8010003a:	66 90                	xchg   %ax,%ax
8010003c:	66 90                	xchg   %ax,%ax
8010003e:	66 90                	xchg   %ax,%ax

80100040 <binit>:
  struct buf head;
} bcache;

void
binit(void)
{
80100040:	55                   	push   %ebp
80100041:	89 e5                	mov    %esp,%ebp
80100043:	53                   	push   %ebx

//PAGEBREAK!
  // Create linked list of buffers
  bcache.head.prev = &bcache.head;
  bcache.head.next = &bcache.head;
  for(b = bcache.buf; b < bcache.buf+NBUF; b++){
80100044:	bb f4 b5 10 80       	mov    $0x8010b5f4,%ebx
{
80100049:	83 ec 0c             	sub    $0xc,%esp
  initlock(&bcache.lock, "bcache");
8010004c:	68 a0 71 10 80       	push   $0x801071a0
80100051:	68 c0 b5 10 80       	push   $0x8010b5c0
80100056:	e8 85 44 00 00       	call   801044e0 <initlock>
  bcache.head.prev = &bcache.head;
8010005b:	c7 05 0c fd 10 80 bc 	movl   $0x8010fcbc,0x8010fd0c
80100062:	fc 10 80 
  bcache.head.next = &bcache.head;
80100065:	c7 05 10 fd 10 80 bc 	movl   $0x8010fcbc,0x8010fd10
8010006c:	fc 10 80 
8010006f:	83 c4 10             	add    $0x10,%esp
80100072:	ba bc fc 10 80       	mov    $0x8010fcbc,%edx
80100077:	eb 09                	jmp    80100082 <binit+0x42>
80100079:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80100080:	89 c3                	mov    %eax,%ebx
    b->next = bcache.head.next;
    b->prev = &bcache.head;
    initsleeplock(&b->lock, "buffer");
80100082:	8d 43 0c             	lea    0xc(%ebx),%eax
80100085:	83 ec 08             	sub    $0x8,%esp
    b->next = bcache.head.next;
80100088:	89 53 54             	mov    %edx,0x54(%ebx)
    b->prev = &bcache.head;
8010008b:	c7 43 50 bc fc 10 80 	movl   $0x8010fcbc,0x50(%ebx)
    initsleeplock(&b->lock, "buffer");
80100092:	68 a7 71 10 80       	push   $0x801071a7
80100097:	50                   	push   %eax
80100098:	e8 13 43 00 00       	call   801043b0 <initsleeplock>
    bcache.head.next->prev = b;
8010009d:	a1 10 fd 10 80       	mov    0x8010fd10,%eax
  for(b = bcache.buf; b < bcache.buf+NBUF; b++){
801000a2:	83 c4 10             	add    $0x10,%esp
801000a5:	89 da                	mov    %ebx,%edx
    bcache.head.next->prev = b;
801000a7:	89 58 50             	mov    %ebx,0x50(%eax)
  for(b = bcache.buf; b < bcache.buf+NBUF; b++){
801000aa:	8d 83 5c 02 00 00    	lea    0x25c(%ebx),%eax
    bcache.head.next = b;
801000b0:	89 1d 10 fd 10 80    	mov    %ebx,0x8010fd10
  for(b = bcache.buf; b < bcache.buf+NBUF; b++){
801000b6:	3d bc fc 10 80       	cmp    $0x8010fcbc,%eax
801000bb:	72 c3                	jb     80100080 <binit+0x40>
  }
}
801000bd:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801000c0:	c9                   	leave  
801000c1:	c3                   	ret    
801000c2:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801000c9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

801000d0 <bread>:
}

// Return a locked buf with the contents of the indicated block.
struct buf*
bread(uint dev, uint blockno)
{
801000d0:	55                   	push   %ebp
801000d1:	89 e5                	mov    %esp,%ebp
801000d3:	57                   	push   %edi
801000d4:	56                   	push   %esi
801000d5:	53                   	push   %ebx
801000d6:	83 ec 18             	sub    $0x18,%esp
801000d9:	8b 75 08             	mov    0x8(%ebp),%esi
801000dc:	8b 7d 0c             	mov    0xc(%ebp),%edi
  acquire(&bcache.lock);
801000df:	68 c0 b5 10 80       	push   $0x8010b5c0
801000e4:	e8 37 45 00 00       	call   80104620 <acquire>
  for(b = bcache.head.next; b != &bcache.head; b = b->next){
801000e9:	8b 1d 10 fd 10 80    	mov    0x8010fd10,%ebx
801000ef:	83 c4 10             	add    $0x10,%esp
801000f2:	81 fb bc fc 10 80    	cmp    $0x8010fcbc,%ebx
801000f8:	75 11                	jne    8010010b <bread+0x3b>
801000fa:	eb 24                	jmp    80100120 <bread+0x50>
801000fc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80100100:	8b 5b 54             	mov    0x54(%ebx),%ebx
80100103:	81 fb bc fc 10 80    	cmp    $0x8010fcbc,%ebx
80100109:	74 15                	je     80100120 <bread+0x50>
    if(b->dev == dev && b->blockno == blockno){
8010010b:	3b 73 04             	cmp    0x4(%ebx),%esi
8010010e:	75 f0                	jne    80100100 <bread+0x30>
80100110:	3b 7b 08             	cmp    0x8(%ebx),%edi
80100113:	75 eb                	jne    80100100 <bread+0x30>
      b->refcnt++;
80100115:	83 43 4c 01          	addl   $0x1,0x4c(%ebx)
80100119:	eb 3f                	jmp    8010015a <bread+0x8a>
8010011b:	90                   	nop
8010011c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  for(b = bcache.head.prev; b != &bcache.head; b = b->prev){
80100120:	8b 1d 0c fd 10 80    	mov    0x8010fd0c,%ebx
80100126:	81 fb bc fc 10 80    	cmp    $0x8010fcbc,%ebx
8010012c:	75 0d                	jne    8010013b <bread+0x6b>
8010012e:	eb 60                	jmp    80100190 <bread+0xc0>
80100130:	8b 5b 50             	mov    0x50(%ebx),%ebx
80100133:	81 fb bc fc 10 80    	cmp    $0x8010fcbc,%ebx
80100139:	74 55                	je     80100190 <bread+0xc0>
    if(b->refcnt == 0 && (b->flags & B_DIRTY) == 0) {
8010013b:	8b 43 4c             	mov    0x4c(%ebx),%eax
8010013e:	85 c0                	test   %eax,%eax
80100140:	75 ee                	jne    80100130 <bread+0x60>
80100142:	f6 03 04             	testb  $0x4,(%ebx)
80100145:	75 e9                	jne    80100130 <bread+0x60>
      b->dev = dev;
80100147:	89 73 04             	mov    %esi,0x4(%ebx)
      b->blockno = blockno;
8010014a:	89 7b 08             	mov    %edi,0x8(%ebx)
      b->flags = 0;
8010014d:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
      b->refcnt = 1;
80100153:	c7 43 4c 01 00 00 00 	movl   $0x1,0x4c(%ebx)
      release(&bcache.lock);
8010015a:	83 ec 0c             	sub    $0xc,%esp
8010015d:	68 c0 b5 10 80       	push   $0x8010b5c0
80100162:	e8 79 45 00 00       	call   801046e0 <release>
      acquiresleep(&b->lock);
80100167:	8d 43 0c             	lea    0xc(%ebx),%eax
8010016a:	89 04 24             	mov    %eax,(%esp)
8010016d:	e8 7e 42 00 00       	call   801043f0 <acquiresleep>
80100172:	83 c4 10             	add    $0x10,%esp
  struct buf *b;

  b = bget(dev, blockno);
  if((b->flags & B_VALID) == 0) {
80100175:	f6 03 02             	testb  $0x2,(%ebx)
80100178:	75 0c                	jne    80100186 <bread+0xb6>
    iderw(b);
8010017a:	83 ec 0c             	sub    $0xc,%esp
8010017d:	53                   	push   %ebx
8010017e:	e8 7d 22 00 00       	call   80102400 <iderw>
80100183:	83 c4 10             	add    $0x10,%esp
  }
  return b;
}
80100186:	8d 65 f4             	lea    -0xc(%ebp),%esp
80100189:	89 d8                	mov    %ebx,%eax
8010018b:	5b                   	pop    %ebx
8010018c:	5e                   	pop    %esi
8010018d:	5f                   	pop    %edi
8010018e:	5d                   	pop    %ebp
8010018f:	c3                   	ret    
  panic("bget: no buffers");
80100190:	83 ec 0c             	sub    $0xc,%esp
80100193:	68 ae 71 10 80       	push   $0x801071ae
80100198:	e8 f3 01 00 00       	call   80100390 <panic>
8010019d:	8d 76 00             	lea    0x0(%esi),%esi

801001a0 <bwrite>:

// Write b's contents to disk.  Must be locked.
void
bwrite(struct buf *b)
{
801001a0:	55                   	push   %ebp
801001a1:	89 e5                	mov    %esp,%ebp
801001a3:	53                   	push   %ebx
801001a4:	83 ec 10             	sub    $0x10,%esp
801001a7:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if(!holdingsleep(&b->lock))
801001aa:	8d 43 0c             	lea    0xc(%ebx),%eax
801001ad:	50                   	push   %eax
801001ae:	e8 dd 42 00 00       	call   80104490 <holdingsleep>
801001b3:	83 c4 10             	add    $0x10,%esp
801001b6:	85 c0                	test   %eax,%eax
801001b8:	74 0f                	je     801001c9 <bwrite+0x29>
    panic("bwrite");
  b->flags |= B_DIRTY;
801001ba:	83 0b 04             	orl    $0x4,(%ebx)
  iderw(b);
801001bd:	89 5d 08             	mov    %ebx,0x8(%ebp)
}
801001c0:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801001c3:	c9                   	leave  
  iderw(b);
801001c4:	e9 37 22 00 00       	jmp    80102400 <iderw>
    panic("bwrite");
801001c9:	83 ec 0c             	sub    $0xc,%esp
801001cc:	68 bf 71 10 80       	push   $0x801071bf
801001d1:	e8 ba 01 00 00       	call   80100390 <panic>
801001d6:	8d 76 00             	lea    0x0(%esi),%esi
801001d9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

801001e0 <brelse>:

// Release a locked buffer.
// Move to the head of the MRU list.
void
brelse(struct buf *b)
{
801001e0:	55                   	push   %ebp
801001e1:	89 e5                	mov    %esp,%ebp
801001e3:	56                   	push   %esi
801001e4:	53                   	push   %ebx
801001e5:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if(!holdingsleep(&b->lock))
801001e8:	83 ec 0c             	sub    $0xc,%esp
801001eb:	8d 73 0c             	lea    0xc(%ebx),%esi
801001ee:	56                   	push   %esi
801001ef:	e8 9c 42 00 00       	call   80104490 <holdingsleep>
801001f4:	83 c4 10             	add    $0x10,%esp
801001f7:	85 c0                	test   %eax,%eax
801001f9:	74 66                	je     80100261 <brelse+0x81>
    panic("brelse");

  releasesleep(&b->lock);
801001fb:	83 ec 0c             	sub    $0xc,%esp
801001fe:	56                   	push   %esi
801001ff:	e8 4c 42 00 00       	call   80104450 <releasesleep>

  acquire(&bcache.lock);
80100204:	c7 04 24 c0 b5 10 80 	movl   $0x8010b5c0,(%esp)
8010020b:	e8 10 44 00 00       	call   80104620 <acquire>
  b->refcnt--;
80100210:	8b 43 4c             	mov    0x4c(%ebx),%eax
  if (b->refcnt == 0) {
80100213:	83 c4 10             	add    $0x10,%esp
  b->refcnt--;
80100216:	83 e8 01             	sub    $0x1,%eax
  if (b->refcnt == 0) {
80100219:	85 c0                	test   %eax,%eax
  b->refcnt--;
8010021b:	89 43 4c             	mov    %eax,0x4c(%ebx)
  if (b->refcnt == 0) {
8010021e:	75 2f                	jne    8010024f <brelse+0x6f>
    // no one is waiting for it.
    b->next->prev = b->prev;
80100220:	8b 43 54             	mov    0x54(%ebx),%eax
80100223:	8b 53 50             	mov    0x50(%ebx),%edx
80100226:	89 50 50             	mov    %edx,0x50(%eax)
    b->prev->next = b->next;
80100229:	8b 43 50             	mov    0x50(%ebx),%eax
8010022c:	8b 53 54             	mov    0x54(%ebx),%edx
8010022f:	89 50 54             	mov    %edx,0x54(%eax)
    b->next = bcache.head.next;
80100232:	a1 10 fd 10 80       	mov    0x8010fd10,%eax
    b->prev = &bcache.head;
80100237:	c7 43 50 bc fc 10 80 	movl   $0x8010fcbc,0x50(%ebx)
    b->next = bcache.head.next;
8010023e:	89 43 54             	mov    %eax,0x54(%ebx)
    bcache.head.next->prev = b;
80100241:	a1 10 fd 10 80       	mov    0x8010fd10,%eax
80100246:	89 58 50             	mov    %ebx,0x50(%eax)
    bcache.head.next = b;
80100249:	89 1d 10 fd 10 80    	mov    %ebx,0x8010fd10
  }
  
  release(&bcache.lock);
8010024f:	c7 45 08 c0 b5 10 80 	movl   $0x8010b5c0,0x8(%ebp)
}
80100256:	8d 65 f8             	lea    -0x8(%ebp),%esp
80100259:	5b                   	pop    %ebx
8010025a:	5e                   	pop    %esi
8010025b:	5d                   	pop    %ebp
  release(&bcache.lock);
8010025c:	e9 7f 44 00 00       	jmp    801046e0 <release>
    panic("brelse");
80100261:	83 ec 0c             	sub    $0xc,%esp
80100264:	68 c6 71 10 80       	push   $0x801071c6
80100269:	e8 22 01 00 00       	call   80100390 <panic>
8010026e:	66 90                	xchg   %ax,%ax

80100270 <consoleread>:
  }
}

int
consoleread(struct inode *ip, char *dst, int n)
{
80100270:	55                   	push   %ebp
80100271:	89 e5                	mov    %esp,%ebp
80100273:	57                   	push   %edi
80100274:	56                   	push   %esi
80100275:	53                   	push   %ebx
80100276:	83 ec 28             	sub    $0x28,%esp
80100279:	8b 7d 08             	mov    0x8(%ebp),%edi
8010027c:	8b 75 0c             	mov    0xc(%ebp),%esi
  uint target;
  int c;

  iunlock(ip);
8010027f:	57                   	push   %edi
80100280:	e8 bb 17 00 00       	call   80101a40 <iunlock>
  target = n;
  acquire(&cons.lock);
80100285:	c7 04 24 20 a5 10 80 	movl   $0x8010a520,(%esp)
8010028c:	e8 8f 43 00 00       	call   80104620 <acquire>
  while(n > 0){
80100291:	8b 5d 10             	mov    0x10(%ebp),%ebx
80100294:	83 c4 10             	add    $0x10,%esp
80100297:	31 c0                	xor    %eax,%eax
80100299:	85 db                	test   %ebx,%ebx
8010029b:	0f 8e a1 00 00 00    	jle    80100342 <consoleread+0xd2>
    while(input.r == input.w){
801002a1:	8b 15 a0 ff 10 80    	mov    0x8010ffa0,%edx
801002a7:	39 15 a4 ff 10 80    	cmp    %edx,0x8010ffa4
801002ad:	74 2c                	je     801002db <consoleread+0x6b>
801002af:	eb 5f                	jmp    80100310 <consoleread+0xa0>
801002b1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
      if(myproc()->killed){
        release(&cons.lock);
        ilock(ip);
        return -1;
      }
      sleep(&input.r, &cons.lock);
801002b8:	83 ec 08             	sub    $0x8,%esp
801002bb:	68 20 a5 10 80       	push   $0x8010a520
801002c0:	68 a0 ff 10 80       	push   $0x8010ffa0
801002c5:	e8 96 3d 00 00       	call   80104060 <sleep>
    while(input.r == input.w){
801002ca:	8b 15 a0 ff 10 80    	mov    0x8010ffa0,%edx
801002d0:	83 c4 10             	add    $0x10,%esp
801002d3:	3b 15 a4 ff 10 80    	cmp    0x8010ffa4,%edx
801002d9:	75 35                	jne    80100310 <consoleread+0xa0>
      if(myproc()->killed){
801002db:	e8 e0 37 00 00       	call   80103ac0 <myproc>
801002e0:	8b 40 24             	mov    0x24(%eax),%eax
801002e3:	85 c0                	test   %eax,%eax
801002e5:	74 d1                	je     801002b8 <consoleread+0x48>
        release(&cons.lock);
801002e7:	83 ec 0c             	sub    $0xc,%esp
801002ea:	68 20 a5 10 80       	push   $0x8010a520
801002ef:	e8 ec 43 00 00       	call   801046e0 <release>
        ilock(ip);
801002f4:	89 3c 24             	mov    %edi,(%esp)
801002f7:	e8 64 16 00 00       	call   80101960 <ilock>
        return -1;
801002fc:	83 c4 10             	add    $0x10,%esp
  }
  release(&cons.lock);
  ilock(ip);

  return target - n;
}
801002ff:	8d 65 f4             	lea    -0xc(%ebp),%esp
        return -1;
80100302:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80100307:	5b                   	pop    %ebx
80100308:	5e                   	pop    %esi
80100309:	5f                   	pop    %edi
8010030a:	5d                   	pop    %ebp
8010030b:	c3                   	ret    
8010030c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    c = input.buf[input.r++ % INPUT_BUF];
80100310:	8d 42 01             	lea    0x1(%edx),%eax
80100313:	a3 a0 ff 10 80       	mov    %eax,0x8010ffa0
80100318:	89 d0                	mov    %edx,%eax
8010031a:	83 e0 7f             	and    $0x7f,%eax
8010031d:	0f be 80 20 ff 10 80 	movsbl -0x7fef00e0(%eax),%eax
    if(c == C('D')){  // EOF
80100324:	83 f8 04             	cmp    $0x4,%eax
80100327:	74 3f                	je     80100368 <consoleread+0xf8>
    *dst++ = c;
80100329:	83 c6 01             	add    $0x1,%esi
    --n;
8010032c:	83 eb 01             	sub    $0x1,%ebx
    if(c == '\n')
8010032f:	83 f8 0a             	cmp    $0xa,%eax
    *dst++ = c;
80100332:	88 46 ff             	mov    %al,-0x1(%esi)
    if(c == '\n')
80100335:	74 43                	je     8010037a <consoleread+0x10a>
  while(n > 0){
80100337:	85 db                	test   %ebx,%ebx
80100339:	0f 85 62 ff ff ff    	jne    801002a1 <consoleread+0x31>
8010033f:	8b 45 10             	mov    0x10(%ebp),%eax
  release(&cons.lock);
80100342:	83 ec 0c             	sub    $0xc,%esp
80100345:	89 45 e4             	mov    %eax,-0x1c(%ebp)
80100348:	68 20 a5 10 80       	push   $0x8010a520
8010034d:	e8 8e 43 00 00       	call   801046e0 <release>
  ilock(ip);
80100352:	89 3c 24             	mov    %edi,(%esp)
80100355:	e8 06 16 00 00       	call   80101960 <ilock>
  return target - n;
8010035a:	8b 45 e4             	mov    -0x1c(%ebp),%eax
8010035d:	83 c4 10             	add    $0x10,%esp
}
80100360:	8d 65 f4             	lea    -0xc(%ebp),%esp
80100363:	5b                   	pop    %ebx
80100364:	5e                   	pop    %esi
80100365:	5f                   	pop    %edi
80100366:	5d                   	pop    %ebp
80100367:	c3                   	ret    
80100368:	8b 45 10             	mov    0x10(%ebp),%eax
8010036b:	29 d8                	sub    %ebx,%eax
      if(n < target){
8010036d:	3b 5d 10             	cmp    0x10(%ebp),%ebx
80100370:	73 d0                	jae    80100342 <consoleread+0xd2>
        input.r--;
80100372:	89 15 a0 ff 10 80    	mov    %edx,0x8010ffa0
80100378:	eb c8                	jmp    80100342 <consoleread+0xd2>
8010037a:	8b 45 10             	mov    0x10(%ebp),%eax
8010037d:	29 d8                	sub    %ebx,%eax
8010037f:	eb c1                	jmp    80100342 <consoleread+0xd2>
80100381:	eb 0d                	jmp    80100390 <panic>
80100383:	90                   	nop
80100384:	90                   	nop
80100385:	90                   	nop
80100386:	90                   	nop
80100387:	90                   	nop
80100388:	90                   	nop
80100389:	90                   	nop
8010038a:	90                   	nop
8010038b:	90                   	nop
8010038c:	90                   	nop
8010038d:	90                   	nop
8010038e:	90                   	nop
8010038f:	90                   	nop

80100390 <panic>:
{
80100390:	55                   	push   %ebp
80100391:	89 e5                	mov    %esp,%ebp
80100393:	56                   	push   %esi
80100394:	53                   	push   %ebx
80100395:	83 ec 30             	sub    $0x30,%esp
}

static inline void
cli(void)
{
  asm volatile("cli");
80100398:	fa                   	cli    
  cons.locking = 0;
80100399:	c7 05 54 a5 10 80 00 	movl   $0x0,0x8010a554
801003a0:	00 00 00 
  getcallerpcs(&s, pcs);
801003a3:	8d 5d d0             	lea    -0x30(%ebp),%ebx
801003a6:	8d 75 f8             	lea    -0x8(%ebp),%esi
  cprintf("lapicid %d: panic: ", lapicid());
801003a9:	e8 62 26 00 00       	call   80102a10 <lapicid>
801003ae:	83 ec 08             	sub    $0x8,%esp
801003b1:	50                   	push   %eax
801003b2:	68 cd 71 10 80       	push   $0x801071cd
801003b7:	e8 d4 03 00 00       	call   80100790 <cprintf>
  cprintf(s);
801003bc:	58                   	pop    %eax
801003bd:	ff 75 08             	pushl  0x8(%ebp)
801003c0:	e8 cb 03 00 00       	call   80100790 <cprintf>
  cprintf("\n");
801003c5:	c7 04 24 17 7b 10 80 	movl   $0x80107b17,(%esp)
801003cc:	e8 bf 03 00 00       	call   80100790 <cprintf>
  getcallerpcs(&s, pcs);
801003d1:	5a                   	pop    %edx
801003d2:	8d 45 08             	lea    0x8(%ebp),%eax
801003d5:	59                   	pop    %ecx
801003d6:	53                   	push   %ebx
801003d7:	50                   	push   %eax
801003d8:	e8 23 41 00 00       	call   80104500 <getcallerpcs>
801003dd:	83 c4 10             	add    $0x10,%esp
    cprintf(" %p", pcs[i]);
801003e0:	83 ec 08             	sub    $0x8,%esp
801003e3:	ff 33                	pushl  (%ebx)
801003e5:	83 c3 04             	add    $0x4,%ebx
801003e8:	68 e1 71 10 80       	push   $0x801071e1
801003ed:	e8 9e 03 00 00       	call   80100790 <cprintf>
  for(i=0; i<10; i++)
801003f2:	83 c4 10             	add    $0x10,%esp
801003f5:	39 f3                	cmp    %esi,%ebx
801003f7:	75 e7                	jne    801003e0 <panic+0x50>
  panicked = 1; // freeze other CPU
801003f9:	c7 05 58 a5 10 80 01 	movl   $0x1,0x8010a558
80100400:	00 00 00 
80100403:	eb fe                	jmp    80100403 <panic+0x73>
80100405:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80100409:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80100410 <cgaputc>:
{
80100410:	55                   	push   %ebp
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80100411:	b9 d4 03 00 00       	mov    $0x3d4,%ecx
80100416:	89 ca                	mov    %ecx,%edx
80100418:	89 e5                	mov    %esp,%ebp
8010041a:	57                   	push   %edi
8010041b:	56                   	push   %esi
8010041c:	53                   	push   %ebx
8010041d:	89 c6                	mov    %eax,%esi
8010041f:	b8 0e 00 00 00       	mov    $0xe,%eax
80100424:	83 ec 1c             	sub    $0x1c,%esp
80100427:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80100428:	bb d5 03 00 00       	mov    $0x3d5,%ebx
8010042d:	89 da                	mov    %ebx,%edx
8010042f:	ec                   	in     (%dx),%al
  pos = inb(CRTPORT+1) << 8;
80100430:	0f b6 f8             	movzbl %al,%edi
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80100433:	89 ca                	mov    %ecx,%edx
80100435:	b8 0f 00 00 00       	mov    $0xf,%eax
8010043a:	c1 e7 08             	shl    $0x8,%edi
8010043d:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
8010043e:	89 da                	mov    %ebx,%edx
80100440:	ec                   	in     (%dx),%al
  pos |= inb(CRTPORT+1);
80100441:	0f b6 c8             	movzbl %al,%ecx
80100444:	09 f9                	or     %edi,%ecx
  if(c == '\n')
80100446:	83 fe 0a             	cmp    $0xa,%esi
80100449:	0f 84 01 01 00 00    	je     80100550 <cgaputc+0x140>
  else if(c == BACKSPACE && pos > 0){
8010044f:	81 fe 00 01 00 00    	cmp    $0x100,%esi
80100455:	0f 85 bd 00 00 00    	jne    80100518 <cgaputc+0x108>
8010045b:	85 c9                	test   %ecx,%ecx
8010045d:	0f 84 b5 00 00 00    	je     80100518 <cgaputc+0x108>
    if (input.e != input.w + input.size)
80100463:	a1 ac ff 10 80       	mov    0x8010ffac,%eax
80100468:	8b 15 a4 ff 10 80    	mov    0x8010ffa4,%edx
8010046e:	01 c2                	add    %eax,%edx
80100470:	39 15 a8 ff 10 80    	cmp    %edx,0x8010ffa8
80100476:	74 29                	je     801004a1 <cgaputc+0x91>
      for (int i = pos; i <= pos + input.size; i++)  //shifts null chars
80100478:	01 c8                	add    %ecx,%eax
8010047a:	72 25                	jb     801004a1 <cgaputc+0x91>
8010047c:	8d 84 09 00 80 0b 80 	lea    -0x7ff48000(%ecx,%ecx,1),%eax
80100483:	89 ca                	mov    %ecx,%edx
80100485:	8d 76 00             	lea    0x0(%esi),%esi
        crt[i - 1] = crt[i];
80100488:	0f b7 18             	movzwl (%eax),%ebx
      for (int i = pos; i <= pos + input.size; i++)  //shifts null chars
8010048b:	83 c2 01             	add    $0x1,%edx
8010048e:	83 c0 02             	add    $0x2,%eax
        crt[i - 1] = crt[i];
80100491:	66 89 58 fc          	mov    %bx,-0x4(%eax)
      for (int i = pos; i <= pos + input.size; i++)  //shifts null chars
80100495:	8b 1d ac ff 10 80    	mov    0x8010ffac,%ebx
8010049b:	01 cb                	add    %ecx,%ebx
8010049d:	39 d3                	cmp    %edx,%ebx
8010049f:	73 e7                	jae    80100488 <cgaputc+0x78>
    pos--;
801004a1:	8d 59 ff             	lea    -0x1(%ecx),%ebx
801004a4:	89 d8                	mov    %ebx,%eax
  if(pos < 0 || pos > 25*80)
801004a6:	3d d0 07 00 00       	cmp    $0x7d0,%eax
801004ab:	0f 87 8f 01 00 00    	ja     80100640 <cgaputc+0x230>
  if((pos/80) >= 24){  // Scroll up.
801004b1:	81 fb 7f 07 00 00    	cmp    $0x77f,%ebx
801004b7:	0f 8f b3 00 00 00    	jg     80100570 <cgaputc+0x160>
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
801004bd:	bf d4 03 00 00       	mov    $0x3d4,%edi
801004c2:	b8 0e 00 00 00       	mov    $0xe,%eax
801004c7:	89 fa                	mov    %edi,%edx
801004c9:	ee                   	out    %al,(%dx)
801004ca:	b9 d5 03 00 00       	mov    $0x3d5,%ecx
  outb(CRTPORT+1, pos>>8);
801004cf:	89 d8                	mov    %ebx,%eax
801004d1:	c1 f8 08             	sar    $0x8,%eax
801004d4:	89 ca                	mov    %ecx,%edx
801004d6:	ee                   	out    %al,(%dx)
801004d7:	b8 0f 00 00 00       	mov    $0xf,%eax
801004dc:	89 fa                	mov    %edi,%edx
801004de:	ee                   	out    %al,(%dx)
801004df:	89 d8                	mov    %ebx,%eax
801004e1:	89 ca                	mov    %ecx,%edx
801004e3:	ee                   	out    %al,(%dx)
  if ((c != '{' && input.w + input.size == input.e))
801004e4:	83 fe 7b             	cmp    $0x7b,%esi
801004e7:	74 20                	je     80100509 <cgaputc+0xf9>
801004e9:	a1 ac ff 10 80       	mov    0x8010ffac,%eax
801004ee:	03 05 a4 ff 10 80    	add    0x8010ffa4,%eax
801004f4:	3b 05 a8 ff 10 80    	cmp    0x8010ffa8,%eax
801004fa:	75 0d                	jne    80100509 <cgaputc+0xf9>
    crt[pos] = ' ' | 0x0700;
801004fc:	b8 20 07 00 00       	mov    $0x720,%eax
80100501:	66 89 84 1b 00 80 0b 	mov    %ax,-0x7ff48000(%ebx,%ebx,1)
80100508:	80 
}
80100509:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010050c:	5b                   	pop    %ebx
8010050d:	5e                   	pop    %esi
8010050e:	5f                   	pop    %edi
8010050f:	5d                   	pop    %ebp
80100510:	c3                   	ret    
80100511:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  }else if (c == '{')
80100518:	83 fe 7b             	cmp    $0x7b,%esi
8010051b:	0f 84 af 00 00 00    	je     801005d0 <cgaputc+0x1c0>
  else if (c == '}')
80100521:	83 fe 7d             	cmp    $0x7d,%esi
80100524:	0f 84 86 00 00 00    	je     801005b0 <cgaputc+0x1a0>
  else if (c == 'U') {
8010052a:	83 fe 55             	cmp    $0x55,%esi
8010052d:	0f 84 b5 00 00 00    	je     801005e8 <cgaputc+0x1d8>
    crt[pos++] = (c&0xff) | 0x0700;  // black on white
80100533:	89 f0                	mov    %esi,%eax
80100535:	8d 59 01             	lea    0x1(%ecx),%ebx
80100538:	0f b6 c0             	movzbl %al,%eax
8010053b:	80 cc 07             	or     $0x7,%ah
8010053e:	66 89 84 09 00 80 0b 	mov    %ax,-0x7ff48000(%ecx,%ecx,1)
80100545:	80 
80100546:	89 d8                	mov    %ebx,%eax
80100548:	e9 59 ff ff ff       	jmp    801004a6 <cgaputc+0x96>
8010054d:	8d 76 00             	lea    0x0(%esi),%esi
    pos += 80 - pos%80;
80100550:	89 c8                	mov    %ecx,%eax
80100552:	ba cd cc cc cc       	mov    $0xcccccccd,%edx
80100557:	f7 e2                	mul    %edx
80100559:	c1 ea 06             	shr    $0x6,%edx
8010055c:	8d 04 92             	lea    (%edx,%edx,4),%eax
8010055f:	c1 e0 04             	shl    $0x4,%eax
80100562:	8d 58 50             	lea    0x50(%eax),%ebx
80100565:	89 d8                	mov    %ebx,%eax
80100567:	e9 3a ff ff ff       	jmp    801004a6 <cgaputc+0x96>
8010056c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    memmove(crt, crt+80, sizeof(crt[0])*23*80);
80100570:	83 ec 04             	sub    $0x4,%esp
    pos -= 80;
80100573:	83 eb 50             	sub    $0x50,%ebx
    memmove(crt, crt+80, sizeof(crt[0])*23*80);
80100576:	68 60 0e 00 00       	push   $0xe60
8010057b:	68 a0 80 0b 80       	push   $0x800b80a0
80100580:	68 00 80 0b 80       	push   $0x800b8000
80100585:	e8 56 42 00 00       	call   801047e0 <memmove>
    memset(crt+pos, 0, sizeof(crt[0])*(24*80 - pos));
8010058a:	b8 80 07 00 00       	mov    $0x780,%eax
8010058f:	83 c4 0c             	add    $0xc,%esp
80100592:	29 d8                	sub    %ebx,%eax
80100594:	01 c0                	add    %eax,%eax
80100596:	50                   	push   %eax
80100597:	8d 84 1b 00 80 0b 80 	lea    -0x7ff48000(%ebx,%ebx,1),%eax
8010059e:	6a 00                	push   $0x0
801005a0:	50                   	push   %eax
801005a1:	e8 8a 41 00 00       	call   80104730 <memset>
801005a6:	83 c4 10             	add    $0x10,%esp
801005a9:	e9 0f ff ff ff       	jmp    801004bd <cgaputc+0xad>
801005ae:	66 90                	xchg   %ax,%ax
    pos += input.w + input.size - input.e;
801005b0:	a1 ac ff 10 80       	mov    0x8010ffac,%eax
801005b5:	03 05 a4 ff 10 80    	add    0x8010ffa4,%eax
801005bb:	2b 05 a8 ff 10 80    	sub    0x8010ffa8,%eax
801005c1:	01 c8                	add    %ecx,%eax
801005c3:	89 c3                	mov    %eax,%ebx
801005c5:	e9 dc fe ff ff       	jmp    801004a6 <cgaputc+0x96>
801005ca:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    pos -= (input.e - input.w);
801005d0:	a1 a4 ff 10 80       	mov    0x8010ffa4,%eax
801005d5:	01 c8                	add    %ecx,%eax
801005d7:	2b 05 a8 ff 10 80    	sub    0x8010ffa8,%eax
801005dd:	89 c3                	mov    %eax,%ebx
801005df:	e9 c2 fe ff ff       	jmp    801004a6 <cgaputc+0x96>
801005e4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    for (int i = pos - pos%80 + 2; i < pos + input.size; i++)
801005e8:	89 c8                	mov    %ecx,%eax
801005ea:	ba cd cc cc cc       	mov    $0xcccccccd,%edx
801005ef:	f7 e2                	mul    %edx
801005f1:	c1 ea 06             	shr    $0x6,%edx
801005f4:	8d 1c 92             	lea    (%edx,%edx,4),%ebx
801005f7:	8b 15 ac ff 10 80    	mov    0x8010ffac,%edx
801005fd:	c1 e3 04             	shl    $0x4,%ebx
80100600:	83 c3 02             	add    $0x2,%ebx
80100603:	01 ca                	add    %ecx,%edx
80100605:	39 da                	cmp    %ebx,%edx
80100607:	89 d8                	mov    %ebx,%eax
80100609:	0f 86 97 fe ff ff    	jbe    801004a6 <cgaputc+0x96>
8010060f:	8d 94 1b 00 80 0b 80 	lea    -0x7ff48000(%ebx,%ebx,1),%edx
80100616:	89 df                	mov    %ebx,%edi
80100618:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
8010061b:	90                   	nop
8010061c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
      crt[i] = '\0';
80100620:	31 db                	xor    %ebx,%ebx
    for (int i = pos - pos%80 + 2; i < pos + input.size; i++)
80100622:	83 c7 01             	add    $0x1,%edi
80100625:	83 c2 02             	add    $0x2,%edx
      crt[i] = '\0';
80100628:	66 89 5a fe          	mov    %bx,-0x2(%edx)
    for (int i = pos - pos%80 + 2; i < pos + input.size; i++)
8010062c:	8b 1d ac ff 10 80    	mov    0x8010ffac,%ebx
80100632:	01 cb                	add    %ecx,%ebx
80100634:	39 fb                	cmp    %edi,%ebx
80100636:	77 e8                	ja     80100620 <cgaputc+0x210>
80100638:	8b 5d e4             	mov    -0x1c(%ebp),%ebx
8010063b:	e9 66 fe ff ff       	jmp    801004a6 <cgaputc+0x96>
    panic("pos under/overflow");
80100640:	83 ec 0c             	sub    $0xc,%esp
80100643:	68 e5 71 10 80       	push   $0x801071e5
80100648:	e8 43 fd ff ff       	call   80100390 <panic>
8010064d:	8d 76 00             	lea    0x0(%esi),%esi

80100650 <consputc>:
  if(panicked){
80100650:	8b 15 58 a5 10 80    	mov    0x8010a558,%edx
80100656:	85 d2                	test   %edx,%edx
80100658:	74 06                	je     80100660 <consputc+0x10>
  asm volatile("cli");
8010065a:	fa                   	cli    
8010065b:	eb fe                	jmp    8010065b <consputc+0xb>
8010065d:	8d 76 00             	lea    0x0(%esi),%esi
{
80100660:	55                   	push   %ebp
80100661:	89 e5                	mov    %esp,%ebp
80100663:	53                   	push   %ebx
80100664:	89 c3                	mov    %eax,%ebx
80100666:	83 ec 04             	sub    $0x4,%esp
  if(c == BACKSPACE){
80100669:	3d 00 01 00 00       	cmp    $0x100,%eax
8010066e:	74 17                	je     80100687 <consputc+0x37>
    uartputc(c);
80100670:	83 ec 0c             	sub    $0xc,%esp
80100673:	50                   	push   %eax
80100674:	e8 27 57 00 00       	call   80105da0 <uartputc>
80100679:	83 c4 10             	add    $0x10,%esp
  cgaputc(c);
8010067c:	89 d8                	mov    %ebx,%eax
}
8010067e:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80100681:	c9                   	leave  
  cgaputc(c);
80100682:	e9 89 fd ff ff       	jmp    80100410 <cgaputc>
    uartputc('\b'); uartputc(' '); uartputc('\b');
80100687:	83 ec 0c             	sub    $0xc,%esp
8010068a:	6a 08                	push   $0x8
8010068c:	e8 0f 57 00 00       	call   80105da0 <uartputc>
80100691:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
80100698:	e8 03 57 00 00       	call   80105da0 <uartputc>
8010069d:	c7 04 24 08 00 00 00 	movl   $0x8,(%esp)
801006a4:	e8 f7 56 00 00       	call   80105da0 <uartputc>
801006a9:	83 c4 10             	add    $0x10,%esp
801006ac:	eb ce                	jmp    8010067c <consputc+0x2c>
801006ae:	66 90                	xchg   %ax,%ax

801006b0 <printint>:
{
801006b0:	55                   	push   %ebp
801006b1:	89 e5                	mov    %esp,%ebp
801006b3:	57                   	push   %edi
801006b4:	56                   	push   %esi
801006b5:	53                   	push   %ebx
801006b6:	89 d3                	mov    %edx,%ebx
801006b8:	83 ec 2c             	sub    $0x2c,%esp
  if(sign && (sign = xx < 0))
801006bb:	85 c9                	test   %ecx,%ecx
{
801006bd:	89 4d d4             	mov    %ecx,-0x2c(%ebp)
  if(sign && (sign = xx < 0))
801006c0:	74 04                	je     801006c6 <printint+0x16>
801006c2:	85 c0                	test   %eax,%eax
801006c4:	78 5a                	js     80100720 <printint+0x70>
    x = xx;
801006c6:	c7 45 d4 00 00 00 00 	movl   $0x0,-0x2c(%ebp)
  i = 0;
801006cd:	31 c9                	xor    %ecx,%ecx
801006cf:	8d 75 d7             	lea    -0x29(%ebp),%esi
801006d2:	eb 06                	jmp    801006da <printint+0x2a>
801006d4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    buf[i++] = digits[x % base];
801006d8:	89 f9                	mov    %edi,%ecx
801006da:	31 d2                	xor    %edx,%edx
801006dc:	8d 79 01             	lea    0x1(%ecx),%edi
801006df:	f7 f3                	div    %ebx
801006e1:	0f b6 92 10 72 10 80 	movzbl -0x7fef8df0(%edx),%edx
  }while((x /= base) != 0);
801006e8:	85 c0                	test   %eax,%eax
    buf[i++] = digits[x % base];
801006ea:	88 14 3e             	mov    %dl,(%esi,%edi,1)
  }while((x /= base) != 0);
801006ed:	75 e9                	jne    801006d8 <printint+0x28>
  if(sign)
801006ef:	8b 45 d4             	mov    -0x2c(%ebp),%eax
801006f2:	85 c0                	test   %eax,%eax
801006f4:	74 08                	je     801006fe <printint+0x4e>
    buf[i++] = '-';
801006f6:	c6 44 3d d8 2d       	movb   $0x2d,-0x28(%ebp,%edi,1)
801006fb:	8d 79 02             	lea    0x2(%ecx),%edi
801006fe:	8d 5c 3d d7          	lea    -0x29(%ebp,%edi,1),%ebx
80100702:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    consputc(buf[i]);
80100708:	0f be 03             	movsbl (%ebx),%eax
8010070b:	83 eb 01             	sub    $0x1,%ebx
8010070e:	e8 3d ff ff ff       	call   80100650 <consputc>
  while(--i >= 0)
80100713:	39 f3                	cmp    %esi,%ebx
80100715:	75 f1                	jne    80100708 <printint+0x58>
}
80100717:	83 c4 2c             	add    $0x2c,%esp
8010071a:	5b                   	pop    %ebx
8010071b:	5e                   	pop    %esi
8010071c:	5f                   	pop    %edi
8010071d:	5d                   	pop    %ebp
8010071e:	c3                   	ret    
8010071f:	90                   	nop
    x = -xx;
80100720:	f7 d8                	neg    %eax
80100722:	eb a9                	jmp    801006cd <printint+0x1d>
80100724:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
8010072a:	8d bf 00 00 00 00    	lea    0x0(%edi),%edi

80100730 <consolewrite>:

int
consolewrite(struct inode *ip, char *buf, int n)
{
80100730:	55                   	push   %ebp
80100731:	89 e5                	mov    %esp,%ebp
80100733:	57                   	push   %edi
80100734:	56                   	push   %esi
80100735:	53                   	push   %ebx
80100736:	83 ec 18             	sub    $0x18,%esp
80100739:	8b 75 10             	mov    0x10(%ebp),%esi
  int i;

  iunlock(ip);
8010073c:	ff 75 08             	pushl  0x8(%ebp)
8010073f:	e8 fc 12 00 00       	call   80101a40 <iunlock>
  acquire(&cons.lock);
80100744:	c7 04 24 20 a5 10 80 	movl   $0x8010a520,(%esp)
8010074b:	e8 d0 3e 00 00       	call   80104620 <acquire>
  for(i = 0; i < n; i++)
80100750:	83 c4 10             	add    $0x10,%esp
80100753:	85 f6                	test   %esi,%esi
80100755:	7e 18                	jle    8010076f <consolewrite+0x3f>
80100757:	8b 7d 0c             	mov    0xc(%ebp),%edi
8010075a:	8d 1c 37             	lea    (%edi,%esi,1),%ebx
8010075d:	8d 76 00             	lea    0x0(%esi),%esi
    consputc(buf[i] & 0xff);
80100760:	0f b6 07             	movzbl (%edi),%eax
80100763:	83 c7 01             	add    $0x1,%edi
80100766:	e8 e5 fe ff ff       	call   80100650 <consputc>
  for(i = 0; i < n; i++)
8010076b:	39 fb                	cmp    %edi,%ebx
8010076d:	75 f1                	jne    80100760 <consolewrite+0x30>
  release(&cons.lock);
8010076f:	83 ec 0c             	sub    $0xc,%esp
80100772:	68 20 a5 10 80       	push   $0x8010a520
80100777:	e8 64 3f 00 00       	call   801046e0 <release>
  ilock(ip);
8010077c:	58                   	pop    %eax
8010077d:	ff 75 08             	pushl  0x8(%ebp)
80100780:	e8 db 11 00 00       	call   80101960 <ilock>

  return n;
}
80100785:	8d 65 f4             	lea    -0xc(%ebp),%esp
80100788:	89 f0                	mov    %esi,%eax
8010078a:	5b                   	pop    %ebx
8010078b:	5e                   	pop    %esi
8010078c:	5f                   	pop    %edi
8010078d:	5d                   	pop    %ebp
8010078e:	c3                   	ret    
8010078f:	90                   	nop

80100790 <cprintf>:
{
80100790:	55                   	push   %ebp
80100791:	89 e5                	mov    %esp,%ebp
80100793:	57                   	push   %edi
80100794:	56                   	push   %esi
80100795:	53                   	push   %ebx
80100796:	83 ec 1c             	sub    $0x1c,%esp
  locking = cons.locking;
80100799:	a1 54 a5 10 80       	mov    0x8010a554,%eax
  if(locking)
8010079e:	85 c0                	test   %eax,%eax
  locking = cons.locking;
801007a0:	89 45 dc             	mov    %eax,-0x24(%ebp)
  if(locking)
801007a3:	0f 85 6f 01 00 00    	jne    80100918 <cprintf+0x188>
  if (fmt == 0)
801007a9:	8b 45 08             	mov    0x8(%ebp),%eax
801007ac:	85 c0                	test   %eax,%eax
801007ae:	89 c7                	mov    %eax,%edi
801007b0:	0f 84 77 01 00 00    	je     8010092d <cprintf+0x19d>
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
801007b6:	0f b6 00             	movzbl (%eax),%eax
  argp = (uint*)(void*)(&fmt + 1);
801007b9:	8d 4d 0c             	lea    0xc(%ebp),%ecx
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
801007bc:	31 db                	xor    %ebx,%ebx
  argp = (uint*)(void*)(&fmt + 1);
801007be:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
801007c1:	85 c0                	test   %eax,%eax
801007c3:	75 56                	jne    8010081b <cprintf+0x8b>
801007c5:	eb 79                	jmp    80100840 <cprintf+0xb0>
801007c7:	89 f6                	mov    %esi,%esi
801007c9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
    c = fmt[++i] & 0xff;
801007d0:	0f b6 16             	movzbl (%esi),%edx
    if(c == 0)
801007d3:	85 d2                	test   %edx,%edx
801007d5:	74 69                	je     80100840 <cprintf+0xb0>
801007d7:	83 c3 02             	add    $0x2,%ebx
    switch(c){
801007da:	83 fa 70             	cmp    $0x70,%edx
801007dd:	8d 34 1f             	lea    (%edi,%ebx,1),%esi
801007e0:	0f 84 84 00 00 00    	je     8010086a <cprintf+0xda>
801007e6:	7f 78                	jg     80100860 <cprintf+0xd0>
801007e8:	83 fa 25             	cmp    $0x25,%edx
801007eb:	0f 84 ff 00 00 00    	je     801008f0 <cprintf+0x160>
801007f1:	83 fa 64             	cmp    $0x64,%edx
801007f4:	0f 85 8e 00 00 00    	jne    80100888 <cprintf+0xf8>
      printint(*argp++, 10, 1);
801007fa:	8b 45 e4             	mov    -0x1c(%ebp),%eax
801007fd:	ba 0a 00 00 00       	mov    $0xa,%edx
80100802:	8d 48 04             	lea    0x4(%eax),%ecx
80100805:	8b 00                	mov    (%eax),%eax
80100807:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
8010080a:	b9 01 00 00 00       	mov    $0x1,%ecx
8010080f:	e8 9c fe ff ff       	call   801006b0 <printint>
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
80100814:	0f b6 06             	movzbl (%esi),%eax
80100817:	85 c0                	test   %eax,%eax
80100819:	74 25                	je     80100840 <cprintf+0xb0>
8010081b:	8d 53 01             	lea    0x1(%ebx),%edx
    if(c != '%'){
8010081e:	83 f8 25             	cmp    $0x25,%eax
80100821:	8d 34 17             	lea    (%edi,%edx,1),%esi
80100824:	74 aa                	je     801007d0 <cprintf+0x40>
80100826:	89 55 e0             	mov    %edx,-0x20(%ebp)
      consputc(c);
80100829:	e8 22 fe ff ff       	call   80100650 <consputc>
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
8010082e:	0f b6 06             	movzbl (%esi),%eax
      continue;
80100831:	8b 55 e0             	mov    -0x20(%ebp),%edx
80100834:	89 d3                	mov    %edx,%ebx
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
80100836:	85 c0                	test   %eax,%eax
80100838:	75 e1                	jne    8010081b <cprintf+0x8b>
8010083a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  if(locking)
80100840:	8b 45 dc             	mov    -0x24(%ebp),%eax
80100843:	85 c0                	test   %eax,%eax
80100845:	74 10                	je     80100857 <cprintf+0xc7>
    release(&cons.lock);
80100847:	83 ec 0c             	sub    $0xc,%esp
8010084a:	68 20 a5 10 80       	push   $0x8010a520
8010084f:	e8 8c 3e 00 00       	call   801046e0 <release>
80100854:	83 c4 10             	add    $0x10,%esp
}
80100857:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010085a:	5b                   	pop    %ebx
8010085b:	5e                   	pop    %esi
8010085c:	5f                   	pop    %edi
8010085d:	5d                   	pop    %ebp
8010085e:	c3                   	ret    
8010085f:	90                   	nop
    switch(c){
80100860:	83 fa 73             	cmp    $0x73,%edx
80100863:	74 43                	je     801008a8 <cprintf+0x118>
80100865:	83 fa 78             	cmp    $0x78,%edx
80100868:	75 1e                	jne    80100888 <cprintf+0xf8>
      printint(*argp++, 16, 0);
8010086a:	8b 45 e4             	mov    -0x1c(%ebp),%eax
8010086d:	ba 10 00 00 00       	mov    $0x10,%edx
80100872:	8d 48 04             	lea    0x4(%eax),%ecx
80100875:	8b 00                	mov    (%eax),%eax
80100877:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
8010087a:	31 c9                	xor    %ecx,%ecx
8010087c:	e8 2f fe ff ff       	call   801006b0 <printint>
      break;
80100881:	eb 91                	jmp    80100814 <cprintf+0x84>
80100883:	90                   	nop
80100884:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
      consputc('%');
80100888:	b8 25 00 00 00       	mov    $0x25,%eax
8010088d:	89 55 e0             	mov    %edx,-0x20(%ebp)
80100890:	e8 bb fd ff ff       	call   80100650 <consputc>
      consputc(c);
80100895:	8b 55 e0             	mov    -0x20(%ebp),%edx
80100898:	89 d0                	mov    %edx,%eax
8010089a:	e8 b1 fd ff ff       	call   80100650 <consputc>
      break;
8010089f:	e9 70 ff ff ff       	jmp    80100814 <cprintf+0x84>
801008a4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
      if((s = (char*)*argp++) == 0)
801008a8:	8b 45 e4             	mov    -0x1c(%ebp),%eax
801008ab:	8b 10                	mov    (%eax),%edx
801008ad:	8d 48 04             	lea    0x4(%eax),%ecx
801008b0:	89 4d e0             	mov    %ecx,-0x20(%ebp)
801008b3:	85 d2                	test   %edx,%edx
801008b5:	74 49                	je     80100900 <cprintf+0x170>
      for(; *s; s++)
801008b7:	0f be 02             	movsbl (%edx),%eax
      if((s = (char*)*argp++) == 0)
801008ba:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
      for(; *s; s++)
801008bd:	84 c0                	test   %al,%al
801008bf:	0f 84 4f ff ff ff    	je     80100814 <cprintf+0x84>
801008c5:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
801008c8:	89 d3                	mov    %edx,%ebx
801008ca:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
801008d0:	83 c3 01             	add    $0x1,%ebx
        consputc(*s);
801008d3:	e8 78 fd ff ff       	call   80100650 <consputc>
      for(; *s; s++)
801008d8:	0f be 03             	movsbl (%ebx),%eax
801008db:	84 c0                	test   %al,%al
801008dd:	75 f1                	jne    801008d0 <cprintf+0x140>
      if((s = (char*)*argp++) == 0)
801008df:	8b 45 e0             	mov    -0x20(%ebp),%eax
801008e2:	8b 5d e4             	mov    -0x1c(%ebp),%ebx
801008e5:	89 45 e4             	mov    %eax,-0x1c(%ebp)
801008e8:	e9 27 ff ff ff       	jmp    80100814 <cprintf+0x84>
801008ed:	8d 76 00             	lea    0x0(%esi),%esi
      consputc('%');
801008f0:	b8 25 00 00 00       	mov    $0x25,%eax
801008f5:	e8 56 fd ff ff       	call   80100650 <consputc>
      break;
801008fa:	e9 15 ff ff ff       	jmp    80100814 <cprintf+0x84>
801008ff:	90                   	nop
        s = "(null)";
80100900:	ba f8 71 10 80       	mov    $0x801071f8,%edx
      for(; *s; s++)
80100905:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
80100908:	b8 28 00 00 00       	mov    $0x28,%eax
8010090d:	89 d3                	mov    %edx,%ebx
8010090f:	eb bf                	jmp    801008d0 <cprintf+0x140>
80100911:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    acquire(&cons.lock);
80100918:	83 ec 0c             	sub    $0xc,%esp
8010091b:	68 20 a5 10 80       	push   $0x8010a520
80100920:	e8 fb 3c 00 00       	call   80104620 <acquire>
80100925:	83 c4 10             	add    $0x10,%esp
80100928:	e9 7c fe ff ff       	jmp    801007a9 <cprintf+0x19>
    panic("null fmt");
8010092d:	83 ec 0c             	sub    $0xc,%esp
80100930:	68 ff 71 10 80       	push   $0x801071ff
80100935:	e8 56 fa ff ff       	call   80100390 <panic>
8010093a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80100940 <consoleintr>:
{
80100940:	55                   	push   %ebp
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80100941:	b8 0e 00 00 00       	mov    $0xe,%eax
80100946:	89 e5                	mov    %esp,%ebp
80100948:	57                   	push   %edi
80100949:	56                   	push   %esi
8010094a:	53                   	push   %ebx
8010094b:	bf d4 03 00 00       	mov    $0x3d4,%edi
80100950:	89 fa                	mov    %edi,%edx
80100952:	83 ec 38             	sub    $0x38,%esp
80100955:	8b 75 08             	mov    0x8(%ebp),%esi
80100958:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80100959:	b9 d5 03 00 00       	mov    $0x3d5,%ecx
8010095e:	89 ca                	mov    %ecx,%edx
80100960:	ec                   	in     (%dx),%al
  pos = inb(CRTPORT+1) << 8;
80100961:	0f b6 d8             	movzbl %al,%ebx
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80100964:	89 fa                	mov    %edi,%edx
80100966:	b8 0f 00 00 00       	mov    $0xf,%eax
8010096b:	c1 e3 08             	shl    $0x8,%ebx
8010096e:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
8010096f:	89 ca                	mov    %ecx,%edx
80100971:	ec                   	in     (%dx),%al
  pos |= inb(CRTPORT+1);
80100972:	0f b6 c0             	movzbl %al,%eax
  acquire(&cons.lock);
80100975:	68 20 a5 10 80       	push   $0x8010a520
  pos |= inb(CRTPORT+1);
8010097a:	09 d8                	or     %ebx,%eax
8010097c:	89 c7                	mov    %eax,%edi
  acquire(&cons.lock);
8010097e:	e8 9d 3c 00 00       	call   80104620 <acquire>
          for (int i = pos - (pos%80) + input.size % INPUT_BUF; i >= pos; i--)
80100983:	89 f8                	mov    %edi,%eax
80100985:	ba cd cc cc cc       	mov    $0xcccccccd,%edx
8010098a:	89 7d e0             	mov    %edi,-0x20(%ebp)
8010098d:	f7 e2                	mul    %edx
8010098f:	83 c4 10             	add    $0x10,%esp
80100992:	c1 ea 06             	shr    $0x6,%edx
80100995:	8d 04 92             	lea    (%edx,%edx,4),%eax
80100998:	89 c1                	mov    %eax,%ecx
8010099a:	8d 84 3f fe 7f 0b 80 	lea    -0x7ff48002(%edi,%edi,1),%eax
  int c, doprocdump = 0, pos;
801009a1:	31 ff                	xor    %edi,%edi
          for (int i = pos - (pos%80) + input.size % INPUT_BUF; i >= pos; i--)
801009a3:	c1 e1 04             	shl    $0x4,%ecx
801009a6:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
801009a9:	89 45 dc             	mov    %eax,-0x24(%ebp)
801009ac:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  while((c = getc()) >= 0){
801009b0:	ff d6                	call   *%esi
801009b2:	85 c0                	test   %eax,%eax
801009b4:	89 c3                	mov    %eax,%ebx
801009b6:	78 48                	js     80100a00 <consoleintr+0xc0>
    switch(c){
801009b8:	83 fb 15             	cmp    $0x15,%ebx
801009bb:	0f 84 a7 01 00 00    	je     80100b68 <consoleintr+0x228>
801009c1:	7e 5d                	jle    80100a20 <consoleintr+0xe0>
801009c3:	83 fb 7d             	cmp    $0x7d,%ebx
801009c6:	0f 84 3c 02 00 00    	je     80100c08 <consoleintr+0x2c8>
801009cc:	83 fb 7f             	cmp    $0x7f,%ebx
801009cf:	0f 84 bb 01 00 00    	je     80100b90 <consoleintr+0x250>
801009d5:	83 fb 7b             	cmp    $0x7b,%ebx
801009d8:	75 66                	jne    80100a40 <consoleintr+0x100>
      consputc(c);
801009da:	b8 7b 00 00 00       	mov    $0x7b,%eax
801009df:	e8 6c fc ff ff       	call   80100650 <consputc>
      input.e = input.w;
801009e4:	a1 a4 ff 10 80       	mov    0x8010ffa4,%eax
801009e9:	a3 a8 ff 10 80       	mov    %eax,0x8010ffa8
  while((c = getc()) >= 0){
801009ee:	ff d6                	call   *%esi
801009f0:	85 c0                	test   %eax,%eax
801009f2:	89 c3                	mov    %eax,%ebx
801009f4:	79 c2                	jns    801009b8 <consoleintr+0x78>
801009f6:	8d 76 00             	lea    0x0(%esi),%esi
801009f9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
  release(&cons.lock);
80100a00:	83 ec 0c             	sub    $0xc,%esp
80100a03:	68 20 a5 10 80       	push   $0x8010a520
80100a08:	e8 d3 3c 00 00       	call   801046e0 <release>
  if(doprocdump) {
80100a0d:	83 c4 10             	add    $0x10,%esp
80100a10:	85 ff                	test   %edi,%edi
80100a12:	0f 85 18 02 00 00    	jne    80100c30 <consoleintr+0x2f0>
}
80100a18:	8d 65 f4             	lea    -0xc(%ebp),%esp
80100a1b:	5b                   	pop    %ebx
80100a1c:	5e                   	pop    %esi
80100a1d:	5f                   	pop    %edi
80100a1e:	5d                   	pop    %ebp
80100a1f:	c3                   	ret    
    switch(c){
80100a20:	83 fb 08             	cmp    $0x8,%ebx
80100a23:	0f 84 67 01 00 00    	je     80100b90 <consoleintr+0x250>
80100a29:	83 fb 10             	cmp    $0x10,%ebx
80100a2c:	75 12                	jne    80100a40 <consoleintr+0x100>
      doprocdump = 1;
80100a2e:	bf 01 00 00 00       	mov    $0x1,%edi
80100a33:	e9 78 ff ff ff       	jmp    801009b0 <consoleintr+0x70>
80100a38:	90                   	nop
80100a39:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
      if(c != 0 && input.e-input.r < INPUT_BUF){
80100a40:	85 db                	test   %ebx,%ebx
80100a42:	0f 84 68 ff ff ff    	je     801009b0 <consoleintr+0x70>
80100a48:	a1 a8 ff 10 80       	mov    0x8010ffa8,%eax
80100a4d:	89 c2                	mov    %eax,%edx
80100a4f:	2b 15 a0 ff 10 80    	sub    0x8010ffa0,%edx
80100a55:	89 45 d8             	mov    %eax,-0x28(%ebp)
80100a58:	83 fa 7f             	cmp    $0x7f,%edx
80100a5b:	0f 87 4f ff ff ff    	ja     801009b0 <consoleintr+0x70>
80100a61:	a1 ac ff 10 80       	mov    0x8010ffac,%eax
80100a66:	8b 15 a4 ff 10 80    	mov    0x8010ffa4,%edx
        c = (c == '\r') ? '\n' : c;
80100a6c:	83 fb 0d             	cmp    $0xd,%ebx
80100a6f:	8d 48 01             	lea    0x1(%eax),%ecx
80100a72:	8d 04 0a             	lea    (%edx,%ecx,1),%eax
        input.size++;
80100a75:	89 0d ac ff 10 80    	mov    %ecx,0x8010ffac
80100a7b:	89 45 d4             	mov    %eax,-0x2c(%ebp)
        c = (c == '\r') ? '\n' : c;
80100a7e:	0f 84 bc 01 00 00    	je     80100c40 <consoleintr+0x300>
        input.e++;
80100a84:	8b 45 d8             	mov    -0x28(%ebp),%eax
80100a87:	83 c0 01             	add    $0x1,%eax
        if (input.w + input.size != input.e && c != '\n') {
80100a8a:	3b 45 d4             	cmp    -0x2c(%ebp),%eax
        input.e++;
80100a8d:	a3 a8 ff 10 80       	mov    %eax,0x8010ffa8
        if (input.w + input.size != input.e && c != '\n') {
80100a92:	0f 84 f5 01 00 00    	je     80100c8d <consoleintr+0x34d>
80100a98:	83 fb 0a             	cmp    $0xa,%ebx
80100a9b:	0f 84 9f 01 00 00    	je     80100c40 <consoleintr+0x300>
          for (int i = pos - (pos%80) + input.size % INPUT_BUF; i >= pos; i--)
80100aa1:	8b 55 e4             	mov    -0x1c(%ebp),%edx
80100aa4:	83 e1 7f             	and    $0x7f,%ecx
80100aa7:	01 ca                	add    %ecx,%edx
80100aa9:	39 55 e0             	cmp    %edx,-0x20(%ebp)
80100aac:	7f 33                	jg     80100ae1 <consoleintr+0x1a1>
80100aae:	8b 4d dc             	mov    -0x24(%ebp),%ecx
80100ab1:	8d 84 12 00 80 0b 80 	lea    -0x7ff48000(%edx,%edx,1),%eax
80100ab8:	90                   	nop
80100ab9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
            crt[i + 1] = crt[i];
80100ac0:	0f b7 10             	movzwl (%eax),%edx
80100ac3:	83 e8 02             	sub    $0x2,%eax
80100ac6:	66 89 50 04          	mov    %dx,0x4(%eax)
          for (int i = pos - (pos%80) + input.size % INPUT_BUF; i >= pos; i--)
80100aca:	39 c8                	cmp    %ecx,%eax
80100acc:	75 f2                	jne    80100ac0 <consoleintr+0x180>
80100ace:	a1 ac ff 10 80       	mov    0x8010ffac,%eax
80100ad3:	03 05 a4 ff 10 80    	add    0x8010ffa4,%eax
80100ad9:	89 45 d4             	mov    %eax,-0x2c(%ebp)
80100adc:	a1 a8 ff 10 80       	mov    0x8010ffa8,%eax
          for (int i = (input.w + input.size) % INPUT_BUF; i >= input.e % INPUT_BUF; i--)
80100ae1:	8b 55 d4             	mov    -0x2c(%ebp),%edx
80100ae4:	83 e0 7f             	and    $0x7f,%eax
80100ae7:	83 e2 7f             	and    $0x7f,%edx
80100aea:	39 d0                	cmp    %edx,%eax
80100aec:	77 16                	ja     80100b04 <consoleintr+0x1c4>
80100aee:	66 90                	xchg   %ax,%ax
            input.buf[i + 1] = input.buf[i];
80100af0:	0f b6 8a 20 ff 10 80 	movzbl -0x7fef00e0(%edx),%ecx
          for (int i = (input.w + input.size) % INPUT_BUF; i >= input.e % INPUT_BUF; i--)
80100af7:	83 ea 01             	sub    $0x1,%edx
            input.buf[i + 1] = input.buf[i];
80100afa:	88 8a 22 ff 10 80    	mov    %cl,-0x7fef00de(%edx)
          for (int i = (input.w + input.size) % INPUT_BUF; i >= input.e % INPUT_BUF; i--)
80100b00:	39 d0                	cmp    %edx,%eax
80100b02:	76 ec                	jbe    80100af0 <consoleintr+0x1b0>
          input.buf[input.e % INPUT_BUF] = c;
80100b04:	88 98 20 ff 10 80    	mov    %bl,-0x7fef00e0(%eax)
        consputc(c);
80100b0a:	89 d8                	mov    %ebx,%eax
80100b0c:	e8 3f fb ff ff       	call   80100650 <consputc>
        if(c == '\n' || c == C('D') || input.e == input.r+INPUT_BUF){
80100b11:	83 fb 04             	cmp    $0x4,%ebx
80100b14:	74 14                	je     80100b2a <consoleintr+0x1ea>
80100b16:	a1 a0 ff 10 80       	mov    0x8010ffa0,%eax
80100b1b:	83 e8 80             	sub    $0xffffff80,%eax
80100b1e:	39 05 a8 ff 10 80    	cmp    %eax,0x8010ffa8
80100b24:	0f 85 86 fe ff ff    	jne    801009b0 <consoleintr+0x70>
          input.e = (input.w + input.size);
80100b2a:	a1 ac ff 10 80       	mov    0x8010ffac,%eax
80100b2f:	03 05 a4 ff 10 80    	add    0x8010ffa4,%eax
          wakeup(&input.r);
80100b35:	83 ec 0c             	sub    $0xc,%esp
80100b38:	68 a0 ff 10 80       	push   $0x8010ffa0
          input.size = 0;
80100b3d:	c7 05 ac ff 10 80 00 	movl   $0x0,0x8010ffac
80100b44:	00 00 00 
          input.e = (input.w + input.size);
80100b47:	a3 a8 ff 10 80       	mov    %eax,0x8010ffa8
          input.buf[input.e] = '\0'; //deleting the second \n
80100b4c:	c6 80 20 ff 10 80 00 	movb   $0x0,-0x7fef00e0(%eax)
          input.w = input.e;
80100b53:	a3 a4 ff 10 80       	mov    %eax,0x8010ffa4
          wakeup(&input.r);
80100b58:	e8 b3 36 00 00       	call   80104210 <wakeup>
80100b5d:	83 c4 10             	add    $0x10,%esp
80100b60:	e9 4b fe ff ff       	jmp    801009b0 <consoleintr+0x70>
80100b65:	8d 76 00             	lea    0x0(%esi),%esi
      cgaputc('U');
80100b68:	b8 55 00 00 00       	mov    $0x55,%eax
80100b6d:	e8 9e f8 ff ff       	call   80100410 <cgaputc>
      input.e = input.w;
80100b72:	a1 a4 ff 10 80       	mov    0x8010ffa4,%eax
      input.size = 0;
80100b77:	c7 05 ac ff 10 80 00 	movl   $0x0,0x8010ffac
80100b7e:	00 00 00 
      input.e = input.w;
80100b81:	a3 a8 ff 10 80       	mov    %eax,0x8010ffa8
      break;
80100b86:	e9 25 fe ff ff       	jmp    801009b0 <consoleintr+0x70>
80100b8b:	90                   	nop
80100b8c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
      if(input.e != input.w){
80100b90:	8b 0d a8 ff 10 80    	mov    0x8010ffa8,%ecx
80100b96:	8b 15 a4 ff 10 80    	mov    0x8010ffa4,%edx
80100b9c:	39 d1                	cmp    %edx,%ecx
80100b9e:	0f 84 0c fe ff ff    	je     801009b0 <consoleintr+0x70>
        for (int i = input.e % INPUT_BUF; i < (input.w + input.size) % INPUT_BUF; i++)
80100ba4:	8b 1d ac ff 10 80    	mov    0x8010ffac,%ebx
80100baa:	89 c8                	mov    %ecx,%eax
80100bac:	83 e0 7f             	and    $0x7f,%eax
80100baf:	01 da                	add    %ebx,%edx
80100bb1:	83 e2 7f             	and    $0x7f,%edx
80100bb4:	39 c2                	cmp    %eax,%edx
80100bb6:	76 29                	jbe    80100be1 <consoleintr+0x2a1>
80100bb8:	05 20 ff 10 80       	add    $0x8010ff20,%eax
80100bbd:	81 c2 20 ff 10 80    	add    $0x8010ff20,%edx
80100bc3:	89 4d d8             	mov    %ecx,-0x28(%ebp)
80100bc6:	8d 76 00             	lea    0x0(%esi),%esi
80100bc9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
          input.buf[i] = input.buf[i + 1];
80100bd0:	0f b6 48 01          	movzbl 0x1(%eax),%ecx
80100bd4:	83 c0 01             	add    $0x1,%eax
80100bd7:	88 48 ff             	mov    %cl,-0x1(%eax)
        for (int i = input.e % INPUT_BUF; i < (input.w + input.size) % INPUT_BUF; i++)
80100bda:	39 c2                	cmp    %eax,%edx
80100bdc:	75 f2                	jne    80100bd0 <consoleintr+0x290>
80100bde:	8b 4d d8             	mov    -0x28(%ebp),%ecx
        input.e--;
80100be1:	83 e9 01             	sub    $0x1,%ecx
        input.size--;
80100be4:	83 eb 01             	sub    $0x1,%ebx
        consputc(BACKSPACE);
80100be7:	b8 00 01 00 00       	mov    $0x100,%eax
        input.e--;
80100bec:	89 0d a8 ff 10 80    	mov    %ecx,0x8010ffa8
        input.size--;
80100bf2:	89 1d ac ff 10 80    	mov    %ebx,0x8010ffac
        consputc(BACKSPACE);
80100bf8:	e8 53 fa ff ff       	call   80100650 <consputc>
80100bfd:	e9 ae fd ff ff       	jmp    801009b0 <consoleintr+0x70>
80100c02:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
      consputc('}');
80100c08:	b8 7d 00 00 00       	mov    $0x7d,%eax
80100c0d:	e8 3e fa ff ff       	call   80100650 <consputc>
      input.e = input.w + input.size;
80100c12:	a1 ac ff 10 80       	mov    0x8010ffac,%eax
80100c17:	03 05 a4 ff 10 80    	add    0x8010ffa4,%eax
80100c1d:	a3 a8 ff 10 80       	mov    %eax,0x8010ffa8
      break;
80100c22:	e9 89 fd ff ff       	jmp    801009b0 <consoleintr+0x70>
80100c27:	89 f6                	mov    %esi,%esi
80100c29:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
}
80100c30:	8d 65 f4             	lea    -0xc(%ebp),%esp
80100c33:	5b                   	pop    %ebx
80100c34:	5e                   	pop    %esi
80100c35:	5f                   	pop    %edi
80100c36:	5d                   	pop    %ebp
    procdump();  // now call procdump() wo. cons.lock held
80100c37:	e9 b4 36 00 00       	jmp    801042f0 <procdump>
80100c3c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
          input.e = input.w + input.size;
80100c40:	8b 45 d4             	mov    -0x2c(%ebp),%eax
          input.buf[input.w % INPUT_BUF + input.size] = c;
80100c43:	83 e2 7f             	and    $0x7f,%edx
80100c46:	c6 84 11 20 ff 10 80 	movb   $0xa,-0x7fef00e0(%ecx,%edx,1)
80100c4d:	0a 
          for (int i = input.w % INPUT_BUF; i < (input.w + input.size) % INPUT_BUF; i++) //shift all buffer chars to delete the first null char
80100c4e:	89 c1                	mov    %eax,%ecx
          input.e = input.w + input.size;
80100c50:	a3 a8 ff 10 80       	mov    %eax,0x8010ffa8
          for (int i = input.w % INPUT_BUF; i < (input.w + input.size) % INPUT_BUF; i++) //shift all buffer chars to delete the first null char
80100c55:	83 e1 7f             	and    $0x7f,%ecx
80100c58:	39 d1                	cmp    %edx,%ecx
80100c5a:	76 22                	jbe    80100c7e <consoleintr+0x33e>
80100c5c:	8d 82 20 ff 10 80    	lea    -0x7fef00e0(%edx),%eax
80100c62:	81 c1 20 ff 10 80    	add    $0x8010ff20,%ecx
80100c68:	90                   	nop
80100c69:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
            input.buf[i] = input.buf[i + 1];
80100c70:	0f b6 50 01          	movzbl 0x1(%eax),%edx
80100c74:	83 c0 01             	add    $0x1,%eax
80100c77:	88 50 ff             	mov    %dl,-0x1(%eax)
          for (int i = input.w % INPUT_BUF; i < (input.w + input.size) % INPUT_BUF; i++) //shift all buffer chars to delete the first null char
80100c7a:	39 c1                	cmp    %eax,%ecx
80100c7c:	75 f2                	jne    80100c70 <consoleintr+0x330>
        consputc(c);
80100c7e:	b8 0a 00 00 00       	mov    $0xa,%eax
80100c83:	e8 c8 f9 ff ff       	call   80100650 <consputc>
80100c88:	e9 9d fe ff ff       	jmp    80100b2a <consoleintr+0x1ea>
        if (c != '\n')
80100c8d:	83 fb 0a             	cmp    $0xa,%ebx
80100c90:	74 ae                	je     80100c40 <consoleintr+0x300>
80100c92:	83 e0 7f             	and    $0x7f,%eax
80100c95:	e9 6a fe ff ff       	jmp    80100b04 <consoleintr+0x1c4>
80100c9a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80100ca0 <consoleinit>:

void
consoleinit(void)
{
80100ca0:	55                   	push   %ebp
80100ca1:	89 e5                	mov    %esp,%ebp
80100ca3:	83 ec 10             	sub    $0x10,%esp
  initlock(&cons.lock, "console");
80100ca6:	68 08 72 10 80       	push   $0x80107208
80100cab:	68 20 a5 10 80       	push   $0x8010a520
80100cb0:	e8 2b 38 00 00       	call   801044e0 <initlock>

  devsw[CONSOLE].write = consolewrite;
  devsw[CONSOLE].read = consoleread;
  cons.locking = 1;

  ioapicenable(IRQ_KBD, 0);
80100cb5:	58                   	pop    %eax
80100cb6:	5a                   	pop    %edx
80100cb7:	6a 00                	push   $0x0
80100cb9:	6a 01                	push   $0x1
  devsw[CONSOLE].write = consolewrite;
80100cbb:	c7 05 6c 09 11 80 30 	movl   $0x80100730,0x8011096c
80100cc2:	07 10 80 
  devsw[CONSOLE].read = consoleread;
80100cc5:	c7 05 68 09 11 80 70 	movl   $0x80100270,0x80110968
80100ccc:	02 10 80 
  cons.locking = 1;
80100ccf:	c7 05 54 a5 10 80 01 	movl   $0x1,0x8010a554
80100cd6:	00 00 00 
  ioapicenable(IRQ_KBD, 0);
80100cd9:	e8 d2 18 00 00       	call   801025b0 <ioapicenable>
80100cde:	83 c4 10             	add    $0x10,%esp
80100ce1:	c9                   	leave  
80100ce2:	c3                   	ret    
80100ce3:	66 90                	xchg   %ax,%ax
80100ce5:	66 90                	xchg   %ax,%ax
80100ce7:	66 90                	xchg   %ax,%ax
80100ce9:	66 90                	xchg   %ax,%ax
80100ceb:	66 90                	xchg   %ax,%ax
80100ced:	66 90                	xchg   %ax,%ax
80100cef:	90                   	nop

80100cf0 <exec>:
#include "x86.h"
#include "elf.h"

int
exec(char *path, char **argv)
{
80100cf0:	55                   	push   %ebp
80100cf1:	89 e5                	mov    %esp,%ebp
80100cf3:	57                   	push   %edi
80100cf4:	56                   	push   %esi
80100cf5:	53                   	push   %ebx
80100cf6:	81 ec 0c 01 00 00    	sub    $0x10c,%esp
  uint argc, sz, sp, ustack[3+MAXARG+1];
  struct elfhdr elf;
  struct inode *ip;
  struct proghdr ph;
  pde_t *pgdir, *oldpgdir;
  struct proc *curproc = myproc();
80100cfc:	e8 bf 2d 00 00       	call   80103ac0 <myproc>
80100d01:	89 85 f4 fe ff ff    	mov    %eax,-0x10c(%ebp)

  begin_op();
80100d07:	e8 74 21 00 00       	call   80102e80 <begin_op>

  if((ip = namei(path)) == 0){
80100d0c:	83 ec 0c             	sub    $0xc,%esp
80100d0f:	ff 75 08             	pushl  0x8(%ebp)
80100d12:	e8 a9 14 00 00       	call   801021c0 <namei>
80100d17:	83 c4 10             	add    $0x10,%esp
80100d1a:	85 c0                	test   %eax,%eax
80100d1c:	0f 84 91 01 00 00    	je     80100eb3 <exec+0x1c3>
    end_op();
    cprintf("exec: fail\n");
    return -1;
  }
  ilock(ip);
80100d22:	83 ec 0c             	sub    $0xc,%esp
80100d25:	89 c3                	mov    %eax,%ebx
80100d27:	50                   	push   %eax
80100d28:	e8 33 0c 00 00       	call   80101960 <ilock>
  pgdir = 0;

  // Check ELF header
  if(readi(ip, (char*)&elf, 0, sizeof(elf)) != sizeof(elf))
80100d2d:	8d 85 24 ff ff ff    	lea    -0xdc(%ebp),%eax
80100d33:	6a 34                	push   $0x34
80100d35:	6a 00                	push   $0x0
80100d37:	50                   	push   %eax
80100d38:	53                   	push   %ebx
80100d39:	e8 02 0f 00 00       	call   80101c40 <readi>
80100d3e:	83 c4 20             	add    $0x20,%esp
80100d41:	83 f8 34             	cmp    $0x34,%eax
80100d44:	74 22                	je     80100d68 <exec+0x78>

 bad:
  if(pgdir)
    freevm(pgdir);
  if(ip){
    iunlockput(ip);
80100d46:	83 ec 0c             	sub    $0xc,%esp
80100d49:	53                   	push   %ebx
80100d4a:	e8 a1 0e 00 00       	call   80101bf0 <iunlockput>
    end_op();
80100d4f:	e8 9c 21 00 00       	call   80102ef0 <end_op>
80100d54:	83 c4 10             	add    $0x10,%esp
  }
  return -1;
80100d57:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80100d5c:	8d 65 f4             	lea    -0xc(%ebp),%esp
80100d5f:	5b                   	pop    %ebx
80100d60:	5e                   	pop    %esi
80100d61:	5f                   	pop    %edi
80100d62:	5d                   	pop    %ebp
80100d63:	c3                   	ret    
80100d64:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  if(elf.magic != ELF_MAGIC)
80100d68:	81 bd 24 ff ff ff 7f 	cmpl   $0x464c457f,-0xdc(%ebp)
80100d6f:	45 4c 46 
80100d72:	75 d2                	jne    80100d46 <exec+0x56>
  if((pgdir = setupkvm()) == 0)
80100d74:	e8 77 61 00 00       	call   80106ef0 <setupkvm>
80100d79:	85 c0                	test   %eax,%eax
80100d7b:	89 85 f0 fe ff ff    	mov    %eax,-0x110(%ebp)
80100d81:	74 c3                	je     80100d46 <exec+0x56>
  sz = 0;
80100d83:	31 ff                	xor    %edi,%edi
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
80100d85:	66 83 bd 50 ff ff ff 	cmpw   $0x0,-0xb0(%ebp)
80100d8c:	00 
80100d8d:	8b 85 40 ff ff ff    	mov    -0xc0(%ebp),%eax
80100d93:	89 85 ec fe ff ff    	mov    %eax,-0x114(%ebp)
80100d99:	0f 84 8c 02 00 00    	je     8010102b <exec+0x33b>
80100d9f:	31 f6                	xor    %esi,%esi
80100da1:	eb 7f                	jmp    80100e22 <exec+0x132>
80100da3:	90                   	nop
80100da4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    if(ph.type != ELF_PROG_LOAD)
80100da8:	83 bd 04 ff ff ff 01 	cmpl   $0x1,-0xfc(%ebp)
80100daf:	75 63                	jne    80100e14 <exec+0x124>
    if(ph.memsz < ph.filesz)
80100db1:	8b 85 18 ff ff ff    	mov    -0xe8(%ebp),%eax
80100db7:	3b 85 14 ff ff ff    	cmp    -0xec(%ebp),%eax
80100dbd:	0f 82 86 00 00 00    	jb     80100e49 <exec+0x159>
80100dc3:	03 85 0c ff ff ff    	add    -0xf4(%ebp),%eax
80100dc9:	72 7e                	jb     80100e49 <exec+0x159>
    if((sz = allocuvm(pgdir, sz, ph.vaddr + ph.memsz)) == 0)
80100dcb:	83 ec 04             	sub    $0x4,%esp
80100dce:	50                   	push   %eax
80100dcf:	57                   	push   %edi
80100dd0:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
80100dd6:	e8 35 5f 00 00       	call   80106d10 <allocuvm>
80100ddb:	83 c4 10             	add    $0x10,%esp
80100dde:	85 c0                	test   %eax,%eax
80100de0:	89 c7                	mov    %eax,%edi
80100de2:	74 65                	je     80100e49 <exec+0x159>
    if(ph.vaddr % PGSIZE != 0)
80100de4:	8b 85 0c ff ff ff    	mov    -0xf4(%ebp),%eax
80100dea:	a9 ff 0f 00 00       	test   $0xfff,%eax
80100def:	75 58                	jne    80100e49 <exec+0x159>
    if(loaduvm(pgdir, (char*)ph.vaddr, ip, ph.off, ph.filesz) < 0)
80100df1:	83 ec 0c             	sub    $0xc,%esp
80100df4:	ff b5 14 ff ff ff    	pushl  -0xec(%ebp)
80100dfa:	ff b5 08 ff ff ff    	pushl  -0xf8(%ebp)
80100e00:	53                   	push   %ebx
80100e01:	50                   	push   %eax
80100e02:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
80100e08:	e8 43 5e 00 00       	call   80106c50 <loaduvm>
80100e0d:	83 c4 20             	add    $0x20,%esp
80100e10:	85 c0                	test   %eax,%eax
80100e12:	78 35                	js     80100e49 <exec+0x159>
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
80100e14:	0f b7 85 50 ff ff ff 	movzwl -0xb0(%ebp),%eax
80100e1b:	83 c6 01             	add    $0x1,%esi
80100e1e:	39 f0                	cmp    %esi,%eax
80100e20:	7e 3d                	jle    80100e5f <exec+0x16f>
    if(readi(ip, (char*)&ph, off, sizeof(ph)) != sizeof(ph))
80100e22:	89 f0                	mov    %esi,%eax
80100e24:	6a 20                	push   $0x20
80100e26:	c1 e0 05             	shl    $0x5,%eax
80100e29:	03 85 ec fe ff ff    	add    -0x114(%ebp),%eax
80100e2f:	50                   	push   %eax
80100e30:	8d 85 04 ff ff ff    	lea    -0xfc(%ebp),%eax
80100e36:	50                   	push   %eax
80100e37:	53                   	push   %ebx
80100e38:	e8 03 0e 00 00       	call   80101c40 <readi>
80100e3d:	83 c4 10             	add    $0x10,%esp
80100e40:	83 f8 20             	cmp    $0x20,%eax
80100e43:	0f 84 5f ff ff ff    	je     80100da8 <exec+0xb8>
    freevm(pgdir);
80100e49:	83 ec 0c             	sub    $0xc,%esp
80100e4c:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
80100e52:	e8 19 60 00 00       	call   80106e70 <freevm>
80100e57:	83 c4 10             	add    $0x10,%esp
80100e5a:	e9 e7 fe ff ff       	jmp    80100d46 <exec+0x56>
80100e5f:	81 c7 ff 0f 00 00    	add    $0xfff,%edi
80100e65:	81 e7 00 f0 ff ff    	and    $0xfffff000,%edi
80100e6b:	8d b7 00 20 00 00    	lea    0x2000(%edi),%esi
  iunlockput(ip);
80100e71:	83 ec 0c             	sub    $0xc,%esp
80100e74:	53                   	push   %ebx
80100e75:	e8 76 0d 00 00       	call   80101bf0 <iunlockput>
  end_op();
80100e7a:	e8 71 20 00 00       	call   80102ef0 <end_op>
  if((sz = allocuvm(pgdir, sz, sz + 2*PGSIZE)) == 0)
80100e7f:	83 c4 0c             	add    $0xc,%esp
80100e82:	56                   	push   %esi
80100e83:	57                   	push   %edi
80100e84:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
80100e8a:	e8 81 5e 00 00       	call   80106d10 <allocuvm>
80100e8f:	83 c4 10             	add    $0x10,%esp
80100e92:	85 c0                	test   %eax,%eax
80100e94:	89 c6                	mov    %eax,%esi
80100e96:	75 3a                	jne    80100ed2 <exec+0x1e2>
    freevm(pgdir);
80100e98:	83 ec 0c             	sub    $0xc,%esp
80100e9b:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
80100ea1:	e8 ca 5f 00 00       	call   80106e70 <freevm>
80100ea6:	83 c4 10             	add    $0x10,%esp
  return -1;
80100ea9:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80100eae:	e9 a9 fe ff ff       	jmp    80100d5c <exec+0x6c>
    end_op();
80100eb3:	e8 38 20 00 00       	call   80102ef0 <end_op>
    cprintf("exec: fail\n");
80100eb8:	83 ec 0c             	sub    $0xc,%esp
80100ebb:	68 21 72 10 80       	push   $0x80107221
80100ec0:	e8 cb f8 ff ff       	call   80100790 <cprintf>
    return -1;
80100ec5:	83 c4 10             	add    $0x10,%esp
80100ec8:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80100ecd:	e9 8a fe ff ff       	jmp    80100d5c <exec+0x6c>
  clearpteu(pgdir, (char*)(sz - 2*PGSIZE));
80100ed2:	8d 80 00 e0 ff ff    	lea    -0x2000(%eax),%eax
80100ed8:	83 ec 08             	sub    $0x8,%esp
  for(argc = 0; argv[argc]; argc++) {
80100edb:	31 ff                	xor    %edi,%edi
80100edd:	89 f3                	mov    %esi,%ebx
  clearpteu(pgdir, (char*)(sz - 2*PGSIZE));
80100edf:	50                   	push   %eax
80100ee0:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
80100ee6:	e8 a5 60 00 00       	call   80106f90 <clearpteu>
  for(argc = 0; argv[argc]; argc++) {
80100eeb:	8b 45 0c             	mov    0xc(%ebp),%eax
80100eee:	83 c4 10             	add    $0x10,%esp
80100ef1:	8d 95 58 ff ff ff    	lea    -0xa8(%ebp),%edx
80100ef7:	8b 00                	mov    (%eax),%eax
80100ef9:	85 c0                	test   %eax,%eax
80100efb:	74 70                	je     80100f6d <exec+0x27d>
80100efd:	89 b5 ec fe ff ff    	mov    %esi,-0x114(%ebp)
80100f03:	8b b5 f0 fe ff ff    	mov    -0x110(%ebp),%esi
80100f09:	eb 0a                	jmp    80100f15 <exec+0x225>
80100f0b:	90                   	nop
80100f0c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    if(argc >= MAXARG)
80100f10:	83 ff 20             	cmp    $0x20,%edi
80100f13:	74 83                	je     80100e98 <exec+0x1a8>
    sp = (sp - (strlen(argv[argc]) + 1)) & ~3;
80100f15:	83 ec 0c             	sub    $0xc,%esp
80100f18:	50                   	push   %eax
80100f19:	e8 32 3a 00 00       	call   80104950 <strlen>
80100f1e:	f7 d0                	not    %eax
80100f20:	01 c3                	add    %eax,%ebx
    if(copyout(pgdir, sp, argv[argc], strlen(argv[argc]) + 1) < 0)
80100f22:	8b 45 0c             	mov    0xc(%ebp),%eax
80100f25:	5a                   	pop    %edx
    sp = (sp - (strlen(argv[argc]) + 1)) & ~3;
80100f26:	83 e3 fc             	and    $0xfffffffc,%ebx
    if(copyout(pgdir, sp, argv[argc], strlen(argv[argc]) + 1) < 0)
80100f29:	ff 34 b8             	pushl  (%eax,%edi,4)
80100f2c:	e8 1f 3a 00 00       	call   80104950 <strlen>
80100f31:	83 c0 01             	add    $0x1,%eax
80100f34:	50                   	push   %eax
80100f35:	8b 45 0c             	mov    0xc(%ebp),%eax
80100f38:	ff 34 b8             	pushl  (%eax,%edi,4)
80100f3b:	53                   	push   %ebx
80100f3c:	56                   	push   %esi
80100f3d:	e8 ae 61 00 00       	call   801070f0 <copyout>
80100f42:	83 c4 20             	add    $0x20,%esp
80100f45:	85 c0                	test   %eax,%eax
80100f47:	0f 88 4b ff ff ff    	js     80100e98 <exec+0x1a8>
  for(argc = 0; argv[argc]; argc++) {
80100f4d:	8b 45 0c             	mov    0xc(%ebp),%eax
    ustack[3+argc] = sp;
80100f50:	89 9c bd 64 ff ff ff 	mov    %ebx,-0x9c(%ebp,%edi,4)
  for(argc = 0; argv[argc]; argc++) {
80100f57:	83 c7 01             	add    $0x1,%edi
    ustack[3+argc] = sp;
80100f5a:	8d 95 58 ff ff ff    	lea    -0xa8(%ebp),%edx
  for(argc = 0; argv[argc]; argc++) {
80100f60:	8b 04 b8             	mov    (%eax,%edi,4),%eax
80100f63:	85 c0                	test   %eax,%eax
80100f65:	75 a9                	jne    80100f10 <exec+0x220>
80100f67:	8b b5 ec fe ff ff    	mov    -0x114(%ebp),%esi
  ustack[2] = sp - (argc+1)*4;  // argv pointer
80100f6d:	8d 04 bd 04 00 00 00 	lea    0x4(,%edi,4),%eax
80100f74:	89 d9                	mov    %ebx,%ecx
  ustack[3+argc] = 0;
80100f76:	c7 84 bd 64 ff ff ff 	movl   $0x0,-0x9c(%ebp,%edi,4)
80100f7d:	00 00 00 00 
  ustack[0] = 0xffffffff;  // fake return PC
80100f81:	c7 85 58 ff ff ff ff 	movl   $0xffffffff,-0xa8(%ebp)
80100f88:	ff ff ff 
  ustack[1] = argc;
80100f8b:	89 bd 5c ff ff ff    	mov    %edi,-0xa4(%ebp)
  ustack[2] = sp - (argc+1)*4;  // argv pointer
80100f91:	29 c1                	sub    %eax,%ecx
  sp -= (3+argc+1) * 4;
80100f93:	83 c0 0c             	add    $0xc,%eax
80100f96:	29 c3                	sub    %eax,%ebx
  if(copyout(pgdir, sp, ustack, (3+argc+1)*4) < 0)
80100f98:	50                   	push   %eax
80100f99:	52                   	push   %edx
80100f9a:	53                   	push   %ebx
80100f9b:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  ustack[2] = sp - (argc+1)*4;  // argv pointer
80100fa1:	89 8d 60 ff ff ff    	mov    %ecx,-0xa0(%ebp)
  if(copyout(pgdir, sp, ustack, (3+argc+1)*4) < 0)
80100fa7:	e8 44 61 00 00       	call   801070f0 <copyout>
80100fac:	83 c4 10             	add    $0x10,%esp
80100faf:	85 c0                	test   %eax,%eax
80100fb1:	0f 88 e1 fe ff ff    	js     80100e98 <exec+0x1a8>
  for(last=s=path; *s; s++)
80100fb7:	8b 45 08             	mov    0x8(%ebp),%eax
80100fba:	0f b6 00             	movzbl (%eax),%eax
80100fbd:	84 c0                	test   %al,%al
80100fbf:	74 17                	je     80100fd8 <exec+0x2e8>
80100fc1:	8b 55 08             	mov    0x8(%ebp),%edx
80100fc4:	89 d1                	mov    %edx,%ecx
80100fc6:	83 c1 01             	add    $0x1,%ecx
80100fc9:	3c 2f                	cmp    $0x2f,%al
80100fcb:	0f b6 01             	movzbl (%ecx),%eax
80100fce:	0f 44 d1             	cmove  %ecx,%edx
80100fd1:	84 c0                	test   %al,%al
80100fd3:	75 f1                	jne    80100fc6 <exec+0x2d6>
80100fd5:	89 55 08             	mov    %edx,0x8(%ebp)
  safestrcpy(curproc->name, last, sizeof(curproc->name));
80100fd8:	8b bd f4 fe ff ff    	mov    -0x10c(%ebp),%edi
80100fde:	50                   	push   %eax
80100fdf:	6a 10                	push   $0x10
80100fe1:	ff 75 08             	pushl  0x8(%ebp)
80100fe4:	89 f8                	mov    %edi,%eax
80100fe6:	83 c0 6c             	add    $0x6c,%eax
80100fe9:	50                   	push   %eax
80100fea:	e8 21 39 00 00       	call   80104910 <safestrcpy>
  curproc->pgdir = pgdir;
80100fef:	8b 95 f0 fe ff ff    	mov    -0x110(%ebp),%edx
  oldpgdir = curproc->pgdir;
80100ff5:	89 f9                	mov    %edi,%ecx
80100ff7:	8b 7f 04             	mov    0x4(%edi),%edi
  curproc->tf->eip = elf.entry;  // main
80100ffa:	8b 41 18             	mov    0x18(%ecx),%eax
  curproc->sz = sz;
80100ffd:	89 31                	mov    %esi,(%ecx)
  curproc->pgdir = pgdir;
80100fff:	89 51 04             	mov    %edx,0x4(%ecx)
  curproc->tf->eip = elf.entry;  // main
80101002:	8b 95 3c ff ff ff    	mov    -0xc4(%ebp),%edx
80101008:	89 50 38             	mov    %edx,0x38(%eax)
  curproc->tf->esp = sp;
8010100b:	8b 41 18             	mov    0x18(%ecx),%eax
8010100e:	89 58 44             	mov    %ebx,0x44(%eax)
  switchuvm(curproc);
80101011:	89 0c 24             	mov    %ecx,(%esp)
80101014:	e8 a7 5a 00 00       	call   80106ac0 <switchuvm>
  freevm(oldpgdir);
80101019:	89 3c 24             	mov    %edi,(%esp)
8010101c:	e8 4f 5e 00 00       	call   80106e70 <freevm>
  return 0;
80101021:	83 c4 10             	add    $0x10,%esp
80101024:	31 c0                	xor    %eax,%eax
80101026:	e9 31 fd ff ff       	jmp    80100d5c <exec+0x6c>
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
8010102b:	be 00 20 00 00       	mov    $0x2000,%esi
80101030:	e9 3c fe ff ff       	jmp    80100e71 <exec+0x181>
80101035:	66 90                	xchg   %ax,%ax
80101037:	66 90                	xchg   %ax,%ax
80101039:	66 90                	xchg   %ax,%ax
8010103b:	66 90                	xchg   %ax,%ax
8010103d:	66 90                	xchg   %ax,%ax
8010103f:	90                   	nop

80101040 <fileinit>:
  struct file file[NFILE];
} ftable;

void
fileinit(void)
{
80101040:	55                   	push   %ebp
80101041:	89 e5                	mov    %esp,%ebp
80101043:	83 ec 10             	sub    $0x10,%esp
  initlock(&ftable.lock, "ftable");
80101046:	68 2d 72 10 80       	push   $0x8010722d
8010104b:	68 c0 ff 10 80       	push   $0x8010ffc0
80101050:	e8 8b 34 00 00       	call   801044e0 <initlock>
}
80101055:	83 c4 10             	add    $0x10,%esp
80101058:	c9                   	leave  
80101059:	c3                   	ret    
8010105a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80101060 <filealloc>:

// Allocate a file structure.
struct file*
filealloc(void)
{
80101060:	55                   	push   %ebp
80101061:	89 e5                	mov    %esp,%ebp
80101063:	53                   	push   %ebx
  struct file *f;

  acquire(&ftable.lock);
  for(f = ftable.file; f < ftable.file + NFILE; f++){
80101064:	bb f4 ff 10 80       	mov    $0x8010fff4,%ebx
{
80101069:	83 ec 10             	sub    $0x10,%esp
  acquire(&ftable.lock);
8010106c:	68 c0 ff 10 80       	push   $0x8010ffc0
80101071:	e8 aa 35 00 00       	call   80104620 <acquire>
80101076:	83 c4 10             	add    $0x10,%esp
80101079:	eb 10                	jmp    8010108b <filealloc+0x2b>
8010107b:	90                   	nop
8010107c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  for(f = ftable.file; f < ftable.file + NFILE; f++){
80101080:	83 c3 18             	add    $0x18,%ebx
80101083:	81 fb 54 09 11 80    	cmp    $0x80110954,%ebx
80101089:	73 25                	jae    801010b0 <filealloc+0x50>
    if(f->ref == 0){
8010108b:	8b 43 04             	mov    0x4(%ebx),%eax
8010108e:	85 c0                	test   %eax,%eax
80101090:	75 ee                	jne    80101080 <filealloc+0x20>
      f->ref = 1;
      release(&ftable.lock);
80101092:	83 ec 0c             	sub    $0xc,%esp
      f->ref = 1;
80101095:	c7 43 04 01 00 00 00 	movl   $0x1,0x4(%ebx)
      release(&ftable.lock);
8010109c:	68 c0 ff 10 80       	push   $0x8010ffc0
801010a1:	e8 3a 36 00 00       	call   801046e0 <release>
      return f;
    }
  }
  release(&ftable.lock);
  return 0;
}
801010a6:	89 d8                	mov    %ebx,%eax
      return f;
801010a8:	83 c4 10             	add    $0x10,%esp
}
801010ab:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801010ae:	c9                   	leave  
801010af:	c3                   	ret    
  release(&ftable.lock);
801010b0:	83 ec 0c             	sub    $0xc,%esp
  return 0;
801010b3:	31 db                	xor    %ebx,%ebx
  release(&ftable.lock);
801010b5:	68 c0 ff 10 80       	push   $0x8010ffc0
801010ba:	e8 21 36 00 00       	call   801046e0 <release>
}
801010bf:	89 d8                	mov    %ebx,%eax
  return 0;
801010c1:	83 c4 10             	add    $0x10,%esp
}
801010c4:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801010c7:	c9                   	leave  
801010c8:	c3                   	ret    
801010c9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

801010d0 <filedup>:

// Increment ref count for file f.
struct file*
filedup(struct file *f)
{
801010d0:	55                   	push   %ebp
801010d1:	89 e5                	mov    %esp,%ebp
801010d3:	53                   	push   %ebx
801010d4:	83 ec 10             	sub    $0x10,%esp
801010d7:	8b 5d 08             	mov    0x8(%ebp),%ebx
  acquire(&ftable.lock);
801010da:	68 c0 ff 10 80       	push   $0x8010ffc0
801010df:	e8 3c 35 00 00       	call   80104620 <acquire>
  if(f->ref < 1)
801010e4:	8b 43 04             	mov    0x4(%ebx),%eax
801010e7:	83 c4 10             	add    $0x10,%esp
801010ea:	85 c0                	test   %eax,%eax
801010ec:	7e 1a                	jle    80101108 <filedup+0x38>
    panic("filedup");
  f->ref++;
801010ee:	83 c0 01             	add    $0x1,%eax
  release(&ftable.lock);
801010f1:	83 ec 0c             	sub    $0xc,%esp
  f->ref++;
801010f4:	89 43 04             	mov    %eax,0x4(%ebx)
  release(&ftable.lock);
801010f7:	68 c0 ff 10 80       	push   $0x8010ffc0
801010fc:	e8 df 35 00 00       	call   801046e0 <release>
  return f;
}
80101101:	89 d8                	mov    %ebx,%eax
80101103:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80101106:	c9                   	leave  
80101107:	c3                   	ret    
    panic("filedup");
80101108:	83 ec 0c             	sub    $0xc,%esp
8010110b:	68 34 72 10 80       	push   $0x80107234
80101110:	e8 7b f2 ff ff       	call   80100390 <panic>
80101115:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80101119:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80101120 <fileclose>:

// Close file f.  (Decrement ref count, close when reaches 0.)
void
fileclose(struct file *f)
{
80101120:	55                   	push   %ebp
80101121:	89 e5                	mov    %esp,%ebp
80101123:	57                   	push   %edi
80101124:	56                   	push   %esi
80101125:	53                   	push   %ebx
80101126:	83 ec 28             	sub    $0x28,%esp
80101129:	8b 5d 08             	mov    0x8(%ebp),%ebx
  struct file ff;

  acquire(&ftable.lock);
8010112c:	68 c0 ff 10 80       	push   $0x8010ffc0
80101131:	e8 ea 34 00 00       	call   80104620 <acquire>
  if(f->ref < 1)
80101136:	8b 43 04             	mov    0x4(%ebx),%eax
80101139:	83 c4 10             	add    $0x10,%esp
8010113c:	85 c0                	test   %eax,%eax
8010113e:	0f 8e 9b 00 00 00    	jle    801011df <fileclose+0xbf>
    panic("fileclose");
  if(--f->ref > 0){
80101144:	83 e8 01             	sub    $0x1,%eax
80101147:	85 c0                	test   %eax,%eax
80101149:	89 43 04             	mov    %eax,0x4(%ebx)
8010114c:	74 1a                	je     80101168 <fileclose+0x48>
    release(&ftable.lock);
8010114e:	c7 45 08 c0 ff 10 80 	movl   $0x8010ffc0,0x8(%ebp)
  else if(ff.type == FD_INODE){
    begin_op();
    iput(ff.ip);
    end_op();
  }
}
80101155:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101158:	5b                   	pop    %ebx
80101159:	5e                   	pop    %esi
8010115a:	5f                   	pop    %edi
8010115b:	5d                   	pop    %ebp
    release(&ftable.lock);
8010115c:	e9 7f 35 00 00       	jmp    801046e0 <release>
80101161:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  ff = *f;
80101168:	0f b6 43 09          	movzbl 0x9(%ebx),%eax
8010116c:	8b 3b                	mov    (%ebx),%edi
  release(&ftable.lock);
8010116e:	83 ec 0c             	sub    $0xc,%esp
  ff = *f;
80101171:	8b 73 0c             	mov    0xc(%ebx),%esi
  f->type = FD_NONE;
80101174:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  ff = *f;
8010117a:	88 45 e7             	mov    %al,-0x19(%ebp)
8010117d:	8b 43 10             	mov    0x10(%ebx),%eax
  release(&ftable.lock);
80101180:	68 c0 ff 10 80       	push   $0x8010ffc0
  ff = *f;
80101185:	89 45 e0             	mov    %eax,-0x20(%ebp)
  release(&ftable.lock);
80101188:	e8 53 35 00 00       	call   801046e0 <release>
  if(ff.type == FD_PIPE)
8010118d:	83 c4 10             	add    $0x10,%esp
80101190:	83 ff 01             	cmp    $0x1,%edi
80101193:	74 13                	je     801011a8 <fileclose+0x88>
  else if(ff.type == FD_INODE){
80101195:	83 ff 02             	cmp    $0x2,%edi
80101198:	74 26                	je     801011c0 <fileclose+0xa0>
}
8010119a:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010119d:	5b                   	pop    %ebx
8010119e:	5e                   	pop    %esi
8010119f:	5f                   	pop    %edi
801011a0:	5d                   	pop    %ebp
801011a1:	c3                   	ret    
801011a2:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    pipeclose(ff.pipe, ff.writable);
801011a8:	0f be 5d e7          	movsbl -0x19(%ebp),%ebx
801011ac:	83 ec 08             	sub    $0x8,%esp
801011af:	53                   	push   %ebx
801011b0:	56                   	push   %esi
801011b1:	e8 7a 24 00 00       	call   80103630 <pipeclose>
801011b6:	83 c4 10             	add    $0x10,%esp
801011b9:	eb df                	jmp    8010119a <fileclose+0x7a>
801011bb:	90                   	nop
801011bc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    begin_op();
801011c0:	e8 bb 1c 00 00       	call   80102e80 <begin_op>
    iput(ff.ip);
801011c5:	83 ec 0c             	sub    $0xc,%esp
801011c8:	ff 75 e0             	pushl  -0x20(%ebp)
801011cb:	e8 c0 08 00 00       	call   80101a90 <iput>
    end_op();
801011d0:	83 c4 10             	add    $0x10,%esp
}
801011d3:	8d 65 f4             	lea    -0xc(%ebp),%esp
801011d6:	5b                   	pop    %ebx
801011d7:	5e                   	pop    %esi
801011d8:	5f                   	pop    %edi
801011d9:	5d                   	pop    %ebp
    end_op();
801011da:	e9 11 1d 00 00       	jmp    80102ef0 <end_op>
    panic("fileclose");
801011df:	83 ec 0c             	sub    $0xc,%esp
801011e2:	68 3c 72 10 80       	push   $0x8010723c
801011e7:	e8 a4 f1 ff ff       	call   80100390 <panic>
801011ec:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

801011f0 <filestat>:

// Get metadata about file f.
int
filestat(struct file *f, struct stat *st)
{
801011f0:	55                   	push   %ebp
801011f1:	89 e5                	mov    %esp,%ebp
801011f3:	53                   	push   %ebx
801011f4:	83 ec 04             	sub    $0x4,%esp
801011f7:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if(f->type == FD_INODE){
801011fa:	83 3b 02             	cmpl   $0x2,(%ebx)
801011fd:	75 31                	jne    80101230 <filestat+0x40>
    ilock(f->ip);
801011ff:	83 ec 0c             	sub    $0xc,%esp
80101202:	ff 73 10             	pushl  0x10(%ebx)
80101205:	e8 56 07 00 00       	call   80101960 <ilock>
    stati(f->ip, st);
8010120a:	58                   	pop    %eax
8010120b:	5a                   	pop    %edx
8010120c:	ff 75 0c             	pushl  0xc(%ebp)
8010120f:	ff 73 10             	pushl  0x10(%ebx)
80101212:	e8 f9 09 00 00       	call   80101c10 <stati>
    iunlock(f->ip);
80101217:	59                   	pop    %ecx
80101218:	ff 73 10             	pushl  0x10(%ebx)
8010121b:	e8 20 08 00 00       	call   80101a40 <iunlock>
    return 0;
80101220:	83 c4 10             	add    $0x10,%esp
80101223:	31 c0                	xor    %eax,%eax
  }
  return -1;
}
80101225:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80101228:	c9                   	leave  
80101229:	c3                   	ret    
8010122a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  return -1;
80101230:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80101235:	eb ee                	jmp    80101225 <filestat+0x35>
80101237:	89 f6                	mov    %esi,%esi
80101239:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80101240 <fileread>:

// Read from file f.
int
fileread(struct file *f, char *addr, int n)
{
80101240:	55                   	push   %ebp
80101241:	89 e5                	mov    %esp,%ebp
80101243:	57                   	push   %edi
80101244:	56                   	push   %esi
80101245:	53                   	push   %ebx
80101246:	83 ec 0c             	sub    $0xc,%esp
80101249:	8b 5d 08             	mov    0x8(%ebp),%ebx
8010124c:	8b 75 0c             	mov    0xc(%ebp),%esi
8010124f:	8b 7d 10             	mov    0x10(%ebp),%edi
  int r;

  if(f->readable == 0)
80101252:	80 7b 08 00          	cmpb   $0x0,0x8(%ebx)
80101256:	74 60                	je     801012b8 <fileread+0x78>
    return -1;
  if(f->type == FD_PIPE)
80101258:	8b 03                	mov    (%ebx),%eax
8010125a:	83 f8 01             	cmp    $0x1,%eax
8010125d:	74 41                	je     801012a0 <fileread+0x60>
    return piperead(f->pipe, addr, n);
  if(f->type == FD_INODE){
8010125f:	83 f8 02             	cmp    $0x2,%eax
80101262:	75 5b                	jne    801012bf <fileread+0x7f>
    ilock(f->ip);
80101264:	83 ec 0c             	sub    $0xc,%esp
80101267:	ff 73 10             	pushl  0x10(%ebx)
8010126a:	e8 f1 06 00 00       	call   80101960 <ilock>
    if((r = readi(f->ip, addr, f->off, n)) > 0)
8010126f:	57                   	push   %edi
80101270:	ff 73 14             	pushl  0x14(%ebx)
80101273:	56                   	push   %esi
80101274:	ff 73 10             	pushl  0x10(%ebx)
80101277:	e8 c4 09 00 00       	call   80101c40 <readi>
8010127c:	83 c4 20             	add    $0x20,%esp
8010127f:	85 c0                	test   %eax,%eax
80101281:	89 c6                	mov    %eax,%esi
80101283:	7e 03                	jle    80101288 <fileread+0x48>
      f->off += r;
80101285:	01 43 14             	add    %eax,0x14(%ebx)
    iunlock(f->ip);
80101288:	83 ec 0c             	sub    $0xc,%esp
8010128b:	ff 73 10             	pushl  0x10(%ebx)
8010128e:	e8 ad 07 00 00       	call   80101a40 <iunlock>
    return r;
80101293:	83 c4 10             	add    $0x10,%esp
  }
  panic("fileread");
}
80101296:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101299:	89 f0                	mov    %esi,%eax
8010129b:	5b                   	pop    %ebx
8010129c:	5e                   	pop    %esi
8010129d:	5f                   	pop    %edi
8010129e:	5d                   	pop    %ebp
8010129f:	c3                   	ret    
    return piperead(f->pipe, addr, n);
801012a0:	8b 43 0c             	mov    0xc(%ebx),%eax
801012a3:	89 45 08             	mov    %eax,0x8(%ebp)
}
801012a6:	8d 65 f4             	lea    -0xc(%ebp),%esp
801012a9:	5b                   	pop    %ebx
801012aa:	5e                   	pop    %esi
801012ab:	5f                   	pop    %edi
801012ac:	5d                   	pop    %ebp
    return piperead(f->pipe, addr, n);
801012ad:	e9 2e 25 00 00       	jmp    801037e0 <piperead>
801012b2:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    return -1;
801012b8:	be ff ff ff ff       	mov    $0xffffffff,%esi
801012bd:	eb d7                	jmp    80101296 <fileread+0x56>
  panic("fileread");
801012bf:	83 ec 0c             	sub    $0xc,%esp
801012c2:	68 46 72 10 80       	push   $0x80107246
801012c7:	e8 c4 f0 ff ff       	call   80100390 <panic>
801012cc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

801012d0 <filewrite>:

//PAGEBREAK!
// Write to file f.
int
filewrite(struct file *f, char *addr, int n)
{
801012d0:	55                   	push   %ebp
801012d1:	89 e5                	mov    %esp,%ebp
801012d3:	57                   	push   %edi
801012d4:	56                   	push   %esi
801012d5:	53                   	push   %ebx
801012d6:	83 ec 1c             	sub    $0x1c,%esp
801012d9:	8b 75 08             	mov    0x8(%ebp),%esi
801012dc:	8b 45 0c             	mov    0xc(%ebp),%eax
  int r;

  if(f->writable == 0)
801012df:	80 7e 09 00          	cmpb   $0x0,0x9(%esi)
{
801012e3:	89 45 dc             	mov    %eax,-0x24(%ebp)
801012e6:	8b 45 10             	mov    0x10(%ebp),%eax
801012e9:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  if(f->writable == 0)
801012ec:	0f 84 aa 00 00 00    	je     8010139c <filewrite+0xcc>
    return -1;
  if(f->type == FD_PIPE)
801012f2:	8b 06                	mov    (%esi),%eax
801012f4:	83 f8 01             	cmp    $0x1,%eax
801012f7:	0f 84 c3 00 00 00    	je     801013c0 <filewrite+0xf0>
    return pipewrite(f->pipe, addr, n);
  if(f->type == FD_INODE){
801012fd:	83 f8 02             	cmp    $0x2,%eax
80101300:	0f 85 d9 00 00 00    	jne    801013df <filewrite+0x10f>
    // and 2 blocks of slop for non-aligned writes.
    // this really belongs lower down, since writei()
    // might be writing a device like the console.
    int max = ((MAXOPBLOCKS-1-1-2) / 2) * 512;
    int i = 0;
    while(i < n){
80101306:	8b 45 e4             	mov    -0x1c(%ebp),%eax
    int i = 0;
80101309:	31 ff                	xor    %edi,%edi
    while(i < n){
8010130b:	85 c0                	test   %eax,%eax
8010130d:	7f 34                	jg     80101343 <filewrite+0x73>
8010130f:	e9 9c 00 00 00       	jmp    801013b0 <filewrite+0xe0>
80101314:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
        n1 = max;

      begin_op();
      ilock(f->ip);
      if ((r = writei(f->ip, addr + i, f->off, n1)) > 0)
        f->off += r;
80101318:	01 46 14             	add    %eax,0x14(%esi)
      iunlock(f->ip);
8010131b:	83 ec 0c             	sub    $0xc,%esp
8010131e:	ff 76 10             	pushl  0x10(%esi)
        f->off += r;
80101321:	89 45 e0             	mov    %eax,-0x20(%ebp)
      iunlock(f->ip);
80101324:	e8 17 07 00 00       	call   80101a40 <iunlock>
      end_op();
80101329:	e8 c2 1b 00 00       	call   80102ef0 <end_op>
8010132e:	8b 45 e0             	mov    -0x20(%ebp),%eax
80101331:	83 c4 10             	add    $0x10,%esp

      if(r < 0)
        break;
      if(r != n1)
80101334:	39 c3                	cmp    %eax,%ebx
80101336:	0f 85 96 00 00 00    	jne    801013d2 <filewrite+0x102>
        panic("short filewrite");
      i += r;
8010133c:	01 df                	add    %ebx,%edi
    while(i < n){
8010133e:	39 7d e4             	cmp    %edi,-0x1c(%ebp)
80101341:	7e 6d                	jle    801013b0 <filewrite+0xe0>
      int n1 = n - i;
80101343:	8b 5d e4             	mov    -0x1c(%ebp),%ebx
80101346:	b8 00 06 00 00       	mov    $0x600,%eax
8010134b:	29 fb                	sub    %edi,%ebx
8010134d:	81 fb 00 06 00 00    	cmp    $0x600,%ebx
80101353:	0f 4f d8             	cmovg  %eax,%ebx
      begin_op();
80101356:	e8 25 1b 00 00       	call   80102e80 <begin_op>
      ilock(f->ip);
8010135b:	83 ec 0c             	sub    $0xc,%esp
8010135e:	ff 76 10             	pushl  0x10(%esi)
80101361:	e8 fa 05 00 00       	call   80101960 <ilock>
      if ((r = writei(f->ip, addr + i, f->off, n1)) > 0)
80101366:	8b 45 dc             	mov    -0x24(%ebp),%eax
80101369:	53                   	push   %ebx
8010136a:	ff 76 14             	pushl  0x14(%esi)
8010136d:	01 f8                	add    %edi,%eax
8010136f:	50                   	push   %eax
80101370:	ff 76 10             	pushl  0x10(%esi)
80101373:	e8 c8 09 00 00       	call   80101d40 <writei>
80101378:	83 c4 20             	add    $0x20,%esp
8010137b:	85 c0                	test   %eax,%eax
8010137d:	7f 99                	jg     80101318 <filewrite+0x48>
      iunlock(f->ip);
8010137f:	83 ec 0c             	sub    $0xc,%esp
80101382:	ff 76 10             	pushl  0x10(%esi)
80101385:	89 45 e0             	mov    %eax,-0x20(%ebp)
80101388:	e8 b3 06 00 00       	call   80101a40 <iunlock>
      end_op();
8010138d:	e8 5e 1b 00 00       	call   80102ef0 <end_op>
      if(r < 0)
80101392:	8b 45 e0             	mov    -0x20(%ebp),%eax
80101395:	83 c4 10             	add    $0x10,%esp
80101398:	85 c0                	test   %eax,%eax
8010139a:	74 98                	je     80101334 <filewrite+0x64>
    }
    return i == n ? n : -1;
  }
  panic("filewrite");
}
8010139c:	8d 65 f4             	lea    -0xc(%ebp),%esp
    return -1;
8010139f:	bf ff ff ff ff       	mov    $0xffffffff,%edi
}
801013a4:	89 f8                	mov    %edi,%eax
801013a6:	5b                   	pop    %ebx
801013a7:	5e                   	pop    %esi
801013a8:	5f                   	pop    %edi
801013a9:	5d                   	pop    %ebp
801013aa:	c3                   	ret    
801013ab:	90                   	nop
801013ac:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    return i == n ? n : -1;
801013b0:	39 7d e4             	cmp    %edi,-0x1c(%ebp)
801013b3:	75 e7                	jne    8010139c <filewrite+0xcc>
}
801013b5:	8d 65 f4             	lea    -0xc(%ebp),%esp
801013b8:	89 f8                	mov    %edi,%eax
801013ba:	5b                   	pop    %ebx
801013bb:	5e                   	pop    %esi
801013bc:	5f                   	pop    %edi
801013bd:	5d                   	pop    %ebp
801013be:	c3                   	ret    
801013bf:	90                   	nop
    return pipewrite(f->pipe, addr, n);
801013c0:	8b 46 0c             	mov    0xc(%esi),%eax
801013c3:	89 45 08             	mov    %eax,0x8(%ebp)
}
801013c6:	8d 65 f4             	lea    -0xc(%ebp),%esp
801013c9:	5b                   	pop    %ebx
801013ca:	5e                   	pop    %esi
801013cb:	5f                   	pop    %edi
801013cc:	5d                   	pop    %ebp
    return pipewrite(f->pipe, addr, n);
801013cd:	e9 fe 22 00 00       	jmp    801036d0 <pipewrite>
        panic("short filewrite");
801013d2:	83 ec 0c             	sub    $0xc,%esp
801013d5:	68 4f 72 10 80       	push   $0x8010724f
801013da:	e8 b1 ef ff ff       	call   80100390 <panic>
  panic("filewrite");
801013df:	83 ec 0c             	sub    $0xc,%esp
801013e2:	68 55 72 10 80       	push   $0x80107255
801013e7:	e8 a4 ef ff ff       	call   80100390 <panic>
801013ec:	66 90                	xchg   %ax,%ax
801013ee:	66 90                	xchg   %ax,%ax

801013f0 <bfree>:
}

// Free a disk block.
static void
bfree(int dev, uint b)
{
801013f0:	55                   	push   %ebp
801013f1:	89 e5                	mov    %esp,%ebp
801013f3:	56                   	push   %esi
801013f4:	53                   	push   %ebx
801013f5:	89 d3                	mov    %edx,%ebx
  struct buf *bp;
  int bi, m;

  bp = bread(dev, BBLOCK(b, sb));
801013f7:	c1 ea 0c             	shr    $0xc,%edx
801013fa:	03 15 d8 09 11 80    	add    0x801109d8,%edx
80101400:	83 ec 08             	sub    $0x8,%esp
80101403:	52                   	push   %edx
80101404:	50                   	push   %eax
80101405:	e8 c6 ec ff ff       	call   801000d0 <bread>
  bi = b % BPB;
  m = 1 << (bi % 8);
8010140a:	89 d9                	mov    %ebx,%ecx
  if((bp->data[bi/8] & m) == 0)
8010140c:	c1 fb 03             	sar    $0x3,%ebx
  m = 1 << (bi % 8);
8010140f:	ba 01 00 00 00       	mov    $0x1,%edx
80101414:	83 e1 07             	and    $0x7,%ecx
  if((bp->data[bi/8] & m) == 0)
80101417:	81 e3 ff 01 00 00    	and    $0x1ff,%ebx
8010141d:	83 c4 10             	add    $0x10,%esp
  m = 1 << (bi % 8);
80101420:	d3 e2                	shl    %cl,%edx
  if((bp->data[bi/8] & m) == 0)
80101422:	0f b6 4c 18 5c       	movzbl 0x5c(%eax,%ebx,1),%ecx
80101427:	85 d1                	test   %edx,%ecx
80101429:	74 25                	je     80101450 <bfree+0x60>
    panic("freeing free block");
  bp->data[bi/8] &= ~m;
8010142b:	f7 d2                	not    %edx
8010142d:	89 c6                	mov    %eax,%esi
  log_write(bp);
8010142f:	83 ec 0c             	sub    $0xc,%esp
  bp->data[bi/8] &= ~m;
80101432:	21 ca                	and    %ecx,%edx
80101434:	88 54 1e 5c          	mov    %dl,0x5c(%esi,%ebx,1)
  log_write(bp);
80101438:	56                   	push   %esi
80101439:	e8 12 1c 00 00       	call   80103050 <log_write>
  brelse(bp);
8010143e:	89 34 24             	mov    %esi,(%esp)
80101441:	e8 9a ed ff ff       	call   801001e0 <brelse>
}
80101446:	83 c4 10             	add    $0x10,%esp
80101449:	8d 65 f8             	lea    -0x8(%ebp),%esp
8010144c:	5b                   	pop    %ebx
8010144d:	5e                   	pop    %esi
8010144e:	5d                   	pop    %ebp
8010144f:	c3                   	ret    
    panic("freeing free block");
80101450:	83 ec 0c             	sub    $0xc,%esp
80101453:	68 5f 72 10 80       	push   $0x8010725f
80101458:	e8 33 ef ff ff       	call   80100390 <panic>
8010145d:	8d 76 00             	lea    0x0(%esi),%esi

80101460 <balloc>:
{
80101460:	55                   	push   %ebp
80101461:	89 e5                	mov    %esp,%ebp
80101463:	57                   	push   %edi
80101464:	56                   	push   %esi
80101465:	53                   	push   %ebx
80101466:	83 ec 1c             	sub    $0x1c,%esp
  for(b = 0; b < sb.size; b += BPB){
80101469:	8b 0d c0 09 11 80    	mov    0x801109c0,%ecx
{
8010146f:	89 45 d8             	mov    %eax,-0x28(%ebp)
  for(b = 0; b < sb.size; b += BPB){
80101472:	85 c9                	test   %ecx,%ecx
80101474:	0f 84 87 00 00 00    	je     80101501 <balloc+0xa1>
8010147a:	c7 45 dc 00 00 00 00 	movl   $0x0,-0x24(%ebp)
    bp = bread(dev, BBLOCK(b, sb));
80101481:	8b 75 dc             	mov    -0x24(%ebp),%esi
80101484:	83 ec 08             	sub    $0x8,%esp
80101487:	89 f0                	mov    %esi,%eax
80101489:	c1 f8 0c             	sar    $0xc,%eax
8010148c:	03 05 d8 09 11 80    	add    0x801109d8,%eax
80101492:	50                   	push   %eax
80101493:	ff 75 d8             	pushl  -0x28(%ebp)
80101496:	e8 35 ec ff ff       	call   801000d0 <bread>
8010149b:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    for(bi = 0; bi < BPB && b + bi < sb.size; bi++){
8010149e:	a1 c0 09 11 80       	mov    0x801109c0,%eax
801014a3:	83 c4 10             	add    $0x10,%esp
801014a6:	89 45 e0             	mov    %eax,-0x20(%ebp)
801014a9:	31 c0                	xor    %eax,%eax
801014ab:	eb 2f                	jmp    801014dc <balloc+0x7c>
801014ad:	8d 76 00             	lea    0x0(%esi),%esi
      m = 1 << (bi % 8);
801014b0:	89 c1                	mov    %eax,%ecx
      if((bp->data[bi/8] & m) == 0){  // Is block free?
801014b2:	8b 55 e4             	mov    -0x1c(%ebp),%edx
      m = 1 << (bi % 8);
801014b5:	bb 01 00 00 00       	mov    $0x1,%ebx
801014ba:	83 e1 07             	and    $0x7,%ecx
801014bd:	d3 e3                	shl    %cl,%ebx
      if((bp->data[bi/8] & m) == 0){  // Is block free?
801014bf:	89 c1                	mov    %eax,%ecx
801014c1:	c1 f9 03             	sar    $0x3,%ecx
801014c4:	0f b6 7c 0a 5c       	movzbl 0x5c(%edx,%ecx,1),%edi
801014c9:	85 df                	test   %ebx,%edi
801014cb:	89 fa                	mov    %edi,%edx
801014cd:	74 41                	je     80101510 <balloc+0xb0>
    for(bi = 0; bi < BPB && b + bi < sb.size; bi++){
801014cf:	83 c0 01             	add    $0x1,%eax
801014d2:	83 c6 01             	add    $0x1,%esi
801014d5:	3d 00 10 00 00       	cmp    $0x1000,%eax
801014da:	74 05                	je     801014e1 <balloc+0x81>
801014dc:	39 75 e0             	cmp    %esi,-0x20(%ebp)
801014df:	77 cf                	ja     801014b0 <balloc+0x50>
    brelse(bp);
801014e1:	83 ec 0c             	sub    $0xc,%esp
801014e4:	ff 75 e4             	pushl  -0x1c(%ebp)
801014e7:	e8 f4 ec ff ff       	call   801001e0 <brelse>
  for(b = 0; b < sb.size; b += BPB){
801014ec:	81 45 dc 00 10 00 00 	addl   $0x1000,-0x24(%ebp)
801014f3:	83 c4 10             	add    $0x10,%esp
801014f6:	8b 45 dc             	mov    -0x24(%ebp),%eax
801014f9:	39 05 c0 09 11 80    	cmp    %eax,0x801109c0
801014ff:	77 80                	ja     80101481 <balloc+0x21>
  panic("balloc: out of blocks");
80101501:	83 ec 0c             	sub    $0xc,%esp
80101504:	68 72 72 10 80       	push   $0x80107272
80101509:	e8 82 ee ff ff       	call   80100390 <panic>
8010150e:	66 90                	xchg   %ax,%ax
        bp->data[bi/8] |= m;  // Mark block in use.
80101510:	8b 7d e4             	mov    -0x1c(%ebp),%edi
        log_write(bp);
80101513:	83 ec 0c             	sub    $0xc,%esp
        bp->data[bi/8] |= m;  // Mark block in use.
80101516:	09 da                	or     %ebx,%edx
80101518:	88 54 0f 5c          	mov    %dl,0x5c(%edi,%ecx,1)
        log_write(bp);
8010151c:	57                   	push   %edi
8010151d:	e8 2e 1b 00 00       	call   80103050 <log_write>
        brelse(bp);
80101522:	89 3c 24             	mov    %edi,(%esp)
80101525:	e8 b6 ec ff ff       	call   801001e0 <brelse>
  bp = bread(dev, bno);
8010152a:	58                   	pop    %eax
8010152b:	5a                   	pop    %edx
8010152c:	56                   	push   %esi
8010152d:	ff 75 d8             	pushl  -0x28(%ebp)
80101530:	e8 9b eb ff ff       	call   801000d0 <bread>
80101535:	89 c3                	mov    %eax,%ebx
  memset(bp->data, 0, BSIZE);
80101537:	8d 40 5c             	lea    0x5c(%eax),%eax
8010153a:	83 c4 0c             	add    $0xc,%esp
8010153d:	68 00 02 00 00       	push   $0x200
80101542:	6a 00                	push   $0x0
80101544:	50                   	push   %eax
80101545:	e8 e6 31 00 00       	call   80104730 <memset>
  log_write(bp);
8010154a:	89 1c 24             	mov    %ebx,(%esp)
8010154d:	e8 fe 1a 00 00       	call   80103050 <log_write>
  brelse(bp);
80101552:	89 1c 24             	mov    %ebx,(%esp)
80101555:	e8 86 ec ff ff       	call   801001e0 <brelse>
}
8010155a:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010155d:	89 f0                	mov    %esi,%eax
8010155f:	5b                   	pop    %ebx
80101560:	5e                   	pop    %esi
80101561:	5f                   	pop    %edi
80101562:	5d                   	pop    %ebp
80101563:	c3                   	ret    
80101564:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
8010156a:	8d bf 00 00 00 00    	lea    0x0(%edi),%edi

80101570 <iget>:
// Find the inode with number inum on device dev
// and return the in-memory copy. Does not lock
// the inode and does not read it from disk.
static struct inode*
iget(uint dev, uint inum)
{
80101570:	55                   	push   %ebp
80101571:	89 e5                	mov    %esp,%ebp
80101573:	57                   	push   %edi
80101574:	56                   	push   %esi
80101575:	53                   	push   %ebx
80101576:	89 c7                	mov    %eax,%edi
  struct inode *ip, *empty;

  acquire(&icache.lock);

  // Is the inode already cached?
  empty = 0;
80101578:	31 f6                	xor    %esi,%esi
  for(ip = &icache.inode[0]; ip < &icache.inode[NINODE]; ip++){
8010157a:	bb 14 0a 11 80       	mov    $0x80110a14,%ebx
{
8010157f:	83 ec 28             	sub    $0x28,%esp
80101582:	89 55 e4             	mov    %edx,-0x1c(%ebp)
  acquire(&icache.lock);
80101585:	68 e0 09 11 80       	push   $0x801109e0
8010158a:	e8 91 30 00 00       	call   80104620 <acquire>
8010158f:	83 c4 10             	add    $0x10,%esp
  for(ip = &icache.inode[0]; ip < &icache.inode[NINODE]; ip++){
80101592:	8b 55 e4             	mov    -0x1c(%ebp),%edx
80101595:	eb 17                	jmp    801015ae <iget+0x3e>
80101597:	89 f6                	mov    %esi,%esi
80101599:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
801015a0:	81 c3 90 00 00 00    	add    $0x90,%ebx
801015a6:	81 fb 34 26 11 80    	cmp    $0x80112634,%ebx
801015ac:	73 22                	jae    801015d0 <iget+0x60>
    if(ip->ref > 0 && ip->dev == dev && ip->inum == inum){
801015ae:	8b 4b 08             	mov    0x8(%ebx),%ecx
801015b1:	85 c9                	test   %ecx,%ecx
801015b3:	7e 04                	jle    801015b9 <iget+0x49>
801015b5:	39 3b                	cmp    %edi,(%ebx)
801015b7:	74 4f                	je     80101608 <iget+0x98>
      ip->ref++;
      release(&icache.lock);
      return ip;
    }
    if(empty == 0 && ip->ref == 0)    // Remember empty slot.
801015b9:	85 f6                	test   %esi,%esi
801015bb:	75 e3                	jne    801015a0 <iget+0x30>
801015bd:	85 c9                	test   %ecx,%ecx
801015bf:	0f 44 f3             	cmove  %ebx,%esi
  for(ip = &icache.inode[0]; ip < &icache.inode[NINODE]; ip++){
801015c2:	81 c3 90 00 00 00    	add    $0x90,%ebx
801015c8:	81 fb 34 26 11 80    	cmp    $0x80112634,%ebx
801015ce:	72 de                	jb     801015ae <iget+0x3e>
      empty = ip;
  }

  // Recycle an inode cache entry.
  if(empty == 0)
801015d0:	85 f6                	test   %esi,%esi
801015d2:	74 5b                	je     8010162f <iget+0xbf>
  ip = empty;
  ip->dev = dev;
  ip->inum = inum;
  ip->ref = 1;
  ip->valid = 0;
  release(&icache.lock);
801015d4:	83 ec 0c             	sub    $0xc,%esp
  ip->dev = dev;
801015d7:	89 3e                	mov    %edi,(%esi)
  ip->inum = inum;
801015d9:	89 56 04             	mov    %edx,0x4(%esi)
  ip->ref = 1;
801015dc:	c7 46 08 01 00 00 00 	movl   $0x1,0x8(%esi)
  ip->valid = 0;
801015e3:	c7 46 4c 00 00 00 00 	movl   $0x0,0x4c(%esi)
  release(&icache.lock);
801015ea:	68 e0 09 11 80       	push   $0x801109e0
801015ef:	e8 ec 30 00 00       	call   801046e0 <release>

  return ip;
801015f4:	83 c4 10             	add    $0x10,%esp
}
801015f7:	8d 65 f4             	lea    -0xc(%ebp),%esp
801015fa:	89 f0                	mov    %esi,%eax
801015fc:	5b                   	pop    %ebx
801015fd:	5e                   	pop    %esi
801015fe:	5f                   	pop    %edi
801015ff:	5d                   	pop    %ebp
80101600:	c3                   	ret    
80101601:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    if(ip->ref > 0 && ip->dev == dev && ip->inum == inum){
80101608:	39 53 04             	cmp    %edx,0x4(%ebx)
8010160b:	75 ac                	jne    801015b9 <iget+0x49>
      release(&icache.lock);
8010160d:	83 ec 0c             	sub    $0xc,%esp
      ip->ref++;
80101610:	83 c1 01             	add    $0x1,%ecx
      return ip;
80101613:	89 de                	mov    %ebx,%esi
      release(&icache.lock);
80101615:	68 e0 09 11 80       	push   $0x801109e0
      ip->ref++;
8010161a:	89 4b 08             	mov    %ecx,0x8(%ebx)
      release(&icache.lock);
8010161d:	e8 be 30 00 00       	call   801046e0 <release>
      return ip;
80101622:	83 c4 10             	add    $0x10,%esp
}
80101625:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101628:	89 f0                	mov    %esi,%eax
8010162a:	5b                   	pop    %ebx
8010162b:	5e                   	pop    %esi
8010162c:	5f                   	pop    %edi
8010162d:	5d                   	pop    %ebp
8010162e:	c3                   	ret    
    panic("iget: no inodes");
8010162f:	83 ec 0c             	sub    $0xc,%esp
80101632:	68 88 72 10 80       	push   $0x80107288
80101637:	e8 54 ed ff ff       	call   80100390 <panic>
8010163c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80101640 <bmap>:

// Return the disk block address of the nth block in inode ip.
// If there is no such block, bmap allocates one.
static uint
bmap(struct inode *ip, uint bn)
{
80101640:	55                   	push   %ebp
80101641:	89 e5                	mov    %esp,%ebp
80101643:	57                   	push   %edi
80101644:	56                   	push   %esi
80101645:	53                   	push   %ebx
80101646:	89 c6                	mov    %eax,%esi
80101648:	83 ec 1c             	sub    $0x1c,%esp
  uint addr, *a;
  struct buf *bp;

  if(bn < NDIRECT){
8010164b:	83 fa 0b             	cmp    $0xb,%edx
8010164e:	77 18                	ja     80101668 <bmap+0x28>
80101650:	8d 3c 90             	lea    (%eax,%edx,4),%edi
    if((addr = ip->addrs[bn]) == 0)
80101653:	8b 5f 5c             	mov    0x5c(%edi),%ebx
80101656:	85 db                	test   %ebx,%ebx
80101658:	74 76                	je     801016d0 <bmap+0x90>
    brelse(bp);
    return addr;
  }

  panic("bmap: out of range");
}
8010165a:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010165d:	89 d8                	mov    %ebx,%eax
8010165f:	5b                   	pop    %ebx
80101660:	5e                   	pop    %esi
80101661:	5f                   	pop    %edi
80101662:	5d                   	pop    %ebp
80101663:	c3                   	ret    
80101664:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  bn -= NDIRECT;
80101668:	8d 5a f4             	lea    -0xc(%edx),%ebx
  if(bn < NINDIRECT){
8010166b:	83 fb 7f             	cmp    $0x7f,%ebx
8010166e:	0f 87 90 00 00 00    	ja     80101704 <bmap+0xc4>
    if((addr = ip->addrs[NDIRECT]) == 0)
80101674:	8b 90 8c 00 00 00    	mov    0x8c(%eax),%edx
8010167a:	8b 00                	mov    (%eax),%eax
8010167c:	85 d2                	test   %edx,%edx
8010167e:	74 70                	je     801016f0 <bmap+0xb0>
    bp = bread(ip->dev, addr);
80101680:	83 ec 08             	sub    $0x8,%esp
80101683:	52                   	push   %edx
80101684:	50                   	push   %eax
80101685:	e8 46 ea ff ff       	call   801000d0 <bread>
    if((addr = a[bn]) == 0){
8010168a:	8d 54 98 5c          	lea    0x5c(%eax,%ebx,4),%edx
8010168e:	83 c4 10             	add    $0x10,%esp
    bp = bread(ip->dev, addr);
80101691:	89 c7                	mov    %eax,%edi
    if((addr = a[bn]) == 0){
80101693:	8b 1a                	mov    (%edx),%ebx
80101695:	85 db                	test   %ebx,%ebx
80101697:	75 1d                	jne    801016b6 <bmap+0x76>
      a[bn] = addr = balloc(ip->dev);
80101699:	8b 06                	mov    (%esi),%eax
8010169b:	89 55 e4             	mov    %edx,-0x1c(%ebp)
8010169e:	e8 bd fd ff ff       	call   80101460 <balloc>
801016a3:	8b 55 e4             	mov    -0x1c(%ebp),%edx
      log_write(bp);
801016a6:	83 ec 0c             	sub    $0xc,%esp
      a[bn] = addr = balloc(ip->dev);
801016a9:	89 c3                	mov    %eax,%ebx
801016ab:	89 02                	mov    %eax,(%edx)
      log_write(bp);
801016ad:	57                   	push   %edi
801016ae:	e8 9d 19 00 00       	call   80103050 <log_write>
801016b3:	83 c4 10             	add    $0x10,%esp
    brelse(bp);
801016b6:	83 ec 0c             	sub    $0xc,%esp
801016b9:	57                   	push   %edi
801016ba:	e8 21 eb ff ff       	call   801001e0 <brelse>
801016bf:	83 c4 10             	add    $0x10,%esp
}
801016c2:	8d 65 f4             	lea    -0xc(%ebp),%esp
801016c5:	89 d8                	mov    %ebx,%eax
801016c7:	5b                   	pop    %ebx
801016c8:	5e                   	pop    %esi
801016c9:	5f                   	pop    %edi
801016ca:	5d                   	pop    %ebp
801016cb:	c3                   	ret    
801016cc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
      ip->addrs[bn] = addr = balloc(ip->dev);
801016d0:	8b 00                	mov    (%eax),%eax
801016d2:	e8 89 fd ff ff       	call   80101460 <balloc>
801016d7:	89 47 5c             	mov    %eax,0x5c(%edi)
}
801016da:	8d 65 f4             	lea    -0xc(%ebp),%esp
      ip->addrs[bn] = addr = balloc(ip->dev);
801016dd:	89 c3                	mov    %eax,%ebx
}
801016df:	89 d8                	mov    %ebx,%eax
801016e1:	5b                   	pop    %ebx
801016e2:	5e                   	pop    %esi
801016e3:	5f                   	pop    %edi
801016e4:	5d                   	pop    %ebp
801016e5:	c3                   	ret    
801016e6:	8d 76 00             	lea    0x0(%esi),%esi
801016e9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
      ip->addrs[NDIRECT] = addr = balloc(ip->dev);
801016f0:	e8 6b fd ff ff       	call   80101460 <balloc>
801016f5:	89 c2                	mov    %eax,%edx
801016f7:	89 86 8c 00 00 00    	mov    %eax,0x8c(%esi)
801016fd:	8b 06                	mov    (%esi),%eax
801016ff:	e9 7c ff ff ff       	jmp    80101680 <bmap+0x40>
  panic("bmap: out of range");
80101704:	83 ec 0c             	sub    $0xc,%esp
80101707:	68 98 72 10 80       	push   $0x80107298
8010170c:	e8 7f ec ff ff       	call   80100390 <panic>
80101711:	eb 0d                	jmp    80101720 <readsb>
80101713:	90                   	nop
80101714:	90                   	nop
80101715:	90                   	nop
80101716:	90                   	nop
80101717:	90                   	nop
80101718:	90                   	nop
80101719:	90                   	nop
8010171a:	90                   	nop
8010171b:	90                   	nop
8010171c:	90                   	nop
8010171d:	90                   	nop
8010171e:	90                   	nop
8010171f:	90                   	nop

80101720 <readsb>:
{
80101720:	55                   	push   %ebp
80101721:	89 e5                	mov    %esp,%ebp
80101723:	56                   	push   %esi
80101724:	53                   	push   %ebx
80101725:	8b 75 0c             	mov    0xc(%ebp),%esi
  bp = bread(dev, 1);
80101728:	83 ec 08             	sub    $0x8,%esp
8010172b:	6a 01                	push   $0x1
8010172d:	ff 75 08             	pushl  0x8(%ebp)
80101730:	e8 9b e9 ff ff       	call   801000d0 <bread>
80101735:	89 c3                	mov    %eax,%ebx
  memmove(sb, bp->data, sizeof(*sb));
80101737:	8d 40 5c             	lea    0x5c(%eax),%eax
8010173a:	83 c4 0c             	add    $0xc,%esp
8010173d:	6a 1c                	push   $0x1c
8010173f:	50                   	push   %eax
80101740:	56                   	push   %esi
80101741:	e8 9a 30 00 00       	call   801047e0 <memmove>
  brelse(bp);
80101746:	89 5d 08             	mov    %ebx,0x8(%ebp)
80101749:	83 c4 10             	add    $0x10,%esp
}
8010174c:	8d 65 f8             	lea    -0x8(%ebp),%esp
8010174f:	5b                   	pop    %ebx
80101750:	5e                   	pop    %esi
80101751:	5d                   	pop    %ebp
  brelse(bp);
80101752:	e9 89 ea ff ff       	jmp    801001e0 <brelse>
80101757:	89 f6                	mov    %esi,%esi
80101759:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80101760 <iinit>:
{
80101760:	55                   	push   %ebp
80101761:	89 e5                	mov    %esp,%ebp
80101763:	53                   	push   %ebx
80101764:	bb 20 0a 11 80       	mov    $0x80110a20,%ebx
80101769:	83 ec 0c             	sub    $0xc,%esp
  initlock(&icache.lock, "icache");
8010176c:	68 ab 72 10 80       	push   $0x801072ab
80101771:	68 e0 09 11 80       	push   $0x801109e0
80101776:	e8 65 2d 00 00       	call   801044e0 <initlock>
8010177b:	83 c4 10             	add    $0x10,%esp
8010177e:	66 90                	xchg   %ax,%ax
    initsleeplock(&icache.inode[i].lock, "inode");
80101780:	83 ec 08             	sub    $0x8,%esp
80101783:	68 b2 72 10 80       	push   $0x801072b2
80101788:	53                   	push   %ebx
80101789:	81 c3 90 00 00 00    	add    $0x90,%ebx
8010178f:	e8 1c 2c 00 00       	call   801043b0 <initsleeplock>
  for(i = 0; i < NINODE; i++) {
80101794:	83 c4 10             	add    $0x10,%esp
80101797:	81 fb 40 26 11 80    	cmp    $0x80112640,%ebx
8010179d:	75 e1                	jne    80101780 <iinit+0x20>
  readsb(dev, &sb);
8010179f:	83 ec 08             	sub    $0x8,%esp
801017a2:	68 c0 09 11 80       	push   $0x801109c0
801017a7:	ff 75 08             	pushl  0x8(%ebp)
801017aa:	e8 71 ff ff ff       	call   80101720 <readsb>
  cprintf("sb: size %d nblocks %d ninodes %d nlog %d logstart %d\
801017af:	ff 35 d8 09 11 80    	pushl  0x801109d8
801017b5:	ff 35 d4 09 11 80    	pushl  0x801109d4
801017bb:	ff 35 d0 09 11 80    	pushl  0x801109d0
801017c1:	ff 35 cc 09 11 80    	pushl  0x801109cc
801017c7:	ff 35 c8 09 11 80    	pushl  0x801109c8
801017cd:	ff 35 c4 09 11 80    	pushl  0x801109c4
801017d3:	ff 35 c0 09 11 80    	pushl  0x801109c0
801017d9:	68 18 73 10 80       	push   $0x80107318
801017de:	e8 ad ef ff ff       	call   80100790 <cprintf>
}
801017e3:	83 c4 30             	add    $0x30,%esp
801017e6:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801017e9:	c9                   	leave  
801017ea:	c3                   	ret    
801017eb:	90                   	nop
801017ec:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

801017f0 <ialloc>:
{
801017f0:	55                   	push   %ebp
801017f1:	89 e5                	mov    %esp,%ebp
801017f3:	57                   	push   %edi
801017f4:	56                   	push   %esi
801017f5:	53                   	push   %ebx
801017f6:	83 ec 1c             	sub    $0x1c,%esp
  for(inum = 1; inum < sb.ninodes; inum++){
801017f9:	83 3d c8 09 11 80 01 	cmpl   $0x1,0x801109c8
{
80101800:	8b 45 0c             	mov    0xc(%ebp),%eax
80101803:	8b 75 08             	mov    0x8(%ebp),%esi
80101806:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  for(inum = 1; inum < sb.ninodes; inum++){
80101809:	0f 86 91 00 00 00    	jbe    801018a0 <ialloc+0xb0>
8010180f:	bb 01 00 00 00       	mov    $0x1,%ebx
80101814:	eb 21                	jmp    80101837 <ialloc+0x47>
80101816:	8d 76 00             	lea    0x0(%esi),%esi
80101819:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
    brelse(bp);
80101820:	83 ec 0c             	sub    $0xc,%esp
  for(inum = 1; inum < sb.ninodes; inum++){
80101823:	83 c3 01             	add    $0x1,%ebx
    brelse(bp);
80101826:	57                   	push   %edi
80101827:	e8 b4 e9 ff ff       	call   801001e0 <brelse>
  for(inum = 1; inum < sb.ninodes; inum++){
8010182c:	83 c4 10             	add    $0x10,%esp
8010182f:	39 1d c8 09 11 80    	cmp    %ebx,0x801109c8
80101835:	76 69                	jbe    801018a0 <ialloc+0xb0>
    bp = bread(dev, IBLOCK(inum, sb));
80101837:	89 d8                	mov    %ebx,%eax
80101839:	83 ec 08             	sub    $0x8,%esp
8010183c:	c1 e8 03             	shr    $0x3,%eax
8010183f:	03 05 d4 09 11 80    	add    0x801109d4,%eax
80101845:	50                   	push   %eax
80101846:	56                   	push   %esi
80101847:	e8 84 e8 ff ff       	call   801000d0 <bread>
8010184c:	89 c7                	mov    %eax,%edi
    dip = (struct dinode*)bp->data + inum%IPB;
8010184e:	89 d8                	mov    %ebx,%eax
    if(dip->type == 0){  // a free inode
80101850:	83 c4 10             	add    $0x10,%esp
    dip = (struct dinode*)bp->data + inum%IPB;
80101853:	83 e0 07             	and    $0x7,%eax
80101856:	c1 e0 06             	shl    $0x6,%eax
80101859:	8d 4c 07 5c          	lea    0x5c(%edi,%eax,1),%ecx
    if(dip->type == 0){  // a free inode
8010185d:	66 83 39 00          	cmpw   $0x0,(%ecx)
80101861:	75 bd                	jne    80101820 <ialloc+0x30>
      memset(dip, 0, sizeof(*dip));
80101863:	83 ec 04             	sub    $0x4,%esp
80101866:	89 4d e0             	mov    %ecx,-0x20(%ebp)
80101869:	6a 40                	push   $0x40
8010186b:	6a 00                	push   $0x0
8010186d:	51                   	push   %ecx
8010186e:	e8 bd 2e 00 00       	call   80104730 <memset>
      dip->type = type;
80101873:	0f b7 45 e4          	movzwl -0x1c(%ebp),%eax
80101877:	8b 4d e0             	mov    -0x20(%ebp),%ecx
8010187a:	66 89 01             	mov    %ax,(%ecx)
      log_write(bp);   // mark it allocated on the disk
8010187d:	89 3c 24             	mov    %edi,(%esp)
80101880:	e8 cb 17 00 00       	call   80103050 <log_write>
      brelse(bp);
80101885:	89 3c 24             	mov    %edi,(%esp)
80101888:	e8 53 e9 ff ff       	call   801001e0 <brelse>
      return iget(dev, inum);
8010188d:	83 c4 10             	add    $0x10,%esp
}
80101890:	8d 65 f4             	lea    -0xc(%ebp),%esp
      return iget(dev, inum);
80101893:	89 da                	mov    %ebx,%edx
80101895:	89 f0                	mov    %esi,%eax
}
80101897:	5b                   	pop    %ebx
80101898:	5e                   	pop    %esi
80101899:	5f                   	pop    %edi
8010189a:	5d                   	pop    %ebp
      return iget(dev, inum);
8010189b:	e9 d0 fc ff ff       	jmp    80101570 <iget>
  panic("ialloc: no inodes");
801018a0:	83 ec 0c             	sub    $0xc,%esp
801018a3:	68 b8 72 10 80       	push   $0x801072b8
801018a8:	e8 e3 ea ff ff       	call   80100390 <panic>
801018ad:	8d 76 00             	lea    0x0(%esi),%esi

801018b0 <iupdate>:
{
801018b0:	55                   	push   %ebp
801018b1:	89 e5                	mov    %esp,%ebp
801018b3:	56                   	push   %esi
801018b4:	53                   	push   %ebx
801018b5:	8b 5d 08             	mov    0x8(%ebp),%ebx
  bp = bread(ip->dev, IBLOCK(ip->inum, sb));
801018b8:	83 ec 08             	sub    $0x8,%esp
801018bb:	8b 43 04             	mov    0x4(%ebx),%eax
  memmove(dip->addrs, ip->addrs, sizeof(ip->addrs));
801018be:	83 c3 5c             	add    $0x5c,%ebx
  bp = bread(ip->dev, IBLOCK(ip->inum, sb));
801018c1:	c1 e8 03             	shr    $0x3,%eax
801018c4:	03 05 d4 09 11 80    	add    0x801109d4,%eax
801018ca:	50                   	push   %eax
801018cb:	ff 73 a4             	pushl  -0x5c(%ebx)
801018ce:	e8 fd e7 ff ff       	call   801000d0 <bread>
801018d3:	89 c6                	mov    %eax,%esi
  dip = (struct dinode*)bp->data + ip->inum%IPB;
801018d5:	8b 43 a8             	mov    -0x58(%ebx),%eax
  dip->type = ip->type;
801018d8:	0f b7 53 f4          	movzwl -0xc(%ebx),%edx
  memmove(dip->addrs, ip->addrs, sizeof(ip->addrs));
801018dc:	83 c4 0c             	add    $0xc,%esp
  dip = (struct dinode*)bp->data + ip->inum%IPB;
801018df:	83 e0 07             	and    $0x7,%eax
801018e2:	c1 e0 06             	shl    $0x6,%eax
801018e5:	8d 44 06 5c          	lea    0x5c(%esi,%eax,1),%eax
  dip->type = ip->type;
801018e9:	66 89 10             	mov    %dx,(%eax)
  dip->major = ip->major;
801018ec:	0f b7 53 f6          	movzwl -0xa(%ebx),%edx
  memmove(dip->addrs, ip->addrs, sizeof(ip->addrs));
801018f0:	83 c0 0c             	add    $0xc,%eax
  dip->major = ip->major;
801018f3:	66 89 50 f6          	mov    %dx,-0xa(%eax)
  dip->minor = ip->minor;
801018f7:	0f b7 53 f8          	movzwl -0x8(%ebx),%edx
801018fb:	66 89 50 f8          	mov    %dx,-0x8(%eax)
  dip->nlink = ip->nlink;
801018ff:	0f b7 53 fa          	movzwl -0x6(%ebx),%edx
80101903:	66 89 50 fa          	mov    %dx,-0x6(%eax)
  dip->size = ip->size;
80101907:	8b 53 fc             	mov    -0x4(%ebx),%edx
8010190a:	89 50 fc             	mov    %edx,-0x4(%eax)
  memmove(dip->addrs, ip->addrs, sizeof(ip->addrs));
8010190d:	6a 34                	push   $0x34
8010190f:	53                   	push   %ebx
80101910:	50                   	push   %eax
80101911:	e8 ca 2e 00 00       	call   801047e0 <memmove>
  log_write(bp);
80101916:	89 34 24             	mov    %esi,(%esp)
80101919:	e8 32 17 00 00       	call   80103050 <log_write>
  brelse(bp);
8010191e:	89 75 08             	mov    %esi,0x8(%ebp)
80101921:	83 c4 10             	add    $0x10,%esp
}
80101924:	8d 65 f8             	lea    -0x8(%ebp),%esp
80101927:	5b                   	pop    %ebx
80101928:	5e                   	pop    %esi
80101929:	5d                   	pop    %ebp
  brelse(bp);
8010192a:	e9 b1 e8 ff ff       	jmp    801001e0 <brelse>
8010192f:	90                   	nop

80101930 <idup>:
{
80101930:	55                   	push   %ebp
80101931:	89 e5                	mov    %esp,%ebp
80101933:	53                   	push   %ebx
80101934:	83 ec 10             	sub    $0x10,%esp
80101937:	8b 5d 08             	mov    0x8(%ebp),%ebx
  acquire(&icache.lock);
8010193a:	68 e0 09 11 80       	push   $0x801109e0
8010193f:	e8 dc 2c 00 00       	call   80104620 <acquire>
  ip->ref++;
80101944:	83 43 08 01          	addl   $0x1,0x8(%ebx)
  release(&icache.lock);
80101948:	c7 04 24 e0 09 11 80 	movl   $0x801109e0,(%esp)
8010194f:	e8 8c 2d 00 00       	call   801046e0 <release>
}
80101954:	89 d8                	mov    %ebx,%eax
80101956:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80101959:	c9                   	leave  
8010195a:	c3                   	ret    
8010195b:	90                   	nop
8010195c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80101960 <ilock>:
{
80101960:	55                   	push   %ebp
80101961:	89 e5                	mov    %esp,%ebp
80101963:	56                   	push   %esi
80101964:	53                   	push   %ebx
80101965:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if(ip == 0 || ip->ref < 1)
80101968:	85 db                	test   %ebx,%ebx
8010196a:	0f 84 b7 00 00 00    	je     80101a27 <ilock+0xc7>
80101970:	8b 53 08             	mov    0x8(%ebx),%edx
80101973:	85 d2                	test   %edx,%edx
80101975:	0f 8e ac 00 00 00    	jle    80101a27 <ilock+0xc7>
  acquiresleep(&ip->lock);
8010197b:	8d 43 0c             	lea    0xc(%ebx),%eax
8010197e:	83 ec 0c             	sub    $0xc,%esp
80101981:	50                   	push   %eax
80101982:	e8 69 2a 00 00       	call   801043f0 <acquiresleep>
  if(ip->valid == 0){
80101987:	8b 43 4c             	mov    0x4c(%ebx),%eax
8010198a:	83 c4 10             	add    $0x10,%esp
8010198d:	85 c0                	test   %eax,%eax
8010198f:	74 0f                	je     801019a0 <ilock+0x40>
}
80101991:	8d 65 f8             	lea    -0x8(%ebp),%esp
80101994:	5b                   	pop    %ebx
80101995:	5e                   	pop    %esi
80101996:	5d                   	pop    %ebp
80101997:	c3                   	ret    
80101998:	90                   	nop
80101999:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    bp = bread(ip->dev, IBLOCK(ip->inum, sb));
801019a0:	8b 43 04             	mov    0x4(%ebx),%eax
801019a3:	83 ec 08             	sub    $0x8,%esp
801019a6:	c1 e8 03             	shr    $0x3,%eax
801019a9:	03 05 d4 09 11 80    	add    0x801109d4,%eax
801019af:	50                   	push   %eax
801019b0:	ff 33                	pushl  (%ebx)
801019b2:	e8 19 e7 ff ff       	call   801000d0 <bread>
801019b7:	89 c6                	mov    %eax,%esi
    dip = (struct dinode*)bp->data + ip->inum%IPB;
801019b9:	8b 43 04             	mov    0x4(%ebx),%eax
    memmove(ip->addrs, dip->addrs, sizeof(ip->addrs));
801019bc:	83 c4 0c             	add    $0xc,%esp
    dip = (struct dinode*)bp->data + ip->inum%IPB;
801019bf:	83 e0 07             	and    $0x7,%eax
801019c2:	c1 e0 06             	shl    $0x6,%eax
801019c5:	8d 44 06 5c          	lea    0x5c(%esi,%eax,1),%eax
    ip->type = dip->type;
801019c9:	0f b7 10             	movzwl (%eax),%edx
    memmove(ip->addrs, dip->addrs, sizeof(ip->addrs));
801019cc:	83 c0 0c             	add    $0xc,%eax
    ip->type = dip->type;
801019cf:	66 89 53 50          	mov    %dx,0x50(%ebx)
    ip->major = dip->major;
801019d3:	0f b7 50 f6          	movzwl -0xa(%eax),%edx
801019d7:	66 89 53 52          	mov    %dx,0x52(%ebx)
    ip->minor = dip->minor;
801019db:	0f b7 50 f8          	movzwl -0x8(%eax),%edx
801019df:	66 89 53 54          	mov    %dx,0x54(%ebx)
    ip->nlink = dip->nlink;
801019e3:	0f b7 50 fa          	movzwl -0x6(%eax),%edx
801019e7:	66 89 53 56          	mov    %dx,0x56(%ebx)
    ip->size = dip->size;
801019eb:	8b 50 fc             	mov    -0x4(%eax),%edx
801019ee:	89 53 58             	mov    %edx,0x58(%ebx)
    memmove(ip->addrs, dip->addrs, sizeof(ip->addrs));
801019f1:	6a 34                	push   $0x34
801019f3:	50                   	push   %eax
801019f4:	8d 43 5c             	lea    0x5c(%ebx),%eax
801019f7:	50                   	push   %eax
801019f8:	e8 e3 2d 00 00       	call   801047e0 <memmove>
    brelse(bp);
801019fd:	89 34 24             	mov    %esi,(%esp)
80101a00:	e8 db e7 ff ff       	call   801001e0 <brelse>
    if(ip->type == 0)
80101a05:	83 c4 10             	add    $0x10,%esp
80101a08:	66 83 7b 50 00       	cmpw   $0x0,0x50(%ebx)
    ip->valid = 1;
80101a0d:	c7 43 4c 01 00 00 00 	movl   $0x1,0x4c(%ebx)
    if(ip->type == 0)
80101a14:	0f 85 77 ff ff ff    	jne    80101991 <ilock+0x31>
      panic("ilock: no type");
80101a1a:	83 ec 0c             	sub    $0xc,%esp
80101a1d:	68 d0 72 10 80       	push   $0x801072d0
80101a22:	e8 69 e9 ff ff       	call   80100390 <panic>
    panic("ilock");
80101a27:	83 ec 0c             	sub    $0xc,%esp
80101a2a:	68 ca 72 10 80       	push   $0x801072ca
80101a2f:	e8 5c e9 ff ff       	call   80100390 <panic>
80101a34:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80101a3a:	8d bf 00 00 00 00    	lea    0x0(%edi),%edi

80101a40 <iunlock>:
{
80101a40:	55                   	push   %ebp
80101a41:	89 e5                	mov    %esp,%ebp
80101a43:	56                   	push   %esi
80101a44:	53                   	push   %ebx
80101a45:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if(ip == 0 || !holdingsleep(&ip->lock) || ip->ref < 1)
80101a48:	85 db                	test   %ebx,%ebx
80101a4a:	74 28                	je     80101a74 <iunlock+0x34>
80101a4c:	8d 73 0c             	lea    0xc(%ebx),%esi
80101a4f:	83 ec 0c             	sub    $0xc,%esp
80101a52:	56                   	push   %esi
80101a53:	e8 38 2a 00 00       	call   80104490 <holdingsleep>
80101a58:	83 c4 10             	add    $0x10,%esp
80101a5b:	85 c0                	test   %eax,%eax
80101a5d:	74 15                	je     80101a74 <iunlock+0x34>
80101a5f:	8b 43 08             	mov    0x8(%ebx),%eax
80101a62:	85 c0                	test   %eax,%eax
80101a64:	7e 0e                	jle    80101a74 <iunlock+0x34>
  releasesleep(&ip->lock);
80101a66:	89 75 08             	mov    %esi,0x8(%ebp)
}
80101a69:	8d 65 f8             	lea    -0x8(%ebp),%esp
80101a6c:	5b                   	pop    %ebx
80101a6d:	5e                   	pop    %esi
80101a6e:	5d                   	pop    %ebp
  releasesleep(&ip->lock);
80101a6f:	e9 dc 29 00 00       	jmp    80104450 <releasesleep>
    panic("iunlock");
80101a74:	83 ec 0c             	sub    $0xc,%esp
80101a77:	68 df 72 10 80       	push   $0x801072df
80101a7c:	e8 0f e9 ff ff       	call   80100390 <panic>
80101a81:	eb 0d                	jmp    80101a90 <iput>
80101a83:	90                   	nop
80101a84:	90                   	nop
80101a85:	90                   	nop
80101a86:	90                   	nop
80101a87:	90                   	nop
80101a88:	90                   	nop
80101a89:	90                   	nop
80101a8a:	90                   	nop
80101a8b:	90                   	nop
80101a8c:	90                   	nop
80101a8d:	90                   	nop
80101a8e:	90                   	nop
80101a8f:	90                   	nop

80101a90 <iput>:
{
80101a90:	55                   	push   %ebp
80101a91:	89 e5                	mov    %esp,%ebp
80101a93:	57                   	push   %edi
80101a94:	56                   	push   %esi
80101a95:	53                   	push   %ebx
80101a96:	83 ec 28             	sub    $0x28,%esp
80101a99:	8b 5d 08             	mov    0x8(%ebp),%ebx
  acquiresleep(&ip->lock);
80101a9c:	8d 7b 0c             	lea    0xc(%ebx),%edi
80101a9f:	57                   	push   %edi
80101aa0:	e8 4b 29 00 00       	call   801043f0 <acquiresleep>
  if(ip->valid && ip->nlink == 0){
80101aa5:	8b 53 4c             	mov    0x4c(%ebx),%edx
80101aa8:	83 c4 10             	add    $0x10,%esp
80101aab:	85 d2                	test   %edx,%edx
80101aad:	74 07                	je     80101ab6 <iput+0x26>
80101aaf:	66 83 7b 56 00       	cmpw   $0x0,0x56(%ebx)
80101ab4:	74 32                	je     80101ae8 <iput+0x58>
  releasesleep(&ip->lock);
80101ab6:	83 ec 0c             	sub    $0xc,%esp
80101ab9:	57                   	push   %edi
80101aba:	e8 91 29 00 00       	call   80104450 <releasesleep>
  acquire(&icache.lock);
80101abf:	c7 04 24 e0 09 11 80 	movl   $0x801109e0,(%esp)
80101ac6:	e8 55 2b 00 00       	call   80104620 <acquire>
  ip->ref--;
80101acb:	83 6b 08 01          	subl   $0x1,0x8(%ebx)
  release(&icache.lock);
80101acf:	83 c4 10             	add    $0x10,%esp
80101ad2:	c7 45 08 e0 09 11 80 	movl   $0x801109e0,0x8(%ebp)
}
80101ad9:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101adc:	5b                   	pop    %ebx
80101add:	5e                   	pop    %esi
80101ade:	5f                   	pop    %edi
80101adf:	5d                   	pop    %ebp
  release(&icache.lock);
80101ae0:	e9 fb 2b 00 00       	jmp    801046e0 <release>
80101ae5:	8d 76 00             	lea    0x0(%esi),%esi
    acquire(&icache.lock);
80101ae8:	83 ec 0c             	sub    $0xc,%esp
80101aeb:	68 e0 09 11 80       	push   $0x801109e0
80101af0:	e8 2b 2b 00 00       	call   80104620 <acquire>
    int r = ip->ref;
80101af5:	8b 73 08             	mov    0x8(%ebx),%esi
    release(&icache.lock);
80101af8:	c7 04 24 e0 09 11 80 	movl   $0x801109e0,(%esp)
80101aff:	e8 dc 2b 00 00       	call   801046e0 <release>
    if(r == 1){
80101b04:	83 c4 10             	add    $0x10,%esp
80101b07:	83 fe 01             	cmp    $0x1,%esi
80101b0a:	75 aa                	jne    80101ab6 <iput+0x26>
80101b0c:	8d 8b 8c 00 00 00    	lea    0x8c(%ebx),%ecx
80101b12:	89 7d e4             	mov    %edi,-0x1c(%ebp)
80101b15:	8d 73 5c             	lea    0x5c(%ebx),%esi
80101b18:	89 cf                	mov    %ecx,%edi
80101b1a:	eb 0b                	jmp    80101b27 <iput+0x97>
80101b1c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80101b20:	83 c6 04             	add    $0x4,%esi
{
  int i, j;
  struct buf *bp;
  uint *a;

  for(i = 0; i < NDIRECT; i++){
80101b23:	39 fe                	cmp    %edi,%esi
80101b25:	74 19                	je     80101b40 <iput+0xb0>
    if(ip->addrs[i]){
80101b27:	8b 16                	mov    (%esi),%edx
80101b29:	85 d2                	test   %edx,%edx
80101b2b:	74 f3                	je     80101b20 <iput+0x90>
      bfree(ip->dev, ip->addrs[i]);
80101b2d:	8b 03                	mov    (%ebx),%eax
80101b2f:	e8 bc f8 ff ff       	call   801013f0 <bfree>
      ip->addrs[i] = 0;
80101b34:	c7 06 00 00 00 00    	movl   $0x0,(%esi)
80101b3a:	eb e4                	jmp    80101b20 <iput+0x90>
80101b3c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    }
  }

  if(ip->addrs[NDIRECT]){
80101b40:	8b 83 8c 00 00 00    	mov    0x8c(%ebx),%eax
80101b46:	8b 7d e4             	mov    -0x1c(%ebp),%edi
80101b49:	85 c0                	test   %eax,%eax
80101b4b:	75 33                	jne    80101b80 <iput+0xf0>
    bfree(ip->dev, ip->addrs[NDIRECT]);
    ip->addrs[NDIRECT] = 0;
  }

  ip->size = 0;
  iupdate(ip);
80101b4d:	83 ec 0c             	sub    $0xc,%esp
  ip->size = 0;
80101b50:	c7 43 58 00 00 00 00 	movl   $0x0,0x58(%ebx)
  iupdate(ip);
80101b57:	53                   	push   %ebx
80101b58:	e8 53 fd ff ff       	call   801018b0 <iupdate>
      ip->type = 0;
80101b5d:	31 c0                	xor    %eax,%eax
80101b5f:	66 89 43 50          	mov    %ax,0x50(%ebx)
      iupdate(ip);
80101b63:	89 1c 24             	mov    %ebx,(%esp)
80101b66:	e8 45 fd ff ff       	call   801018b0 <iupdate>
      ip->valid = 0;
80101b6b:	c7 43 4c 00 00 00 00 	movl   $0x0,0x4c(%ebx)
80101b72:	83 c4 10             	add    $0x10,%esp
80101b75:	e9 3c ff ff ff       	jmp    80101ab6 <iput+0x26>
80101b7a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    bp = bread(ip->dev, ip->addrs[NDIRECT]);
80101b80:	83 ec 08             	sub    $0x8,%esp
80101b83:	50                   	push   %eax
80101b84:	ff 33                	pushl  (%ebx)
80101b86:	e8 45 e5 ff ff       	call   801000d0 <bread>
80101b8b:	8d 88 5c 02 00 00    	lea    0x25c(%eax),%ecx
80101b91:	89 7d e0             	mov    %edi,-0x20(%ebp)
80101b94:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    a = (uint*)bp->data;
80101b97:	8d 70 5c             	lea    0x5c(%eax),%esi
80101b9a:	83 c4 10             	add    $0x10,%esp
80101b9d:	89 cf                	mov    %ecx,%edi
80101b9f:	eb 0e                	jmp    80101baf <iput+0x11f>
80101ba1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80101ba8:	83 c6 04             	add    $0x4,%esi
    for(j = 0; j < NINDIRECT; j++){
80101bab:	39 fe                	cmp    %edi,%esi
80101bad:	74 0f                	je     80101bbe <iput+0x12e>
      if(a[j])
80101baf:	8b 16                	mov    (%esi),%edx
80101bb1:	85 d2                	test   %edx,%edx
80101bb3:	74 f3                	je     80101ba8 <iput+0x118>
        bfree(ip->dev, a[j]);
80101bb5:	8b 03                	mov    (%ebx),%eax
80101bb7:	e8 34 f8 ff ff       	call   801013f0 <bfree>
80101bbc:	eb ea                	jmp    80101ba8 <iput+0x118>
    brelse(bp);
80101bbe:	83 ec 0c             	sub    $0xc,%esp
80101bc1:	ff 75 e4             	pushl  -0x1c(%ebp)
80101bc4:	8b 7d e0             	mov    -0x20(%ebp),%edi
80101bc7:	e8 14 e6 ff ff       	call   801001e0 <brelse>
    bfree(ip->dev, ip->addrs[NDIRECT]);
80101bcc:	8b 93 8c 00 00 00    	mov    0x8c(%ebx),%edx
80101bd2:	8b 03                	mov    (%ebx),%eax
80101bd4:	e8 17 f8 ff ff       	call   801013f0 <bfree>
    ip->addrs[NDIRECT] = 0;
80101bd9:	c7 83 8c 00 00 00 00 	movl   $0x0,0x8c(%ebx)
80101be0:	00 00 00 
80101be3:	83 c4 10             	add    $0x10,%esp
80101be6:	e9 62 ff ff ff       	jmp    80101b4d <iput+0xbd>
80101beb:	90                   	nop
80101bec:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80101bf0 <iunlockput>:
{
80101bf0:	55                   	push   %ebp
80101bf1:	89 e5                	mov    %esp,%ebp
80101bf3:	53                   	push   %ebx
80101bf4:	83 ec 10             	sub    $0x10,%esp
80101bf7:	8b 5d 08             	mov    0x8(%ebp),%ebx
  iunlock(ip);
80101bfa:	53                   	push   %ebx
80101bfb:	e8 40 fe ff ff       	call   80101a40 <iunlock>
  iput(ip);
80101c00:	89 5d 08             	mov    %ebx,0x8(%ebp)
80101c03:	83 c4 10             	add    $0x10,%esp
}
80101c06:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80101c09:	c9                   	leave  
  iput(ip);
80101c0a:	e9 81 fe ff ff       	jmp    80101a90 <iput>
80101c0f:	90                   	nop

80101c10 <stati>:

// Copy stat information from inode.
// Caller must hold ip->lock.
void
stati(struct inode *ip, struct stat *st)
{
80101c10:	55                   	push   %ebp
80101c11:	89 e5                	mov    %esp,%ebp
80101c13:	8b 55 08             	mov    0x8(%ebp),%edx
80101c16:	8b 45 0c             	mov    0xc(%ebp),%eax
  st->dev = ip->dev;
80101c19:	8b 0a                	mov    (%edx),%ecx
80101c1b:	89 48 04             	mov    %ecx,0x4(%eax)
  st->ino = ip->inum;
80101c1e:	8b 4a 04             	mov    0x4(%edx),%ecx
80101c21:	89 48 08             	mov    %ecx,0x8(%eax)
  st->type = ip->type;
80101c24:	0f b7 4a 50          	movzwl 0x50(%edx),%ecx
80101c28:	66 89 08             	mov    %cx,(%eax)
  st->nlink = ip->nlink;
80101c2b:	0f b7 4a 56          	movzwl 0x56(%edx),%ecx
80101c2f:	66 89 48 0c          	mov    %cx,0xc(%eax)
  st->size = ip->size;
80101c33:	8b 52 58             	mov    0x58(%edx),%edx
80101c36:	89 50 10             	mov    %edx,0x10(%eax)
}
80101c39:	5d                   	pop    %ebp
80101c3a:	c3                   	ret    
80101c3b:	90                   	nop
80101c3c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80101c40 <readi>:
//PAGEBREAK!
// Read data from inode.
// Caller must hold ip->lock.
int
readi(struct inode *ip, char *dst, uint off, uint n)
{
80101c40:	55                   	push   %ebp
80101c41:	89 e5                	mov    %esp,%ebp
80101c43:	57                   	push   %edi
80101c44:	56                   	push   %esi
80101c45:	53                   	push   %ebx
80101c46:	83 ec 1c             	sub    $0x1c,%esp
80101c49:	8b 45 08             	mov    0x8(%ebp),%eax
80101c4c:	8b 75 0c             	mov    0xc(%ebp),%esi
80101c4f:	8b 7d 14             	mov    0x14(%ebp),%edi
  uint tot, m;
  struct buf *bp;

  if(ip->type == T_DEV){
80101c52:	66 83 78 50 03       	cmpw   $0x3,0x50(%eax)
{
80101c57:	89 75 e0             	mov    %esi,-0x20(%ebp)
80101c5a:	89 45 d8             	mov    %eax,-0x28(%ebp)
80101c5d:	8b 75 10             	mov    0x10(%ebp),%esi
80101c60:	89 7d e4             	mov    %edi,-0x1c(%ebp)
  if(ip->type == T_DEV){
80101c63:	0f 84 a7 00 00 00    	je     80101d10 <readi+0xd0>
    if(ip->major < 0 || ip->major >= NDEV || !devsw[ip->major].read)
      return -1;
    return devsw[ip->major].read(ip, dst, n);
  }

  if(off > ip->size || off + n < off)
80101c69:	8b 45 d8             	mov    -0x28(%ebp),%eax
80101c6c:	8b 40 58             	mov    0x58(%eax),%eax
80101c6f:	39 c6                	cmp    %eax,%esi
80101c71:	0f 87 ba 00 00 00    	ja     80101d31 <readi+0xf1>
80101c77:	8b 7d e4             	mov    -0x1c(%ebp),%edi
80101c7a:	89 f9                	mov    %edi,%ecx
80101c7c:	01 f1                	add    %esi,%ecx
80101c7e:	0f 82 ad 00 00 00    	jb     80101d31 <readi+0xf1>
    return -1;
  if(off + n > ip->size)
    n = ip->size - off;
80101c84:	89 c2                	mov    %eax,%edx
80101c86:	29 f2                	sub    %esi,%edx
80101c88:	39 c8                	cmp    %ecx,%eax
80101c8a:	0f 43 d7             	cmovae %edi,%edx

  for(tot=0; tot<n; tot+=m, off+=m, dst+=m){
80101c8d:	31 ff                	xor    %edi,%edi
80101c8f:	85 d2                	test   %edx,%edx
    n = ip->size - off;
80101c91:	89 55 e4             	mov    %edx,-0x1c(%ebp)
  for(tot=0; tot<n; tot+=m, off+=m, dst+=m){
80101c94:	74 6c                	je     80101d02 <readi+0xc2>
80101c96:	8d 76 00             	lea    0x0(%esi),%esi
80101c99:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
80101ca0:	8b 5d d8             	mov    -0x28(%ebp),%ebx
80101ca3:	89 f2                	mov    %esi,%edx
80101ca5:	c1 ea 09             	shr    $0x9,%edx
80101ca8:	89 d8                	mov    %ebx,%eax
80101caa:	e8 91 f9 ff ff       	call   80101640 <bmap>
80101caf:	83 ec 08             	sub    $0x8,%esp
80101cb2:	50                   	push   %eax
80101cb3:	ff 33                	pushl  (%ebx)
80101cb5:	e8 16 e4 ff ff       	call   801000d0 <bread>
    m = min(n - tot, BSIZE - off%BSIZE);
80101cba:	8b 5d e4             	mov    -0x1c(%ebp),%ebx
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
80101cbd:	89 c2                	mov    %eax,%edx
    m = min(n - tot, BSIZE - off%BSIZE);
80101cbf:	89 f0                	mov    %esi,%eax
80101cc1:	25 ff 01 00 00       	and    $0x1ff,%eax
80101cc6:	b9 00 02 00 00       	mov    $0x200,%ecx
80101ccb:	83 c4 0c             	add    $0xc,%esp
80101cce:	29 c1                	sub    %eax,%ecx
    memmove(dst, bp->data + off%BSIZE, m);
80101cd0:	8d 44 02 5c          	lea    0x5c(%edx,%eax,1),%eax
80101cd4:	89 55 dc             	mov    %edx,-0x24(%ebp)
    m = min(n - tot, BSIZE - off%BSIZE);
80101cd7:	29 fb                	sub    %edi,%ebx
80101cd9:	39 d9                	cmp    %ebx,%ecx
80101cdb:	0f 46 d9             	cmovbe %ecx,%ebx
    memmove(dst, bp->data + off%BSIZE, m);
80101cde:	53                   	push   %ebx
80101cdf:	50                   	push   %eax
  for(tot=0; tot<n; tot+=m, off+=m, dst+=m){
80101ce0:	01 df                	add    %ebx,%edi
    memmove(dst, bp->data + off%BSIZE, m);
80101ce2:	ff 75 e0             	pushl  -0x20(%ebp)
  for(tot=0; tot<n; tot+=m, off+=m, dst+=m){
80101ce5:	01 de                	add    %ebx,%esi
    memmove(dst, bp->data + off%BSIZE, m);
80101ce7:	e8 f4 2a 00 00       	call   801047e0 <memmove>
    brelse(bp);
80101cec:	8b 55 dc             	mov    -0x24(%ebp),%edx
80101cef:	89 14 24             	mov    %edx,(%esp)
80101cf2:	e8 e9 e4 ff ff       	call   801001e0 <brelse>
  for(tot=0; tot<n; tot+=m, off+=m, dst+=m){
80101cf7:	01 5d e0             	add    %ebx,-0x20(%ebp)
80101cfa:	83 c4 10             	add    $0x10,%esp
80101cfd:	39 7d e4             	cmp    %edi,-0x1c(%ebp)
80101d00:	77 9e                	ja     80101ca0 <readi+0x60>
  }
  return n;
80101d02:	8b 45 e4             	mov    -0x1c(%ebp),%eax
}
80101d05:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101d08:	5b                   	pop    %ebx
80101d09:	5e                   	pop    %esi
80101d0a:	5f                   	pop    %edi
80101d0b:	5d                   	pop    %ebp
80101d0c:	c3                   	ret    
80101d0d:	8d 76 00             	lea    0x0(%esi),%esi
    if(ip->major < 0 || ip->major >= NDEV || !devsw[ip->major].read)
80101d10:	0f bf 40 52          	movswl 0x52(%eax),%eax
80101d14:	66 83 f8 09          	cmp    $0x9,%ax
80101d18:	77 17                	ja     80101d31 <readi+0xf1>
80101d1a:	8b 04 c5 60 09 11 80 	mov    -0x7feef6a0(,%eax,8),%eax
80101d21:	85 c0                	test   %eax,%eax
80101d23:	74 0c                	je     80101d31 <readi+0xf1>
    return devsw[ip->major].read(ip, dst, n);
80101d25:	89 7d 10             	mov    %edi,0x10(%ebp)
}
80101d28:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101d2b:	5b                   	pop    %ebx
80101d2c:	5e                   	pop    %esi
80101d2d:	5f                   	pop    %edi
80101d2e:	5d                   	pop    %ebp
    return devsw[ip->major].read(ip, dst, n);
80101d2f:	ff e0                	jmp    *%eax
      return -1;
80101d31:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80101d36:	eb cd                	jmp    80101d05 <readi+0xc5>
80101d38:	90                   	nop
80101d39:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80101d40 <writei>:
// PAGEBREAK!
// Write data to inode.
// Caller must hold ip->lock.
int
writei(struct inode *ip, char *src, uint off, uint n)
{
80101d40:	55                   	push   %ebp
80101d41:	89 e5                	mov    %esp,%ebp
80101d43:	57                   	push   %edi
80101d44:	56                   	push   %esi
80101d45:	53                   	push   %ebx
80101d46:	83 ec 1c             	sub    $0x1c,%esp
80101d49:	8b 45 08             	mov    0x8(%ebp),%eax
80101d4c:	8b 75 0c             	mov    0xc(%ebp),%esi
80101d4f:	8b 7d 14             	mov    0x14(%ebp),%edi
  uint tot, m;
  struct buf *bp;

  if(ip->type == T_DEV){
80101d52:	66 83 78 50 03       	cmpw   $0x3,0x50(%eax)
{
80101d57:	89 75 dc             	mov    %esi,-0x24(%ebp)
80101d5a:	89 45 d8             	mov    %eax,-0x28(%ebp)
80101d5d:	8b 75 10             	mov    0x10(%ebp),%esi
80101d60:	89 7d e0             	mov    %edi,-0x20(%ebp)
  if(ip->type == T_DEV){
80101d63:	0f 84 b7 00 00 00    	je     80101e20 <writei+0xe0>
    if(ip->major < 0 || ip->major >= NDEV || !devsw[ip->major].write)
      return -1;
    return devsw[ip->major].write(ip, src, n);
  }

  if(off > ip->size || off + n < off)
80101d69:	8b 45 d8             	mov    -0x28(%ebp),%eax
80101d6c:	39 70 58             	cmp    %esi,0x58(%eax)
80101d6f:	0f 82 eb 00 00 00    	jb     80101e60 <writei+0x120>
80101d75:	8b 7d e0             	mov    -0x20(%ebp),%edi
80101d78:	31 d2                	xor    %edx,%edx
80101d7a:	89 f8                	mov    %edi,%eax
80101d7c:	01 f0                	add    %esi,%eax
80101d7e:	0f 92 c2             	setb   %dl
    return -1;
  if(off + n > MAXFILE*BSIZE)
80101d81:	3d 00 18 01 00       	cmp    $0x11800,%eax
80101d86:	0f 87 d4 00 00 00    	ja     80101e60 <writei+0x120>
80101d8c:	85 d2                	test   %edx,%edx
80101d8e:	0f 85 cc 00 00 00    	jne    80101e60 <writei+0x120>
    return -1;

  for(tot=0; tot<n; tot+=m, off+=m, src+=m){
80101d94:	85 ff                	test   %edi,%edi
80101d96:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
80101d9d:	74 72                	je     80101e11 <writei+0xd1>
80101d9f:	90                   	nop
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
80101da0:	8b 7d d8             	mov    -0x28(%ebp),%edi
80101da3:	89 f2                	mov    %esi,%edx
80101da5:	c1 ea 09             	shr    $0x9,%edx
80101da8:	89 f8                	mov    %edi,%eax
80101daa:	e8 91 f8 ff ff       	call   80101640 <bmap>
80101daf:	83 ec 08             	sub    $0x8,%esp
80101db2:	50                   	push   %eax
80101db3:	ff 37                	pushl  (%edi)
80101db5:	e8 16 e3 ff ff       	call   801000d0 <bread>
    m = min(n - tot, BSIZE - off%BSIZE);
80101dba:	8b 5d e0             	mov    -0x20(%ebp),%ebx
80101dbd:	2b 5d e4             	sub    -0x1c(%ebp),%ebx
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
80101dc0:	89 c7                	mov    %eax,%edi
    m = min(n - tot, BSIZE - off%BSIZE);
80101dc2:	89 f0                	mov    %esi,%eax
80101dc4:	b9 00 02 00 00       	mov    $0x200,%ecx
80101dc9:	83 c4 0c             	add    $0xc,%esp
80101dcc:	25 ff 01 00 00       	and    $0x1ff,%eax
80101dd1:	29 c1                	sub    %eax,%ecx
    memmove(bp->data + off%BSIZE, src, m);
80101dd3:	8d 44 07 5c          	lea    0x5c(%edi,%eax,1),%eax
    m = min(n - tot, BSIZE - off%BSIZE);
80101dd7:	39 d9                	cmp    %ebx,%ecx
80101dd9:	0f 46 d9             	cmovbe %ecx,%ebx
    memmove(bp->data + off%BSIZE, src, m);
80101ddc:	53                   	push   %ebx
80101ddd:	ff 75 dc             	pushl  -0x24(%ebp)
  for(tot=0; tot<n; tot+=m, off+=m, src+=m){
80101de0:	01 de                	add    %ebx,%esi
    memmove(bp->data + off%BSIZE, src, m);
80101de2:	50                   	push   %eax
80101de3:	e8 f8 29 00 00       	call   801047e0 <memmove>
    log_write(bp);
80101de8:	89 3c 24             	mov    %edi,(%esp)
80101deb:	e8 60 12 00 00       	call   80103050 <log_write>
    brelse(bp);
80101df0:	89 3c 24             	mov    %edi,(%esp)
80101df3:	e8 e8 e3 ff ff       	call   801001e0 <brelse>
  for(tot=0; tot<n; tot+=m, off+=m, src+=m){
80101df8:	01 5d e4             	add    %ebx,-0x1c(%ebp)
80101dfb:	01 5d dc             	add    %ebx,-0x24(%ebp)
80101dfe:	83 c4 10             	add    $0x10,%esp
80101e01:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80101e04:	39 45 e0             	cmp    %eax,-0x20(%ebp)
80101e07:	77 97                	ja     80101da0 <writei+0x60>
  }

  if(n > 0 && off > ip->size){
80101e09:	8b 45 d8             	mov    -0x28(%ebp),%eax
80101e0c:	3b 70 58             	cmp    0x58(%eax),%esi
80101e0f:	77 37                	ja     80101e48 <writei+0x108>
    ip->size = off;
    iupdate(ip);
  }
  return n;
80101e11:	8b 45 e0             	mov    -0x20(%ebp),%eax
}
80101e14:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101e17:	5b                   	pop    %ebx
80101e18:	5e                   	pop    %esi
80101e19:	5f                   	pop    %edi
80101e1a:	5d                   	pop    %ebp
80101e1b:	c3                   	ret    
80101e1c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    if(ip->major < 0 || ip->major >= NDEV || !devsw[ip->major].write)
80101e20:	0f bf 40 52          	movswl 0x52(%eax),%eax
80101e24:	66 83 f8 09          	cmp    $0x9,%ax
80101e28:	77 36                	ja     80101e60 <writei+0x120>
80101e2a:	8b 04 c5 64 09 11 80 	mov    -0x7feef69c(,%eax,8),%eax
80101e31:	85 c0                	test   %eax,%eax
80101e33:	74 2b                	je     80101e60 <writei+0x120>
    return devsw[ip->major].write(ip, src, n);
80101e35:	89 7d 10             	mov    %edi,0x10(%ebp)
}
80101e38:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101e3b:	5b                   	pop    %ebx
80101e3c:	5e                   	pop    %esi
80101e3d:	5f                   	pop    %edi
80101e3e:	5d                   	pop    %ebp
    return devsw[ip->major].write(ip, src, n);
80101e3f:	ff e0                	jmp    *%eax
80101e41:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    ip->size = off;
80101e48:	8b 45 d8             	mov    -0x28(%ebp),%eax
    iupdate(ip);
80101e4b:	83 ec 0c             	sub    $0xc,%esp
    ip->size = off;
80101e4e:	89 70 58             	mov    %esi,0x58(%eax)
    iupdate(ip);
80101e51:	50                   	push   %eax
80101e52:	e8 59 fa ff ff       	call   801018b0 <iupdate>
80101e57:	83 c4 10             	add    $0x10,%esp
80101e5a:	eb b5                	jmp    80101e11 <writei+0xd1>
80101e5c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
      return -1;
80101e60:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80101e65:	eb ad                	jmp    80101e14 <writei+0xd4>
80101e67:	89 f6                	mov    %esi,%esi
80101e69:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80101e70 <namecmp>:
//PAGEBREAK!
// Directories

int
namecmp(const char *s, const char *t)
{
80101e70:	55                   	push   %ebp
80101e71:	89 e5                	mov    %esp,%ebp
80101e73:	83 ec 0c             	sub    $0xc,%esp
  return strncmp(s, t, DIRSIZ);
80101e76:	6a 0e                	push   $0xe
80101e78:	ff 75 0c             	pushl  0xc(%ebp)
80101e7b:	ff 75 08             	pushl  0x8(%ebp)
80101e7e:	e8 cd 29 00 00       	call   80104850 <strncmp>
}
80101e83:	c9                   	leave  
80101e84:	c3                   	ret    
80101e85:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80101e89:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80101e90 <dirlookup>:

// Look for a directory entry in a directory.
// If found, set *poff to byte offset of entry.
struct inode*
dirlookup(struct inode *dp, char *name, uint *poff)
{
80101e90:	55                   	push   %ebp
80101e91:	89 e5                	mov    %esp,%ebp
80101e93:	57                   	push   %edi
80101e94:	56                   	push   %esi
80101e95:	53                   	push   %ebx
80101e96:	83 ec 1c             	sub    $0x1c,%esp
80101e99:	8b 5d 08             	mov    0x8(%ebp),%ebx
  uint off, inum;
  struct dirent de;

  if(dp->type != T_DIR)
80101e9c:	66 83 7b 50 01       	cmpw   $0x1,0x50(%ebx)
80101ea1:	0f 85 85 00 00 00    	jne    80101f2c <dirlookup+0x9c>
    panic("dirlookup not DIR");

  for(off = 0; off < dp->size; off += sizeof(de)){
80101ea7:	8b 53 58             	mov    0x58(%ebx),%edx
80101eaa:	31 ff                	xor    %edi,%edi
80101eac:	8d 75 d8             	lea    -0x28(%ebp),%esi
80101eaf:	85 d2                	test   %edx,%edx
80101eb1:	74 3e                	je     80101ef1 <dirlookup+0x61>
80101eb3:	90                   	nop
80101eb4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
80101eb8:	6a 10                	push   $0x10
80101eba:	57                   	push   %edi
80101ebb:	56                   	push   %esi
80101ebc:	53                   	push   %ebx
80101ebd:	e8 7e fd ff ff       	call   80101c40 <readi>
80101ec2:	83 c4 10             	add    $0x10,%esp
80101ec5:	83 f8 10             	cmp    $0x10,%eax
80101ec8:	75 55                	jne    80101f1f <dirlookup+0x8f>
      panic("dirlookup read");
    if(de.inum == 0)
80101eca:	66 83 7d d8 00       	cmpw   $0x0,-0x28(%ebp)
80101ecf:	74 18                	je     80101ee9 <dirlookup+0x59>
  return strncmp(s, t, DIRSIZ);
80101ed1:	8d 45 da             	lea    -0x26(%ebp),%eax
80101ed4:	83 ec 04             	sub    $0x4,%esp
80101ed7:	6a 0e                	push   $0xe
80101ed9:	50                   	push   %eax
80101eda:	ff 75 0c             	pushl  0xc(%ebp)
80101edd:	e8 6e 29 00 00       	call   80104850 <strncmp>
      continue;
    if(namecmp(name, de.name) == 0){
80101ee2:	83 c4 10             	add    $0x10,%esp
80101ee5:	85 c0                	test   %eax,%eax
80101ee7:	74 17                	je     80101f00 <dirlookup+0x70>
  for(off = 0; off < dp->size; off += sizeof(de)){
80101ee9:	83 c7 10             	add    $0x10,%edi
80101eec:	3b 7b 58             	cmp    0x58(%ebx),%edi
80101eef:	72 c7                	jb     80101eb8 <dirlookup+0x28>
      return iget(dp->dev, inum);
    }
  }

  return 0;
}
80101ef1:	8d 65 f4             	lea    -0xc(%ebp),%esp
  return 0;
80101ef4:	31 c0                	xor    %eax,%eax
}
80101ef6:	5b                   	pop    %ebx
80101ef7:	5e                   	pop    %esi
80101ef8:	5f                   	pop    %edi
80101ef9:	5d                   	pop    %ebp
80101efa:	c3                   	ret    
80101efb:	90                   	nop
80101efc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
      if(poff)
80101f00:	8b 45 10             	mov    0x10(%ebp),%eax
80101f03:	85 c0                	test   %eax,%eax
80101f05:	74 05                	je     80101f0c <dirlookup+0x7c>
        *poff = off;
80101f07:	8b 45 10             	mov    0x10(%ebp),%eax
80101f0a:	89 38                	mov    %edi,(%eax)
      inum = de.inum;
80101f0c:	0f b7 55 d8          	movzwl -0x28(%ebp),%edx
      return iget(dp->dev, inum);
80101f10:	8b 03                	mov    (%ebx),%eax
80101f12:	e8 59 f6 ff ff       	call   80101570 <iget>
}
80101f17:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101f1a:	5b                   	pop    %ebx
80101f1b:	5e                   	pop    %esi
80101f1c:	5f                   	pop    %edi
80101f1d:	5d                   	pop    %ebp
80101f1e:	c3                   	ret    
      panic("dirlookup read");
80101f1f:	83 ec 0c             	sub    $0xc,%esp
80101f22:	68 f9 72 10 80       	push   $0x801072f9
80101f27:	e8 64 e4 ff ff       	call   80100390 <panic>
    panic("dirlookup not DIR");
80101f2c:	83 ec 0c             	sub    $0xc,%esp
80101f2f:	68 e7 72 10 80       	push   $0x801072e7
80101f34:	e8 57 e4 ff ff       	call   80100390 <panic>
80101f39:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80101f40 <namex>:
// If parent != 0, return the inode for the parent and copy the final
// path element into name, which must have room for DIRSIZ bytes.
// Must be called inside a transaction since it calls iput().
static struct inode*
namex(char *path, int nameiparent, char *name)
{
80101f40:	55                   	push   %ebp
80101f41:	89 e5                	mov    %esp,%ebp
80101f43:	57                   	push   %edi
80101f44:	56                   	push   %esi
80101f45:	53                   	push   %ebx
80101f46:	89 cf                	mov    %ecx,%edi
80101f48:	89 c3                	mov    %eax,%ebx
80101f4a:	83 ec 1c             	sub    $0x1c,%esp
  struct inode *ip, *next;

  if(*path == '/')
80101f4d:	80 38 2f             	cmpb   $0x2f,(%eax)
{
80101f50:	89 55 e0             	mov    %edx,-0x20(%ebp)
  if(*path == '/')
80101f53:	0f 84 67 01 00 00    	je     801020c0 <namex+0x180>
    ip = iget(ROOTDEV, ROOTINO);
  else
    ip = idup(myproc()->cwd);
80101f59:	e8 62 1b 00 00       	call   80103ac0 <myproc>
  acquire(&icache.lock);
80101f5e:	83 ec 0c             	sub    $0xc,%esp
    ip = idup(myproc()->cwd);
80101f61:	8b 70 68             	mov    0x68(%eax),%esi
  acquire(&icache.lock);
80101f64:	68 e0 09 11 80       	push   $0x801109e0
80101f69:	e8 b2 26 00 00       	call   80104620 <acquire>
  ip->ref++;
80101f6e:	83 46 08 01          	addl   $0x1,0x8(%esi)
  release(&icache.lock);
80101f72:	c7 04 24 e0 09 11 80 	movl   $0x801109e0,(%esp)
80101f79:	e8 62 27 00 00       	call   801046e0 <release>
80101f7e:	83 c4 10             	add    $0x10,%esp
80101f81:	eb 08                	jmp    80101f8b <namex+0x4b>
80101f83:	90                   	nop
80101f84:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    path++;
80101f88:	83 c3 01             	add    $0x1,%ebx
  while(*path == '/')
80101f8b:	0f b6 03             	movzbl (%ebx),%eax
80101f8e:	3c 2f                	cmp    $0x2f,%al
80101f90:	74 f6                	je     80101f88 <namex+0x48>
  if(*path == 0)
80101f92:	84 c0                	test   %al,%al
80101f94:	0f 84 ee 00 00 00    	je     80102088 <namex+0x148>
  while(*path != '/' && *path != 0)
80101f9a:	0f b6 03             	movzbl (%ebx),%eax
80101f9d:	3c 2f                	cmp    $0x2f,%al
80101f9f:	0f 84 b3 00 00 00    	je     80102058 <namex+0x118>
80101fa5:	84 c0                	test   %al,%al
80101fa7:	89 da                	mov    %ebx,%edx
80101fa9:	75 09                	jne    80101fb4 <namex+0x74>
80101fab:	e9 a8 00 00 00       	jmp    80102058 <namex+0x118>
80101fb0:	84 c0                	test   %al,%al
80101fb2:	74 0a                	je     80101fbe <namex+0x7e>
    path++;
80101fb4:	83 c2 01             	add    $0x1,%edx
  while(*path != '/' && *path != 0)
80101fb7:	0f b6 02             	movzbl (%edx),%eax
80101fba:	3c 2f                	cmp    $0x2f,%al
80101fbc:	75 f2                	jne    80101fb0 <namex+0x70>
80101fbe:	89 d1                	mov    %edx,%ecx
80101fc0:	29 d9                	sub    %ebx,%ecx
  if(len >= DIRSIZ)
80101fc2:	83 f9 0d             	cmp    $0xd,%ecx
80101fc5:	0f 8e 91 00 00 00    	jle    8010205c <namex+0x11c>
    memmove(name, s, DIRSIZ);
80101fcb:	83 ec 04             	sub    $0x4,%esp
80101fce:	89 55 e4             	mov    %edx,-0x1c(%ebp)
80101fd1:	6a 0e                	push   $0xe
80101fd3:	53                   	push   %ebx
80101fd4:	57                   	push   %edi
80101fd5:	e8 06 28 00 00       	call   801047e0 <memmove>
    path++;
80101fda:	8b 55 e4             	mov    -0x1c(%ebp),%edx
    memmove(name, s, DIRSIZ);
80101fdd:	83 c4 10             	add    $0x10,%esp
    path++;
80101fe0:	89 d3                	mov    %edx,%ebx
  while(*path == '/')
80101fe2:	80 3a 2f             	cmpb   $0x2f,(%edx)
80101fe5:	75 11                	jne    80101ff8 <namex+0xb8>
80101fe7:	89 f6                	mov    %esi,%esi
80101fe9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
    path++;
80101ff0:	83 c3 01             	add    $0x1,%ebx
  while(*path == '/')
80101ff3:	80 3b 2f             	cmpb   $0x2f,(%ebx)
80101ff6:	74 f8                	je     80101ff0 <namex+0xb0>

  while((path = skipelem(path, name)) != 0){
    ilock(ip);
80101ff8:	83 ec 0c             	sub    $0xc,%esp
80101ffb:	56                   	push   %esi
80101ffc:	e8 5f f9 ff ff       	call   80101960 <ilock>
    if(ip->type != T_DIR){
80102001:	83 c4 10             	add    $0x10,%esp
80102004:	66 83 7e 50 01       	cmpw   $0x1,0x50(%esi)
80102009:	0f 85 91 00 00 00    	jne    801020a0 <namex+0x160>
      iunlockput(ip);
      return 0;
    }
    if(nameiparent && *path == '\0'){
8010200f:	8b 55 e0             	mov    -0x20(%ebp),%edx
80102012:	85 d2                	test   %edx,%edx
80102014:	74 09                	je     8010201f <namex+0xdf>
80102016:	80 3b 00             	cmpb   $0x0,(%ebx)
80102019:	0f 84 b7 00 00 00    	je     801020d6 <namex+0x196>
      // Stop one level early.
      iunlock(ip);
      return ip;
    }
    if((next = dirlookup(ip, name, 0)) == 0){
8010201f:	83 ec 04             	sub    $0x4,%esp
80102022:	6a 00                	push   $0x0
80102024:	57                   	push   %edi
80102025:	56                   	push   %esi
80102026:	e8 65 fe ff ff       	call   80101e90 <dirlookup>
8010202b:	83 c4 10             	add    $0x10,%esp
8010202e:	85 c0                	test   %eax,%eax
80102030:	74 6e                	je     801020a0 <namex+0x160>
  iunlock(ip);
80102032:	83 ec 0c             	sub    $0xc,%esp
80102035:	89 45 e4             	mov    %eax,-0x1c(%ebp)
80102038:	56                   	push   %esi
80102039:	e8 02 fa ff ff       	call   80101a40 <iunlock>
  iput(ip);
8010203e:	89 34 24             	mov    %esi,(%esp)
80102041:	e8 4a fa ff ff       	call   80101a90 <iput>
80102046:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80102049:	83 c4 10             	add    $0x10,%esp
8010204c:	89 c6                	mov    %eax,%esi
8010204e:	e9 38 ff ff ff       	jmp    80101f8b <namex+0x4b>
80102053:	90                   	nop
80102054:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  while(*path != '/' && *path != 0)
80102058:	89 da                	mov    %ebx,%edx
8010205a:	31 c9                	xor    %ecx,%ecx
    memmove(name, s, len);
8010205c:	83 ec 04             	sub    $0x4,%esp
8010205f:	89 55 dc             	mov    %edx,-0x24(%ebp)
80102062:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
80102065:	51                   	push   %ecx
80102066:	53                   	push   %ebx
80102067:	57                   	push   %edi
80102068:	e8 73 27 00 00       	call   801047e0 <memmove>
    name[len] = 0;
8010206d:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
80102070:	8b 55 dc             	mov    -0x24(%ebp),%edx
80102073:	83 c4 10             	add    $0x10,%esp
80102076:	c6 04 0f 00          	movb   $0x0,(%edi,%ecx,1)
8010207a:	89 d3                	mov    %edx,%ebx
8010207c:	e9 61 ff ff ff       	jmp    80101fe2 <namex+0xa2>
80102081:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
      return 0;
    }
    iunlockput(ip);
    ip = next;
  }
  if(nameiparent){
80102088:	8b 45 e0             	mov    -0x20(%ebp),%eax
8010208b:	85 c0                	test   %eax,%eax
8010208d:	75 5d                	jne    801020ec <namex+0x1ac>
    iput(ip);
    return 0;
  }
  return ip;
}
8010208f:	8d 65 f4             	lea    -0xc(%ebp),%esp
80102092:	89 f0                	mov    %esi,%eax
80102094:	5b                   	pop    %ebx
80102095:	5e                   	pop    %esi
80102096:	5f                   	pop    %edi
80102097:	5d                   	pop    %ebp
80102098:	c3                   	ret    
80102099:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  iunlock(ip);
801020a0:	83 ec 0c             	sub    $0xc,%esp
801020a3:	56                   	push   %esi
801020a4:	e8 97 f9 ff ff       	call   80101a40 <iunlock>
  iput(ip);
801020a9:	89 34 24             	mov    %esi,(%esp)
      return 0;
801020ac:	31 f6                	xor    %esi,%esi
  iput(ip);
801020ae:	e8 dd f9 ff ff       	call   80101a90 <iput>
      return 0;
801020b3:	83 c4 10             	add    $0x10,%esp
}
801020b6:	8d 65 f4             	lea    -0xc(%ebp),%esp
801020b9:	89 f0                	mov    %esi,%eax
801020bb:	5b                   	pop    %ebx
801020bc:	5e                   	pop    %esi
801020bd:	5f                   	pop    %edi
801020be:	5d                   	pop    %ebp
801020bf:	c3                   	ret    
    ip = iget(ROOTDEV, ROOTINO);
801020c0:	ba 01 00 00 00       	mov    $0x1,%edx
801020c5:	b8 01 00 00 00       	mov    $0x1,%eax
801020ca:	e8 a1 f4 ff ff       	call   80101570 <iget>
801020cf:	89 c6                	mov    %eax,%esi
801020d1:	e9 b5 fe ff ff       	jmp    80101f8b <namex+0x4b>
      iunlock(ip);
801020d6:	83 ec 0c             	sub    $0xc,%esp
801020d9:	56                   	push   %esi
801020da:	e8 61 f9 ff ff       	call   80101a40 <iunlock>
      return ip;
801020df:	83 c4 10             	add    $0x10,%esp
}
801020e2:	8d 65 f4             	lea    -0xc(%ebp),%esp
801020e5:	89 f0                	mov    %esi,%eax
801020e7:	5b                   	pop    %ebx
801020e8:	5e                   	pop    %esi
801020e9:	5f                   	pop    %edi
801020ea:	5d                   	pop    %ebp
801020eb:	c3                   	ret    
    iput(ip);
801020ec:	83 ec 0c             	sub    $0xc,%esp
801020ef:	56                   	push   %esi
    return 0;
801020f0:	31 f6                	xor    %esi,%esi
    iput(ip);
801020f2:	e8 99 f9 ff ff       	call   80101a90 <iput>
    return 0;
801020f7:	83 c4 10             	add    $0x10,%esp
801020fa:	eb 93                	jmp    8010208f <namex+0x14f>
801020fc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80102100 <dirlink>:
{
80102100:	55                   	push   %ebp
80102101:	89 e5                	mov    %esp,%ebp
80102103:	57                   	push   %edi
80102104:	56                   	push   %esi
80102105:	53                   	push   %ebx
80102106:	83 ec 20             	sub    $0x20,%esp
80102109:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if((ip = dirlookup(dp, name, 0)) != 0){
8010210c:	6a 00                	push   $0x0
8010210e:	ff 75 0c             	pushl  0xc(%ebp)
80102111:	53                   	push   %ebx
80102112:	e8 79 fd ff ff       	call   80101e90 <dirlookup>
80102117:	83 c4 10             	add    $0x10,%esp
8010211a:	85 c0                	test   %eax,%eax
8010211c:	75 67                	jne    80102185 <dirlink+0x85>
  for(off = 0; off < dp->size; off += sizeof(de)){
8010211e:	8b 7b 58             	mov    0x58(%ebx),%edi
80102121:	8d 75 d8             	lea    -0x28(%ebp),%esi
80102124:	85 ff                	test   %edi,%edi
80102126:	74 29                	je     80102151 <dirlink+0x51>
80102128:	31 ff                	xor    %edi,%edi
8010212a:	8d 75 d8             	lea    -0x28(%ebp),%esi
8010212d:	eb 09                	jmp    80102138 <dirlink+0x38>
8010212f:	90                   	nop
80102130:	83 c7 10             	add    $0x10,%edi
80102133:	3b 7b 58             	cmp    0x58(%ebx),%edi
80102136:	73 19                	jae    80102151 <dirlink+0x51>
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
80102138:	6a 10                	push   $0x10
8010213a:	57                   	push   %edi
8010213b:	56                   	push   %esi
8010213c:	53                   	push   %ebx
8010213d:	e8 fe fa ff ff       	call   80101c40 <readi>
80102142:	83 c4 10             	add    $0x10,%esp
80102145:	83 f8 10             	cmp    $0x10,%eax
80102148:	75 4e                	jne    80102198 <dirlink+0x98>
    if(de.inum == 0)
8010214a:	66 83 7d d8 00       	cmpw   $0x0,-0x28(%ebp)
8010214f:	75 df                	jne    80102130 <dirlink+0x30>
  strncpy(de.name, name, DIRSIZ);
80102151:	8d 45 da             	lea    -0x26(%ebp),%eax
80102154:	83 ec 04             	sub    $0x4,%esp
80102157:	6a 0e                	push   $0xe
80102159:	ff 75 0c             	pushl  0xc(%ebp)
8010215c:	50                   	push   %eax
8010215d:	e8 4e 27 00 00       	call   801048b0 <strncpy>
  de.inum = inum;
80102162:	8b 45 10             	mov    0x10(%ebp),%eax
  if(writei(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
80102165:	6a 10                	push   $0x10
80102167:	57                   	push   %edi
80102168:	56                   	push   %esi
80102169:	53                   	push   %ebx
  de.inum = inum;
8010216a:	66 89 45 d8          	mov    %ax,-0x28(%ebp)
  if(writei(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
8010216e:	e8 cd fb ff ff       	call   80101d40 <writei>
80102173:	83 c4 20             	add    $0x20,%esp
80102176:	83 f8 10             	cmp    $0x10,%eax
80102179:	75 2a                	jne    801021a5 <dirlink+0xa5>
  return 0;
8010217b:	31 c0                	xor    %eax,%eax
}
8010217d:	8d 65 f4             	lea    -0xc(%ebp),%esp
80102180:	5b                   	pop    %ebx
80102181:	5e                   	pop    %esi
80102182:	5f                   	pop    %edi
80102183:	5d                   	pop    %ebp
80102184:	c3                   	ret    
    iput(ip);
80102185:	83 ec 0c             	sub    $0xc,%esp
80102188:	50                   	push   %eax
80102189:	e8 02 f9 ff ff       	call   80101a90 <iput>
    return -1;
8010218e:	83 c4 10             	add    $0x10,%esp
80102191:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80102196:	eb e5                	jmp    8010217d <dirlink+0x7d>
      panic("dirlink read");
80102198:	83 ec 0c             	sub    $0xc,%esp
8010219b:	68 08 73 10 80       	push   $0x80107308
801021a0:	e8 eb e1 ff ff       	call   80100390 <panic>
    panic("dirlink");
801021a5:	83 ec 0c             	sub    $0xc,%esp
801021a8:	68 fe 78 10 80       	push   $0x801078fe
801021ad:	e8 de e1 ff ff       	call   80100390 <panic>
801021b2:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801021b9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

801021c0 <namei>:

struct inode*
namei(char *path)
{
801021c0:	55                   	push   %ebp
  char name[DIRSIZ];
  return namex(path, 0, name);
801021c1:	31 d2                	xor    %edx,%edx
{
801021c3:	89 e5                	mov    %esp,%ebp
801021c5:	83 ec 18             	sub    $0x18,%esp
  return namex(path, 0, name);
801021c8:	8b 45 08             	mov    0x8(%ebp),%eax
801021cb:	8d 4d ea             	lea    -0x16(%ebp),%ecx
801021ce:	e8 6d fd ff ff       	call   80101f40 <namex>
}
801021d3:	c9                   	leave  
801021d4:	c3                   	ret    
801021d5:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801021d9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

801021e0 <nameiparent>:

struct inode*
nameiparent(char *path, char *name)
{
801021e0:	55                   	push   %ebp
  return namex(path, 1, name);
801021e1:	ba 01 00 00 00       	mov    $0x1,%edx
{
801021e6:	89 e5                	mov    %esp,%ebp
  return namex(path, 1, name);
801021e8:	8b 4d 0c             	mov    0xc(%ebp),%ecx
801021eb:	8b 45 08             	mov    0x8(%ebp),%eax
}
801021ee:	5d                   	pop    %ebp
  return namex(path, 1, name);
801021ef:	e9 4c fd ff ff       	jmp    80101f40 <namex>
801021f4:	66 90                	xchg   %ax,%ax
801021f6:	66 90                	xchg   %ax,%ax
801021f8:	66 90                	xchg   %ax,%ax
801021fa:	66 90                	xchg   %ax,%ax
801021fc:	66 90                	xchg   %ax,%ax
801021fe:	66 90                	xchg   %ax,%ax

80102200 <idestart>:
}

// Start the request for b.  Caller must hold idelock.
static void
idestart(struct buf *b)
{
80102200:	55                   	push   %ebp
80102201:	89 e5                	mov    %esp,%ebp
80102203:	57                   	push   %edi
80102204:	56                   	push   %esi
80102205:	53                   	push   %ebx
80102206:	83 ec 0c             	sub    $0xc,%esp
  if(b == 0)
80102209:	85 c0                	test   %eax,%eax
8010220b:	0f 84 b4 00 00 00    	je     801022c5 <idestart+0xc5>
    panic("idestart");
  if(b->blockno >= FSSIZE)
80102211:	8b 58 08             	mov    0x8(%eax),%ebx
80102214:	89 c6                	mov    %eax,%esi
80102216:	81 fb e7 03 00 00    	cmp    $0x3e7,%ebx
8010221c:	0f 87 96 00 00 00    	ja     801022b8 <idestart+0xb8>
80102222:	b9 f7 01 00 00       	mov    $0x1f7,%ecx
80102227:	89 f6                	mov    %esi,%esi
80102229:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
80102230:	89 ca                	mov    %ecx,%edx
80102232:	ec                   	in     (%dx),%al
  while(((r = inb(0x1f7)) & (IDE_BSY|IDE_DRDY)) != IDE_DRDY)
80102233:	83 e0 c0             	and    $0xffffffc0,%eax
80102236:	3c 40                	cmp    $0x40,%al
80102238:	75 f6                	jne    80102230 <idestart+0x30>
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
8010223a:	31 ff                	xor    %edi,%edi
8010223c:	ba f6 03 00 00       	mov    $0x3f6,%edx
80102241:	89 f8                	mov    %edi,%eax
80102243:	ee                   	out    %al,(%dx)
80102244:	b8 01 00 00 00       	mov    $0x1,%eax
80102249:	ba f2 01 00 00       	mov    $0x1f2,%edx
8010224e:	ee                   	out    %al,(%dx)
8010224f:	ba f3 01 00 00       	mov    $0x1f3,%edx
80102254:	89 d8                	mov    %ebx,%eax
80102256:	ee                   	out    %al,(%dx)

  idewait(0);
  outb(0x3f6, 0);  // generate interrupt
  outb(0x1f2, sector_per_block);  // number of sectors
  outb(0x1f3, sector & 0xff);
  outb(0x1f4, (sector >> 8) & 0xff);
80102257:	89 d8                	mov    %ebx,%eax
80102259:	ba f4 01 00 00       	mov    $0x1f4,%edx
8010225e:	c1 f8 08             	sar    $0x8,%eax
80102261:	ee                   	out    %al,(%dx)
80102262:	ba f5 01 00 00       	mov    $0x1f5,%edx
80102267:	89 f8                	mov    %edi,%eax
80102269:	ee                   	out    %al,(%dx)
  outb(0x1f5, (sector >> 16) & 0xff);
  outb(0x1f6, 0xe0 | ((b->dev&1)<<4) | ((sector>>24)&0x0f));
8010226a:	0f b6 46 04          	movzbl 0x4(%esi),%eax
8010226e:	ba f6 01 00 00       	mov    $0x1f6,%edx
80102273:	c1 e0 04             	shl    $0x4,%eax
80102276:	83 e0 10             	and    $0x10,%eax
80102279:	83 c8 e0             	or     $0xffffffe0,%eax
8010227c:	ee                   	out    %al,(%dx)
  if(b->flags & B_DIRTY){
8010227d:	f6 06 04             	testb  $0x4,(%esi)
80102280:	75 16                	jne    80102298 <idestart+0x98>
80102282:	b8 20 00 00 00       	mov    $0x20,%eax
80102287:	89 ca                	mov    %ecx,%edx
80102289:	ee                   	out    %al,(%dx)
    outb(0x1f7, write_cmd);
    outsl(0x1f0, b->data, BSIZE/4);
  } else {
    outb(0x1f7, read_cmd);
  }
}
8010228a:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010228d:	5b                   	pop    %ebx
8010228e:	5e                   	pop    %esi
8010228f:	5f                   	pop    %edi
80102290:	5d                   	pop    %ebp
80102291:	c3                   	ret    
80102292:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80102298:	b8 30 00 00 00       	mov    $0x30,%eax
8010229d:	89 ca                	mov    %ecx,%edx
8010229f:	ee                   	out    %al,(%dx)
  asm volatile("cld; rep outsl" :
801022a0:	b9 80 00 00 00       	mov    $0x80,%ecx
    outsl(0x1f0, b->data, BSIZE/4);
801022a5:	83 c6 5c             	add    $0x5c,%esi
801022a8:	ba f0 01 00 00       	mov    $0x1f0,%edx
801022ad:	fc                   	cld    
801022ae:	f3 6f                	rep outsl %ds:(%esi),(%dx)
}
801022b0:	8d 65 f4             	lea    -0xc(%ebp),%esp
801022b3:	5b                   	pop    %ebx
801022b4:	5e                   	pop    %esi
801022b5:	5f                   	pop    %edi
801022b6:	5d                   	pop    %ebp
801022b7:	c3                   	ret    
    panic("incorrect blockno");
801022b8:	83 ec 0c             	sub    $0xc,%esp
801022bb:	68 74 73 10 80       	push   $0x80107374
801022c0:	e8 cb e0 ff ff       	call   80100390 <panic>
    panic("idestart");
801022c5:	83 ec 0c             	sub    $0xc,%esp
801022c8:	68 6b 73 10 80       	push   $0x8010736b
801022cd:	e8 be e0 ff ff       	call   80100390 <panic>
801022d2:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801022d9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

801022e0 <ideinit>:
{
801022e0:	55                   	push   %ebp
801022e1:	89 e5                	mov    %esp,%ebp
801022e3:	83 ec 10             	sub    $0x10,%esp
  initlock(&idelock, "ide");
801022e6:	68 86 73 10 80       	push   $0x80107386
801022eb:	68 80 a5 10 80       	push   $0x8010a580
801022f0:	e8 eb 21 00 00       	call   801044e0 <initlock>
  ioapicenable(IRQ_IDE, ncpu - 1);
801022f5:	58                   	pop    %eax
801022f6:	a1 00 2d 11 80       	mov    0x80112d00,%eax
801022fb:	5a                   	pop    %edx
801022fc:	83 e8 01             	sub    $0x1,%eax
801022ff:	50                   	push   %eax
80102300:	6a 0e                	push   $0xe
80102302:	e8 a9 02 00 00       	call   801025b0 <ioapicenable>
80102307:	83 c4 10             	add    $0x10,%esp
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
8010230a:	ba f7 01 00 00       	mov    $0x1f7,%edx
8010230f:	90                   	nop
80102310:	ec                   	in     (%dx),%al
  while(((r = inb(0x1f7)) & (IDE_BSY|IDE_DRDY)) != IDE_DRDY)
80102311:	83 e0 c0             	and    $0xffffffc0,%eax
80102314:	3c 40                	cmp    $0x40,%al
80102316:	75 f8                	jne    80102310 <ideinit+0x30>
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102318:	b8 f0 ff ff ff       	mov    $0xfffffff0,%eax
8010231d:	ba f6 01 00 00       	mov    $0x1f6,%edx
80102322:	ee                   	out    %al,(%dx)
80102323:	b9 e8 03 00 00       	mov    $0x3e8,%ecx
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102328:	ba f7 01 00 00       	mov    $0x1f7,%edx
8010232d:	eb 06                	jmp    80102335 <ideinit+0x55>
8010232f:	90                   	nop
  for(i=0; i<1000; i++){
80102330:	83 e9 01             	sub    $0x1,%ecx
80102333:	74 0f                	je     80102344 <ideinit+0x64>
80102335:	ec                   	in     (%dx),%al
    if(inb(0x1f7) != 0){
80102336:	84 c0                	test   %al,%al
80102338:	74 f6                	je     80102330 <ideinit+0x50>
      havedisk1 = 1;
8010233a:	c7 05 60 a5 10 80 01 	movl   $0x1,0x8010a560
80102341:	00 00 00 
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102344:	b8 e0 ff ff ff       	mov    $0xffffffe0,%eax
80102349:	ba f6 01 00 00       	mov    $0x1f6,%edx
8010234e:	ee                   	out    %al,(%dx)
}
8010234f:	c9                   	leave  
80102350:	c3                   	ret    
80102351:	eb 0d                	jmp    80102360 <ideintr>
80102353:	90                   	nop
80102354:	90                   	nop
80102355:	90                   	nop
80102356:	90                   	nop
80102357:	90                   	nop
80102358:	90                   	nop
80102359:	90                   	nop
8010235a:	90                   	nop
8010235b:	90                   	nop
8010235c:	90                   	nop
8010235d:	90                   	nop
8010235e:	90                   	nop
8010235f:	90                   	nop

80102360 <ideintr>:

// Interrupt handler.
void
ideintr(void)
{
80102360:	55                   	push   %ebp
80102361:	89 e5                	mov    %esp,%ebp
80102363:	57                   	push   %edi
80102364:	56                   	push   %esi
80102365:	53                   	push   %ebx
80102366:	83 ec 18             	sub    $0x18,%esp
  struct buf *b;

  // First queued buffer is the active request.
  acquire(&idelock);
80102369:	68 80 a5 10 80       	push   $0x8010a580
8010236e:	e8 ad 22 00 00       	call   80104620 <acquire>

  if((b = idequeue) == 0){
80102373:	8b 1d 64 a5 10 80    	mov    0x8010a564,%ebx
80102379:	83 c4 10             	add    $0x10,%esp
8010237c:	85 db                	test   %ebx,%ebx
8010237e:	74 67                	je     801023e7 <ideintr+0x87>
    release(&idelock);
    return;
  }
  idequeue = b->qnext;
80102380:	8b 43 58             	mov    0x58(%ebx),%eax
80102383:	a3 64 a5 10 80       	mov    %eax,0x8010a564

  // Read data if needed.
  if(!(b->flags & B_DIRTY) && idewait(1) >= 0)
80102388:	8b 3b                	mov    (%ebx),%edi
8010238a:	f7 c7 04 00 00 00    	test   $0x4,%edi
80102390:	75 31                	jne    801023c3 <ideintr+0x63>
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102392:	ba f7 01 00 00       	mov    $0x1f7,%edx
80102397:	89 f6                	mov    %esi,%esi
80102399:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
801023a0:	ec                   	in     (%dx),%al
  while(((r = inb(0x1f7)) & (IDE_BSY|IDE_DRDY)) != IDE_DRDY)
801023a1:	89 c6                	mov    %eax,%esi
801023a3:	83 e6 c0             	and    $0xffffffc0,%esi
801023a6:	89 f1                	mov    %esi,%ecx
801023a8:	80 f9 40             	cmp    $0x40,%cl
801023ab:	75 f3                	jne    801023a0 <ideintr+0x40>
  if(checkerr && (r & (IDE_DF|IDE_ERR)) != 0)
801023ad:	a8 21                	test   $0x21,%al
801023af:	75 12                	jne    801023c3 <ideintr+0x63>
    insl(0x1f0, b->data, BSIZE/4);
801023b1:	8d 7b 5c             	lea    0x5c(%ebx),%edi
  asm volatile("cld; rep insl" :
801023b4:	b9 80 00 00 00       	mov    $0x80,%ecx
801023b9:	ba f0 01 00 00       	mov    $0x1f0,%edx
801023be:	fc                   	cld    
801023bf:	f3 6d                	rep insl (%dx),%es:(%edi)
801023c1:	8b 3b                	mov    (%ebx),%edi

  // Wake process waiting for this buf.
  b->flags |= B_VALID;
  b->flags &= ~B_DIRTY;
801023c3:	83 e7 fb             	and    $0xfffffffb,%edi
  wakeup(b);
801023c6:	83 ec 0c             	sub    $0xc,%esp
  b->flags &= ~B_DIRTY;
801023c9:	89 f9                	mov    %edi,%ecx
801023cb:	83 c9 02             	or     $0x2,%ecx
801023ce:	89 0b                	mov    %ecx,(%ebx)
  wakeup(b);
801023d0:	53                   	push   %ebx
801023d1:	e8 3a 1e 00 00       	call   80104210 <wakeup>

  // Start disk on next buf in queue.
  if(idequeue != 0)
801023d6:	a1 64 a5 10 80       	mov    0x8010a564,%eax
801023db:	83 c4 10             	add    $0x10,%esp
801023de:	85 c0                	test   %eax,%eax
801023e0:	74 05                	je     801023e7 <ideintr+0x87>
    idestart(idequeue);
801023e2:	e8 19 fe ff ff       	call   80102200 <idestart>
    release(&idelock);
801023e7:	83 ec 0c             	sub    $0xc,%esp
801023ea:	68 80 a5 10 80       	push   $0x8010a580
801023ef:	e8 ec 22 00 00       	call   801046e0 <release>

  release(&idelock);
}
801023f4:	8d 65 f4             	lea    -0xc(%ebp),%esp
801023f7:	5b                   	pop    %ebx
801023f8:	5e                   	pop    %esi
801023f9:	5f                   	pop    %edi
801023fa:	5d                   	pop    %ebp
801023fb:	c3                   	ret    
801023fc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80102400 <iderw>:
// Sync buf with disk.
// If B_DIRTY is set, write buf to disk, clear B_DIRTY, set B_VALID.
// Else if B_VALID is not set, read buf from disk, set B_VALID.
void
iderw(struct buf *b)
{
80102400:	55                   	push   %ebp
80102401:	89 e5                	mov    %esp,%ebp
80102403:	53                   	push   %ebx
80102404:	83 ec 10             	sub    $0x10,%esp
80102407:	8b 5d 08             	mov    0x8(%ebp),%ebx
  struct buf **pp;

  if(!holdingsleep(&b->lock))
8010240a:	8d 43 0c             	lea    0xc(%ebx),%eax
8010240d:	50                   	push   %eax
8010240e:	e8 7d 20 00 00       	call   80104490 <holdingsleep>
80102413:	83 c4 10             	add    $0x10,%esp
80102416:	85 c0                	test   %eax,%eax
80102418:	0f 84 c6 00 00 00    	je     801024e4 <iderw+0xe4>
    panic("iderw: buf not locked");
  if((b->flags & (B_VALID|B_DIRTY)) == B_VALID)
8010241e:	8b 03                	mov    (%ebx),%eax
80102420:	83 e0 06             	and    $0x6,%eax
80102423:	83 f8 02             	cmp    $0x2,%eax
80102426:	0f 84 ab 00 00 00    	je     801024d7 <iderw+0xd7>
    panic("iderw: nothing to do");
  if(b->dev != 0 && !havedisk1)
8010242c:	8b 53 04             	mov    0x4(%ebx),%edx
8010242f:	85 d2                	test   %edx,%edx
80102431:	74 0d                	je     80102440 <iderw+0x40>
80102433:	a1 60 a5 10 80       	mov    0x8010a560,%eax
80102438:	85 c0                	test   %eax,%eax
8010243a:	0f 84 b1 00 00 00    	je     801024f1 <iderw+0xf1>
    panic("iderw: ide disk 1 not present");

  acquire(&idelock);  //DOC:acquire-lock
80102440:	83 ec 0c             	sub    $0xc,%esp
80102443:	68 80 a5 10 80       	push   $0x8010a580
80102448:	e8 d3 21 00 00       	call   80104620 <acquire>

  // Append b to idequeue.
  b->qnext = 0;
  for(pp=&idequeue; *pp; pp=&(*pp)->qnext)  //DOC:insert-queue
8010244d:	8b 15 64 a5 10 80    	mov    0x8010a564,%edx
80102453:	83 c4 10             	add    $0x10,%esp
  b->qnext = 0;
80102456:	c7 43 58 00 00 00 00 	movl   $0x0,0x58(%ebx)
  for(pp=&idequeue; *pp; pp=&(*pp)->qnext)  //DOC:insert-queue
8010245d:	85 d2                	test   %edx,%edx
8010245f:	75 09                	jne    8010246a <iderw+0x6a>
80102461:	eb 6d                	jmp    801024d0 <iderw+0xd0>
80102463:	90                   	nop
80102464:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80102468:	89 c2                	mov    %eax,%edx
8010246a:	8b 42 58             	mov    0x58(%edx),%eax
8010246d:	85 c0                	test   %eax,%eax
8010246f:	75 f7                	jne    80102468 <iderw+0x68>
80102471:	83 c2 58             	add    $0x58,%edx
    ;
  *pp = b;
80102474:	89 1a                	mov    %ebx,(%edx)

  // Start disk if necessary.
  if(idequeue == b)
80102476:	39 1d 64 a5 10 80    	cmp    %ebx,0x8010a564
8010247c:	74 42                	je     801024c0 <iderw+0xc0>
    idestart(b);

  // Wait for request to finish.
  while((b->flags & (B_VALID|B_DIRTY)) != B_VALID){
8010247e:	8b 03                	mov    (%ebx),%eax
80102480:	83 e0 06             	and    $0x6,%eax
80102483:	83 f8 02             	cmp    $0x2,%eax
80102486:	74 23                	je     801024ab <iderw+0xab>
80102488:	90                   	nop
80102489:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    sleep(b, &idelock);
80102490:	83 ec 08             	sub    $0x8,%esp
80102493:	68 80 a5 10 80       	push   $0x8010a580
80102498:	53                   	push   %ebx
80102499:	e8 c2 1b 00 00       	call   80104060 <sleep>
  while((b->flags & (B_VALID|B_DIRTY)) != B_VALID){
8010249e:	8b 03                	mov    (%ebx),%eax
801024a0:	83 c4 10             	add    $0x10,%esp
801024a3:	83 e0 06             	and    $0x6,%eax
801024a6:	83 f8 02             	cmp    $0x2,%eax
801024a9:	75 e5                	jne    80102490 <iderw+0x90>
  }


  release(&idelock);
801024ab:	c7 45 08 80 a5 10 80 	movl   $0x8010a580,0x8(%ebp)
}
801024b2:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801024b5:	c9                   	leave  
  release(&idelock);
801024b6:	e9 25 22 00 00       	jmp    801046e0 <release>
801024bb:	90                   	nop
801024bc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    idestart(b);
801024c0:	89 d8                	mov    %ebx,%eax
801024c2:	e8 39 fd ff ff       	call   80102200 <idestart>
801024c7:	eb b5                	jmp    8010247e <iderw+0x7e>
801024c9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  for(pp=&idequeue; *pp; pp=&(*pp)->qnext)  //DOC:insert-queue
801024d0:	ba 64 a5 10 80       	mov    $0x8010a564,%edx
801024d5:	eb 9d                	jmp    80102474 <iderw+0x74>
    panic("iderw: nothing to do");
801024d7:	83 ec 0c             	sub    $0xc,%esp
801024da:	68 a0 73 10 80       	push   $0x801073a0
801024df:	e8 ac de ff ff       	call   80100390 <panic>
    panic("iderw: buf not locked");
801024e4:	83 ec 0c             	sub    $0xc,%esp
801024e7:	68 8a 73 10 80       	push   $0x8010738a
801024ec:	e8 9f de ff ff       	call   80100390 <panic>
    panic("iderw: ide disk 1 not present");
801024f1:	83 ec 0c             	sub    $0xc,%esp
801024f4:	68 b5 73 10 80       	push   $0x801073b5
801024f9:	e8 92 de ff ff       	call   80100390 <panic>
801024fe:	66 90                	xchg   %ax,%ax

80102500 <ioapicinit>:
  ioapic->data = data;
}

void
ioapicinit(void)
{
80102500:	55                   	push   %ebp
  int i, id, maxintr;

  ioapic = (volatile struct ioapic*)IOAPIC;
80102501:	c7 05 34 26 11 80 00 	movl   $0xfec00000,0x80112634
80102508:	00 c0 fe 
{
8010250b:	89 e5                	mov    %esp,%ebp
8010250d:	56                   	push   %esi
8010250e:	53                   	push   %ebx
  ioapic->reg = reg;
8010250f:	c7 05 00 00 c0 fe 01 	movl   $0x1,0xfec00000
80102516:	00 00 00 
  return ioapic->data;
80102519:	a1 34 26 11 80       	mov    0x80112634,%eax
8010251e:	8b 58 10             	mov    0x10(%eax),%ebx
  ioapic->reg = reg;
80102521:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
  return ioapic->data;
80102527:	8b 0d 34 26 11 80    	mov    0x80112634,%ecx
  maxintr = (ioapicread(REG_VER) >> 16) & 0xFF;
  id = ioapicread(REG_ID) >> 24;
  if(id != ioapicid)
8010252d:	0f b6 15 60 27 11 80 	movzbl 0x80112760,%edx
  maxintr = (ioapicread(REG_VER) >> 16) & 0xFF;
80102534:	c1 eb 10             	shr    $0x10,%ebx
  return ioapic->data;
80102537:	8b 41 10             	mov    0x10(%ecx),%eax
  maxintr = (ioapicread(REG_VER) >> 16) & 0xFF;
8010253a:	0f b6 db             	movzbl %bl,%ebx
  id = ioapicread(REG_ID) >> 24;
8010253d:	c1 e8 18             	shr    $0x18,%eax
  if(id != ioapicid)
80102540:	39 c2                	cmp    %eax,%edx
80102542:	74 16                	je     8010255a <ioapicinit+0x5a>
    cprintf("ioapicinit: id isn't equal to ioapicid; not a MP\n");
80102544:	83 ec 0c             	sub    $0xc,%esp
80102547:	68 d4 73 10 80       	push   $0x801073d4
8010254c:	e8 3f e2 ff ff       	call   80100790 <cprintf>
80102551:	8b 0d 34 26 11 80    	mov    0x80112634,%ecx
80102557:	83 c4 10             	add    $0x10,%esp
8010255a:	83 c3 21             	add    $0x21,%ebx
{
8010255d:	ba 10 00 00 00       	mov    $0x10,%edx
80102562:	b8 20 00 00 00       	mov    $0x20,%eax
80102567:	89 f6                	mov    %esi,%esi
80102569:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
  ioapic->reg = reg;
80102570:	89 11                	mov    %edx,(%ecx)
  ioapic->data = data;
80102572:	8b 0d 34 26 11 80    	mov    0x80112634,%ecx

  // Mark all interrupts edge-triggered, active high, disabled,
  // and not routed to any CPUs.
  for(i = 0; i <= maxintr; i++){
    ioapicwrite(REG_TABLE+2*i, INT_DISABLED | (T_IRQ0 + i));
80102578:	89 c6                	mov    %eax,%esi
8010257a:	81 ce 00 00 01 00    	or     $0x10000,%esi
80102580:	83 c0 01             	add    $0x1,%eax
  ioapic->data = data;
80102583:	89 71 10             	mov    %esi,0x10(%ecx)
80102586:	8d 72 01             	lea    0x1(%edx),%esi
80102589:	83 c2 02             	add    $0x2,%edx
  for(i = 0; i <= maxintr; i++){
8010258c:	39 d8                	cmp    %ebx,%eax
  ioapic->reg = reg;
8010258e:	89 31                	mov    %esi,(%ecx)
  ioapic->data = data;
80102590:	8b 0d 34 26 11 80    	mov    0x80112634,%ecx
80102596:	c7 41 10 00 00 00 00 	movl   $0x0,0x10(%ecx)
  for(i = 0; i <= maxintr; i++){
8010259d:	75 d1                	jne    80102570 <ioapicinit+0x70>
    ioapicwrite(REG_TABLE+2*i+1, 0);
  }
}
8010259f:	8d 65 f8             	lea    -0x8(%ebp),%esp
801025a2:	5b                   	pop    %ebx
801025a3:	5e                   	pop    %esi
801025a4:	5d                   	pop    %ebp
801025a5:	c3                   	ret    
801025a6:	8d 76 00             	lea    0x0(%esi),%esi
801025a9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

801025b0 <ioapicenable>:

void
ioapicenable(int irq, int cpunum)
{
801025b0:	55                   	push   %ebp
  ioapic->reg = reg;
801025b1:	8b 0d 34 26 11 80    	mov    0x80112634,%ecx
{
801025b7:	89 e5                	mov    %esp,%ebp
801025b9:	8b 45 08             	mov    0x8(%ebp),%eax
  // Mark interrupt edge-triggered, active high,
  // enabled, and routed to the given cpunum,
  // which happens to be that cpu's APIC ID.
  ioapicwrite(REG_TABLE+2*irq, T_IRQ0 + irq);
801025bc:	8d 50 20             	lea    0x20(%eax),%edx
801025bf:	8d 44 00 10          	lea    0x10(%eax,%eax,1),%eax
  ioapic->reg = reg;
801025c3:	89 01                	mov    %eax,(%ecx)
  ioapic->data = data;
801025c5:	8b 0d 34 26 11 80    	mov    0x80112634,%ecx
  ioapicwrite(REG_TABLE+2*irq+1, cpunum << 24);
801025cb:	83 c0 01             	add    $0x1,%eax
  ioapic->data = data;
801025ce:	89 51 10             	mov    %edx,0x10(%ecx)
  ioapicwrite(REG_TABLE+2*irq+1, cpunum << 24);
801025d1:	8b 55 0c             	mov    0xc(%ebp),%edx
  ioapic->reg = reg;
801025d4:	89 01                	mov    %eax,(%ecx)
  ioapic->data = data;
801025d6:	a1 34 26 11 80       	mov    0x80112634,%eax
  ioapicwrite(REG_TABLE+2*irq+1, cpunum << 24);
801025db:	c1 e2 18             	shl    $0x18,%edx
  ioapic->data = data;
801025de:	89 50 10             	mov    %edx,0x10(%eax)
}
801025e1:	5d                   	pop    %ebp
801025e2:	c3                   	ret    
801025e3:	66 90                	xchg   %ax,%ax
801025e5:	66 90                	xchg   %ax,%ax
801025e7:	66 90                	xchg   %ax,%ax
801025e9:	66 90                	xchg   %ax,%ax
801025eb:	66 90                	xchg   %ax,%ax
801025ed:	66 90                	xchg   %ax,%ax
801025ef:	90                   	nop

801025f0 <kfree>:
// which normally should have been returned by a
// call to kalloc().  (The exception is when
// initializing the allocator; see kinit above.)
void
kfree(char *v)
{
801025f0:	55                   	push   %ebp
801025f1:	89 e5                	mov    %esp,%ebp
801025f3:	53                   	push   %ebx
801025f4:	83 ec 04             	sub    $0x4,%esp
801025f7:	8b 5d 08             	mov    0x8(%ebp),%ebx
  struct run *r;

  if((uint)v % PGSIZE || v < end || V2P(v) >= PHYSTOP)
801025fa:	f7 c3 ff 0f 00 00    	test   $0xfff,%ebx
80102600:	75 70                	jne    80102672 <kfree+0x82>
80102602:	81 fb a8 54 11 80    	cmp    $0x801154a8,%ebx
80102608:	72 68                	jb     80102672 <kfree+0x82>
8010260a:	8d 83 00 00 00 80    	lea    -0x80000000(%ebx),%eax
80102610:	3d ff ff ff 0d       	cmp    $0xdffffff,%eax
80102615:	77 5b                	ja     80102672 <kfree+0x82>
    panic("kfree");

  // Fill with junk to catch dangling refs.
  memset(v, 1, PGSIZE);
80102617:	83 ec 04             	sub    $0x4,%esp
8010261a:	68 00 10 00 00       	push   $0x1000
8010261f:	6a 01                	push   $0x1
80102621:	53                   	push   %ebx
80102622:	e8 09 21 00 00       	call   80104730 <memset>

  if(kmem.use_lock)
80102627:	8b 15 74 26 11 80    	mov    0x80112674,%edx
8010262d:	83 c4 10             	add    $0x10,%esp
80102630:	85 d2                	test   %edx,%edx
80102632:	75 2c                	jne    80102660 <kfree+0x70>
    acquire(&kmem.lock);
  r = (struct run*)v;
  r->next = kmem.freelist;
80102634:	a1 78 26 11 80       	mov    0x80112678,%eax
80102639:	89 03                	mov    %eax,(%ebx)
  kmem.freelist = r;
  if(kmem.use_lock)
8010263b:	a1 74 26 11 80       	mov    0x80112674,%eax
  kmem.freelist = r;
80102640:	89 1d 78 26 11 80    	mov    %ebx,0x80112678
  if(kmem.use_lock)
80102646:	85 c0                	test   %eax,%eax
80102648:	75 06                	jne    80102650 <kfree+0x60>
    release(&kmem.lock);
}
8010264a:	8b 5d fc             	mov    -0x4(%ebp),%ebx
8010264d:	c9                   	leave  
8010264e:	c3                   	ret    
8010264f:	90                   	nop
    release(&kmem.lock);
80102650:	c7 45 08 40 26 11 80 	movl   $0x80112640,0x8(%ebp)
}
80102657:	8b 5d fc             	mov    -0x4(%ebp),%ebx
8010265a:	c9                   	leave  
    release(&kmem.lock);
8010265b:	e9 80 20 00 00       	jmp    801046e0 <release>
    acquire(&kmem.lock);
80102660:	83 ec 0c             	sub    $0xc,%esp
80102663:	68 40 26 11 80       	push   $0x80112640
80102668:	e8 b3 1f 00 00       	call   80104620 <acquire>
8010266d:	83 c4 10             	add    $0x10,%esp
80102670:	eb c2                	jmp    80102634 <kfree+0x44>
    panic("kfree");
80102672:	83 ec 0c             	sub    $0xc,%esp
80102675:	68 06 74 10 80       	push   $0x80107406
8010267a:	e8 11 dd ff ff       	call   80100390 <panic>
8010267f:	90                   	nop

80102680 <freerange>:
{
80102680:	55                   	push   %ebp
80102681:	89 e5                	mov    %esp,%ebp
80102683:	56                   	push   %esi
80102684:	53                   	push   %ebx
  p = (char*)PGROUNDUP((uint)vstart);
80102685:	8b 45 08             	mov    0x8(%ebp),%eax
{
80102688:	8b 75 0c             	mov    0xc(%ebp),%esi
  p = (char*)PGROUNDUP((uint)vstart);
8010268b:	8d 98 ff 0f 00 00    	lea    0xfff(%eax),%ebx
80102691:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
80102697:	81 c3 00 10 00 00    	add    $0x1000,%ebx
8010269d:	39 de                	cmp    %ebx,%esi
8010269f:	72 23                	jb     801026c4 <freerange+0x44>
801026a1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    kfree(p);
801026a8:	8d 83 00 f0 ff ff    	lea    -0x1000(%ebx),%eax
801026ae:	83 ec 0c             	sub    $0xc,%esp
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
801026b1:	81 c3 00 10 00 00    	add    $0x1000,%ebx
    kfree(p);
801026b7:	50                   	push   %eax
801026b8:	e8 33 ff ff ff       	call   801025f0 <kfree>
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
801026bd:	83 c4 10             	add    $0x10,%esp
801026c0:	39 f3                	cmp    %esi,%ebx
801026c2:	76 e4                	jbe    801026a8 <freerange+0x28>
}
801026c4:	8d 65 f8             	lea    -0x8(%ebp),%esp
801026c7:	5b                   	pop    %ebx
801026c8:	5e                   	pop    %esi
801026c9:	5d                   	pop    %ebp
801026ca:	c3                   	ret    
801026cb:	90                   	nop
801026cc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

801026d0 <kinit1>:
{
801026d0:	55                   	push   %ebp
801026d1:	89 e5                	mov    %esp,%ebp
801026d3:	56                   	push   %esi
801026d4:	53                   	push   %ebx
801026d5:	8b 75 0c             	mov    0xc(%ebp),%esi
  initlock(&kmem.lock, "kmem");
801026d8:	83 ec 08             	sub    $0x8,%esp
801026db:	68 0c 74 10 80       	push   $0x8010740c
801026e0:	68 40 26 11 80       	push   $0x80112640
801026e5:	e8 f6 1d 00 00       	call   801044e0 <initlock>
  p = (char*)PGROUNDUP((uint)vstart);
801026ea:	8b 45 08             	mov    0x8(%ebp),%eax
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
801026ed:	83 c4 10             	add    $0x10,%esp
  kmem.use_lock = 0;
801026f0:	c7 05 74 26 11 80 00 	movl   $0x0,0x80112674
801026f7:	00 00 00 
  p = (char*)PGROUNDUP((uint)vstart);
801026fa:	8d 98 ff 0f 00 00    	lea    0xfff(%eax),%ebx
80102700:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
80102706:	81 c3 00 10 00 00    	add    $0x1000,%ebx
8010270c:	39 de                	cmp    %ebx,%esi
8010270e:	72 1c                	jb     8010272c <kinit1+0x5c>
    kfree(p);
80102710:	8d 83 00 f0 ff ff    	lea    -0x1000(%ebx),%eax
80102716:	83 ec 0c             	sub    $0xc,%esp
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
80102719:	81 c3 00 10 00 00    	add    $0x1000,%ebx
    kfree(p);
8010271f:	50                   	push   %eax
80102720:	e8 cb fe ff ff       	call   801025f0 <kfree>
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
80102725:	83 c4 10             	add    $0x10,%esp
80102728:	39 de                	cmp    %ebx,%esi
8010272a:	73 e4                	jae    80102710 <kinit1+0x40>
}
8010272c:	8d 65 f8             	lea    -0x8(%ebp),%esp
8010272f:	5b                   	pop    %ebx
80102730:	5e                   	pop    %esi
80102731:	5d                   	pop    %ebp
80102732:	c3                   	ret    
80102733:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80102739:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80102740 <kinit2>:
{
80102740:	55                   	push   %ebp
80102741:	89 e5                	mov    %esp,%ebp
80102743:	56                   	push   %esi
80102744:	53                   	push   %ebx
  p = (char*)PGROUNDUP((uint)vstart);
80102745:	8b 45 08             	mov    0x8(%ebp),%eax
{
80102748:	8b 75 0c             	mov    0xc(%ebp),%esi
  p = (char*)PGROUNDUP((uint)vstart);
8010274b:	8d 98 ff 0f 00 00    	lea    0xfff(%eax),%ebx
80102751:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
80102757:	81 c3 00 10 00 00    	add    $0x1000,%ebx
8010275d:	39 de                	cmp    %ebx,%esi
8010275f:	72 23                	jb     80102784 <kinit2+0x44>
80102761:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    kfree(p);
80102768:	8d 83 00 f0 ff ff    	lea    -0x1000(%ebx),%eax
8010276e:	83 ec 0c             	sub    $0xc,%esp
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
80102771:	81 c3 00 10 00 00    	add    $0x1000,%ebx
    kfree(p);
80102777:	50                   	push   %eax
80102778:	e8 73 fe ff ff       	call   801025f0 <kfree>
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
8010277d:	83 c4 10             	add    $0x10,%esp
80102780:	39 de                	cmp    %ebx,%esi
80102782:	73 e4                	jae    80102768 <kinit2+0x28>
  kmem.use_lock = 1;
80102784:	c7 05 74 26 11 80 01 	movl   $0x1,0x80112674
8010278b:	00 00 00 
}
8010278e:	8d 65 f8             	lea    -0x8(%ebp),%esp
80102791:	5b                   	pop    %ebx
80102792:	5e                   	pop    %esi
80102793:	5d                   	pop    %ebp
80102794:	c3                   	ret    
80102795:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80102799:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

801027a0 <kalloc>:
char*
kalloc(void)
{
  struct run *r;

  if(kmem.use_lock)
801027a0:	a1 74 26 11 80       	mov    0x80112674,%eax
801027a5:	85 c0                	test   %eax,%eax
801027a7:	75 1f                	jne    801027c8 <kalloc+0x28>
    acquire(&kmem.lock);
  r = kmem.freelist;
801027a9:	a1 78 26 11 80       	mov    0x80112678,%eax
  if(r)
801027ae:	85 c0                	test   %eax,%eax
801027b0:	74 0e                	je     801027c0 <kalloc+0x20>
    kmem.freelist = r->next;
801027b2:	8b 10                	mov    (%eax),%edx
801027b4:	89 15 78 26 11 80    	mov    %edx,0x80112678
801027ba:	c3                   	ret    
801027bb:	90                   	nop
801027bc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  if(kmem.use_lock)
    release(&kmem.lock);
  return (char*)r;
}
801027c0:	f3 c3                	repz ret 
801027c2:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
{
801027c8:	55                   	push   %ebp
801027c9:	89 e5                	mov    %esp,%ebp
801027cb:	83 ec 24             	sub    $0x24,%esp
    acquire(&kmem.lock);
801027ce:	68 40 26 11 80       	push   $0x80112640
801027d3:	e8 48 1e 00 00       	call   80104620 <acquire>
  r = kmem.freelist;
801027d8:	a1 78 26 11 80       	mov    0x80112678,%eax
  if(r)
801027dd:	83 c4 10             	add    $0x10,%esp
801027e0:	8b 15 74 26 11 80    	mov    0x80112674,%edx
801027e6:	85 c0                	test   %eax,%eax
801027e8:	74 08                	je     801027f2 <kalloc+0x52>
    kmem.freelist = r->next;
801027ea:	8b 08                	mov    (%eax),%ecx
801027ec:	89 0d 78 26 11 80    	mov    %ecx,0x80112678
  if(kmem.use_lock)
801027f2:	85 d2                	test   %edx,%edx
801027f4:	74 16                	je     8010280c <kalloc+0x6c>
    release(&kmem.lock);
801027f6:	83 ec 0c             	sub    $0xc,%esp
801027f9:	89 45 f4             	mov    %eax,-0xc(%ebp)
801027fc:	68 40 26 11 80       	push   $0x80112640
80102801:	e8 da 1e 00 00       	call   801046e0 <release>
  return (char*)r;
80102806:	8b 45 f4             	mov    -0xc(%ebp),%eax
    release(&kmem.lock);
80102809:	83 c4 10             	add    $0x10,%esp
}
8010280c:	c9                   	leave  
8010280d:	c3                   	ret    
8010280e:	66 90                	xchg   %ax,%ax

80102810 <kbdgetc>:
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102810:	ba 64 00 00 00       	mov    $0x64,%edx
80102815:	ec                   	in     (%dx),%al
    normalmap, shiftmap, ctlmap, ctlmap
  };
  uint st, data, c;

  st = inb(KBSTATP);
  if((st & KBS_DIB) == 0)
80102816:	a8 01                	test   $0x1,%al
80102818:	0f 84 c2 00 00 00    	je     801028e0 <kbdgetc+0xd0>
8010281e:	ba 60 00 00 00       	mov    $0x60,%edx
80102823:	ec                   	in     (%dx),%al
    return -1;
  data = inb(KBDATAP);
80102824:	0f b6 d0             	movzbl %al,%edx
80102827:	8b 0d b4 a5 10 80    	mov    0x8010a5b4,%ecx

  if(data == 0xE0){
8010282d:	81 fa e0 00 00 00    	cmp    $0xe0,%edx
80102833:	0f 84 7f 00 00 00    	je     801028b8 <kbdgetc+0xa8>
{
80102839:	55                   	push   %ebp
8010283a:	89 e5                	mov    %esp,%ebp
8010283c:	53                   	push   %ebx
8010283d:	89 cb                	mov    %ecx,%ebx
8010283f:	83 e3 40             	and    $0x40,%ebx
    shift |= E0ESC;
    return 0;
  } else if(data & 0x80){
80102842:	84 c0                	test   %al,%al
80102844:	78 4a                	js     80102890 <kbdgetc+0x80>
    // Key released
    data = (shift & E0ESC ? data : data & 0x7F);
    shift &= ~(shiftcode[data] | E0ESC);
    return 0;
  } else if(shift & E0ESC){
80102846:	85 db                	test   %ebx,%ebx
80102848:	74 09                	je     80102853 <kbdgetc+0x43>
    // Last character was an E0 escape; or with 0x80
    data |= 0x80;
8010284a:	83 c8 80             	or     $0xffffff80,%eax
    shift &= ~E0ESC;
8010284d:	83 e1 bf             	and    $0xffffffbf,%ecx
    data |= 0x80;
80102850:	0f b6 d0             	movzbl %al,%edx
  }

  shift |= shiftcode[data];
80102853:	0f b6 82 40 75 10 80 	movzbl -0x7fef8ac0(%edx),%eax
8010285a:	09 c1                	or     %eax,%ecx
  shift ^= togglecode[data];
8010285c:	0f b6 82 40 74 10 80 	movzbl -0x7fef8bc0(%edx),%eax
80102863:	31 c1                	xor    %eax,%ecx
  c = charcode[shift & (CTL | SHIFT)][data];
80102865:	89 c8                	mov    %ecx,%eax
  shift ^= togglecode[data];
80102867:	89 0d b4 a5 10 80    	mov    %ecx,0x8010a5b4
  c = charcode[shift & (CTL | SHIFT)][data];
8010286d:	83 e0 03             	and    $0x3,%eax
  if(shift & CAPSLOCK){
80102870:	83 e1 08             	and    $0x8,%ecx
  c = charcode[shift & (CTL | SHIFT)][data];
80102873:	8b 04 85 20 74 10 80 	mov    -0x7fef8be0(,%eax,4),%eax
8010287a:	0f b6 04 10          	movzbl (%eax,%edx,1),%eax
  if(shift & CAPSLOCK){
8010287e:	74 31                	je     801028b1 <kbdgetc+0xa1>
    if('a' <= c && c <= 'z')
80102880:	8d 50 9f             	lea    -0x61(%eax),%edx
80102883:	83 fa 19             	cmp    $0x19,%edx
80102886:	77 40                	ja     801028c8 <kbdgetc+0xb8>
      c += 'A' - 'a';
80102888:	83 e8 20             	sub    $0x20,%eax
    else if('A' <= c && c <= 'Z')
      c += 'a' - 'A';
  }
  return c;
}
8010288b:	5b                   	pop    %ebx
8010288c:	5d                   	pop    %ebp
8010288d:	c3                   	ret    
8010288e:	66 90                	xchg   %ax,%ax
    data = (shift & E0ESC ? data : data & 0x7F);
80102890:	83 e0 7f             	and    $0x7f,%eax
80102893:	85 db                	test   %ebx,%ebx
80102895:	0f 44 d0             	cmove  %eax,%edx
    shift &= ~(shiftcode[data] | E0ESC);
80102898:	0f b6 82 40 75 10 80 	movzbl -0x7fef8ac0(%edx),%eax
8010289f:	83 c8 40             	or     $0x40,%eax
801028a2:	0f b6 c0             	movzbl %al,%eax
801028a5:	f7 d0                	not    %eax
801028a7:	21 c1                	and    %eax,%ecx
    return 0;
801028a9:	31 c0                	xor    %eax,%eax
    shift &= ~(shiftcode[data] | E0ESC);
801028ab:	89 0d b4 a5 10 80    	mov    %ecx,0x8010a5b4
}
801028b1:	5b                   	pop    %ebx
801028b2:	5d                   	pop    %ebp
801028b3:	c3                   	ret    
801028b4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    shift |= E0ESC;
801028b8:	83 c9 40             	or     $0x40,%ecx
    return 0;
801028bb:	31 c0                	xor    %eax,%eax
    shift |= E0ESC;
801028bd:	89 0d b4 a5 10 80    	mov    %ecx,0x8010a5b4
    return 0;
801028c3:	c3                   	ret    
801028c4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    else if('A' <= c && c <= 'Z')
801028c8:	8d 48 bf             	lea    -0x41(%eax),%ecx
      c += 'a' - 'A';
801028cb:	8d 50 20             	lea    0x20(%eax),%edx
}
801028ce:	5b                   	pop    %ebx
      c += 'a' - 'A';
801028cf:	83 f9 1a             	cmp    $0x1a,%ecx
801028d2:	0f 42 c2             	cmovb  %edx,%eax
}
801028d5:	5d                   	pop    %ebp
801028d6:	c3                   	ret    
801028d7:	89 f6                	mov    %esi,%esi
801028d9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
    return -1;
801028e0:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
801028e5:	c3                   	ret    
801028e6:	8d 76 00             	lea    0x0(%esi),%esi
801028e9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

801028f0 <kbdintr>:

void
kbdintr(void)
{
801028f0:	55                   	push   %ebp
801028f1:	89 e5                	mov    %esp,%ebp
801028f3:	83 ec 14             	sub    $0x14,%esp
  consoleintr(kbdgetc);
801028f6:	68 10 28 10 80       	push   $0x80102810
801028fb:	e8 40 e0 ff ff       	call   80100940 <consoleintr>
}
80102900:	83 c4 10             	add    $0x10,%esp
80102903:	c9                   	leave  
80102904:	c3                   	ret    
80102905:	66 90                	xchg   %ax,%ax
80102907:	66 90                	xchg   %ax,%ax
80102909:	66 90                	xchg   %ax,%ax
8010290b:	66 90                	xchg   %ax,%ax
8010290d:	66 90                	xchg   %ax,%ax
8010290f:	90                   	nop

80102910 <lapicinit>:
}

void
lapicinit(void)
{
  if(!lapic)
80102910:	a1 7c 26 11 80       	mov    0x8011267c,%eax
{
80102915:	55                   	push   %ebp
80102916:	89 e5                	mov    %esp,%ebp
  if(!lapic)
80102918:	85 c0                	test   %eax,%eax
8010291a:	0f 84 c8 00 00 00    	je     801029e8 <lapicinit+0xd8>
  lapic[index] = value;
80102920:	c7 80 f0 00 00 00 3f 	movl   $0x13f,0xf0(%eax)
80102927:	01 00 00 
  lapic[ID];  // wait for write to finish, by reading
8010292a:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
8010292d:	c7 80 e0 03 00 00 0b 	movl   $0xb,0x3e0(%eax)
80102934:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
80102937:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
8010293a:	c7 80 20 03 00 00 20 	movl   $0x20020,0x320(%eax)
80102941:	00 02 00 
  lapic[ID];  // wait for write to finish, by reading
80102944:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
80102947:	c7 80 80 03 00 00 80 	movl   $0x989680,0x380(%eax)
8010294e:	96 98 00 
  lapic[ID];  // wait for write to finish, by reading
80102951:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
80102954:	c7 80 50 03 00 00 00 	movl   $0x10000,0x350(%eax)
8010295b:	00 01 00 
  lapic[ID];  // wait for write to finish, by reading
8010295e:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
80102961:	c7 80 60 03 00 00 00 	movl   $0x10000,0x360(%eax)
80102968:	00 01 00 
  lapic[ID];  // wait for write to finish, by reading
8010296b:	8b 50 20             	mov    0x20(%eax),%edx
  lapicw(LINT0, MASKED);
  lapicw(LINT1, MASKED);

  // Disable performance counter overflow interrupts
  // on machines that provide that interrupt entry.
  if(((lapic[VER]>>16) & 0xFF) >= 4)
8010296e:	8b 50 30             	mov    0x30(%eax),%edx
80102971:	c1 ea 10             	shr    $0x10,%edx
80102974:	80 fa 03             	cmp    $0x3,%dl
80102977:	77 77                	ja     801029f0 <lapicinit+0xe0>
  lapic[index] = value;
80102979:	c7 80 70 03 00 00 33 	movl   $0x33,0x370(%eax)
80102980:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
80102983:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
80102986:	c7 80 80 02 00 00 00 	movl   $0x0,0x280(%eax)
8010298d:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
80102990:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
80102993:	c7 80 80 02 00 00 00 	movl   $0x0,0x280(%eax)
8010299a:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
8010299d:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
801029a0:	c7 80 b0 00 00 00 00 	movl   $0x0,0xb0(%eax)
801029a7:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
801029aa:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
801029ad:	c7 80 10 03 00 00 00 	movl   $0x0,0x310(%eax)
801029b4:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
801029b7:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
801029ba:	c7 80 00 03 00 00 00 	movl   $0x88500,0x300(%eax)
801029c1:	85 08 00 
  lapic[ID];  // wait for write to finish, by reading
801029c4:	8b 50 20             	mov    0x20(%eax),%edx
801029c7:	89 f6                	mov    %esi,%esi
801029c9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
  lapicw(EOI, 0);

  // Send an Init Level De-Assert to synchronise arbitration ID's.
  lapicw(ICRHI, 0);
  lapicw(ICRLO, BCAST | INIT | LEVEL);
  while(lapic[ICRLO] & DELIVS)
801029d0:	8b 90 00 03 00 00    	mov    0x300(%eax),%edx
801029d6:	80 e6 10             	and    $0x10,%dh
801029d9:	75 f5                	jne    801029d0 <lapicinit+0xc0>
  lapic[index] = value;
801029db:	c7 80 80 00 00 00 00 	movl   $0x0,0x80(%eax)
801029e2:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
801029e5:	8b 40 20             	mov    0x20(%eax),%eax
    ;

  // Enable interrupts on the APIC (but not on the processor).
  lapicw(TPR, 0);
}
801029e8:	5d                   	pop    %ebp
801029e9:	c3                   	ret    
801029ea:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  lapic[index] = value;
801029f0:	c7 80 40 03 00 00 00 	movl   $0x10000,0x340(%eax)
801029f7:	00 01 00 
  lapic[ID];  // wait for write to finish, by reading
801029fa:	8b 50 20             	mov    0x20(%eax),%edx
801029fd:	e9 77 ff ff ff       	jmp    80102979 <lapicinit+0x69>
80102a02:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80102a09:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80102a10 <lapicid>:

int
lapicid(void)
{
  if (!lapic)
80102a10:	8b 15 7c 26 11 80    	mov    0x8011267c,%edx
{
80102a16:	55                   	push   %ebp
80102a17:	31 c0                	xor    %eax,%eax
80102a19:	89 e5                	mov    %esp,%ebp
  if (!lapic)
80102a1b:	85 d2                	test   %edx,%edx
80102a1d:	74 06                	je     80102a25 <lapicid+0x15>
    return 0;
  return lapic[ID] >> 24;
80102a1f:	8b 42 20             	mov    0x20(%edx),%eax
80102a22:	c1 e8 18             	shr    $0x18,%eax
}
80102a25:	5d                   	pop    %ebp
80102a26:	c3                   	ret    
80102a27:	89 f6                	mov    %esi,%esi
80102a29:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80102a30 <lapiceoi>:

// Acknowledge interrupt.
void
lapiceoi(void)
{
  if(lapic)
80102a30:	a1 7c 26 11 80       	mov    0x8011267c,%eax
{
80102a35:	55                   	push   %ebp
80102a36:	89 e5                	mov    %esp,%ebp
  if(lapic)
80102a38:	85 c0                	test   %eax,%eax
80102a3a:	74 0d                	je     80102a49 <lapiceoi+0x19>
  lapic[index] = value;
80102a3c:	c7 80 b0 00 00 00 00 	movl   $0x0,0xb0(%eax)
80102a43:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
80102a46:	8b 40 20             	mov    0x20(%eax),%eax
    lapicw(EOI, 0);
}
80102a49:	5d                   	pop    %ebp
80102a4a:	c3                   	ret    
80102a4b:	90                   	nop
80102a4c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80102a50 <microdelay>:

// Spin for a given number of microseconds.
// On real hardware would want to tune this dynamically.
void
microdelay(int us)
{
80102a50:	55                   	push   %ebp
80102a51:	89 e5                	mov    %esp,%ebp
}
80102a53:	5d                   	pop    %ebp
80102a54:	c3                   	ret    
80102a55:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80102a59:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80102a60 <lapicstartap>:

// Start additional processor running entry code at addr.
// See Appendix B of MultiProcessor Specification.
void
lapicstartap(uchar apicid, uint addr)
{
80102a60:	55                   	push   %ebp
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102a61:	b8 0f 00 00 00       	mov    $0xf,%eax
80102a66:	ba 70 00 00 00       	mov    $0x70,%edx
80102a6b:	89 e5                	mov    %esp,%ebp
80102a6d:	53                   	push   %ebx
80102a6e:	8b 4d 0c             	mov    0xc(%ebp),%ecx
80102a71:	8b 5d 08             	mov    0x8(%ebp),%ebx
80102a74:	ee                   	out    %al,(%dx)
80102a75:	b8 0a 00 00 00       	mov    $0xa,%eax
80102a7a:	ba 71 00 00 00       	mov    $0x71,%edx
80102a7f:	ee                   	out    %al,(%dx)
  // and the warm reset vector (DWORD based at 40:67) to point at
  // the AP startup code prior to the [universal startup algorithm]."
  outb(CMOS_PORT, 0xF);  // offset 0xF is shutdown code
  outb(CMOS_PORT+1, 0x0A);
  wrv = (ushort*)P2V((0x40<<4 | 0x67));  // Warm reset vector
  wrv[0] = 0;
80102a80:	31 c0                	xor    %eax,%eax
  wrv[1] = addr >> 4;

  // "Universal startup algorithm."
  // Send INIT (level-triggered) interrupt to reset other CPU.
  lapicw(ICRHI, apicid<<24);
80102a82:	c1 e3 18             	shl    $0x18,%ebx
  wrv[0] = 0;
80102a85:	66 a3 67 04 00 80    	mov    %ax,0x80000467
  wrv[1] = addr >> 4;
80102a8b:	89 c8                	mov    %ecx,%eax
  // when it is in the halted state due to an INIT.  So the second
  // should be ignored, but it is part of the official Intel algorithm.
  // Bochs complains about the second one.  Too bad for Bochs.
  for(i = 0; i < 2; i++){
    lapicw(ICRHI, apicid<<24);
    lapicw(ICRLO, STARTUP | (addr>>12));
80102a8d:	c1 e9 0c             	shr    $0xc,%ecx
  wrv[1] = addr >> 4;
80102a90:	c1 e8 04             	shr    $0x4,%eax
  lapicw(ICRHI, apicid<<24);
80102a93:	89 da                	mov    %ebx,%edx
    lapicw(ICRLO, STARTUP | (addr>>12));
80102a95:	80 cd 06             	or     $0x6,%ch
  wrv[1] = addr >> 4;
80102a98:	66 a3 69 04 00 80    	mov    %ax,0x80000469
  lapic[index] = value;
80102a9e:	a1 7c 26 11 80       	mov    0x8011267c,%eax
80102aa3:	89 98 10 03 00 00    	mov    %ebx,0x310(%eax)
  lapic[ID];  // wait for write to finish, by reading
80102aa9:	8b 58 20             	mov    0x20(%eax),%ebx
  lapic[index] = value;
80102aac:	c7 80 00 03 00 00 00 	movl   $0xc500,0x300(%eax)
80102ab3:	c5 00 00 
  lapic[ID];  // wait for write to finish, by reading
80102ab6:	8b 58 20             	mov    0x20(%eax),%ebx
  lapic[index] = value;
80102ab9:	c7 80 00 03 00 00 00 	movl   $0x8500,0x300(%eax)
80102ac0:	85 00 00 
  lapic[ID];  // wait for write to finish, by reading
80102ac3:	8b 58 20             	mov    0x20(%eax),%ebx
  lapic[index] = value;
80102ac6:	89 90 10 03 00 00    	mov    %edx,0x310(%eax)
  lapic[ID];  // wait for write to finish, by reading
80102acc:	8b 58 20             	mov    0x20(%eax),%ebx
  lapic[index] = value;
80102acf:	89 88 00 03 00 00    	mov    %ecx,0x300(%eax)
  lapic[ID];  // wait for write to finish, by reading
80102ad5:	8b 58 20             	mov    0x20(%eax),%ebx
  lapic[index] = value;
80102ad8:	89 90 10 03 00 00    	mov    %edx,0x310(%eax)
  lapic[ID];  // wait for write to finish, by reading
80102ade:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
80102ae1:	89 88 00 03 00 00    	mov    %ecx,0x300(%eax)
  lapic[ID];  // wait for write to finish, by reading
80102ae7:	8b 40 20             	mov    0x20(%eax),%eax
    microdelay(200);
  }
}
80102aea:	5b                   	pop    %ebx
80102aeb:	5d                   	pop    %ebp
80102aec:	c3                   	ret    
80102aed:	8d 76 00             	lea    0x0(%esi),%esi

80102af0 <cmostime>:
}

// qemu seems to use 24-hour GWT and the values are BCD encoded
void
cmostime(struct rtcdate *r)
{
80102af0:	55                   	push   %ebp
80102af1:	b8 0b 00 00 00       	mov    $0xb,%eax
80102af6:	ba 70 00 00 00       	mov    $0x70,%edx
80102afb:	89 e5                	mov    %esp,%ebp
80102afd:	57                   	push   %edi
80102afe:	56                   	push   %esi
80102aff:	53                   	push   %ebx
80102b00:	83 ec 4c             	sub    $0x4c,%esp
80102b03:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102b04:	ba 71 00 00 00       	mov    $0x71,%edx
80102b09:	ec                   	in     (%dx),%al
80102b0a:	83 e0 04             	and    $0x4,%eax
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102b0d:	bb 70 00 00 00       	mov    $0x70,%ebx
80102b12:	88 45 b3             	mov    %al,-0x4d(%ebp)
80102b15:	8d 76 00             	lea    0x0(%esi),%esi
80102b18:	31 c0                	xor    %eax,%eax
80102b1a:	89 da                	mov    %ebx,%edx
80102b1c:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102b1d:	b9 71 00 00 00       	mov    $0x71,%ecx
80102b22:	89 ca                	mov    %ecx,%edx
80102b24:	ec                   	in     (%dx),%al
80102b25:	88 45 b7             	mov    %al,-0x49(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102b28:	89 da                	mov    %ebx,%edx
80102b2a:	b8 02 00 00 00       	mov    $0x2,%eax
80102b2f:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102b30:	89 ca                	mov    %ecx,%edx
80102b32:	ec                   	in     (%dx),%al
80102b33:	88 45 b6             	mov    %al,-0x4a(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102b36:	89 da                	mov    %ebx,%edx
80102b38:	b8 04 00 00 00       	mov    $0x4,%eax
80102b3d:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102b3e:	89 ca                	mov    %ecx,%edx
80102b40:	ec                   	in     (%dx),%al
80102b41:	88 45 b5             	mov    %al,-0x4b(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102b44:	89 da                	mov    %ebx,%edx
80102b46:	b8 07 00 00 00       	mov    $0x7,%eax
80102b4b:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102b4c:	89 ca                	mov    %ecx,%edx
80102b4e:	ec                   	in     (%dx),%al
80102b4f:	88 45 b4             	mov    %al,-0x4c(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102b52:	89 da                	mov    %ebx,%edx
80102b54:	b8 08 00 00 00       	mov    $0x8,%eax
80102b59:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102b5a:	89 ca                	mov    %ecx,%edx
80102b5c:	ec                   	in     (%dx),%al
80102b5d:	89 c7                	mov    %eax,%edi
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102b5f:	89 da                	mov    %ebx,%edx
80102b61:	b8 09 00 00 00       	mov    $0x9,%eax
80102b66:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102b67:	89 ca                	mov    %ecx,%edx
80102b69:	ec                   	in     (%dx),%al
80102b6a:	89 c6                	mov    %eax,%esi
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102b6c:	89 da                	mov    %ebx,%edx
80102b6e:	b8 0a 00 00 00       	mov    $0xa,%eax
80102b73:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102b74:	89 ca                	mov    %ecx,%edx
80102b76:	ec                   	in     (%dx),%al
  bcd = (sb & (1 << 2)) == 0;

  // make sure CMOS doesn't modify time while we read it
  for(;;) {
    fill_rtcdate(&t1);
    if(cmos_read(CMOS_STATA) & CMOS_UIP)
80102b77:	84 c0                	test   %al,%al
80102b79:	78 9d                	js     80102b18 <cmostime+0x28>
  return inb(CMOS_RETURN);
80102b7b:	0f b6 45 b7          	movzbl -0x49(%ebp),%eax
80102b7f:	89 fa                	mov    %edi,%edx
80102b81:	0f b6 fa             	movzbl %dl,%edi
80102b84:	89 f2                	mov    %esi,%edx
80102b86:	0f b6 f2             	movzbl %dl,%esi
80102b89:	89 7d c8             	mov    %edi,-0x38(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102b8c:	89 da                	mov    %ebx,%edx
80102b8e:	89 75 cc             	mov    %esi,-0x34(%ebp)
80102b91:	89 45 b8             	mov    %eax,-0x48(%ebp)
80102b94:	0f b6 45 b6          	movzbl -0x4a(%ebp),%eax
80102b98:	89 45 bc             	mov    %eax,-0x44(%ebp)
80102b9b:	0f b6 45 b5          	movzbl -0x4b(%ebp),%eax
80102b9f:	89 45 c0             	mov    %eax,-0x40(%ebp)
80102ba2:	0f b6 45 b4          	movzbl -0x4c(%ebp),%eax
80102ba6:	89 45 c4             	mov    %eax,-0x3c(%ebp)
80102ba9:	31 c0                	xor    %eax,%eax
80102bab:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102bac:	89 ca                	mov    %ecx,%edx
80102bae:	ec                   	in     (%dx),%al
80102baf:	0f b6 c0             	movzbl %al,%eax
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102bb2:	89 da                	mov    %ebx,%edx
80102bb4:	89 45 d0             	mov    %eax,-0x30(%ebp)
80102bb7:	b8 02 00 00 00       	mov    $0x2,%eax
80102bbc:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102bbd:	89 ca                	mov    %ecx,%edx
80102bbf:	ec                   	in     (%dx),%al
80102bc0:	0f b6 c0             	movzbl %al,%eax
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102bc3:	89 da                	mov    %ebx,%edx
80102bc5:	89 45 d4             	mov    %eax,-0x2c(%ebp)
80102bc8:	b8 04 00 00 00       	mov    $0x4,%eax
80102bcd:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102bce:	89 ca                	mov    %ecx,%edx
80102bd0:	ec                   	in     (%dx),%al
80102bd1:	0f b6 c0             	movzbl %al,%eax
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102bd4:	89 da                	mov    %ebx,%edx
80102bd6:	89 45 d8             	mov    %eax,-0x28(%ebp)
80102bd9:	b8 07 00 00 00       	mov    $0x7,%eax
80102bde:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102bdf:	89 ca                	mov    %ecx,%edx
80102be1:	ec                   	in     (%dx),%al
80102be2:	0f b6 c0             	movzbl %al,%eax
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102be5:	89 da                	mov    %ebx,%edx
80102be7:	89 45 dc             	mov    %eax,-0x24(%ebp)
80102bea:	b8 08 00 00 00       	mov    $0x8,%eax
80102bef:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102bf0:	89 ca                	mov    %ecx,%edx
80102bf2:	ec                   	in     (%dx),%al
80102bf3:	0f b6 c0             	movzbl %al,%eax
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102bf6:	89 da                	mov    %ebx,%edx
80102bf8:	89 45 e0             	mov    %eax,-0x20(%ebp)
80102bfb:	b8 09 00 00 00       	mov    $0x9,%eax
80102c00:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102c01:	89 ca                	mov    %ecx,%edx
80102c03:	ec                   	in     (%dx),%al
80102c04:	0f b6 c0             	movzbl %al,%eax
        continue;
    fill_rtcdate(&t2);
    if(memcmp(&t1, &t2, sizeof(t1)) == 0)
80102c07:	83 ec 04             	sub    $0x4,%esp
  return inb(CMOS_RETURN);
80102c0a:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    if(memcmp(&t1, &t2, sizeof(t1)) == 0)
80102c0d:	8d 45 d0             	lea    -0x30(%ebp),%eax
80102c10:	6a 18                	push   $0x18
80102c12:	50                   	push   %eax
80102c13:	8d 45 b8             	lea    -0x48(%ebp),%eax
80102c16:	50                   	push   %eax
80102c17:	e8 64 1b 00 00       	call   80104780 <memcmp>
80102c1c:	83 c4 10             	add    $0x10,%esp
80102c1f:	85 c0                	test   %eax,%eax
80102c21:	0f 85 f1 fe ff ff    	jne    80102b18 <cmostime+0x28>
      break;
  }

  // convert
  if(bcd) {
80102c27:	80 7d b3 00          	cmpb   $0x0,-0x4d(%ebp)
80102c2b:	75 78                	jne    80102ca5 <cmostime+0x1b5>
#define    CONV(x)     (t1.x = ((t1.x >> 4) * 10) + (t1.x & 0xf))
    CONV(second);
80102c2d:	8b 45 b8             	mov    -0x48(%ebp),%eax
80102c30:	89 c2                	mov    %eax,%edx
80102c32:	83 e0 0f             	and    $0xf,%eax
80102c35:	c1 ea 04             	shr    $0x4,%edx
80102c38:	8d 14 92             	lea    (%edx,%edx,4),%edx
80102c3b:	8d 04 50             	lea    (%eax,%edx,2),%eax
80102c3e:	89 45 b8             	mov    %eax,-0x48(%ebp)
    CONV(minute);
80102c41:	8b 45 bc             	mov    -0x44(%ebp),%eax
80102c44:	89 c2                	mov    %eax,%edx
80102c46:	83 e0 0f             	and    $0xf,%eax
80102c49:	c1 ea 04             	shr    $0x4,%edx
80102c4c:	8d 14 92             	lea    (%edx,%edx,4),%edx
80102c4f:	8d 04 50             	lea    (%eax,%edx,2),%eax
80102c52:	89 45 bc             	mov    %eax,-0x44(%ebp)
    CONV(hour  );
80102c55:	8b 45 c0             	mov    -0x40(%ebp),%eax
80102c58:	89 c2                	mov    %eax,%edx
80102c5a:	83 e0 0f             	and    $0xf,%eax
80102c5d:	c1 ea 04             	shr    $0x4,%edx
80102c60:	8d 14 92             	lea    (%edx,%edx,4),%edx
80102c63:	8d 04 50             	lea    (%eax,%edx,2),%eax
80102c66:	89 45 c0             	mov    %eax,-0x40(%ebp)
    CONV(day   );
80102c69:	8b 45 c4             	mov    -0x3c(%ebp),%eax
80102c6c:	89 c2                	mov    %eax,%edx
80102c6e:	83 e0 0f             	and    $0xf,%eax
80102c71:	c1 ea 04             	shr    $0x4,%edx
80102c74:	8d 14 92             	lea    (%edx,%edx,4),%edx
80102c77:	8d 04 50             	lea    (%eax,%edx,2),%eax
80102c7a:	89 45 c4             	mov    %eax,-0x3c(%ebp)
    CONV(month );
80102c7d:	8b 45 c8             	mov    -0x38(%ebp),%eax
80102c80:	89 c2                	mov    %eax,%edx
80102c82:	83 e0 0f             	and    $0xf,%eax
80102c85:	c1 ea 04             	shr    $0x4,%edx
80102c88:	8d 14 92             	lea    (%edx,%edx,4),%edx
80102c8b:	8d 04 50             	lea    (%eax,%edx,2),%eax
80102c8e:	89 45 c8             	mov    %eax,-0x38(%ebp)
    CONV(year  );
80102c91:	8b 45 cc             	mov    -0x34(%ebp),%eax
80102c94:	89 c2                	mov    %eax,%edx
80102c96:	83 e0 0f             	and    $0xf,%eax
80102c99:	c1 ea 04             	shr    $0x4,%edx
80102c9c:	8d 14 92             	lea    (%edx,%edx,4),%edx
80102c9f:	8d 04 50             	lea    (%eax,%edx,2),%eax
80102ca2:	89 45 cc             	mov    %eax,-0x34(%ebp)
#undef     CONV
  }

  *r = t1;
80102ca5:	8b 75 08             	mov    0x8(%ebp),%esi
80102ca8:	8b 45 b8             	mov    -0x48(%ebp),%eax
80102cab:	89 06                	mov    %eax,(%esi)
80102cad:	8b 45 bc             	mov    -0x44(%ebp),%eax
80102cb0:	89 46 04             	mov    %eax,0x4(%esi)
80102cb3:	8b 45 c0             	mov    -0x40(%ebp),%eax
80102cb6:	89 46 08             	mov    %eax,0x8(%esi)
80102cb9:	8b 45 c4             	mov    -0x3c(%ebp),%eax
80102cbc:	89 46 0c             	mov    %eax,0xc(%esi)
80102cbf:	8b 45 c8             	mov    -0x38(%ebp),%eax
80102cc2:	89 46 10             	mov    %eax,0x10(%esi)
80102cc5:	8b 45 cc             	mov    -0x34(%ebp),%eax
80102cc8:	89 46 14             	mov    %eax,0x14(%esi)
  r->year += 2000;
80102ccb:	81 46 14 d0 07 00 00 	addl   $0x7d0,0x14(%esi)
}
80102cd2:	8d 65 f4             	lea    -0xc(%ebp),%esp
80102cd5:	5b                   	pop    %ebx
80102cd6:	5e                   	pop    %esi
80102cd7:	5f                   	pop    %edi
80102cd8:	5d                   	pop    %ebp
80102cd9:	c3                   	ret    
80102cda:	66 90                	xchg   %ax,%ax
80102cdc:	66 90                	xchg   %ax,%ax
80102cde:	66 90                	xchg   %ax,%ax

80102ce0 <install_trans>:
static void
install_trans(void)
{
  int tail;

  for (tail = 0; tail < log.lh.n; tail++) {
80102ce0:	8b 0d c8 26 11 80    	mov    0x801126c8,%ecx
80102ce6:	85 c9                	test   %ecx,%ecx
80102ce8:	0f 8e 8a 00 00 00    	jle    80102d78 <install_trans+0x98>
{
80102cee:	55                   	push   %ebp
80102cef:	89 e5                	mov    %esp,%ebp
80102cf1:	57                   	push   %edi
80102cf2:	56                   	push   %esi
80102cf3:	53                   	push   %ebx
  for (tail = 0; tail < log.lh.n; tail++) {
80102cf4:	31 db                	xor    %ebx,%ebx
{
80102cf6:	83 ec 0c             	sub    $0xc,%esp
80102cf9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    struct buf *lbuf = bread(log.dev, log.start+tail+1); // read log block
80102d00:	a1 b4 26 11 80       	mov    0x801126b4,%eax
80102d05:	83 ec 08             	sub    $0x8,%esp
80102d08:	01 d8                	add    %ebx,%eax
80102d0a:	83 c0 01             	add    $0x1,%eax
80102d0d:	50                   	push   %eax
80102d0e:	ff 35 c4 26 11 80    	pushl  0x801126c4
80102d14:	e8 b7 d3 ff ff       	call   801000d0 <bread>
80102d19:	89 c7                	mov    %eax,%edi
    struct buf *dbuf = bread(log.dev, log.lh.block[tail]); // read dst
80102d1b:	58                   	pop    %eax
80102d1c:	5a                   	pop    %edx
80102d1d:	ff 34 9d cc 26 11 80 	pushl  -0x7feed934(,%ebx,4)
80102d24:	ff 35 c4 26 11 80    	pushl  0x801126c4
  for (tail = 0; tail < log.lh.n; tail++) {
80102d2a:	83 c3 01             	add    $0x1,%ebx
    struct buf *dbuf = bread(log.dev, log.lh.block[tail]); // read dst
80102d2d:	e8 9e d3 ff ff       	call   801000d0 <bread>
80102d32:	89 c6                	mov    %eax,%esi
    memmove(dbuf->data, lbuf->data, BSIZE);  // copy block to dst
80102d34:	8d 47 5c             	lea    0x5c(%edi),%eax
80102d37:	83 c4 0c             	add    $0xc,%esp
80102d3a:	68 00 02 00 00       	push   $0x200
80102d3f:	50                   	push   %eax
80102d40:	8d 46 5c             	lea    0x5c(%esi),%eax
80102d43:	50                   	push   %eax
80102d44:	e8 97 1a 00 00       	call   801047e0 <memmove>
    bwrite(dbuf);  // write dst to disk
80102d49:	89 34 24             	mov    %esi,(%esp)
80102d4c:	e8 4f d4 ff ff       	call   801001a0 <bwrite>
    brelse(lbuf);
80102d51:	89 3c 24             	mov    %edi,(%esp)
80102d54:	e8 87 d4 ff ff       	call   801001e0 <brelse>
    brelse(dbuf);
80102d59:	89 34 24             	mov    %esi,(%esp)
80102d5c:	e8 7f d4 ff ff       	call   801001e0 <brelse>
  for (tail = 0; tail < log.lh.n; tail++) {
80102d61:	83 c4 10             	add    $0x10,%esp
80102d64:	39 1d c8 26 11 80    	cmp    %ebx,0x801126c8
80102d6a:	7f 94                	jg     80102d00 <install_trans+0x20>
  }
}
80102d6c:	8d 65 f4             	lea    -0xc(%ebp),%esp
80102d6f:	5b                   	pop    %ebx
80102d70:	5e                   	pop    %esi
80102d71:	5f                   	pop    %edi
80102d72:	5d                   	pop    %ebp
80102d73:	c3                   	ret    
80102d74:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80102d78:	f3 c3                	repz ret 
80102d7a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80102d80 <write_head>:
// Write in-memory log header to disk.
// This is the true point at which the
// current transaction commits.
static void
write_head(void)
{
80102d80:	55                   	push   %ebp
80102d81:	89 e5                	mov    %esp,%ebp
80102d83:	56                   	push   %esi
80102d84:	53                   	push   %ebx
  struct buf *buf = bread(log.dev, log.start);
80102d85:	83 ec 08             	sub    $0x8,%esp
80102d88:	ff 35 b4 26 11 80    	pushl  0x801126b4
80102d8e:	ff 35 c4 26 11 80    	pushl  0x801126c4
80102d94:	e8 37 d3 ff ff       	call   801000d0 <bread>
  struct logheader *hb = (struct logheader *) (buf->data);
  int i;
  hb->n = log.lh.n;
80102d99:	8b 1d c8 26 11 80    	mov    0x801126c8,%ebx
  for (i = 0; i < log.lh.n; i++) {
80102d9f:	83 c4 10             	add    $0x10,%esp
  struct buf *buf = bread(log.dev, log.start);
80102da2:	89 c6                	mov    %eax,%esi
  for (i = 0; i < log.lh.n; i++) {
80102da4:	85 db                	test   %ebx,%ebx
  hb->n = log.lh.n;
80102da6:	89 58 5c             	mov    %ebx,0x5c(%eax)
  for (i = 0; i < log.lh.n; i++) {
80102da9:	7e 16                	jle    80102dc1 <write_head+0x41>
80102dab:	c1 e3 02             	shl    $0x2,%ebx
80102dae:	31 d2                	xor    %edx,%edx
    hb->block[i] = log.lh.block[i];
80102db0:	8b 8a cc 26 11 80    	mov    -0x7feed934(%edx),%ecx
80102db6:	89 4c 16 60          	mov    %ecx,0x60(%esi,%edx,1)
80102dba:	83 c2 04             	add    $0x4,%edx
  for (i = 0; i < log.lh.n; i++) {
80102dbd:	39 da                	cmp    %ebx,%edx
80102dbf:	75 ef                	jne    80102db0 <write_head+0x30>
  }
  bwrite(buf);
80102dc1:	83 ec 0c             	sub    $0xc,%esp
80102dc4:	56                   	push   %esi
80102dc5:	e8 d6 d3 ff ff       	call   801001a0 <bwrite>
  brelse(buf);
80102dca:	89 34 24             	mov    %esi,(%esp)
80102dcd:	e8 0e d4 ff ff       	call   801001e0 <brelse>
}
80102dd2:	83 c4 10             	add    $0x10,%esp
80102dd5:	8d 65 f8             	lea    -0x8(%ebp),%esp
80102dd8:	5b                   	pop    %ebx
80102dd9:	5e                   	pop    %esi
80102dda:	5d                   	pop    %ebp
80102ddb:	c3                   	ret    
80102ddc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80102de0 <initlog>:
{
80102de0:	55                   	push   %ebp
80102de1:	89 e5                	mov    %esp,%ebp
80102de3:	53                   	push   %ebx
80102de4:	83 ec 2c             	sub    $0x2c,%esp
80102de7:	8b 5d 08             	mov    0x8(%ebp),%ebx
  initlock(&log.lock, "log");
80102dea:	68 40 76 10 80       	push   $0x80107640
80102def:	68 80 26 11 80       	push   $0x80112680
80102df4:	e8 e7 16 00 00       	call   801044e0 <initlock>
  readsb(dev, &sb);
80102df9:	58                   	pop    %eax
80102dfa:	8d 45 dc             	lea    -0x24(%ebp),%eax
80102dfd:	5a                   	pop    %edx
80102dfe:	50                   	push   %eax
80102dff:	53                   	push   %ebx
80102e00:	e8 1b e9 ff ff       	call   80101720 <readsb>
  log.size = sb.nlog;
80102e05:	8b 55 e8             	mov    -0x18(%ebp),%edx
  log.start = sb.logstart;
80102e08:	8b 45 ec             	mov    -0x14(%ebp),%eax
  struct buf *buf = bread(log.dev, log.start);
80102e0b:	59                   	pop    %ecx
  log.dev = dev;
80102e0c:	89 1d c4 26 11 80    	mov    %ebx,0x801126c4
  log.size = sb.nlog;
80102e12:	89 15 b8 26 11 80    	mov    %edx,0x801126b8
  log.start = sb.logstart;
80102e18:	a3 b4 26 11 80       	mov    %eax,0x801126b4
  struct buf *buf = bread(log.dev, log.start);
80102e1d:	5a                   	pop    %edx
80102e1e:	50                   	push   %eax
80102e1f:	53                   	push   %ebx
80102e20:	e8 ab d2 ff ff       	call   801000d0 <bread>
  log.lh.n = lh->n;
80102e25:	8b 58 5c             	mov    0x5c(%eax),%ebx
  for (i = 0; i < log.lh.n; i++) {
80102e28:	83 c4 10             	add    $0x10,%esp
80102e2b:	85 db                	test   %ebx,%ebx
  log.lh.n = lh->n;
80102e2d:	89 1d c8 26 11 80    	mov    %ebx,0x801126c8
  for (i = 0; i < log.lh.n; i++) {
80102e33:	7e 1c                	jle    80102e51 <initlog+0x71>
80102e35:	c1 e3 02             	shl    $0x2,%ebx
80102e38:	31 d2                	xor    %edx,%edx
80102e3a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    log.lh.block[i] = lh->block[i];
80102e40:	8b 4c 10 60          	mov    0x60(%eax,%edx,1),%ecx
80102e44:	83 c2 04             	add    $0x4,%edx
80102e47:	89 8a c8 26 11 80    	mov    %ecx,-0x7feed938(%edx)
  for (i = 0; i < log.lh.n; i++) {
80102e4d:	39 d3                	cmp    %edx,%ebx
80102e4f:	75 ef                	jne    80102e40 <initlog+0x60>
  brelse(buf);
80102e51:	83 ec 0c             	sub    $0xc,%esp
80102e54:	50                   	push   %eax
80102e55:	e8 86 d3 ff ff       	call   801001e0 <brelse>

static void
recover_from_log(void)
{
  read_head();
  install_trans(); // if committed, copy from log to disk
80102e5a:	e8 81 fe ff ff       	call   80102ce0 <install_trans>
  log.lh.n = 0;
80102e5f:	c7 05 c8 26 11 80 00 	movl   $0x0,0x801126c8
80102e66:	00 00 00 
  write_head(); // clear the log
80102e69:	e8 12 ff ff ff       	call   80102d80 <write_head>
}
80102e6e:	83 c4 10             	add    $0x10,%esp
80102e71:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80102e74:	c9                   	leave  
80102e75:	c3                   	ret    
80102e76:	8d 76 00             	lea    0x0(%esi),%esi
80102e79:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80102e80 <begin_op>:
}

// called at the start of each FS system call.
void
begin_op(void)
{
80102e80:	55                   	push   %ebp
80102e81:	89 e5                	mov    %esp,%ebp
80102e83:	83 ec 14             	sub    $0x14,%esp
  acquire(&log.lock);
80102e86:	68 80 26 11 80       	push   $0x80112680
80102e8b:	e8 90 17 00 00       	call   80104620 <acquire>
80102e90:	83 c4 10             	add    $0x10,%esp
80102e93:	eb 18                	jmp    80102ead <begin_op+0x2d>
80102e95:	8d 76 00             	lea    0x0(%esi),%esi
  while(1){
    if(log.committing){
      sleep(&log, &log.lock);
80102e98:	83 ec 08             	sub    $0x8,%esp
80102e9b:	68 80 26 11 80       	push   $0x80112680
80102ea0:	68 80 26 11 80       	push   $0x80112680
80102ea5:	e8 b6 11 00 00       	call   80104060 <sleep>
80102eaa:	83 c4 10             	add    $0x10,%esp
    if(log.committing){
80102ead:	a1 c0 26 11 80       	mov    0x801126c0,%eax
80102eb2:	85 c0                	test   %eax,%eax
80102eb4:	75 e2                	jne    80102e98 <begin_op+0x18>
    } else if(log.lh.n + (log.outstanding+1)*MAXOPBLOCKS > LOGSIZE){
80102eb6:	a1 bc 26 11 80       	mov    0x801126bc,%eax
80102ebb:	8b 15 c8 26 11 80    	mov    0x801126c8,%edx
80102ec1:	83 c0 01             	add    $0x1,%eax
80102ec4:	8d 0c 80             	lea    (%eax,%eax,4),%ecx
80102ec7:	8d 14 4a             	lea    (%edx,%ecx,2),%edx
80102eca:	83 fa 1e             	cmp    $0x1e,%edx
80102ecd:	7f c9                	jg     80102e98 <begin_op+0x18>
      // this op might exhaust log space; wait for commit.
      sleep(&log, &log.lock);
    } else {
      log.outstanding += 1;
      release(&log.lock);
80102ecf:	83 ec 0c             	sub    $0xc,%esp
      log.outstanding += 1;
80102ed2:	a3 bc 26 11 80       	mov    %eax,0x801126bc
      release(&log.lock);
80102ed7:	68 80 26 11 80       	push   $0x80112680
80102edc:	e8 ff 17 00 00       	call   801046e0 <release>
      break;
    }
  }
}
80102ee1:	83 c4 10             	add    $0x10,%esp
80102ee4:	c9                   	leave  
80102ee5:	c3                   	ret    
80102ee6:	8d 76 00             	lea    0x0(%esi),%esi
80102ee9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80102ef0 <end_op>:

// called at the end of each FS system call.
// commits if this was the last outstanding operation.
void
end_op(void)
{
80102ef0:	55                   	push   %ebp
80102ef1:	89 e5                	mov    %esp,%ebp
80102ef3:	57                   	push   %edi
80102ef4:	56                   	push   %esi
80102ef5:	53                   	push   %ebx
80102ef6:	83 ec 18             	sub    $0x18,%esp
  int do_commit = 0;

  acquire(&log.lock);
80102ef9:	68 80 26 11 80       	push   $0x80112680
80102efe:	e8 1d 17 00 00       	call   80104620 <acquire>
  log.outstanding -= 1;
80102f03:	a1 bc 26 11 80       	mov    0x801126bc,%eax
  if(log.committing)
80102f08:	8b 35 c0 26 11 80    	mov    0x801126c0,%esi
80102f0e:	83 c4 10             	add    $0x10,%esp
  log.outstanding -= 1;
80102f11:	8d 58 ff             	lea    -0x1(%eax),%ebx
  if(log.committing)
80102f14:	85 f6                	test   %esi,%esi
  log.outstanding -= 1;
80102f16:	89 1d bc 26 11 80    	mov    %ebx,0x801126bc
  if(log.committing)
80102f1c:	0f 85 1a 01 00 00    	jne    8010303c <end_op+0x14c>
    panic("log.committing");
  if(log.outstanding == 0){
80102f22:	85 db                	test   %ebx,%ebx
80102f24:	0f 85 ee 00 00 00    	jne    80103018 <end_op+0x128>
    // begin_op() may be waiting for log space,
    // and decrementing log.outstanding has decreased
    // the amount of reserved space.
    wakeup(&log);
  }
  release(&log.lock);
80102f2a:	83 ec 0c             	sub    $0xc,%esp
    log.committing = 1;
80102f2d:	c7 05 c0 26 11 80 01 	movl   $0x1,0x801126c0
80102f34:	00 00 00 
  release(&log.lock);
80102f37:	68 80 26 11 80       	push   $0x80112680
80102f3c:	e8 9f 17 00 00       	call   801046e0 <release>
}

static void
commit()
{
  if (log.lh.n > 0) {
80102f41:	8b 0d c8 26 11 80    	mov    0x801126c8,%ecx
80102f47:	83 c4 10             	add    $0x10,%esp
80102f4a:	85 c9                	test   %ecx,%ecx
80102f4c:	0f 8e 85 00 00 00    	jle    80102fd7 <end_op+0xe7>
    struct buf *to = bread(log.dev, log.start+tail+1); // log block
80102f52:	a1 b4 26 11 80       	mov    0x801126b4,%eax
80102f57:	83 ec 08             	sub    $0x8,%esp
80102f5a:	01 d8                	add    %ebx,%eax
80102f5c:	83 c0 01             	add    $0x1,%eax
80102f5f:	50                   	push   %eax
80102f60:	ff 35 c4 26 11 80    	pushl  0x801126c4
80102f66:	e8 65 d1 ff ff       	call   801000d0 <bread>
80102f6b:	89 c6                	mov    %eax,%esi
    struct buf *from = bread(log.dev, log.lh.block[tail]); // cache block
80102f6d:	58                   	pop    %eax
80102f6e:	5a                   	pop    %edx
80102f6f:	ff 34 9d cc 26 11 80 	pushl  -0x7feed934(,%ebx,4)
80102f76:	ff 35 c4 26 11 80    	pushl  0x801126c4
  for (tail = 0; tail < log.lh.n; tail++) {
80102f7c:	83 c3 01             	add    $0x1,%ebx
    struct buf *from = bread(log.dev, log.lh.block[tail]); // cache block
80102f7f:	e8 4c d1 ff ff       	call   801000d0 <bread>
80102f84:	89 c7                	mov    %eax,%edi
    memmove(to->data, from->data, BSIZE);
80102f86:	8d 40 5c             	lea    0x5c(%eax),%eax
80102f89:	83 c4 0c             	add    $0xc,%esp
80102f8c:	68 00 02 00 00       	push   $0x200
80102f91:	50                   	push   %eax
80102f92:	8d 46 5c             	lea    0x5c(%esi),%eax
80102f95:	50                   	push   %eax
80102f96:	e8 45 18 00 00       	call   801047e0 <memmove>
    bwrite(to);  // write the log
80102f9b:	89 34 24             	mov    %esi,(%esp)
80102f9e:	e8 fd d1 ff ff       	call   801001a0 <bwrite>
    brelse(from);
80102fa3:	89 3c 24             	mov    %edi,(%esp)
80102fa6:	e8 35 d2 ff ff       	call   801001e0 <brelse>
    brelse(to);
80102fab:	89 34 24             	mov    %esi,(%esp)
80102fae:	e8 2d d2 ff ff       	call   801001e0 <brelse>
  for (tail = 0; tail < log.lh.n; tail++) {
80102fb3:	83 c4 10             	add    $0x10,%esp
80102fb6:	3b 1d c8 26 11 80    	cmp    0x801126c8,%ebx
80102fbc:	7c 94                	jl     80102f52 <end_op+0x62>
    write_log();     // Write modified blocks from cache to log
    write_head();    // Write header to disk -- the real commit
80102fbe:	e8 bd fd ff ff       	call   80102d80 <write_head>
    install_trans(); // Now install writes to home locations
80102fc3:	e8 18 fd ff ff       	call   80102ce0 <install_trans>
    log.lh.n = 0;
80102fc8:	c7 05 c8 26 11 80 00 	movl   $0x0,0x801126c8
80102fcf:	00 00 00 
    write_head();    // Erase the transaction from the log
80102fd2:	e8 a9 fd ff ff       	call   80102d80 <write_head>
    acquire(&log.lock);
80102fd7:	83 ec 0c             	sub    $0xc,%esp
80102fda:	68 80 26 11 80       	push   $0x80112680
80102fdf:	e8 3c 16 00 00       	call   80104620 <acquire>
    wakeup(&log);
80102fe4:	c7 04 24 80 26 11 80 	movl   $0x80112680,(%esp)
    log.committing = 0;
80102feb:	c7 05 c0 26 11 80 00 	movl   $0x0,0x801126c0
80102ff2:	00 00 00 
    wakeup(&log);
80102ff5:	e8 16 12 00 00       	call   80104210 <wakeup>
    release(&log.lock);
80102ffa:	c7 04 24 80 26 11 80 	movl   $0x80112680,(%esp)
80103001:	e8 da 16 00 00       	call   801046e0 <release>
80103006:	83 c4 10             	add    $0x10,%esp
}
80103009:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010300c:	5b                   	pop    %ebx
8010300d:	5e                   	pop    %esi
8010300e:	5f                   	pop    %edi
8010300f:	5d                   	pop    %ebp
80103010:	c3                   	ret    
80103011:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    wakeup(&log);
80103018:	83 ec 0c             	sub    $0xc,%esp
8010301b:	68 80 26 11 80       	push   $0x80112680
80103020:	e8 eb 11 00 00       	call   80104210 <wakeup>
  release(&log.lock);
80103025:	c7 04 24 80 26 11 80 	movl   $0x80112680,(%esp)
8010302c:	e8 af 16 00 00       	call   801046e0 <release>
80103031:	83 c4 10             	add    $0x10,%esp
}
80103034:	8d 65 f4             	lea    -0xc(%ebp),%esp
80103037:	5b                   	pop    %ebx
80103038:	5e                   	pop    %esi
80103039:	5f                   	pop    %edi
8010303a:	5d                   	pop    %ebp
8010303b:	c3                   	ret    
    panic("log.committing");
8010303c:	83 ec 0c             	sub    $0xc,%esp
8010303f:	68 44 76 10 80       	push   $0x80107644
80103044:	e8 47 d3 ff ff       	call   80100390 <panic>
80103049:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80103050 <log_write>:
//   modify bp->data[]
//   log_write(bp)
//   brelse(bp)
void
log_write(struct buf *b)
{
80103050:	55                   	push   %ebp
80103051:	89 e5                	mov    %esp,%ebp
80103053:	53                   	push   %ebx
80103054:	83 ec 04             	sub    $0x4,%esp
  int i;

  if (log.lh.n >= LOGSIZE || log.lh.n >= log.size - 1)
80103057:	8b 15 c8 26 11 80    	mov    0x801126c8,%edx
{
8010305d:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if (log.lh.n >= LOGSIZE || log.lh.n >= log.size - 1)
80103060:	83 fa 1d             	cmp    $0x1d,%edx
80103063:	0f 8f 9d 00 00 00    	jg     80103106 <log_write+0xb6>
80103069:	a1 b8 26 11 80       	mov    0x801126b8,%eax
8010306e:	83 e8 01             	sub    $0x1,%eax
80103071:	39 c2                	cmp    %eax,%edx
80103073:	0f 8d 8d 00 00 00    	jge    80103106 <log_write+0xb6>
    panic("too big a transaction");
  if (log.outstanding < 1)
80103079:	a1 bc 26 11 80       	mov    0x801126bc,%eax
8010307e:	85 c0                	test   %eax,%eax
80103080:	0f 8e 8d 00 00 00    	jle    80103113 <log_write+0xc3>
    panic("log_write outside of trans");

  acquire(&log.lock);
80103086:	83 ec 0c             	sub    $0xc,%esp
80103089:	68 80 26 11 80       	push   $0x80112680
8010308e:	e8 8d 15 00 00       	call   80104620 <acquire>
  for (i = 0; i < log.lh.n; i++) {
80103093:	8b 0d c8 26 11 80    	mov    0x801126c8,%ecx
80103099:	83 c4 10             	add    $0x10,%esp
8010309c:	83 f9 00             	cmp    $0x0,%ecx
8010309f:	7e 57                	jle    801030f8 <log_write+0xa8>
    if (log.lh.block[i] == b->blockno)   // log absorbtion
801030a1:	8b 53 08             	mov    0x8(%ebx),%edx
  for (i = 0; i < log.lh.n; i++) {
801030a4:	31 c0                	xor    %eax,%eax
    if (log.lh.block[i] == b->blockno)   // log absorbtion
801030a6:	3b 15 cc 26 11 80    	cmp    0x801126cc,%edx
801030ac:	75 0b                	jne    801030b9 <log_write+0x69>
801030ae:	eb 38                	jmp    801030e8 <log_write+0x98>
801030b0:	39 14 85 cc 26 11 80 	cmp    %edx,-0x7feed934(,%eax,4)
801030b7:	74 2f                	je     801030e8 <log_write+0x98>
  for (i = 0; i < log.lh.n; i++) {
801030b9:	83 c0 01             	add    $0x1,%eax
801030bc:	39 c1                	cmp    %eax,%ecx
801030be:	75 f0                	jne    801030b0 <log_write+0x60>
      break;
  }
  log.lh.block[i] = b->blockno;
801030c0:	89 14 85 cc 26 11 80 	mov    %edx,-0x7feed934(,%eax,4)
  if (i == log.lh.n)
    log.lh.n++;
801030c7:	83 c0 01             	add    $0x1,%eax
801030ca:	a3 c8 26 11 80       	mov    %eax,0x801126c8
  b->flags |= B_DIRTY; // prevent eviction
801030cf:	83 0b 04             	orl    $0x4,(%ebx)
  release(&log.lock);
801030d2:	c7 45 08 80 26 11 80 	movl   $0x80112680,0x8(%ebp)
}
801030d9:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801030dc:	c9                   	leave  
  release(&log.lock);
801030dd:	e9 fe 15 00 00       	jmp    801046e0 <release>
801030e2:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  log.lh.block[i] = b->blockno;
801030e8:	89 14 85 cc 26 11 80 	mov    %edx,-0x7feed934(,%eax,4)
801030ef:	eb de                	jmp    801030cf <log_write+0x7f>
801030f1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801030f8:	8b 43 08             	mov    0x8(%ebx),%eax
801030fb:	a3 cc 26 11 80       	mov    %eax,0x801126cc
  if (i == log.lh.n)
80103100:	75 cd                	jne    801030cf <log_write+0x7f>
80103102:	31 c0                	xor    %eax,%eax
80103104:	eb c1                	jmp    801030c7 <log_write+0x77>
    panic("too big a transaction");
80103106:	83 ec 0c             	sub    $0xc,%esp
80103109:	68 53 76 10 80       	push   $0x80107653
8010310e:	e8 7d d2 ff ff       	call   80100390 <panic>
    panic("log_write outside of trans");
80103113:	83 ec 0c             	sub    $0xc,%esp
80103116:	68 69 76 10 80       	push   $0x80107669
8010311b:	e8 70 d2 ff ff       	call   80100390 <panic>

80103120 <mpmain>:
}

// Common CPU setup code.
static void
mpmain(void)
{
80103120:	55                   	push   %ebp
80103121:	89 e5                	mov    %esp,%ebp
80103123:	53                   	push   %ebx
80103124:	83 ec 04             	sub    $0x4,%esp
  cprintf("cpu%d: starting %d\n", cpuid(), cpuid());
80103127:	e8 74 09 00 00       	call   80103aa0 <cpuid>
8010312c:	89 c3                	mov    %eax,%ebx
8010312e:	e8 6d 09 00 00       	call   80103aa0 <cpuid>
80103133:	83 ec 04             	sub    $0x4,%esp
80103136:	53                   	push   %ebx
80103137:	50                   	push   %eax
80103138:	68 84 76 10 80       	push   $0x80107684
8010313d:	e8 4e d6 ff ff       	call   80100790 <cprintf>
  idtinit();       // load idt register
80103142:	e8 69 28 00 00       	call   801059b0 <idtinit>
  xchg(&(mycpu()->started), 1); // tell startothers() we're up
80103147:	e8 d4 08 00 00       	call   80103a20 <mycpu>
8010314c:	89 c2                	mov    %eax,%edx
xchg(volatile uint *addr, uint newval)
{
  uint result;

  // The + in "+m" denotes a read-modify-write operand.
  asm volatile("lock; xchgl %0, %1" :
8010314e:	b8 01 00 00 00       	mov    $0x1,%eax
80103153:	f0 87 82 a0 00 00 00 	lock xchg %eax,0xa0(%edx)
  scheduler();     // start running processes
8010315a:	e8 21 0c 00 00       	call   80103d80 <scheduler>
8010315f:	90                   	nop

80103160 <mpenter>:
{
80103160:	55                   	push   %ebp
80103161:	89 e5                	mov    %esp,%ebp
80103163:	83 ec 08             	sub    $0x8,%esp
  switchkvm();
80103166:	e8 35 39 00 00       	call   80106aa0 <switchkvm>
  seginit();
8010316b:	e8 a0 38 00 00       	call   80106a10 <seginit>
  lapicinit();
80103170:	e8 9b f7 ff ff       	call   80102910 <lapicinit>
  mpmain();
80103175:	e8 a6 ff ff ff       	call   80103120 <mpmain>
8010317a:	66 90                	xchg   %ax,%ax
8010317c:	66 90                	xchg   %ax,%ax
8010317e:	66 90                	xchg   %ax,%ax

80103180 <main>:
{
80103180:	8d 4c 24 04          	lea    0x4(%esp),%ecx
80103184:	83 e4 f0             	and    $0xfffffff0,%esp
80103187:	ff 71 fc             	pushl  -0x4(%ecx)
8010318a:	55                   	push   %ebp
8010318b:	89 e5                	mov    %esp,%ebp
8010318d:	53                   	push   %ebx
8010318e:	51                   	push   %ecx
  kinit1(end, P2V(4*1024*1024)); // phys page allocator
8010318f:	83 ec 08             	sub    $0x8,%esp
80103192:	68 00 00 40 80       	push   $0x80400000
80103197:	68 a8 54 11 80       	push   $0x801154a8
8010319c:	e8 2f f5 ff ff       	call   801026d0 <kinit1>
  kvmalloc();      // kernel page table
801031a1:	e8 ca 3d 00 00       	call   80106f70 <kvmalloc>
  mpinit();        // detect other processors
801031a6:	e8 75 01 00 00       	call   80103320 <mpinit>
  lapicinit();     // interrupt controller
801031ab:	e8 60 f7 ff ff       	call   80102910 <lapicinit>
  seginit();       // segment descriptors
801031b0:	e8 5b 38 00 00       	call   80106a10 <seginit>
  picinit();       // disable pic
801031b5:	e8 46 03 00 00       	call   80103500 <picinit>
  ioapicinit();    // another interrupt controller
801031ba:	e8 41 f3 ff ff       	call   80102500 <ioapicinit>
  consoleinit();   // console hardware
801031bf:	e8 dc da ff ff       	call   80100ca0 <consoleinit>
  uartinit();      // serial port
801031c4:	e8 17 2b 00 00       	call   80105ce0 <uartinit>
  pinit();         // process table
801031c9:	e8 32 08 00 00       	call   80103a00 <pinit>
  tvinit();        // trap vectors
801031ce:	e8 5d 27 00 00       	call   80105930 <tvinit>
  binit();         // buffer cache
801031d3:	e8 68 ce ff ff       	call   80100040 <binit>
  fileinit();      // file table
801031d8:	e8 63 de ff ff       	call   80101040 <fileinit>
  ideinit();       // disk 
801031dd:	e8 fe f0 ff ff       	call   801022e0 <ideinit>

  // Write entry code to unused memory at 0x7000.
  // The linker has placed the image of entryother.S in
  // _binary_entryother_start.
  code = P2V(0x7000);
  memmove(code, _binary_entryother_start, (uint)_binary_entryother_size);
801031e2:	83 c4 0c             	add    $0xc,%esp
801031e5:	68 8a 00 00 00       	push   $0x8a
801031ea:	68 8c a4 10 80       	push   $0x8010a48c
801031ef:	68 00 70 00 80       	push   $0x80007000
801031f4:	e8 e7 15 00 00       	call   801047e0 <memmove>

  for(c = cpus; c < cpus+ncpu; c++){
801031f9:	69 05 00 2d 11 80 b0 	imul   $0xb0,0x80112d00,%eax
80103200:	00 00 00 
80103203:	83 c4 10             	add    $0x10,%esp
80103206:	05 80 27 11 80       	add    $0x80112780,%eax
8010320b:	3d 80 27 11 80       	cmp    $0x80112780,%eax
80103210:	76 71                	jbe    80103283 <main+0x103>
80103212:	bb 80 27 11 80       	mov    $0x80112780,%ebx
80103217:	89 f6                	mov    %esi,%esi
80103219:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
    if(c == mycpu())  // We've started already.
80103220:	e8 fb 07 00 00       	call   80103a20 <mycpu>
80103225:	39 d8                	cmp    %ebx,%eax
80103227:	74 41                	je     8010326a <main+0xea>
      continue;

    // Tell entryother.S what stack to use, where to enter, and what
    // pgdir to use. We cannot use kpgdir yet, because the AP processor
    // is running in low  memory, so we use entrypgdir for the APs too.
    stack = kalloc();
80103229:	e8 72 f5 ff ff       	call   801027a0 <kalloc>
    *(void**)(code-4) = stack + KSTACKSIZE;
8010322e:	05 00 10 00 00       	add    $0x1000,%eax
    *(void(**)(void))(code-8) = mpenter;
80103233:	c7 05 f8 6f 00 80 60 	movl   $0x80103160,0x80006ff8
8010323a:	31 10 80 
    *(int**)(code-12) = (void *) V2P(entrypgdir);
8010323d:	c7 05 f4 6f 00 80 00 	movl   $0x109000,0x80006ff4
80103244:	90 10 00 
    *(void**)(code-4) = stack + KSTACKSIZE;
80103247:	a3 fc 6f 00 80       	mov    %eax,0x80006ffc

    lapicstartap(c->apicid, V2P(code));
8010324c:	0f b6 03             	movzbl (%ebx),%eax
8010324f:	83 ec 08             	sub    $0x8,%esp
80103252:	68 00 70 00 00       	push   $0x7000
80103257:	50                   	push   %eax
80103258:	e8 03 f8 ff ff       	call   80102a60 <lapicstartap>
8010325d:	83 c4 10             	add    $0x10,%esp

    // wait for cpu to finish mpmain()
    while(c->started == 0)
80103260:	8b 83 a0 00 00 00    	mov    0xa0(%ebx),%eax
80103266:	85 c0                	test   %eax,%eax
80103268:	74 f6                	je     80103260 <main+0xe0>
  for(c = cpus; c < cpus+ncpu; c++){
8010326a:	69 05 00 2d 11 80 b0 	imul   $0xb0,0x80112d00,%eax
80103271:	00 00 00 
80103274:	81 c3 b0 00 00 00    	add    $0xb0,%ebx
8010327a:	05 80 27 11 80       	add    $0x80112780,%eax
8010327f:	39 c3                	cmp    %eax,%ebx
80103281:	72 9d                	jb     80103220 <main+0xa0>
  kinit2(P2V(4*1024*1024), P2V(PHYSTOP)); // must come after startothers()
80103283:	83 ec 08             	sub    $0x8,%esp
80103286:	68 00 00 00 8e       	push   $0x8e000000
8010328b:	68 00 00 40 80       	push   $0x80400000
80103290:	e8 ab f4 ff ff       	call   80102740 <kinit2>
  userinit();      // first user process
80103295:	e8 56 08 00 00       	call   80103af0 <userinit>
  mpmain();        // finish this processor's setup
8010329a:	e8 81 fe ff ff       	call   80103120 <mpmain>
8010329f:	90                   	nop

801032a0 <mpsearch1>:
}

// Look for an MP structure in the len bytes at addr.
static struct mp*
mpsearch1(uint a, int len)
{
801032a0:	55                   	push   %ebp
801032a1:	89 e5                	mov    %esp,%ebp
801032a3:	57                   	push   %edi
801032a4:	56                   	push   %esi
  uchar *e, *p, *addr;

  addr = P2V(a);
801032a5:	8d b0 00 00 00 80    	lea    -0x80000000(%eax),%esi
{
801032ab:	53                   	push   %ebx
  e = addr+len;
801032ac:	8d 1c 16             	lea    (%esi,%edx,1),%ebx
{
801032af:	83 ec 0c             	sub    $0xc,%esp
  for(p = addr; p < e; p += sizeof(struct mp))
801032b2:	39 de                	cmp    %ebx,%esi
801032b4:	72 10                	jb     801032c6 <mpsearch1+0x26>
801032b6:	eb 50                	jmp    80103308 <mpsearch1+0x68>
801032b8:	90                   	nop
801032b9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801032c0:	39 fb                	cmp    %edi,%ebx
801032c2:	89 fe                	mov    %edi,%esi
801032c4:	76 42                	jbe    80103308 <mpsearch1+0x68>
    if(memcmp(p, "_MP_", 4) == 0 && sum(p, sizeof(struct mp)) == 0)
801032c6:	83 ec 04             	sub    $0x4,%esp
801032c9:	8d 7e 10             	lea    0x10(%esi),%edi
801032cc:	6a 04                	push   $0x4
801032ce:	68 98 76 10 80       	push   $0x80107698
801032d3:	56                   	push   %esi
801032d4:	e8 a7 14 00 00       	call   80104780 <memcmp>
801032d9:	83 c4 10             	add    $0x10,%esp
801032dc:	85 c0                	test   %eax,%eax
801032de:	75 e0                	jne    801032c0 <mpsearch1+0x20>
801032e0:	89 f1                	mov    %esi,%ecx
801032e2:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    sum += addr[i];
801032e8:	0f b6 11             	movzbl (%ecx),%edx
801032eb:	83 c1 01             	add    $0x1,%ecx
801032ee:	01 d0                	add    %edx,%eax
  for(i=0; i<len; i++)
801032f0:	39 f9                	cmp    %edi,%ecx
801032f2:	75 f4                	jne    801032e8 <mpsearch1+0x48>
    if(memcmp(p, "_MP_", 4) == 0 && sum(p, sizeof(struct mp)) == 0)
801032f4:	84 c0                	test   %al,%al
801032f6:	75 c8                	jne    801032c0 <mpsearch1+0x20>
      return (struct mp*)p;
  return 0;
}
801032f8:	8d 65 f4             	lea    -0xc(%ebp),%esp
801032fb:	89 f0                	mov    %esi,%eax
801032fd:	5b                   	pop    %ebx
801032fe:	5e                   	pop    %esi
801032ff:	5f                   	pop    %edi
80103300:	5d                   	pop    %ebp
80103301:	c3                   	ret    
80103302:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80103308:	8d 65 f4             	lea    -0xc(%ebp),%esp
  return 0;
8010330b:	31 f6                	xor    %esi,%esi
}
8010330d:	89 f0                	mov    %esi,%eax
8010330f:	5b                   	pop    %ebx
80103310:	5e                   	pop    %esi
80103311:	5f                   	pop    %edi
80103312:	5d                   	pop    %ebp
80103313:	c3                   	ret    
80103314:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
8010331a:	8d bf 00 00 00 00    	lea    0x0(%edi),%edi

80103320 <mpinit>:
  return conf;
}

void
mpinit(void)
{
80103320:	55                   	push   %ebp
80103321:	89 e5                	mov    %esp,%ebp
80103323:	57                   	push   %edi
80103324:	56                   	push   %esi
80103325:	53                   	push   %ebx
80103326:	83 ec 1c             	sub    $0x1c,%esp
  if((p = ((bda[0x0F]<<8)| bda[0x0E]) << 4)){
80103329:	0f b6 05 0f 04 00 80 	movzbl 0x8000040f,%eax
80103330:	0f b6 15 0e 04 00 80 	movzbl 0x8000040e,%edx
80103337:	c1 e0 08             	shl    $0x8,%eax
8010333a:	09 d0                	or     %edx,%eax
8010333c:	c1 e0 04             	shl    $0x4,%eax
8010333f:	85 c0                	test   %eax,%eax
80103341:	75 1b                	jne    8010335e <mpinit+0x3e>
    p = ((bda[0x14]<<8)|bda[0x13])*1024;
80103343:	0f b6 05 14 04 00 80 	movzbl 0x80000414,%eax
8010334a:	0f b6 15 13 04 00 80 	movzbl 0x80000413,%edx
80103351:	c1 e0 08             	shl    $0x8,%eax
80103354:	09 d0                	or     %edx,%eax
80103356:	c1 e0 0a             	shl    $0xa,%eax
    if((mp = mpsearch1(p-1024, 1024)))
80103359:	2d 00 04 00 00       	sub    $0x400,%eax
    if((mp = mpsearch1(p, 1024)))
8010335e:	ba 00 04 00 00       	mov    $0x400,%edx
80103363:	e8 38 ff ff ff       	call   801032a0 <mpsearch1>
80103368:	85 c0                	test   %eax,%eax
8010336a:	89 45 e4             	mov    %eax,-0x1c(%ebp)
8010336d:	0f 84 3d 01 00 00    	je     801034b0 <mpinit+0x190>
  if((mp = mpsearch()) == 0 || mp->physaddr == 0)
80103373:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80103376:	8b 58 04             	mov    0x4(%eax),%ebx
80103379:	85 db                	test   %ebx,%ebx
8010337b:	0f 84 4f 01 00 00    	je     801034d0 <mpinit+0x1b0>
  conf = (struct mpconf*) P2V((uint) mp->physaddr);
80103381:	8d b3 00 00 00 80    	lea    -0x80000000(%ebx),%esi
  if(memcmp(conf, "PCMP", 4) != 0)
80103387:	83 ec 04             	sub    $0x4,%esp
8010338a:	6a 04                	push   $0x4
8010338c:	68 b5 76 10 80       	push   $0x801076b5
80103391:	56                   	push   %esi
80103392:	e8 e9 13 00 00       	call   80104780 <memcmp>
80103397:	83 c4 10             	add    $0x10,%esp
8010339a:	85 c0                	test   %eax,%eax
8010339c:	0f 85 2e 01 00 00    	jne    801034d0 <mpinit+0x1b0>
  if(conf->version != 1 && conf->version != 4)
801033a2:	0f b6 83 06 00 00 80 	movzbl -0x7ffffffa(%ebx),%eax
801033a9:	3c 01                	cmp    $0x1,%al
801033ab:	0f 95 c2             	setne  %dl
801033ae:	3c 04                	cmp    $0x4,%al
801033b0:	0f 95 c0             	setne  %al
801033b3:	20 c2                	and    %al,%dl
801033b5:	0f 85 15 01 00 00    	jne    801034d0 <mpinit+0x1b0>
  if(sum((uchar*)conf, conf->length) != 0)
801033bb:	0f b7 bb 04 00 00 80 	movzwl -0x7ffffffc(%ebx),%edi
  for(i=0; i<len; i++)
801033c2:	66 85 ff             	test   %di,%di
801033c5:	74 1a                	je     801033e1 <mpinit+0xc1>
801033c7:	89 f0                	mov    %esi,%eax
801033c9:	01 f7                	add    %esi,%edi
  sum = 0;
801033cb:	31 d2                	xor    %edx,%edx
801033cd:	8d 76 00             	lea    0x0(%esi),%esi
    sum += addr[i];
801033d0:	0f b6 08             	movzbl (%eax),%ecx
801033d3:	83 c0 01             	add    $0x1,%eax
801033d6:	01 ca                	add    %ecx,%edx
  for(i=0; i<len; i++)
801033d8:	39 c7                	cmp    %eax,%edi
801033da:	75 f4                	jne    801033d0 <mpinit+0xb0>
801033dc:	84 d2                	test   %dl,%dl
801033de:	0f 95 c2             	setne  %dl
  struct mp *mp;
  struct mpconf *conf;
  struct mpproc *proc;
  struct mpioapic *ioapic;

  if((conf = mpconfig(&mp)) == 0)
801033e1:	85 f6                	test   %esi,%esi
801033e3:	0f 84 e7 00 00 00    	je     801034d0 <mpinit+0x1b0>
801033e9:	84 d2                	test   %dl,%dl
801033eb:	0f 85 df 00 00 00    	jne    801034d0 <mpinit+0x1b0>
    panic("Expect to run on an SMP");
  ismp = 1;
  lapic = (uint*)conf->lapicaddr;
801033f1:	8b 83 24 00 00 80    	mov    -0x7fffffdc(%ebx),%eax
801033f7:	a3 7c 26 11 80       	mov    %eax,0x8011267c
  for(p=(uchar*)(conf+1), e=(uchar*)conf+conf->length; p<e; ){
801033fc:	0f b7 93 04 00 00 80 	movzwl -0x7ffffffc(%ebx),%edx
80103403:	8d 83 2c 00 00 80    	lea    -0x7fffffd4(%ebx),%eax
  ismp = 1;
80103409:	bb 01 00 00 00       	mov    $0x1,%ebx
  for(p=(uchar*)(conf+1), e=(uchar*)conf+conf->length; p<e; ){
8010340e:	01 d6                	add    %edx,%esi
80103410:	39 c6                	cmp    %eax,%esi
80103412:	76 23                	jbe    80103437 <mpinit+0x117>
    switch(*p){
80103414:	0f b6 10             	movzbl (%eax),%edx
80103417:	80 fa 04             	cmp    $0x4,%dl
8010341a:	0f 87 ca 00 00 00    	ja     801034ea <mpinit+0x1ca>
80103420:	ff 24 95 dc 76 10 80 	jmp    *-0x7fef8924(,%edx,4)
80103427:	89 f6                	mov    %esi,%esi
80103429:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
      p += sizeof(struct mpioapic);
      continue;
    case MPBUS:
    case MPIOINTR:
    case MPLINTR:
      p += 8;
80103430:	83 c0 08             	add    $0x8,%eax
  for(p=(uchar*)(conf+1), e=(uchar*)conf+conf->length; p<e; ){
80103433:	39 c6                	cmp    %eax,%esi
80103435:	77 dd                	ja     80103414 <mpinit+0xf4>
    default:
      ismp = 0;
      break;
    }
  }
  if(!ismp)
80103437:	85 db                	test   %ebx,%ebx
80103439:	0f 84 9e 00 00 00    	je     801034dd <mpinit+0x1bd>
    panic("Didn't find a suitable machine");

  if(mp->imcrp){
8010343f:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80103442:	80 78 0c 00          	cmpb   $0x0,0xc(%eax)
80103446:	74 15                	je     8010345d <mpinit+0x13d>
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80103448:	b8 70 00 00 00       	mov    $0x70,%eax
8010344d:	ba 22 00 00 00       	mov    $0x22,%edx
80103452:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80103453:	ba 23 00 00 00       	mov    $0x23,%edx
80103458:	ec                   	in     (%dx),%al
    // Bochs doesn't support IMCR, so this doesn't run on Bochs.
    // But it would on real hardware.
    outb(0x22, 0x70);   // Select IMCR
    outb(0x23, inb(0x23) | 1);  // Mask external interrupts.
80103459:	83 c8 01             	or     $0x1,%eax
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
8010345c:	ee                   	out    %al,(%dx)
  }
}
8010345d:	8d 65 f4             	lea    -0xc(%ebp),%esp
80103460:	5b                   	pop    %ebx
80103461:	5e                   	pop    %esi
80103462:	5f                   	pop    %edi
80103463:	5d                   	pop    %ebp
80103464:	c3                   	ret    
80103465:	8d 76 00             	lea    0x0(%esi),%esi
      if(ncpu < NCPU) {
80103468:	8b 0d 00 2d 11 80    	mov    0x80112d00,%ecx
8010346e:	83 f9 07             	cmp    $0x7,%ecx
80103471:	7f 19                	jg     8010348c <mpinit+0x16c>
        cpus[ncpu].apicid = proc->apicid;  // apicid may differ from ncpu
80103473:	0f b6 50 01          	movzbl 0x1(%eax),%edx
80103477:	69 f9 b0 00 00 00    	imul   $0xb0,%ecx,%edi
        ncpu++;
8010347d:	83 c1 01             	add    $0x1,%ecx
80103480:	89 0d 00 2d 11 80    	mov    %ecx,0x80112d00
        cpus[ncpu].apicid = proc->apicid;  // apicid may differ from ncpu
80103486:	88 97 80 27 11 80    	mov    %dl,-0x7feed880(%edi)
      p += sizeof(struct mpproc);
8010348c:	83 c0 14             	add    $0x14,%eax
      continue;
8010348f:	e9 7c ff ff ff       	jmp    80103410 <mpinit+0xf0>
80103494:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
      ioapicid = ioapic->apicno;
80103498:	0f b6 50 01          	movzbl 0x1(%eax),%edx
      p += sizeof(struct mpioapic);
8010349c:	83 c0 08             	add    $0x8,%eax
      ioapicid = ioapic->apicno;
8010349f:	88 15 60 27 11 80    	mov    %dl,0x80112760
      continue;
801034a5:	e9 66 ff ff ff       	jmp    80103410 <mpinit+0xf0>
801034aa:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  return mpsearch1(0xF0000, 0x10000);
801034b0:	ba 00 00 01 00       	mov    $0x10000,%edx
801034b5:	b8 00 00 0f 00       	mov    $0xf0000,%eax
801034ba:	e8 e1 fd ff ff       	call   801032a0 <mpsearch1>
  if((mp = mpsearch()) == 0 || mp->physaddr == 0)
801034bf:	85 c0                	test   %eax,%eax
  return mpsearch1(0xF0000, 0x10000);
801034c1:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  if((mp = mpsearch()) == 0 || mp->physaddr == 0)
801034c4:	0f 85 a9 fe ff ff    	jne    80103373 <mpinit+0x53>
801034ca:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    panic("Expect to run on an SMP");
801034d0:	83 ec 0c             	sub    $0xc,%esp
801034d3:	68 9d 76 10 80       	push   $0x8010769d
801034d8:	e8 b3 ce ff ff       	call   80100390 <panic>
    panic("Didn't find a suitable machine");
801034dd:	83 ec 0c             	sub    $0xc,%esp
801034e0:	68 bc 76 10 80       	push   $0x801076bc
801034e5:	e8 a6 ce ff ff       	call   80100390 <panic>
      ismp = 0;
801034ea:	31 db                	xor    %ebx,%ebx
801034ec:	e9 26 ff ff ff       	jmp    80103417 <mpinit+0xf7>
801034f1:	66 90                	xchg   %ax,%ax
801034f3:	66 90                	xchg   %ax,%ax
801034f5:	66 90                	xchg   %ax,%ax
801034f7:	66 90                	xchg   %ax,%ax
801034f9:	66 90                	xchg   %ax,%ax
801034fb:	66 90                	xchg   %ax,%ax
801034fd:	66 90                	xchg   %ax,%ax
801034ff:	90                   	nop

80103500 <picinit>:
#define IO_PIC2         0xA0    // Slave (IRQs 8-15)

// Don't use the 8259A interrupt controllers.  Xv6 assumes SMP hardware.
void
picinit(void)
{
80103500:	55                   	push   %ebp
80103501:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80103506:	ba 21 00 00 00       	mov    $0x21,%edx
8010350b:	89 e5                	mov    %esp,%ebp
8010350d:	ee                   	out    %al,(%dx)
8010350e:	ba a1 00 00 00       	mov    $0xa1,%edx
80103513:	ee                   	out    %al,(%dx)
  // mask all interrupts
  outb(IO_PIC1+1, 0xFF);
  outb(IO_PIC2+1, 0xFF);
}
80103514:	5d                   	pop    %ebp
80103515:	c3                   	ret    
80103516:	66 90                	xchg   %ax,%ax
80103518:	66 90                	xchg   %ax,%ax
8010351a:	66 90                	xchg   %ax,%ax
8010351c:	66 90                	xchg   %ax,%ax
8010351e:	66 90                	xchg   %ax,%ax

80103520 <pipealloc>:
  int writeopen;  // write fd is still open
};

int
pipealloc(struct file **f0, struct file **f1)
{
80103520:	55                   	push   %ebp
80103521:	89 e5                	mov    %esp,%ebp
80103523:	57                   	push   %edi
80103524:	56                   	push   %esi
80103525:	53                   	push   %ebx
80103526:	83 ec 0c             	sub    $0xc,%esp
80103529:	8b 5d 08             	mov    0x8(%ebp),%ebx
8010352c:	8b 75 0c             	mov    0xc(%ebp),%esi
  struct pipe *p;

  p = 0;
  *f0 = *f1 = 0;
8010352f:	c7 06 00 00 00 00    	movl   $0x0,(%esi)
80103535:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  if((*f0 = filealloc()) == 0 || (*f1 = filealloc()) == 0)
8010353b:	e8 20 db ff ff       	call   80101060 <filealloc>
80103540:	85 c0                	test   %eax,%eax
80103542:	89 03                	mov    %eax,(%ebx)
80103544:	74 22                	je     80103568 <pipealloc+0x48>
80103546:	e8 15 db ff ff       	call   80101060 <filealloc>
8010354b:	85 c0                	test   %eax,%eax
8010354d:	89 06                	mov    %eax,(%esi)
8010354f:	74 3f                	je     80103590 <pipealloc+0x70>
    goto bad;
  if((p = (struct pipe*)kalloc()) == 0)
80103551:	e8 4a f2 ff ff       	call   801027a0 <kalloc>
80103556:	85 c0                	test   %eax,%eax
80103558:	89 c7                	mov    %eax,%edi
8010355a:	75 54                	jne    801035b0 <pipealloc+0x90>

//PAGEBREAK: 20
 bad:
  if(p)
    kfree((char*)p);
  if(*f0)
8010355c:	8b 03                	mov    (%ebx),%eax
8010355e:	85 c0                	test   %eax,%eax
80103560:	75 34                	jne    80103596 <pipealloc+0x76>
80103562:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    fileclose(*f0);
  if(*f1)
80103568:	8b 06                	mov    (%esi),%eax
8010356a:	85 c0                	test   %eax,%eax
8010356c:	74 0c                	je     8010357a <pipealloc+0x5a>
    fileclose(*f1);
8010356e:	83 ec 0c             	sub    $0xc,%esp
80103571:	50                   	push   %eax
80103572:	e8 a9 db ff ff       	call   80101120 <fileclose>
80103577:	83 c4 10             	add    $0x10,%esp
  return -1;
}
8010357a:	8d 65 f4             	lea    -0xc(%ebp),%esp
  return -1;
8010357d:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80103582:	5b                   	pop    %ebx
80103583:	5e                   	pop    %esi
80103584:	5f                   	pop    %edi
80103585:	5d                   	pop    %ebp
80103586:	c3                   	ret    
80103587:	89 f6                	mov    %esi,%esi
80103589:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
  if(*f0)
80103590:	8b 03                	mov    (%ebx),%eax
80103592:	85 c0                	test   %eax,%eax
80103594:	74 e4                	je     8010357a <pipealloc+0x5a>
    fileclose(*f0);
80103596:	83 ec 0c             	sub    $0xc,%esp
80103599:	50                   	push   %eax
8010359a:	e8 81 db ff ff       	call   80101120 <fileclose>
  if(*f1)
8010359f:	8b 06                	mov    (%esi),%eax
    fileclose(*f0);
801035a1:	83 c4 10             	add    $0x10,%esp
  if(*f1)
801035a4:	85 c0                	test   %eax,%eax
801035a6:	75 c6                	jne    8010356e <pipealloc+0x4e>
801035a8:	eb d0                	jmp    8010357a <pipealloc+0x5a>
801035aa:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  initlock(&p->lock, "pipe");
801035b0:	83 ec 08             	sub    $0x8,%esp
  p->readopen = 1;
801035b3:	c7 80 3c 02 00 00 01 	movl   $0x1,0x23c(%eax)
801035ba:	00 00 00 
  p->writeopen = 1;
801035bd:	c7 80 40 02 00 00 01 	movl   $0x1,0x240(%eax)
801035c4:	00 00 00 
  p->nwrite = 0;
801035c7:	c7 80 38 02 00 00 00 	movl   $0x0,0x238(%eax)
801035ce:	00 00 00 
  p->nread = 0;
801035d1:	c7 80 34 02 00 00 00 	movl   $0x0,0x234(%eax)
801035d8:	00 00 00 
  initlock(&p->lock, "pipe");
801035db:	68 f0 76 10 80       	push   $0x801076f0
801035e0:	50                   	push   %eax
801035e1:	e8 fa 0e 00 00       	call   801044e0 <initlock>
  (*f0)->type = FD_PIPE;
801035e6:	8b 03                	mov    (%ebx),%eax
  return 0;
801035e8:	83 c4 10             	add    $0x10,%esp
  (*f0)->type = FD_PIPE;
801035eb:	c7 00 01 00 00 00    	movl   $0x1,(%eax)
  (*f0)->readable = 1;
801035f1:	8b 03                	mov    (%ebx),%eax
801035f3:	c6 40 08 01          	movb   $0x1,0x8(%eax)
  (*f0)->writable = 0;
801035f7:	8b 03                	mov    (%ebx),%eax
801035f9:	c6 40 09 00          	movb   $0x0,0x9(%eax)
  (*f0)->pipe = p;
801035fd:	8b 03                	mov    (%ebx),%eax
801035ff:	89 78 0c             	mov    %edi,0xc(%eax)
  (*f1)->type = FD_PIPE;
80103602:	8b 06                	mov    (%esi),%eax
80103604:	c7 00 01 00 00 00    	movl   $0x1,(%eax)
  (*f1)->readable = 0;
8010360a:	8b 06                	mov    (%esi),%eax
8010360c:	c6 40 08 00          	movb   $0x0,0x8(%eax)
  (*f1)->writable = 1;
80103610:	8b 06                	mov    (%esi),%eax
80103612:	c6 40 09 01          	movb   $0x1,0x9(%eax)
  (*f1)->pipe = p;
80103616:	8b 06                	mov    (%esi),%eax
80103618:	89 78 0c             	mov    %edi,0xc(%eax)
}
8010361b:	8d 65 f4             	lea    -0xc(%ebp),%esp
  return 0;
8010361e:	31 c0                	xor    %eax,%eax
}
80103620:	5b                   	pop    %ebx
80103621:	5e                   	pop    %esi
80103622:	5f                   	pop    %edi
80103623:	5d                   	pop    %ebp
80103624:	c3                   	ret    
80103625:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80103629:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80103630 <pipeclose>:

void
pipeclose(struct pipe *p, int writable)
{
80103630:	55                   	push   %ebp
80103631:	89 e5                	mov    %esp,%ebp
80103633:	56                   	push   %esi
80103634:	53                   	push   %ebx
80103635:	8b 5d 08             	mov    0x8(%ebp),%ebx
80103638:	8b 75 0c             	mov    0xc(%ebp),%esi
  acquire(&p->lock);
8010363b:	83 ec 0c             	sub    $0xc,%esp
8010363e:	53                   	push   %ebx
8010363f:	e8 dc 0f 00 00       	call   80104620 <acquire>
  if(writable){
80103644:	83 c4 10             	add    $0x10,%esp
80103647:	85 f6                	test   %esi,%esi
80103649:	74 45                	je     80103690 <pipeclose+0x60>
    p->writeopen = 0;
    wakeup(&p->nread);
8010364b:	8d 83 34 02 00 00    	lea    0x234(%ebx),%eax
80103651:	83 ec 0c             	sub    $0xc,%esp
    p->writeopen = 0;
80103654:	c7 83 40 02 00 00 00 	movl   $0x0,0x240(%ebx)
8010365b:	00 00 00 
    wakeup(&p->nread);
8010365e:	50                   	push   %eax
8010365f:	e8 ac 0b 00 00       	call   80104210 <wakeup>
80103664:	83 c4 10             	add    $0x10,%esp
  } else {
    p->readopen = 0;
    wakeup(&p->nwrite);
  }
  if(p->readopen == 0 && p->writeopen == 0){
80103667:	8b 93 3c 02 00 00    	mov    0x23c(%ebx),%edx
8010366d:	85 d2                	test   %edx,%edx
8010366f:	75 0a                	jne    8010367b <pipeclose+0x4b>
80103671:	8b 83 40 02 00 00    	mov    0x240(%ebx),%eax
80103677:	85 c0                	test   %eax,%eax
80103679:	74 35                	je     801036b0 <pipeclose+0x80>
    release(&p->lock);
    kfree((char*)p);
  } else
    release(&p->lock);
8010367b:	89 5d 08             	mov    %ebx,0x8(%ebp)
}
8010367e:	8d 65 f8             	lea    -0x8(%ebp),%esp
80103681:	5b                   	pop    %ebx
80103682:	5e                   	pop    %esi
80103683:	5d                   	pop    %ebp
    release(&p->lock);
80103684:	e9 57 10 00 00       	jmp    801046e0 <release>
80103689:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    wakeup(&p->nwrite);
80103690:	8d 83 38 02 00 00    	lea    0x238(%ebx),%eax
80103696:	83 ec 0c             	sub    $0xc,%esp
    p->readopen = 0;
80103699:	c7 83 3c 02 00 00 00 	movl   $0x0,0x23c(%ebx)
801036a0:	00 00 00 
    wakeup(&p->nwrite);
801036a3:	50                   	push   %eax
801036a4:	e8 67 0b 00 00       	call   80104210 <wakeup>
801036a9:	83 c4 10             	add    $0x10,%esp
801036ac:	eb b9                	jmp    80103667 <pipeclose+0x37>
801036ae:	66 90                	xchg   %ax,%ax
    release(&p->lock);
801036b0:	83 ec 0c             	sub    $0xc,%esp
801036b3:	53                   	push   %ebx
801036b4:	e8 27 10 00 00       	call   801046e0 <release>
    kfree((char*)p);
801036b9:	89 5d 08             	mov    %ebx,0x8(%ebp)
801036bc:	83 c4 10             	add    $0x10,%esp
}
801036bf:	8d 65 f8             	lea    -0x8(%ebp),%esp
801036c2:	5b                   	pop    %ebx
801036c3:	5e                   	pop    %esi
801036c4:	5d                   	pop    %ebp
    kfree((char*)p);
801036c5:	e9 26 ef ff ff       	jmp    801025f0 <kfree>
801036ca:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

801036d0 <pipewrite>:

//PAGEBREAK: 40
int
pipewrite(struct pipe *p, char *addr, int n)
{
801036d0:	55                   	push   %ebp
801036d1:	89 e5                	mov    %esp,%ebp
801036d3:	57                   	push   %edi
801036d4:	56                   	push   %esi
801036d5:	53                   	push   %ebx
801036d6:	83 ec 28             	sub    $0x28,%esp
801036d9:	8b 5d 08             	mov    0x8(%ebp),%ebx
  int i;

  acquire(&p->lock);
801036dc:	53                   	push   %ebx
801036dd:	e8 3e 0f 00 00       	call   80104620 <acquire>
  for(i = 0; i < n; i++){
801036e2:	8b 45 10             	mov    0x10(%ebp),%eax
801036e5:	83 c4 10             	add    $0x10,%esp
801036e8:	85 c0                	test   %eax,%eax
801036ea:	0f 8e c9 00 00 00    	jle    801037b9 <pipewrite+0xe9>
801036f0:	8b 4d 0c             	mov    0xc(%ebp),%ecx
801036f3:	8b 83 38 02 00 00    	mov    0x238(%ebx),%eax
    while(p->nwrite == p->nread + PIPESIZE){  //DOC: pipewrite-full
      if(p->readopen == 0 || myproc()->killed){
        release(&p->lock);
        return -1;
      }
      wakeup(&p->nread);
801036f9:	8d bb 34 02 00 00    	lea    0x234(%ebx),%edi
801036ff:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
80103702:	03 4d 10             	add    0x10(%ebp),%ecx
80103705:	89 4d e0             	mov    %ecx,-0x20(%ebp)
    while(p->nwrite == p->nread + PIPESIZE){  //DOC: pipewrite-full
80103708:	8b 8b 34 02 00 00    	mov    0x234(%ebx),%ecx
8010370e:	8d 91 00 02 00 00    	lea    0x200(%ecx),%edx
80103714:	39 d0                	cmp    %edx,%eax
80103716:	75 71                	jne    80103789 <pipewrite+0xb9>
      if(p->readopen == 0 || myproc()->killed){
80103718:	8b 83 3c 02 00 00    	mov    0x23c(%ebx),%eax
8010371e:	85 c0                	test   %eax,%eax
80103720:	74 4e                	je     80103770 <pipewrite+0xa0>
      sleep(&p->nwrite, &p->lock);  //DOC: pipewrite-sleep
80103722:	8d b3 38 02 00 00    	lea    0x238(%ebx),%esi
80103728:	eb 3a                	jmp    80103764 <pipewrite+0x94>
8010372a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
      wakeup(&p->nread);
80103730:	83 ec 0c             	sub    $0xc,%esp
80103733:	57                   	push   %edi
80103734:	e8 d7 0a 00 00       	call   80104210 <wakeup>
      sleep(&p->nwrite, &p->lock);  //DOC: pipewrite-sleep
80103739:	5a                   	pop    %edx
8010373a:	59                   	pop    %ecx
8010373b:	53                   	push   %ebx
8010373c:	56                   	push   %esi
8010373d:	e8 1e 09 00 00       	call   80104060 <sleep>
    while(p->nwrite == p->nread + PIPESIZE){  //DOC: pipewrite-full
80103742:	8b 83 34 02 00 00    	mov    0x234(%ebx),%eax
80103748:	8b 93 38 02 00 00    	mov    0x238(%ebx),%edx
8010374e:	83 c4 10             	add    $0x10,%esp
80103751:	05 00 02 00 00       	add    $0x200,%eax
80103756:	39 c2                	cmp    %eax,%edx
80103758:	75 36                	jne    80103790 <pipewrite+0xc0>
      if(p->readopen == 0 || myproc()->killed){
8010375a:	8b 83 3c 02 00 00    	mov    0x23c(%ebx),%eax
80103760:	85 c0                	test   %eax,%eax
80103762:	74 0c                	je     80103770 <pipewrite+0xa0>
80103764:	e8 57 03 00 00       	call   80103ac0 <myproc>
80103769:	8b 40 24             	mov    0x24(%eax),%eax
8010376c:	85 c0                	test   %eax,%eax
8010376e:	74 c0                	je     80103730 <pipewrite+0x60>
        release(&p->lock);
80103770:	83 ec 0c             	sub    $0xc,%esp
80103773:	53                   	push   %ebx
80103774:	e8 67 0f 00 00       	call   801046e0 <release>
        return -1;
80103779:	83 c4 10             	add    $0x10,%esp
8010377c:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
    p->data[p->nwrite++ % PIPESIZE] = addr[i];
  }
  wakeup(&p->nread);  //DOC: pipewrite-wakeup1
  release(&p->lock);
  return n;
}
80103781:	8d 65 f4             	lea    -0xc(%ebp),%esp
80103784:	5b                   	pop    %ebx
80103785:	5e                   	pop    %esi
80103786:	5f                   	pop    %edi
80103787:	5d                   	pop    %ebp
80103788:	c3                   	ret    
    while(p->nwrite == p->nread + PIPESIZE){  //DOC: pipewrite-full
80103789:	89 c2                	mov    %eax,%edx
8010378b:	90                   	nop
8010378c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    p->data[p->nwrite++ % PIPESIZE] = addr[i];
80103790:	8b 75 e4             	mov    -0x1c(%ebp),%esi
80103793:	8d 42 01             	lea    0x1(%edx),%eax
80103796:	81 e2 ff 01 00 00    	and    $0x1ff,%edx
8010379c:	89 83 38 02 00 00    	mov    %eax,0x238(%ebx)
801037a2:	83 c6 01             	add    $0x1,%esi
801037a5:	0f b6 4e ff          	movzbl -0x1(%esi),%ecx
  for(i = 0; i < n; i++){
801037a9:	3b 75 e0             	cmp    -0x20(%ebp),%esi
801037ac:	89 75 e4             	mov    %esi,-0x1c(%ebp)
    p->data[p->nwrite++ % PIPESIZE] = addr[i];
801037af:	88 4c 13 34          	mov    %cl,0x34(%ebx,%edx,1)
  for(i = 0; i < n; i++){
801037b3:	0f 85 4f ff ff ff    	jne    80103708 <pipewrite+0x38>
  wakeup(&p->nread);  //DOC: pipewrite-wakeup1
801037b9:	8d 83 34 02 00 00    	lea    0x234(%ebx),%eax
801037bf:	83 ec 0c             	sub    $0xc,%esp
801037c2:	50                   	push   %eax
801037c3:	e8 48 0a 00 00       	call   80104210 <wakeup>
  release(&p->lock);
801037c8:	89 1c 24             	mov    %ebx,(%esp)
801037cb:	e8 10 0f 00 00       	call   801046e0 <release>
  return n;
801037d0:	83 c4 10             	add    $0x10,%esp
801037d3:	8b 45 10             	mov    0x10(%ebp),%eax
801037d6:	eb a9                	jmp    80103781 <pipewrite+0xb1>
801037d8:	90                   	nop
801037d9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

801037e0 <piperead>:

int
piperead(struct pipe *p, char *addr, int n)
{
801037e0:	55                   	push   %ebp
801037e1:	89 e5                	mov    %esp,%ebp
801037e3:	57                   	push   %edi
801037e4:	56                   	push   %esi
801037e5:	53                   	push   %ebx
801037e6:	83 ec 18             	sub    $0x18,%esp
801037e9:	8b 75 08             	mov    0x8(%ebp),%esi
801037ec:	8b 7d 0c             	mov    0xc(%ebp),%edi
  int i;

  acquire(&p->lock);
801037ef:	56                   	push   %esi
801037f0:	e8 2b 0e 00 00       	call   80104620 <acquire>
  while(p->nread == p->nwrite && p->writeopen){  //DOC: pipe-empty
801037f5:	83 c4 10             	add    $0x10,%esp
801037f8:	8b 8e 34 02 00 00    	mov    0x234(%esi),%ecx
801037fe:	3b 8e 38 02 00 00    	cmp    0x238(%esi),%ecx
80103804:	75 6a                	jne    80103870 <piperead+0x90>
80103806:	8b 9e 40 02 00 00    	mov    0x240(%esi),%ebx
8010380c:	85 db                	test   %ebx,%ebx
8010380e:	0f 84 c4 00 00 00    	je     801038d8 <piperead+0xf8>
    if(myproc()->killed){
      release(&p->lock);
      return -1;
    }
    sleep(&p->nread, &p->lock); //DOC: piperead-sleep
80103814:	8d 9e 34 02 00 00    	lea    0x234(%esi),%ebx
8010381a:	eb 2d                	jmp    80103849 <piperead+0x69>
8010381c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80103820:	83 ec 08             	sub    $0x8,%esp
80103823:	56                   	push   %esi
80103824:	53                   	push   %ebx
80103825:	e8 36 08 00 00       	call   80104060 <sleep>
  while(p->nread == p->nwrite && p->writeopen){  //DOC: pipe-empty
8010382a:	83 c4 10             	add    $0x10,%esp
8010382d:	8b 8e 34 02 00 00    	mov    0x234(%esi),%ecx
80103833:	3b 8e 38 02 00 00    	cmp    0x238(%esi),%ecx
80103839:	75 35                	jne    80103870 <piperead+0x90>
8010383b:	8b 96 40 02 00 00    	mov    0x240(%esi),%edx
80103841:	85 d2                	test   %edx,%edx
80103843:	0f 84 8f 00 00 00    	je     801038d8 <piperead+0xf8>
    if(myproc()->killed){
80103849:	e8 72 02 00 00       	call   80103ac0 <myproc>
8010384e:	8b 48 24             	mov    0x24(%eax),%ecx
80103851:	85 c9                	test   %ecx,%ecx
80103853:	74 cb                	je     80103820 <piperead+0x40>
      release(&p->lock);
80103855:	83 ec 0c             	sub    $0xc,%esp
      return -1;
80103858:	bb ff ff ff ff       	mov    $0xffffffff,%ebx
      release(&p->lock);
8010385d:	56                   	push   %esi
8010385e:	e8 7d 0e 00 00       	call   801046e0 <release>
      return -1;
80103863:	83 c4 10             	add    $0x10,%esp
    addr[i] = p->data[p->nread++ % PIPESIZE];
  }
  wakeup(&p->nwrite);  //DOC: piperead-wakeup
  release(&p->lock);
  return i;
}
80103866:	8d 65 f4             	lea    -0xc(%ebp),%esp
80103869:	89 d8                	mov    %ebx,%eax
8010386b:	5b                   	pop    %ebx
8010386c:	5e                   	pop    %esi
8010386d:	5f                   	pop    %edi
8010386e:	5d                   	pop    %ebp
8010386f:	c3                   	ret    
  for(i = 0; i < n; i++){  //DOC: piperead-copy
80103870:	8b 45 10             	mov    0x10(%ebp),%eax
80103873:	85 c0                	test   %eax,%eax
80103875:	7e 61                	jle    801038d8 <piperead+0xf8>
    if(p->nread == p->nwrite)
80103877:	31 db                	xor    %ebx,%ebx
80103879:	eb 13                	jmp    8010388e <piperead+0xae>
8010387b:	90                   	nop
8010387c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80103880:	8b 8e 34 02 00 00    	mov    0x234(%esi),%ecx
80103886:	3b 8e 38 02 00 00    	cmp    0x238(%esi),%ecx
8010388c:	74 1f                	je     801038ad <piperead+0xcd>
    addr[i] = p->data[p->nread++ % PIPESIZE];
8010388e:	8d 41 01             	lea    0x1(%ecx),%eax
80103891:	81 e1 ff 01 00 00    	and    $0x1ff,%ecx
80103897:	89 86 34 02 00 00    	mov    %eax,0x234(%esi)
8010389d:	0f b6 44 0e 34       	movzbl 0x34(%esi,%ecx,1),%eax
801038a2:	88 04 1f             	mov    %al,(%edi,%ebx,1)
  for(i = 0; i < n; i++){  //DOC: piperead-copy
801038a5:	83 c3 01             	add    $0x1,%ebx
801038a8:	39 5d 10             	cmp    %ebx,0x10(%ebp)
801038ab:	75 d3                	jne    80103880 <piperead+0xa0>
  wakeup(&p->nwrite);  //DOC: piperead-wakeup
801038ad:	8d 86 38 02 00 00    	lea    0x238(%esi),%eax
801038b3:	83 ec 0c             	sub    $0xc,%esp
801038b6:	50                   	push   %eax
801038b7:	e8 54 09 00 00       	call   80104210 <wakeup>
  release(&p->lock);
801038bc:	89 34 24             	mov    %esi,(%esp)
801038bf:	e8 1c 0e 00 00       	call   801046e0 <release>
  return i;
801038c4:	83 c4 10             	add    $0x10,%esp
}
801038c7:	8d 65 f4             	lea    -0xc(%ebp),%esp
801038ca:	89 d8                	mov    %ebx,%eax
801038cc:	5b                   	pop    %ebx
801038cd:	5e                   	pop    %esi
801038ce:	5f                   	pop    %edi
801038cf:	5d                   	pop    %ebp
801038d0:	c3                   	ret    
801038d1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801038d8:	31 db                	xor    %ebx,%ebx
801038da:	eb d1                	jmp    801038ad <piperead+0xcd>
801038dc:	66 90                	xchg   %ax,%ax
801038de:	66 90                	xchg   %ax,%ax

801038e0 <allocproc>:
// If found, change state to EMBRYO and initialize
// state required to run in the kernel.
// Otherwise return 0.
static struct proc*
allocproc(void)
{
801038e0:	55                   	push   %ebp
801038e1:	89 e5                	mov    %esp,%ebp
801038e3:	53                   	push   %ebx
  struct proc *p;
  char *sp;

  acquire(&ptable.lock);

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
801038e4:	bb 54 2d 11 80       	mov    $0x80112d54,%ebx
{
801038e9:	83 ec 10             	sub    $0x10,%esp
  acquire(&ptable.lock);
801038ec:	68 20 2d 11 80       	push   $0x80112d20
801038f1:	e8 2a 0d 00 00       	call   80104620 <acquire>
801038f6:	83 c4 10             	add    $0x10,%esp
801038f9:	eb 10                	jmp    8010390b <allocproc+0x2b>
801038fb:	90                   	nop
801038fc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
80103900:	83 c3 7c             	add    $0x7c,%ebx
80103903:	81 fb 54 4c 11 80    	cmp    $0x80114c54,%ebx
80103909:	73 75                	jae    80103980 <allocproc+0xa0>
    if(p->state == UNUSED)
8010390b:	8b 43 0c             	mov    0xc(%ebx),%eax
8010390e:	85 c0                	test   %eax,%eax
80103910:	75 ee                	jne    80103900 <allocproc+0x20>
  release(&ptable.lock);
  return 0;

found:
  p->state = EMBRYO;
  p->pid = nextpid++;
80103912:	a1 04 a0 10 80       	mov    0x8010a004,%eax

  release(&ptable.lock);
80103917:	83 ec 0c             	sub    $0xc,%esp
  p->state = EMBRYO;
8010391a:	c7 43 0c 01 00 00 00 	movl   $0x1,0xc(%ebx)
  p->pid = nextpid++;
80103921:	8d 50 01             	lea    0x1(%eax),%edx
80103924:	89 43 10             	mov    %eax,0x10(%ebx)
  release(&ptable.lock);
80103927:	68 20 2d 11 80       	push   $0x80112d20
  p->pid = nextpid++;
8010392c:	89 15 04 a0 10 80    	mov    %edx,0x8010a004
  release(&ptable.lock);
80103932:	e8 a9 0d 00 00       	call   801046e0 <release>

  // Allocate kernel stack.
  if((p->kstack = kalloc()) == 0){
80103937:	e8 64 ee ff ff       	call   801027a0 <kalloc>
8010393c:	83 c4 10             	add    $0x10,%esp
8010393f:	85 c0                	test   %eax,%eax
80103941:	89 43 08             	mov    %eax,0x8(%ebx)
80103944:	74 53                	je     80103999 <allocproc+0xb9>
    return 0;
  }
  sp = p->kstack + KSTACKSIZE;

  // Leave room for trap frame.
  sp -= sizeof *p->tf;
80103946:	8d 90 b4 0f 00 00    	lea    0xfb4(%eax),%edx
  sp -= 4;
  *(uint*)sp = (uint)trapret;

  sp -= sizeof *p->context;
  p->context = (struct context*)sp;
  memset(p->context, 0, sizeof *p->context);
8010394c:	83 ec 04             	sub    $0x4,%esp
  sp -= sizeof *p->context;
8010394f:	05 9c 0f 00 00       	add    $0xf9c,%eax
  sp -= sizeof *p->tf;
80103954:	89 53 18             	mov    %edx,0x18(%ebx)
  *(uint*)sp = (uint)trapret;
80103957:	c7 40 14 22 59 10 80 	movl   $0x80105922,0x14(%eax)
  p->context = (struct context*)sp;
8010395e:	89 43 1c             	mov    %eax,0x1c(%ebx)
  memset(p->context, 0, sizeof *p->context);
80103961:	6a 14                	push   $0x14
80103963:	6a 00                	push   $0x0
80103965:	50                   	push   %eax
80103966:	e8 c5 0d 00 00       	call   80104730 <memset>
  p->context->eip = (uint)forkret;
8010396b:	8b 43 1c             	mov    0x1c(%ebx),%eax

  return p;
8010396e:	83 c4 10             	add    $0x10,%esp
  p->context->eip = (uint)forkret;
80103971:	c7 40 10 b0 39 10 80 	movl   $0x801039b0,0x10(%eax)
}
80103978:	89 d8                	mov    %ebx,%eax
8010397a:	8b 5d fc             	mov    -0x4(%ebp),%ebx
8010397d:	c9                   	leave  
8010397e:	c3                   	ret    
8010397f:	90                   	nop
  release(&ptable.lock);
80103980:	83 ec 0c             	sub    $0xc,%esp
  return 0;
80103983:	31 db                	xor    %ebx,%ebx
  release(&ptable.lock);
80103985:	68 20 2d 11 80       	push   $0x80112d20
8010398a:	e8 51 0d 00 00       	call   801046e0 <release>
}
8010398f:	89 d8                	mov    %ebx,%eax
  return 0;
80103991:	83 c4 10             	add    $0x10,%esp
}
80103994:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80103997:	c9                   	leave  
80103998:	c3                   	ret    
    p->state = UNUSED;
80103999:	c7 43 0c 00 00 00 00 	movl   $0x0,0xc(%ebx)
    return 0;
801039a0:	31 db                	xor    %ebx,%ebx
801039a2:	eb d4                	jmp    80103978 <allocproc+0x98>
801039a4:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
801039aa:	8d bf 00 00 00 00    	lea    0x0(%edi),%edi

801039b0 <forkret>:

// A fork child's very first scheduling by scheduler()
// will swtch here.  "Return" to user space.
void
forkret(void)
{
801039b0:	55                   	push   %ebp
801039b1:	89 e5                	mov    %esp,%ebp
801039b3:	83 ec 14             	sub    $0x14,%esp
  static int first = 1;
  // Still holding ptable.lock from scheduler.
  release(&ptable.lock);
801039b6:	68 20 2d 11 80       	push   $0x80112d20
801039bb:	e8 20 0d 00 00       	call   801046e0 <release>

  if (first) {
801039c0:	a1 00 a0 10 80       	mov    0x8010a000,%eax
801039c5:	83 c4 10             	add    $0x10,%esp
801039c8:	85 c0                	test   %eax,%eax
801039ca:	75 04                	jne    801039d0 <forkret+0x20>
    iinit(ROOTDEV);
    initlog(ROOTDEV);
  }

  // Return to "caller", actually trapret (see allocproc).
}
801039cc:	c9                   	leave  
801039cd:	c3                   	ret    
801039ce:	66 90                	xchg   %ax,%ax
    iinit(ROOTDEV);
801039d0:	83 ec 0c             	sub    $0xc,%esp
    first = 0;
801039d3:	c7 05 00 a0 10 80 00 	movl   $0x0,0x8010a000
801039da:	00 00 00 
    iinit(ROOTDEV);
801039dd:	6a 01                	push   $0x1
801039df:	e8 7c dd ff ff       	call   80101760 <iinit>
    initlog(ROOTDEV);
801039e4:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
801039eb:	e8 f0 f3 ff ff       	call   80102de0 <initlog>
801039f0:	83 c4 10             	add    $0x10,%esp
}
801039f3:	c9                   	leave  
801039f4:	c3                   	ret    
801039f5:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801039f9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80103a00 <pinit>:
{
80103a00:	55                   	push   %ebp
80103a01:	89 e5                	mov    %esp,%ebp
80103a03:	83 ec 10             	sub    $0x10,%esp
  initlock(&ptable.lock, "ptable");
80103a06:	68 f5 76 10 80       	push   $0x801076f5
80103a0b:	68 20 2d 11 80       	push   $0x80112d20
80103a10:	e8 cb 0a 00 00       	call   801044e0 <initlock>
}
80103a15:	83 c4 10             	add    $0x10,%esp
80103a18:	c9                   	leave  
80103a19:	c3                   	ret    
80103a1a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80103a20 <mycpu>:
{
80103a20:	55                   	push   %ebp
80103a21:	89 e5                	mov    %esp,%ebp
80103a23:	56                   	push   %esi
80103a24:	53                   	push   %ebx
  asm volatile("pushfl; popl %0" : "=r" (eflags));
80103a25:	9c                   	pushf  
80103a26:	58                   	pop    %eax
  if(readeflags()&FL_IF)
80103a27:	f6 c4 02             	test   $0x2,%ah
80103a2a:	75 5e                	jne    80103a8a <mycpu+0x6a>
  apicid = lapicid();
80103a2c:	e8 df ef ff ff       	call   80102a10 <lapicid>
  for (i = 0; i < ncpu; ++i) {
80103a31:	8b 35 00 2d 11 80    	mov    0x80112d00,%esi
80103a37:	85 f6                	test   %esi,%esi
80103a39:	7e 42                	jle    80103a7d <mycpu+0x5d>
    if (cpus[i].apicid == apicid)
80103a3b:	0f b6 15 80 27 11 80 	movzbl 0x80112780,%edx
80103a42:	39 d0                	cmp    %edx,%eax
80103a44:	74 30                	je     80103a76 <mycpu+0x56>
80103a46:	b9 30 28 11 80       	mov    $0x80112830,%ecx
  for (i = 0; i < ncpu; ++i) {
80103a4b:	31 d2                	xor    %edx,%edx
80103a4d:	8d 76 00             	lea    0x0(%esi),%esi
80103a50:	83 c2 01             	add    $0x1,%edx
80103a53:	39 f2                	cmp    %esi,%edx
80103a55:	74 26                	je     80103a7d <mycpu+0x5d>
    if (cpus[i].apicid == apicid)
80103a57:	0f b6 19             	movzbl (%ecx),%ebx
80103a5a:	81 c1 b0 00 00 00    	add    $0xb0,%ecx
80103a60:	39 c3                	cmp    %eax,%ebx
80103a62:	75 ec                	jne    80103a50 <mycpu+0x30>
80103a64:	69 c2 b0 00 00 00    	imul   $0xb0,%edx,%eax
80103a6a:	05 80 27 11 80       	add    $0x80112780,%eax
}
80103a6f:	8d 65 f8             	lea    -0x8(%ebp),%esp
80103a72:	5b                   	pop    %ebx
80103a73:	5e                   	pop    %esi
80103a74:	5d                   	pop    %ebp
80103a75:	c3                   	ret    
    if (cpus[i].apicid == apicid)
80103a76:	b8 80 27 11 80       	mov    $0x80112780,%eax
      return &cpus[i];
80103a7b:	eb f2                	jmp    80103a6f <mycpu+0x4f>
  panic("unknown apicid\n");
80103a7d:	83 ec 0c             	sub    $0xc,%esp
80103a80:	68 fc 76 10 80       	push   $0x801076fc
80103a85:	e8 06 c9 ff ff       	call   80100390 <panic>
    panic("mycpu called with interrupts enabled\n");
80103a8a:	83 ec 0c             	sub    $0xc,%esp
80103a8d:	68 d8 77 10 80       	push   $0x801077d8
80103a92:	e8 f9 c8 ff ff       	call   80100390 <panic>
80103a97:	89 f6                	mov    %esi,%esi
80103a99:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80103aa0 <cpuid>:
cpuid() {
80103aa0:	55                   	push   %ebp
80103aa1:	89 e5                	mov    %esp,%ebp
80103aa3:	83 ec 08             	sub    $0x8,%esp
  return mycpu()-cpus;
80103aa6:	e8 75 ff ff ff       	call   80103a20 <mycpu>
80103aab:	2d 80 27 11 80       	sub    $0x80112780,%eax
}
80103ab0:	c9                   	leave  
  return mycpu()-cpus;
80103ab1:	c1 f8 04             	sar    $0x4,%eax
80103ab4:	69 c0 a3 8b 2e ba    	imul   $0xba2e8ba3,%eax,%eax
}
80103aba:	c3                   	ret    
80103abb:	90                   	nop
80103abc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80103ac0 <myproc>:
myproc(void) {
80103ac0:	55                   	push   %ebp
80103ac1:	89 e5                	mov    %esp,%ebp
80103ac3:	53                   	push   %ebx
80103ac4:	83 ec 04             	sub    $0x4,%esp
  pushcli();
80103ac7:	e8 84 0a 00 00       	call   80104550 <pushcli>
  c = mycpu();
80103acc:	e8 4f ff ff ff       	call   80103a20 <mycpu>
  p = c->proc;
80103ad1:	8b 98 ac 00 00 00    	mov    0xac(%eax),%ebx
  popcli();
80103ad7:	e8 b4 0a 00 00       	call   80104590 <popcli>
}
80103adc:	83 c4 04             	add    $0x4,%esp
80103adf:	89 d8                	mov    %ebx,%eax
80103ae1:	5b                   	pop    %ebx
80103ae2:	5d                   	pop    %ebp
80103ae3:	c3                   	ret    
80103ae4:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80103aea:	8d bf 00 00 00 00    	lea    0x0(%edi),%edi

80103af0 <userinit>:
{
80103af0:	55                   	push   %ebp
80103af1:	89 e5                	mov    %esp,%ebp
80103af3:	53                   	push   %ebx
80103af4:	83 ec 04             	sub    $0x4,%esp
  p = allocproc();
80103af7:	e8 e4 fd ff ff       	call   801038e0 <allocproc>
80103afc:	89 c3                	mov    %eax,%ebx
  initproc = p;
80103afe:	a3 b8 a5 10 80       	mov    %eax,0x8010a5b8
  if((p->pgdir = setupkvm()) == 0)
80103b03:	e8 e8 33 00 00       	call   80106ef0 <setupkvm>
80103b08:	85 c0                	test   %eax,%eax
80103b0a:	89 43 04             	mov    %eax,0x4(%ebx)
80103b0d:	0f 84 bd 00 00 00    	je     80103bd0 <userinit+0xe0>
  inituvm(p->pgdir, _binary_initcode_start, (int)_binary_initcode_size);
80103b13:	83 ec 04             	sub    $0x4,%esp
80103b16:	68 2c 00 00 00       	push   $0x2c
80103b1b:	68 60 a4 10 80       	push   $0x8010a460
80103b20:	50                   	push   %eax
80103b21:	e8 aa 30 00 00       	call   80106bd0 <inituvm>
  memset(p->tf, 0, sizeof(*p->tf));
80103b26:	83 c4 0c             	add    $0xc,%esp
  p->sz = PGSIZE;
80103b29:	c7 03 00 10 00 00    	movl   $0x1000,(%ebx)
  memset(p->tf, 0, sizeof(*p->tf));
80103b2f:	6a 4c                	push   $0x4c
80103b31:	6a 00                	push   $0x0
80103b33:	ff 73 18             	pushl  0x18(%ebx)
80103b36:	e8 f5 0b 00 00       	call   80104730 <memset>
  p->tf->cs = (SEG_UCODE << 3) | DPL_USER;
80103b3b:	8b 43 18             	mov    0x18(%ebx),%eax
80103b3e:	ba 1b 00 00 00       	mov    $0x1b,%edx
  p->tf->ds = (SEG_UDATA << 3) | DPL_USER;
80103b43:	b9 23 00 00 00       	mov    $0x23,%ecx
  safestrcpy(p->name, "initcode", sizeof(p->name));
80103b48:	83 c4 0c             	add    $0xc,%esp
  p->tf->cs = (SEG_UCODE << 3) | DPL_USER;
80103b4b:	66 89 50 3c          	mov    %dx,0x3c(%eax)
  p->tf->ds = (SEG_UDATA << 3) | DPL_USER;
80103b4f:	8b 43 18             	mov    0x18(%ebx),%eax
80103b52:	66 89 48 2c          	mov    %cx,0x2c(%eax)
  p->tf->es = p->tf->ds;
80103b56:	8b 43 18             	mov    0x18(%ebx),%eax
80103b59:	0f b7 50 2c          	movzwl 0x2c(%eax),%edx
80103b5d:	66 89 50 28          	mov    %dx,0x28(%eax)
  p->tf->ss = p->tf->ds;
80103b61:	8b 43 18             	mov    0x18(%ebx),%eax
80103b64:	0f b7 50 2c          	movzwl 0x2c(%eax),%edx
80103b68:	66 89 50 48          	mov    %dx,0x48(%eax)
  p->tf->eflags = FL_IF;
80103b6c:	8b 43 18             	mov    0x18(%ebx),%eax
80103b6f:	c7 40 40 00 02 00 00 	movl   $0x200,0x40(%eax)
  p->tf->esp = PGSIZE;
80103b76:	8b 43 18             	mov    0x18(%ebx),%eax
80103b79:	c7 40 44 00 10 00 00 	movl   $0x1000,0x44(%eax)
  p->tf->eip = 0;  // beginning of initcode.S
80103b80:	8b 43 18             	mov    0x18(%ebx),%eax
80103b83:	c7 40 38 00 00 00 00 	movl   $0x0,0x38(%eax)
  safestrcpy(p->name, "initcode", sizeof(p->name));
80103b8a:	8d 43 6c             	lea    0x6c(%ebx),%eax
80103b8d:	6a 10                	push   $0x10
80103b8f:	68 25 77 10 80       	push   $0x80107725
80103b94:	50                   	push   %eax
80103b95:	e8 76 0d 00 00       	call   80104910 <safestrcpy>
  p->cwd = namei("/");
80103b9a:	c7 04 24 2e 77 10 80 	movl   $0x8010772e,(%esp)
80103ba1:	e8 1a e6 ff ff       	call   801021c0 <namei>
80103ba6:	89 43 68             	mov    %eax,0x68(%ebx)
  acquire(&ptable.lock);
80103ba9:	c7 04 24 20 2d 11 80 	movl   $0x80112d20,(%esp)
80103bb0:	e8 6b 0a 00 00       	call   80104620 <acquire>
  p->state = RUNNABLE;
80103bb5:	c7 43 0c 03 00 00 00 	movl   $0x3,0xc(%ebx)
  release(&ptable.lock);
80103bbc:	c7 04 24 20 2d 11 80 	movl   $0x80112d20,(%esp)
80103bc3:	e8 18 0b 00 00       	call   801046e0 <release>
}
80103bc8:	83 c4 10             	add    $0x10,%esp
80103bcb:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80103bce:	c9                   	leave  
80103bcf:	c3                   	ret    
    panic("userinit: out of memory?");
80103bd0:	83 ec 0c             	sub    $0xc,%esp
80103bd3:	68 0c 77 10 80       	push   $0x8010770c
80103bd8:	e8 b3 c7 ff ff       	call   80100390 <panic>
80103bdd:	8d 76 00             	lea    0x0(%esi),%esi

80103be0 <growproc>:
{
80103be0:	55                   	push   %ebp
80103be1:	89 e5                	mov    %esp,%ebp
80103be3:	56                   	push   %esi
80103be4:	53                   	push   %ebx
80103be5:	8b 75 08             	mov    0x8(%ebp),%esi
  pushcli();
80103be8:	e8 63 09 00 00       	call   80104550 <pushcli>
  c = mycpu();
80103bed:	e8 2e fe ff ff       	call   80103a20 <mycpu>
  p = c->proc;
80103bf2:	8b 98 ac 00 00 00    	mov    0xac(%eax),%ebx
  popcli();
80103bf8:	e8 93 09 00 00       	call   80104590 <popcli>
  if(n > 0){
80103bfd:	83 fe 00             	cmp    $0x0,%esi
  sz = curproc->sz;
80103c00:	8b 03                	mov    (%ebx),%eax
  if(n > 0){
80103c02:	7f 1c                	jg     80103c20 <growproc+0x40>
  } else if(n < 0){
80103c04:	75 3a                	jne    80103c40 <growproc+0x60>
  switchuvm(curproc);
80103c06:	83 ec 0c             	sub    $0xc,%esp
  curproc->sz = sz;
80103c09:	89 03                	mov    %eax,(%ebx)
  switchuvm(curproc);
80103c0b:	53                   	push   %ebx
80103c0c:	e8 af 2e 00 00       	call   80106ac0 <switchuvm>
  return 0;
80103c11:	83 c4 10             	add    $0x10,%esp
80103c14:	31 c0                	xor    %eax,%eax
}
80103c16:	8d 65 f8             	lea    -0x8(%ebp),%esp
80103c19:	5b                   	pop    %ebx
80103c1a:	5e                   	pop    %esi
80103c1b:	5d                   	pop    %ebp
80103c1c:	c3                   	ret    
80103c1d:	8d 76 00             	lea    0x0(%esi),%esi
    if((sz = allocuvm(curproc->pgdir, sz, sz + n)) == 0)
80103c20:	83 ec 04             	sub    $0x4,%esp
80103c23:	01 c6                	add    %eax,%esi
80103c25:	56                   	push   %esi
80103c26:	50                   	push   %eax
80103c27:	ff 73 04             	pushl  0x4(%ebx)
80103c2a:	e8 e1 30 00 00       	call   80106d10 <allocuvm>
80103c2f:	83 c4 10             	add    $0x10,%esp
80103c32:	85 c0                	test   %eax,%eax
80103c34:	75 d0                	jne    80103c06 <growproc+0x26>
      return -1;
80103c36:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80103c3b:	eb d9                	jmp    80103c16 <growproc+0x36>
80103c3d:	8d 76 00             	lea    0x0(%esi),%esi
    if((sz = deallocuvm(curproc->pgdir, sz, sz + n)) == 0)
80103c40:	83 ec 04             	sub    $0x4,%esp
80103c43:	01 c6                	add    %eax,%esi
80103c45:	56                   	push   %esi
80103c46:	50                   	push   %eax
80103c47:	ff 73 04             	pushl  0x4(%ebx)
80103c4a:	e8 f1 31 00 00       	call   80106e40 <deallocuvm>
80103c4f:	83 c4 10             	add    $0x10,%esp
80103c52:	85 c0                	test   %eax,%eax
80103c54:	75 b0                	jne    80103c06 <growproc+0x26>
80103c56:	eb de                	jmp    80103c36 <growproc+0x56>
80103c58:	90                   	nop
80103c59:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80103c60 <fork>:
{
80103c60:	55                   	push   %ebp
80103c61:	89 e5                	mov    %esp,%ebp
80103c63:	57                   	push   %edi
80103c64:	56                   	push   %esi
80103c65:	53                   	push   %ebx
80103c66:	83 ec 1c             	sub    $0x1c,%esp
  pushcli();
80103c69:	e8 e2 08 00 00       	call   80104550 <pushcli>
  c = mycpu();
80103c6e:	e8 ad fd ff ff       	call   80103a20 <mycpu>
  p = c->proc;
80103c73:	8b 98 ac 00 00 00    	mov    0xac(%eax),%ebx
  popcli();
80103c79:	e8 12 09 00 00       	call   80104590 <popcli>
  if((np = allocproc()) == 0){
80103c7e:	e8 5d fc ff ff       	call   801038e0 <allocproc>
80103c83:	85 c0                	test   %eax,%eax
80103c85:	89 45 e4             	mov    %eax,-0x1c(%ebp)
80103c88:	0f 84 b7 00 00 00    	je     80103d45 <fork+0xe5>
  if((np->pgdir = copyuvm(curproc->pgdir, curproc->sz)) == 0){
80103c8e:	83 ec 08             	sub    $0x8,%esp
80103c91:	ff 33                	pushl  (%ebx)
80103c93:	ff 73 04             	pushl  0x4(%ebx)
80103c96:	89 c7                	mov    %eax,%edi
80103c98:	e8 23 33 00 00       	call   80106fc0 <copyuvm>
80103c9d:	83 c4 10             	add    $0x10,%esp
80103ca0:	85 c0                	test   %eax,%eax
80103ca2:	89 47 04             	mov    %eax,0x4(%edi)
80103ca5:	0f 84 a1 00 00 00    	je     80103d4c <fork+0xec>
  np->sz = curproc->sz;
80103cab:	8b 03                	mov    (%ebx),%eax
80103cad:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
80103cb0:	89 01                	mov    %eax,(%ecx)
  np->parent = curproc;
80103cb2:	89 59 14             	mov    %ebx,0x14(%ecx)
80103cb5:	89 c8                	mov    %ecx,%eax
  *np->tf = *curproc->tf;
80103cb7:	8b 79 18             	mov    0x18(%ecx),%edi
80103cba:	8b 73 18             	mov    0x18(%ebx),%esi
80103cbd:	b9 13 00 00 00       	mov    $0x13,%ecx
80103cc2:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  for(i = 0; i < NOFILE; i++)
80103cc4:	31 f6                	xor    %esi,%esi
  np->tf->eax = 0;
80103cc6:	8b 40 18             	mov    0x18(%eax),%eax
80103cc9:	c7 40 1c 00 00 00 00 	movl   $0x0,0x1c(%eax)
    if(curproc->ofile[i])
80103cd0:	8b 44 b3 28          	mov    0x28(%ebx,%esi,4),%eax
80103cd4:	85 c0                	test   %eax,%eax
80103cd6:	74 13                	je     80103ceb <fork+0x8b>
      np->ofile[i] = filedup(curproc->ofile[i]);
80103cd8:	83 ec 0c             	sub    $0xc,%esp
80103cdb:	50                   	push   %eax
80103cdc:	e8 ef d3 ff ff       	call   801010d0 <filedup>
80103ce1:	8b 55 e4             	mov    -0x1c(%ebp),%edx
80103ce4:	83 c4 10             	add    $0x10,%esp
80103ce7:	89 44 b2 28          	mov    %eax,0x28(%edx,%esi,4)
  for(i = 0; i < NOFILE; i++)
80103ceb:	83 c6 01             	add    $0x1,%esi
80103cee:	83 fe 10             	cmp    $0x10,%esi
80103cf1:	75 dd                	jne    80103cd0 <fork+0x70>
  np->cwd = idup(curproc->cwd);
80103cf3:	83 ec 0c             	sub    $0xc,%esp
80103cf6:	ff 73 68             	pushl  0x68(%ebx)
  safestrcpy(np->name, curproc->name, sizeof(curproc->name));
80103cf9:	83 c3 6c             	add    $0x6c,%ebx
  np->cwd = idup(curproc->cwd);
80103cfc:	e8 2f dc ff ff       	call   80101930 <idup>
80103d01:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  safestrcpy(np->name, curproc->name, sizeof(curproc->name));
80103d04:	83 c4 0c             	add    $0xc,%esp
  np->cwd = idup(curproc->cwd);
80103d07:	89 47 68             	mov    %eax,0x68(%edi)
  safestrcpy(np->name, curproc->name, sizeof(curproc->name));
80103d0a:	8d 47 6c             	lea    0x6c(%edi),%eax
80103d0d:	6a 10                	push   $0x10
80103d0f:	53                   	push   %ebx
80103d10:	50                   	push   %eax
80103d11:	e8 fa 0b 00 00       	call   80104910 <safestrcpy>
  pid = np->pid;
80103d16:	8b 5f 10             	mov    0x10(%edi),%ebx
  acquire(&ptable.lock);
80103d19:	c7 04 24 20 2d 11 80 	movl   $0x80112d20,(%esp)
80103d20:	e8 fb 08 00 00       	call   80104620 <acquire>
  np->state = RUNNABLE;
80103d25:	c7 47 0c 03 00 00 00 	movl   $0x3,0xc(%edi)
  release(&ptable.lock);
80103d2c:	c7 04 24 20 2d 11 80 	movl   $0x80112d20,(%esp)
80103d33:	e8 a8 09 00 00       	call   801046e0 <release>
  return pid;
80103d38:	83 c4 10             	add    $0x10,%esp
}
80103d3b:	8d 65 f4             	lea    -0xc(%ebp),%esp
80103d3e:	89 d8                	mov    %ebx,%eax
80103d40:	5b                   	pop    %ebx
80103d41:	5e                   	pop    %esi
80103d42:	5f                   	pop    %edi
80103d43:	5d                   	pop    %ebp
80103d44:	c3                   	ret    
    return -1;
80103d45:	bb ff ff ff ff       	mov    $0xffffffff,%ebx
80103d4a:	eb ef                	jmp    80103d3b <fork+0xdb>
    kfree(np->kstack);
80103d4c:	8b 5d e4             	mov    -0x1c(%ebp),%ebx
80103d4f:	83 ec 0c             	sub    $0xc,%esp
80103d52:	ff 73 08             	pushl  0x8(%ebx)
80103d55:	e8 96 e8 ff ff       	call   801025f0 <kfree>
    np->kstack = 0;
80103d5a:	c7 43 08 00 00 00 00 	movl   $0x0,0x8(%ebx)
    np->state = UNUSED;
80103d61:	c7 43 0c 00 00 00 00 	movl   $0x0,0xc(%ebx)
    return -1;
80103d68:	83 c4 10             	add    $0x10,%esp
80103d6b:	bb ff ff ff ff       	mov    $0xffffffff,%ebx
80103d70:	eb c9                	jmp    80103d3b <fork+0xdb>
80103d72:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80103d79:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80103d80 <scheduler>:
{
80103d80:	55                   	push   %ebp
80103d81:	89 e5                	mov    %esp,%ebp
80103d83:	57                   	push   %edi
80103d84:	56                   	push   %esi
80103d85:	53                   	push   %ebx
80103d86:	83 ec 0c             	sub    $0xc,%esp
  struct cpu *c = mycpu();
80103d89:	e8 92 fc ff ff       	call   80103a20 <mycpu>
80103d8e:	8d 78 04             	lea    0x4(%eax),%edi
80103d91:	89 c6                	mov    %eax,%esi
  c->proc = 0;
80103d93:	c7 80 ac 00 00 00 00 	movl   $0x0,0xac(%eax)
80103d9a:	00 00 00 
80103d9d:	8d 76 00             	lea    0x0(%esi),%esi
  asm volatile("sti");
80103da0:	fb                   	sti    
    acquire(&ptable.lock);
80103da1:	83 ec 0c             	sub    $0xc,%esp
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80103da4:	bb 54 2d 11 80       	mov    $0x80112d54,%ebx
    acquire(&ptable.lock);
80103da9:	68 20 2d 11 80       	push   $0x80112d20
80103dae:	e8 6d 08 00 00       	call   80104620 <acquire>
80103db3:	83 c4 10             	add    $0x10,%esp
80103db6:	8d 76 00             	lea    0x0(%esi),%esi
80103db9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
      if(p->state != RUNNABLE)
80103dc0:	83 7b 0c 03          	cmpl   $0x3,0xc(%ebx)
80103dc4:	75 33                	jne    80103df9 <scheduler+0x79>
      switchuvm(p);
80103dc6:	83 ec 0c             	sub    $0xc,%esp
      c->proc = p;
80103dc9:	89 9e ac 00 00 00    	mov    %ebx,0xac(%esi)
      switchuvm(p);
80103dcf:	53                   	push   %ebx
80103dd0:	e8 eb 2c 00 00       	call   80106ac0 <switchuvm>
      swtch(&(c->scheduler), p->context);
80103dd5:	58                   	pop    %eax
80103dd6:	5a                   	pop    %edx
80103dd7:	ff 73 1c             	pushl  0x1c(%ebx)
80103dda:	57                   	push   %edi
      p->state = RUNNING;
80103ddb:	c7 43 0c 04 00 00 00 	movl   $0x4,0xc(%ebx)
      swtch(&(c->scheduler), p->context);
80103de2:	e8 84 0b 00 00       	call   8010496b <swtch>
      switchkvm();
80103de7:	e8 b4 2c 00 00       	call   80106aa0 <switchkvm>
      c->proc = 0;
80103dec:	c7 86 ac 00 00 00 00 	movl   $0x0,0xac(%esi)
80103df3:	00 00 00 
80103df6:	83 c4 10             	add    $0x10,%esp
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80103df9:	83 c3 7c             	add    $0x7c,%ebx
80103dfc:	81 fb 54 4c 11 80    	cmp    $0x80114c54,%ebx
80103e02:	72 bc                	jb     80103dc0 <scheduler+0x40>
    release(&ptable.lock);
80103e04:	83 ec 0c             	sub    $0xc,%esp
80103e07:	68 20 2d 11 80       	push   $0x80112d20
80103e0c:	e8 cf 08 00 00       	call   801046e0 <release>
    sti();
80103e11:	83 c4 10             	add    $0x10,%esp
80103e14:	eb 8a                	jmp    80103da0 <scheduler+0x20>
80103e16:	8d 76 00             	lea    0x0(%esi),%esi
80103e19:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80103e20 <sched>:
{
80103e20:	55                   	push   %ebp
80103e21:	89 e5                	mov    %esp,%ebp
80103e23:	56                   	push   %esi
80103e24:	53                   	push   %ebx
  pushcli();
80103e25:	e8 26 07 00 00       	call   80104550 <pushcli>
  c = mycpu();
80103e2a:	e8 f1 fb ff ff       	call   80103a20 <mycpu>
  p = c->proc;
80103e2f:	8b 98 ac 00 00 00    	mov    0xac(%eax),%ebx
  popcli();
80103e35:	e8 56 07 00 00       	call   80104590 <popcli>
  if(!holding(&ptable.lock))
80103e3a:	83 ec 0c             	sub    $0xc,%esp
80103e3d:	68 20 2d 11 80       	push   $0x80112d20
80103e42:	e8 a9 07 00 00       	call   801045f0 <holding>
80103e47:	83 c4 10             	add    $0x10,%esp
80103e4a:	85 c0                	test   %eax,%eax
80103e4c:	74 4f                	je     80103e9d <sched+0x7d>
  if(mycpu()->ncli != 1)
80103e4e:	e8 cd fb ff ff       	call   80103a20 <mycpu>
80103e53:	83 b8 a4 00 00 00 01 	cmpl   $0x1,0xa4(%eax)
80103e5a:	75 68                	jne    80103ec4 <sched+0xa4>
  if(p->state == RUNNING)
80103e5c:	83 7b 0c 04          	cmpl   $0x4,0xc(%ebx)
80103e60:	74 55                	je     80103eb7 <sched+0x97>
  asm volatile("pushfl; popl %0" : "=r" (eflags));
80103e62:	9c                   	pushf  
80103e63:	58                   	pop    %eax
  if(readeflags()&FL_IF)
80103e64:	f6 c4 02             	test   $0x2,%ah
80103e67:	75 41                	jne    80103eaa <sched+0x8a>
  intena = mycpu()->intena;
80103e69:	e8 b2 fb ff ff       	call   80103a20 <mycpu>
  swtch(&p->context, mycpu()->scheduler);
80103e6e:	83 c3 1c             	add    $0x1c,%ebx
  intena = mycpu()->intena;
80103e71:	8b b0 a8 00 00 00    	mov    0xa8(%eax),%esi
  swtch(&p->context, mycpu()->scheduler);
80103e77:	e8 a4 fb ff ff       	call   80103a20 <mycpu>
80103e7c:	83 ec 08             	sub    $0x8,%esp
80103e7f:	ff 70 04             	pushl  0x4(%eax)
80103e82:	53                   	push   %ebx
80103e83:	e8 e3 0a 00 00       	call   8010496b <swtch>
  mycpu()->intena = intena;
80103e88:	e8 93 fb ff ff       	call   80103a20 <mycpu>
}
80103e8d:	83 c4 10             	add    $0x10,%esp
  mycpu()->intena = intena;
80103e90:	89 b0 a8 00 00 00    	mov    %esi,0xa8(%eax)
}
80103e96:	8d 65 f8             	lea    -0x8(%ebp),%esp
80103e99:	5b                   	pop    %ebx
80103e9a:	5e                   	pop    %esi
80103e9b:	5d                   	pop    %ebp
80103e9c:	c3                   	ret    
    panic("sched ptable.lock");
80103e9d:	83 ec 0c             	sub    $0xc,%esp
80103ea0:	68 30 77 10 80       	push   $0x80107730
80103ea5:	e8 e6 c4 ff ff       	call   80100390 <panic>
    panic("sched interruptible");
80103eaa:	83 ec 0c             	sub    $0xc,%esp
80103ead:	68 5c 77 10 80       	push   $0x8010775c
80103eb2:	e8 d9 c4 ff ff       	call   80100390 <panic>
    panic("sched running");
80103eb7:	83 ec 0c             	sub    $0xc,%esp
80103eba:	68 4e 77 10 80       	push   $0x8010774e
80103ebf:	e8 cc c4 ff ff       	call   80100390 <panic>
    panic("sched locks");
80103ec4:	83 ec 0c             	sub    $0xc,%esp
80103ec7:	68 42 77 10 80       	push   $0x80107742
80103ecc:	e8 bf c4 ff ff       	call   80100390 <panic>
80103ed1:	eb 0d                	jmp    80103ee0 <exit>
80103ed3:	90                   	nop
80103ed4:	90                   	nop
80103ed5:	90                   	nop
80103ed6:	90                   	nop
80103ed7:	90                   	nop
80103ed8:	90                   	nop
80103ed9:	90                   	nop
80103eda:	90                   	nop
80103edb:	90                   	nop
80103edc:	90                   	nop
80103edd:	90                   	nop
80103ede:	90                   	nop
80103edf:	90                   	nop

80103ee0 <exit>:
{
80103ee0:	55                   	push   %ebp
80103ee1:	89 e5                	mov    %esp,%ebp
80103ee3:	57                   	push   %edi
80103ee4:	56                   	push   %esi
80103ee5:	53                   	push   %ebx
80103ee6:	83 ec 0c             	sub    $0xc,%esp
  pushcli();
80103ee9:	e8 62 06 00 00       	call   80104550 <pushcli>
  c = mycpu();
80103eee:	e8 2d fb ff ff       	call   80103a20 <mycpu>
  p = c->proc;
80103ef3:	8b b0 ac 00 00 00    	mov    0xac(%eax),%esi
  popcli();
80103ef9:	e8 92 06 00 00       	call   80104590 <popcli>
  if(curproc == initproc)
80103efe:	39 35 b8 a5 10 80    	cmp    %esi,0x8010a5b8
80103f04:	8d 5e 28             	lea    0x28(%esi),%ebx
80103f07:	8d 7e 68             	lea    0x68(%esi),%edi
80103f0a:	0f 84 e7 00 00 00    	je     80103ff7 <exit+0x117>
    if(curproc->ofile[fd]){
80103f10:	8b 03                	mov    (%ebx),%eax
80103f12:	85 c0                	test   %eax,%eax
80103f14:	74 12                	je     80103f28 <exit+0x48>
      fileclose(curproc->ofile[fd]);
80103f16:	83 ec 0c             	sub    $0xc,%esp
80103f19:	50                   	push   %eax
80103f1a:	e8 01 d2 ff ff       	call   80101120 <fileclose>
      curproc->ofile[fd] = 0;
80103f1f:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
80103f25:	83 c4 10             	add    $0x10,%esp
80103f28:	83 c3 04             	add    $0x4,%ebx
  for(fd = 0; fd < NOFILE; fd++){
80103f2b:	39 fb                	cmp    %edi,%ebx
80103f2d:	75 e1                	jne    80103f10 <exit+0x30>
  begin_op();
80103f2f:	e8 4c ef ff ff       	call   80102e80 <begin_op>
  iput(curproc->cwd);
80103f34:	83 ec 0c             	sub    $0xc,%esp
80103f37:	ff 76 68             	pushl  0x68(%esi)
80103f3a:	e8 51 db ff ff       	call   80101a90 <iput>
  end_op();
80103f3f:	e8 ac ef ff ff       	call   80102ef0 <end_op>
  curproc->cwd = 0;
80103f44:	c7 46 68 00 00 00 00 	movl   $0x0,0x68(%esi)
  acquire(&ptable.lock);
80103f4b:	c7 04 24 20 2d 11 80 	movl   $0x80112d20,(%esp)
80103f52:	e8 c9 06 00 00       	call   80104620 <acquire>
  wakeup1(curproc->parent);
80103f57:	8b 56 14             	mov    0x14(%esi),%edx
80103f5a:	83 c4 10             	add    $0x10,%esp
static void
wakeup1(void *chan)
{
  struct proc *p;

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
80103f5d:	b8 54 2d 11 80       	mov    $0x80112d54,%eax
80103f62:	eb 0e                	jmp    80103f72 <exit+0x92>
80103f64:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80103f68:	83 c0 7c             	add    $0x7c,%eax
80103f6b:	3d 54 4c 11 80       	cmp    $0x80114c54,%eax
80103f70:	73 1c                	jae    80103f8e <exit+0xae>
    if(p->state == SLEEPING && p->chan == chan)
80103f72:	83 78 0c 02          	cmpl   $0x2,0xc(%eax)
80103f76:	75 f0                	jne    80103f68 <exit+0x88>
80103f78:	3b 50 20             	cmp    0x20(%eax),%edx
80103f7b:	75 eb                	jne    80103f68 <exit+0x88>
      p->state = RUNNABLE;
80103f7d:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
80103f84:	83 c0 7c             	add    $0x7c,%eax
80103f87:	3d 54 4c 11 80       	cmp    $0x80114c54,%eax
80103f8c:	72 e4                	jb     80103f72 <exit+0x92>
      p->parent = initproc;
80103f8e:	8b 0d b8 a5 10 80    	mov    0x8010a5b8,%ecx
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80103f94:	ba 54 2d 11 80       	mov    $0x80112d54,%edx
80103f99:	eb 10                	jmp    80103fab <exit+0xcb>
80103f9b:	90                   	nop
80103f9c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80103fa0:	83 c2 7c             	add    $0x7c,%edx
80103fa3:	81 fa 54 4c 11 80    	cmp    $0x80114c54,%edx
80103fa9:	73 33                	jae    80103fde <exit+0xfe>
    if(p->parent == curproc){
80103fab:	39 72 14             	cmp    %esi,0x14(%edx)
80103fae:	75 f0                	jne    80103fa0 <exit+0xc0>
      if(p->state == ZOMBIE)
80103fb0:	83 7a 0c 05          	cmpl   $0x5,0xc(%edx)
      p->parent = initproc;
80103fb4:	89 4a 14             	mov    %ecx,0x14(%edx)
      if(p->state == ZOMBIE)
80103fb7:	75 e7                	jne    80103fa0 <exit+0xc0>
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
80103fb9:	b8 54 2d 11 80       	mov    $0x80112d54,%eax
80103fbe:	eb 0a                	jmp    80103fca <exit+0xea>
80103fc0:	83 c0 7c             	add    $0x7c,%eax
80103fc3:	3d 54 4c 11 80       	cmp    $0x80114c54,%eax
80103fc8:	73 d6                	jae    80103fa0 <exit+0xc0>
    if(p->state == SLEEPING && p->chan == chan)
80103fca:	83 78 0c 02          	cmpl   $0x2,0xc(%eax)
80103fce:	75 f0                	jne    80103fc0 <exit+0xe0>
80103fd0:	3b 48 20             	cmp    0x20(%eax),%ecx
80103fd3:	75 eb                	jne    80103fc0 <exit+0xe0>
      p->state = RUNNABLE;
80103fd5:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
80103fdc:	eb e2                	jmp    80103fc0 <exit+0xe0>
  curproc->state = ZOMBIE;
80103fde:	c7 46 0c 05 00 00 00 	movl   $0x5,0xc(%esi)
  sched();
80103fe5:	e8 36 fe ff ff       	call   80103e20 <sched>
  panic("zombie exit");
80103fea:	83 ec 0c             	sub    $0xc,%esp
80103fed:	68 7d 77 10 80       	push   $0x8010777d
80103ff2:	e8 99 c3 ff ff       	call   80100390 <panic>
    panic("init exiting");
80103ff7:	83 ec 0c             	sub    $0xc,%esp
80103ffa:	68 70 77 10 80       	push   $0x80107770
80103fff:	e8 8c c3 ff ff       	call   80100390 <panic>
80104004:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
8010400a:	8d bf 00 00 00 00    	lea    0x0(%edi),%edi

80104010 <yield>:
{
80104010:	55                   	push   %ebp
80104011:	89 e5                	mov    %esp,%ebp
80104013:	53                   	push   %ebx
80104014:	83 ec 10             	sub    $0x10,%esp
  acquire(&ptable.lock);  //DOC: yieldlock
80104017:	68 20 2d 11 80       	push   $0x80112d20
8010401c:	e8 ff 05 00 00       	call   80104620 <acquire>
  pushcli();
80104021:	e8 2a 05 00 00       	call   80104550 <pushcli>
  c = mycpu();
80104026:	e8 f5 f9 ff ff       	call   80103a20 <mycpu>
  p = c->proc;
8010402b:	8b 98 ac 00 00 00    	mov    0xac(%eax),%ebx
  popcli();
80104031:	e8 5a 05 00 00       	call   80104590 <popcli>
  myproc()->state = RUNNABLE;
80104036:	c7 43 0c 03 00 00 00 	movl   $0x3,0xc(%ebx)
  sched();
8010403d:	e8 de fd ff ff       	call   80103e20 <sched>
  release(&ptable.lock);
80104042:	c7 04 24 20 2d 11 80 	movl   $0x80112d20,(%esp)
80104049:	e8 92 06 00 00       	call   801046e0 <release>
}
8010404e:	83 c4 10             	add    $0x10,%esp
80104051:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80104054:	c9                   	leave  
80104055:	c3                   	ret    
80104056:	8d 76 00             	lea    0x0(%esi),%esi
80104059:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80104060 <sleep>:
{
80104060:	55                   	push   %ebp
80104061:	89 e5                	mov    %esp,%ebp
80104063:	57                   	push   %edi
80104064:	56                   	push   %esi
80104065:	53                   	push   %ebx
80104066:	83 ec 0c             	sub    $0xc,%esp
80104069:	8b 7d 08             	mov    0x8(%ebp),%edi
8010406c:	8b 75 0c             	mov    0xc(%ebp),%esi
  pushcli();
8010406f:	e8 dc 04 00 00       	call   80104550 <pushcli>
  c = mycpu();
80104074:	e8 a7 f9 ff ff       	call   80103a20 <mycpu>
  p = c->proc;
80104079:	8b 98 ac 00 00 00    	mov    0xac(%eax),%ebx
  popcli();
8010407f:	e8 0c 05 00 00       	call   80104590 <popcli>
  if(p == 0)
80104084:	85 db                	test   %ebx,%ebx
80104086:	0f 84 87 00 00 00    	je     80104113 <sleep+0xb3>
  if(lk == 0)
8010408c:	85 f6                	test   %esi,%esi
8010408e:	74 76                	je     80104106 <sleep+0xa6>
  if(lk != &ptable.lock){  //DOC: sleeplock0
80104090:	81 fe 20 2d 11 80    	cmp    $0x80112d20,%esi
80104096:	74 50                	je     801040e8 <sleep+0x88>
    acquire(&ptable.lock);  //DOC: sleeplock1
80104098:	83 ec 0c             	sub    $0xc,%esp
8010409b:	68 20 2d 11 80       	push   $0x80112d20
801040a0:	e8 7b 05 00 00       	call   80104620 <acquire>
    release(lk);
801040a5:	89 34 24             	mov    %esi,(%esp)
801040a8:	e8 33 06 00 00       	call   801046e0 <release>
  p->chan = chan;
801040ad:	89 7b 20             	mov    %edi,0x20(%ebx)
  p->state = SLEEPING;
801040b0:	c7 43 0c 02 00 00 00 	movl   $0x2,0xc(%ebx)
  sched();
801040b7:	e8 64 fd ff ff       	call   80103e20 <sched>
  p->chan = 0;
801040bc:	c7 43 20 00 00 00 00 	movl   $0x0,0x20(%ebx)
    release(&ptable.lock);
801040c3:	c7 04 24 20 2d 11 80 	movl   $0x80112d20,(%esp)
801040ca:	e8 11 06 00 00       	call   801046e0 <release>
    acquire(lk);
801040cf:	89 75 08             	mov    %esi,0x8(%ebp)
801040d2:	83 c4 10             	add    $0x10,%esp
}
801040d5:	8d 65 f4             	lea    -0xc(%ebp),%esp
801040d8:	5b                   	pop    %ebx
801040d9:	5e                   	pop    %esi
801040da:	5f                   	pop    %edi
801040db:	5d                   	pop    %ebp
    acquire(lk);
801040dc:	e9 3f 05 00 00       	jmp    80104620 <acquire>
801040e1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  p->chan = chan;
801040e8:	89 7b 20             	mov    %edi,0x20(%ebx)
  p->state = SLEEPING;
801040eb:	c7 43 0c 02 00 00 00 	movl   $0x2,0xc(%ebx)
  sched();
801040f2:	e8 29 fd ff ff       	call   80103e20 <sched>
  p->chan = 0;
801040f7:	c7 43 20 00 00 00 00 	movl   $0x0,0x20(%ebx)
}
801040fe:	8d 65 f4             	lea    -0xc(%ebp),%esp
80104101:	5b                   	pop    %ebx
80104102:	5e                   	pop    %esi
80104103:	5f                   	pop    %edi
80104104:	5d                   	pop    %ebp
80104105:	c3                   	ret    
    panic("sleep without lk");
80104106:	83 ec 0c             	sub    $0xc,%esp
80104109:	68 8f 77 10 80       	push   $0x8010778f
8010410e:	e8 7d c2 ff ff       	call   80100390 <panic>
    panic("sleep");
80104113:	83 ec 0c             	sub    $0xc,%esp
80104116:	68 89 77 10 80       	push   $0x80107789
8010411b:	e8 70 c2 ff ff       	call   80100390 <panic>

80104120 <wait>:
{
80104120:	55                   	push   %ebp
80104121:	89 e5                	mov    %esp,%ebp
80104123:	56                   	push   %esi
80104124:	53                   	push   %ebx
  pushcli();
80104125:	e8 26 04 00 00       	call   80104550 <pushcli>
  c = mycpu();
8010412a:	e8 f1 f8 ff ff       	call   80103a20 <mycpu>
  p = c->proc;
8010412f:	8b b0 ac 00 00 00    	mov    0xac(%eax),%esi
  popcli();
80104135:	e8 56 04 00 00       	call   80104590 <popcli>
  acquire(&ptable.lock);
8010413a:	83 ec 0c             	sub    $0xc,%esp
8010413d:	68 20 2d 11 80       	push   $0x80112d20
80104142:	e8 d9 04 00 00       	call   80104620 <acquire>
80104147:	83 c4 10             	add    $0x10,%esp
    havekids = 0;
8010414a:	31 c0                	xor    %eax,%eax
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
8010414c:	bb 54 2d 11 80       	mov    $0x80112d54,%ebx
80104151:	eb 10                	jmp    80104163 <wait+0x43>
80104153:	90                   	nop
80104154:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80104158:	83 c3 7c             	add    $0x7c,%ebx
8010415b:	81 fb 54 4c 11 80    	cmp    $0x80114c54,%ebx
80104161:	73 1b                	jae    8010417e <wait+0x5e>
      if(p->parent != curproc)
80104163:	39 73 14             	cmp    %esi,0x14(%ebx)
80104166:	75 f0                	jne    80104158 <wait+0x38>
      if(p->state == ZOMBIE){
80104168:	83 7b 0c 05          	cmpl   $0x5,0xc(%ebx)
8010416c:	74 32                	je     801041a0 <wait+0x80>
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
8010416e:	83 c3 7c             	add    $0x7c,%ebx
      havekids = 1;
80104171:	b8 01 00 00 00       	mov    $0x1,%eax
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80104176:	81 fb 54 4c 11 80    	cmp    $0x80114c54,%ebx
8010417c:	72 e5                	jb     80104163 <wait+0x43>
    if(!havekids || curproc->killed){
8010417e:	85 c0                	test   %eax,%eax
80104180:	74 74                	je     801041f6 <wait+0xd6>
80104182:	8b 46 24             	mov    0x24(%esi),%eax
80104185:	85 c0                	test   %eax,%eax
80104187:	75 6d                	jne    801041f6 <wait+0xd6>
    sleep(curproc, &ptable.lock);  //DOC: wait-sleep
80104189:	83 ec 08             	sub    $0x8,%esp
8010418c:	68 20 2d 11 80       	push   $0x80112d20
80104191:	56                   	push   %esi
80104192:	e8 c9 fe ff ff       	call   80104060 <sleep>
    havekids = 0;
80104197:	83 c4 10             	add    $0x10,%esp
8010419a:	eb ae                	jmp    8010414a <wait+0x2a>
8010419c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
        kfree(p->kstack);
801041a0:	83 ec 0c             	sub    $0xc,%esp
801041a3:	ff 73 08             	pushl  0x8(%ebx)
        pid = p->pid;
801041a6:	8b 73 10             	mov    0x10(%ebx),%esi
        kfree(p->kstack);
801041a9:	e8 42 e4 ff ff       	call   801025f0 <kfree>
        freevm(p->pgdir);
801041ae:	5a                   	pop    %edx
801041af:	ff 73 04             	pushl  0x4(%ebx)
        p->kstack = 0;
801041b2:	c7 43 08 00 00 00 00 	movl   $0x0,0x8(%ebx)
        freevm(p->pgdir);
801041b9:	e8 b2 2c 00 00       	call   80106e70 <freevm>
        release(&ptable.lock);
801041be:	c7 04 24 20 2d 11 80 	movl   $0x80112d20,(%esp)
        p->pid = 0;
801041c5:	c7 43 10 00 00 00 00 	movl   $0x0,0x10(%ebx)
        p->parent = 0;
801041cc:	c7 43 14 00 00 00 00 	movl   $0x0,0x14(%ebx)
        p->name[0] = 0;
801041d3:	c6 43 6c 00          	movb   $0x0,0x6c(%ebx)
        p->killed = 0;
801041d7:	c7 43 24 00 00 00 00 	movl   $0x0,0x24(%ebx)
        p->state = UNUSED;
801041de:	c7 43 0c 00 00 00 00 	movl   $0x0,0xc(%ebx)
        release(&ptable.lock);
801041e5:	e8 f6 04 00 00       	call   801046e0 <release>
        return pid;
801041ea:	83 c4 10             	add    $0x10,%esp
}
801041ed:	8d 65 f8             	lea    -0x8(%ebp),%esp
801041f0:	89 f0                	mov    %esi,%eax
801041f2:	5b                   	pop    %ebx
801041f3:	5e                   	pop    %esi
801041f4:	5d                   	pop    %ebp
801041f5:	c3                   	ret    
      release(&ptable.lock);
801041f6:	83 ec 0c             	sub    $0xc,%esp
      return -1;
801041f9:	be ff ff ff ff       	mov    $0xffffffff,%esi
      release(&ptable.lock);
801041fe:	68 20 2d 11 80       	push   $0x80112d20
80104203:	e8 d8 04 00 00       	call   801046e0 <release>
      return -1;
80104208:	83 c4 10             	add    $0x10,%esp
8010420b:	eb e0                	jmp    801041ed <wait+0xcd>
8010420d:	8d 76 00             	lea    0x0(%esi),%esi

80104210 <wakeup>:
}

// Wake up all processes sleeping on chan.
void
wakeup(void *chan)
{
80104210:	55                   	push   %ebp
80104211:	89 e5                	mov    %esp,%ebp
80104213:	53                   	push   %ebx
80104214:	83 ec 10             	sub    $0x10,%esp
80104217:	8b 5d 08             	mov    0x8(%ebp),%ebx
  acquire(&ptable.lock);
8010421a:	68 20 2d 11 80       	push   $0x80112d20
8010421f:	e8 fc 03 00 00       	call   80104620 <acquire>
80104224:	83 c4 10             	add    $0x10,%esp
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
80104227:	b8 54 2d 11 80       	mov    $0x80112d54,%eax
8010422c:	eb 0c                	jmp    8010423a <wakeup+0x2a>
8010422e:	66 90                	xchg   %ax,%ax
80104230:	83 c0 7c             	add    $0x7c,%eax
80104233:	3d 54 4c 11 80       	cmp    $0x80114c54,%eax
80104238:	73 1c                	jae    80104256 <wakeup+0x46>
    if(p->state == SLEEPING && p->chan == chan)
8010423a:	83 78 0c 02          	cmpl   $0x2,0xc(%eax)
8010423e:	75 f0                	jne    80104230 <wakeup+0x20>
80104240:	3b 58 20             	cmp    0x20(%eax),%ebx
80104243:	75 eb                	jne    80104230 <wakeup+0x20>
      p->state = RUNNABLE;
80104245:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
8010424c:	83 c0 7c             	add    $0x7c,%eax
8010424f:	3d 54 4c 11 80       	cmp    $0x80114c54,%eax
80104254:	72 e4                	jb     8010423a <wakeup+0x2a>
  wakeup1(chan);
  release(&ptable.lock);
80104256:	c7 45 08 20 2d 11 80 	movl   $0x80112d20,0x8(%ebp)
}
8010425d:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80104260:	c9                   	leave  
  release(&ptable.lock);
80104261:	e9 7a 04 00 00       	jmp    801046e0 <release>
80104266:	8d 76 00             	lea    0x0(%esi),%esi
80104269:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80104270 <kill>:
// Kill the process with the given pid.
// Process won't exit until it returns
// to user space (see trap in trap.c).
int
kill(int pid)
{
80104270:	55                   	push   %ebp
80104271:	89 e5                	mov    %esp,%ebp
80104273:	53                   	push   %ebx
80104274:	83 ec 10             	sub    $0x10,%esp
80104277:	8b 5d 08             	mov    0x8(%ebp),%ebx
  struct proc *p;

  acquire(&ptable.lock);
8010427a:	68 20 2d 11 80       	push   $0x80112d20
8010427f:	e8 9c 03 00 00       	call   80104620 <acquire>
80104284:	83 c4 10             	add    $0x10,%esp
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80104287:	b8 54 2d 11 80       	mov    $0x80112d54,%eax
8010428c:	eb 0c                	jmp    8010429a <kill+0x2a>
8010428e:	66 90                	xchg   %ax,%ax
80104290:	83 c0 7c             	add    $0x7c,%eax
80104293:	3d 54 4c 11 80       	cmp    $0x80114c54,%eax
80104298:	73 36                	jae    801042d0 <kill+0x60>
    if(p->pid == pid){
8010429a:	39 58 10             	cmp    %ebx,0x10(%eax)
8010429d:	75 f1                	jne    80104290 <kill+0x20>
      p->killed = 1;
      // Wake process from sleep if necessary.
      if(p->state == SLEEPING)
8010429f:	83 78 0c 02          	cmpl   $0x2,0xc(%eax)
      p->killed = 1;
801042a3:	c7 40 24 01 00 00 00 	movl   $0x1,0x24(%eax)
      if(p->state == SLEEPING)
801042aa:	75 07                	jne    801042b3 <kill+0x43>
        p->state = RUNNABLE;
801042ac:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
      release(&ptable.lock);
801042b3:	83 ec 0c             	sub    $0xc,%esp
801042b6:	68 20 2d 11 80       	push   $0x80112d20
801042bb:	e8 20 04 00 00       	call   801046e0 <release>
      return 0;
801042c0:	83 c4 10             	add    $0x10,%esp
801042c3:	31 c0                	xor    %eax,%eax
    }
  }
  release(&ptable.lock);
  return -1;
}
801042c5:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801042c8:	c9                   	leave  
801042c9:	c3                   	ret    
801042ca:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  release(&ptable.lock);
801042d0:	83 ec 0c             	sub    $0xc,%esp
801042d3:	68 20 2d 11 80       	push   $0x80112d20
801042d8:	e8 03 04 00 00       	call   801046e0 <release>
  return -1;
801042dd:	83 c4 10             	add    $0x10,%esp
801042e0:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
801042e5:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801042e8:	c9                   	leave  
801042e9:	c3                   	ret    
801042ea:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

801042f0 <procdump>:
// Print a process listing to console.  For debugging.
// Runs when user types ^P on console.
// No lock to avoid wedging a stuck machine further.
void
procdump(void)
{
801042f0:	55                   	push   %ebp
801042f1:	89 e5                	mov    %esp,%ebp
801042f3:	57                   	push   %edi
801042f4:	56                   	push   %esi
801042f5:	53                   	push   %ebx
801042f6:	8d 75 e8             	lea    -0x18(%ebp),%esi
  int i;
  struct proc *p;
  char *state;
  uint pc[10];

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
801042f9:	bb 54 2d 11 80       	mov    $0x80112d54,%ebx
{
801042fe:	83 ec 3c             	sub    $0x3c,%esp
80104301:	eb 24                	jmp    80104327 <procdump+0x37>
80104303:	90                   	nop
80104304:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    if(p->state == SLEEPING){
      getcallerpcs((uint*)p->context->ebp+2, pc);
      for(i=0; i<10 && pc[i] != 0; i++)
        cprintf(" %p", pc[i]);
    }
    cprintf("\n");
80104308:	83 ec 0c             	sub    $0xc,%esp
8010430b:	68 17 7b 10 80       	push   $0x80107b17
80104310:	e8 7b c4 ff ff       	call   80100790 <cprintf>
80104315:	83 c4 10             	add    $0x10,%esp
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80104318:	83 c3 7c             	add    $0x7c,%ebx
8010431b:	81 fb 54 4c 11 80    	cmp    $0x80114c54,%ebx
80104321:	0f 83 81 00 00 00    	jae    801043a8 <procdump+0xb8>
    if(p->state == UNUSED)
80104327:	8b 43 0c             	mov    0xc(%ebx),%eax
8010432a:	85 c0                	test   %eax,%eax
8010432c:	74 ea                	je     80104318 <procdump+0x28>
    if(p->state >= 0 && p->state < NELEM(states) && states[p->state])
8010432e:	83 f8 05             	cmp    $0x5,%eax
      state = "???";
80104331:	ba a0 77 10 80       	mov    $0x801077a0,%edx
    if(p->state >= 0 && p->state < NELEM(states) && states[p->state])
80104336:	77 11                	ja     80104349 <procdump+0x59>
80104338:	8b 14 85 00 78 10 80 	mov    -0x7fef8800(,%eax,4),%edx
      state = "???";
8010433f:	b8 a0 77 10 80       	mov    $0x801077a0,%eax
80104344:	85 d2                	test   %edx,%edx
80104346:	0f 44 d0             	cmove  %eax,%edx
    cprintf("%d %s %s", p->pid, state, p->name);
80104349:	8d 43 6c             	lea    0x6c(%ebx),%eax
8010434c:	50                   	push   %eax
8010434d:	52                   	push   %edx
8010434e:	ff 73 10             	pushl  0x10(%ebx)
80104351:	68 a4 77 10 80       	push   $0x801077a4
80104356:	e8 35 c4 ff ff       	call   80100790 <cprintf>
    if(p->state == SLEEPING){
8010435b:	83 c4 10             	add    $0x10,%esp
8010435e:	83 7b 0c 02          	cmpl   $0x2,0xc(%ebx)
80104362:	75 a4                	jne    80104308 <procdump+0x18>
      getcallerpcs((uint*)p->context->ebp+2, pc);
80104364:	8d 45 c0             	lea    -0x40(%ebp),%eax
80104367:	83 ec 08             	sub    $0x8,%esp
8010436a:	8d 7d c0             	lea    -0x40(%ebp),%edi
8010436d:	50                   	push   %eax
8010436e:	8b 43 1c             	mov    0x1c(%ebx),%eax
80104371:	8b 40 0c             	mov    0xc(%eax),%eax
80104374:	83 c0 08             	add    $0x8,%eax
80104377:	50                   	push   %eax
80104378:	e8 83 01 00 00       	call   80104500 <getcallerpcs>
8010437d:	83 c4 10             	add    $0x10,%esp
      for(i=0; i<10 && pc[i] != 0; i++)
80104380:	8b 17                	mov    (%edi),%edx
80104382:	85 d2                	test   %edx,%edx
80104384:	74 82                	je     80104308 <procdump+0x18>
        cprintf(" %p", pc[i]);
80104386:	83 ec 08             	sub    $0x8,%esp
80104389:	83 c7 04             	add    $0x4,%edi
8010438c:	52                   	push   %edx
8010438d:	68 e1 71 10 80       	push   $0x801071e1
80104392:	e8 f9 c3 ff ff       	call   80100790 <cprintf>
      for(i=0; i<10 && pc[i] != 0; i++)
80104397:	83 c4 10             	add    $0x10,%esp
8010439a:	39 fe                	cmp    %edi,%esi
8010439c:	75 e2                	jne    80104380 <procdump+0x90>
8010439e:	e9 65 ff ff ff       	jmp    80104308 <procdump+0x18>
801043a3:	90                   	nop
801043a4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  }
}
801043a8:	8d 65 f4             	lea    -0xc(%ebp),%esp
801043ab:	5b                   	pop    %ebx
801043ac:	5e                   	pop    %esi
801043ad:	5f                   	pop    %edi
801043ae:	5d                   	pop    %ebp
801043af:	c3                   	ret    

801043b0 <initsleeplock>:
#include "spinlock.h"
#include "sleeplock.h"

void
initsleeplock(struct sleeplock *lk, char *name)
{
801043b0:	55                   	push   %ebp
801043b1:	89 e5                	mov    %esp,%ebp
801043b3:	53                   	push   %ebx
801043b4:	83 ec 0c             	sub    $0xc,%esp
801043b7:	8b 5d 08             	mov    0x8(%ebp),%ebx
  initlock(&lk->lk, "sleep lock");
801043ba:	68 18 78 10 80       	push   $0x80107818
801043bf:	8d 43 04             	lea    0x4(%ebx),%eax
801043c2:	50                   	push   %eax
801043c3:	e8 18 01 00 00       	call   801044e0 <initlock>
  lk->name = name;
801043c8:	8b 45 0c             	mov    0xc(%ebp),%eax
  lk->locked = 0;
801043cb:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  lk->pid = 0;
}
801043d1:	83 c4 10             	add    $0x10,%esp
  lk->pid = 0;
801043d4:	c7 43 3c 00 00 00 00 	movl   $0x0,0x3c(%ebx)
  lk->name = name;
801043db:	89 43 38             	mov    %eax,0x38(%ebx)
}
801043de:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801043e1:	c9                   	leave  
801043e2:	c3                   	ret    
801043e3:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
801043e9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

801043f0 <acquiresleep>:

void
acquiresleep(struct sleeplock *lk)
{
801043f0:	55                   	push   %ebp
801043f1:	89 e5                	mov    %esp,%ebp
801043f3:	56                   	push   %esi
801043f4:	53                   	push   %ebx
801043f5:	8b 5d 08             	mov    0x8(%ebp),%ebx
  acquire(&lk->lk);
801043f8:	83 ec 0c             	sub    $0xc,%esp
801043fb:	8d 73 04             	lea    0x4(%ebx),%esi
801043fe:	56                   	push   %esi
801043ff:	e8 1c 02 00 00       	call   80104620 <acquire>
  while (lk->locked) {
80104404:	8b 13                	mov    (%ebx),%edx
80104406:	83 c4 10             	add    $0x10,%esp
80104409:	85 d2                	test   %edx,%edx
8010440b:	74 16                	je     80104423 <acquiresleep+0x33>
8010440d:	8d 76 00             	lea    0x0(%esi),%esi
    sleep(lk, &lk->lk);
80104410:	83 ec 08             	sub    $0x8,%esp
80104413:	56                   	push   %esi
80104414:	53                   	push   %ebx
80104415:	e8 46 fc ff ff       	call   80104060 <sleep>
  while (lk->locked) {
8010441a:	8b 03                	mov    (%ebx),%eax
8010441c:	83 c4 10             	add    $0x10,%esp
8010441f:	85 c0                	test   %eax,%eax
80104421:	75 ed                	jne    80104410 <acquiresleep+0x20>
  }
  lk->locked = 1;
80104423:	c7 03 01 00 00 00    	movl   $0x1,(%ebx)
  lk->pid = myproc()->pid;
80104429:	e8 92 f6 ff ff       	call   80103ac0 <myproc>
8010442e:	8b 40 10             	mov    0x10(%eax),%eax
80104431:	89 43 3c             	mov    %eax,0x3c(%ebx)
  release(&lk->lk);
80104434:	89 75 08             	mov    %esi,0x8(%ebp)
}
80104437:	8d 65 f8             	lea    -0x8(%ebp),%esp
8010443a:	5b                   	pop    %ebx
8010443b:	5e                   	pop    %esi
8010443c:	5d                   	pop    %ebp
  release(&lk->lk);
8010443d:	e9 9e 02 00 00       	jmp    801046e0 <release>
80104442:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104449:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80104450 <releasesleep>:

void
releasesleep(struct sleeplock *lk)
{
80104450:	55                   	push   %ebp
80104451:	89 e5                	mov    %esp,%ebp
80104453:	56                   	push   %esi
80104454:	53                   	push   %ebx
80104455:	8b 5d 08             	mov    0x8(%ebp),%ebx
  acquire(&lk->lk);
80104458:	83 ec 0c             	sub    $0xc,%esp
8010445b:	8d 73 04             	lea    0x4(%ebx),%esi
8010445e:	56                   	push   %esi
8010445f:	e8 bc 01 00 00       	call   80104620 <acquire>
  lk->locked = 0;
80104464:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  lk->pid = 0;
8010446a:	c7 43 3c 00 00 00 00 	movl   $0x0,0x3c(%ebx)
  wakeup(lk);
80104471:	89 1c 24             	mov    %ebx,(%esp)
80104474:	e8 97 fd ff ff       	call   80104210 <wakeup>
  release(&lk->lk);
80104479:	89 75 08             	mov    %esi,0x8(%ebp)
8010447c:	83 c4 10             	add    $0x10,%esp
}
8010447f:	8d 65 f8             	lea    -0x8(%ebp),%esp
80104482:	5b                   	pop    %ebx
80104483:	5e                   	pop    %esi
80104484:	5d                   	pop    %ebp
  release(&lk->lk);
80104485:	e9 56 02 00 00       	jmp    801046e0 <release>
8010448a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80104490 <holdingsleep>:

int
holdingsleep(struct sleeplock *lk)
{
80104490:	55                   	push   %ebp
80104491:	89 e5                	mov    %esp,%ebp
80104493:	57                   	push   %edi
80104494:	56                   	push   %esi
80104495:	53                   	push   %ebx
80104496:	31 ff                	xor    %edi,%edi
80104498:	83 ec 18             	sub    $0x18,%esp
8010449b:	8b 5d 08             	mov    0x8(%ebp),%ebx
  int r;
  
  acquire(&lk->lk);
8010449e:	8d 73 04             	lea    0x4(%ebx),%esi
801044a1:	56                   	push   %esi
801044a2:	e8 79 01 00 00       	call   80104620 <acquire>
  r = lk->locked && (lk->pid == myproc()->pid);
801044a7:	8b 03                	mov    (%ebx),%eax
801044a9:	83 c4 10             	add    $0x10,%esp
801044ac:	85 c0                	test   %eax,%eax
801044ae:	74 13                	je     801044c3 <holdingsleep+0x33>
801044b0:	8b 5b 3c             	mov    0x3c(%ebx),%ebx
801044b3:	e8 08 f6 ff ff       	call   80103ac0 <myproc>
801044b8:	39 58 10             	cmp    %ebx,0x10(%eax)
801044bb:	0f 94 c0             	sete   %al
801044be:	0f b6 c0             	movzbl %al,%eax
801044c1:	89 c7                	mov    %eax,%edi
  release(&lk->lk);
801044c3:	83 ec 0c             	sub    $0xc,%esp
801044c6:	56                   	push   %esi
801044c7:	e8 14 02 00 00       	call   801046e0 <release>
  return r;
}
801044cc:	8d 65 f4             	lea    -0xc(%ebp),%esp
801044cf:	89 f8                	mov    %edi,%eax
801044d1:	5b                   	pop    %ebx
801044d2:	5e                   	pop    %esi
801044d3:	5f                   	pop    %edi
801044d4:	5d                   	pop    %ebp
801044d5:	c3                   	ret    
801044d6:	66 90                	xchg   %ax,%ax
801044d8:	66 90                	xchg   %ax,%ax
801044da:	66 90                	xchg   %ax,%ax
801044dc:	66 90                	xchg   %ax,%ax
801044de:	66 90                	xchg   %ax,%ax

801044e0 <initlock>:
#include "proc.h"
#include "spinlock.h"

void
initlock(struct spinlock *lk, char *name)
{
801044e0:	55                   	push   %ebp
801044e1:	89 e5                	mov    %esp,%ebp
801044e3:	8b 45 08             	mov    0x8(%ebp),%eax
  lk->name = name;
801044e6:	8b 55 0c             	mov    0xc(%ebp),%edx
  lk->locked = 0;
801044e9:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
  lk->name = name;
801044ef:	89 50 04             	mov    %edx,0x4(%eax)
  lk->cpu = 0;
801044f2:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)
}
801044f9:	5d                   	pop    %ebp
801044fa:	c3                   	ret    
801044fb:	90                   	nop
801044fc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80104500 <getcallerpcs>:
}

// Record the current call stack in pcs[] by following the %ebp chain.
void
getcallerpcs(void *v, uint pcs[])
{
80104500:	55                   	push   %ebp
  uint *ebp;
  int i;

  ebp = (uint*)v - 2;
  for(i = 0; i < 10; i++){
80104501:	31 d2                	xor    %edx,%edx
{
80104503:	89 e5                	mov    %esp,%ebp
80104505:	53                   	push   %ebx
  ebp = (uint*)v - 2;
80104506:	8b 45 08             	mov    0x8(%ebp),%eax
{
80104509:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  ebp = (uint*)v - 2;
8010450c:	83 e8 08             	sub    $0x8,%eax
8010450f:	90                   	nop
    if(ebp == 0 || ebp < (uint*)KERNBASE || ebp == (uint*)0xffffffff)
80104510:	8d 98 00 00 00 80    	lea    -0x80000000(%eax),%ebx
80104516:	81 fb fe ff ff 7f    	cmp    $0x7ffffffe,%ebx
8010451c:	77 1a                	ja     80104538 <getcallerpcs+0x38>
      break;
    pcs[i] = ebp[1];     // saved %eip
8010451e:	8b 58 04             	mov    0x4(%eax),%ebx
80104521:	89 1c 91             	mov    %ebx,(%ecx,%edx,4)
  for(i = 0; i < 10; i++){
80104524:	83 c2 01             	add    $0x1,%edx
    ebp = (uint*)ebp[0]; // saved %ebp
80104527:	8b 00                	mov    (%eax),%eax
  for(i = 0; i < 10; i++){
80104529:	83 fa 0a             	cmp    $0xa,%edx
8010452c:	75 e2                	jne    80104510 <getcallerpcs+0x10>
  }
  for(; i < 10; i++)
    pcs[i] = 0;
}
8010452e:	5b                   	pop    %ebx
8010452f:	5d                   	pop    %ebp
80104530:	c3                   	ret    
80104531:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104538:	8d 04 91             	lea    (%ecx,%edx,4),%eax
8010453b:	83 c1 28             	add    $0x28,%ecx
8010453e:	66 90                	xchg   %ax,%ax
    pcs[i] = 0;
80104540:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
80104546:	83 c0 04             	add    $0x4,%eax
  for(; i < 10; i++)
80104549:	39 c1                	cmp    %eax,%ecx
8010454b:	75 f3                	jne    80104540 <getcallerpcs+0x40>
}
8010454d:	5b                   	pop    %ebx
8010454e:	5d                   	pop    %ebp
8010454f:	c3                   	ret    

80104550 <pushcli>:
// it takes two popcli to undo two pushcli.  Also, if interrupts
// are off, then pushcli, popcli leaves them off.

void
pushcli(void)
{
80104550:	55                   	push   %ebp
80104551:	89 e5                	mov    %esp,%ebp
80104553:	53                   	push   %ebx
80104554:	83 ec 04             	sub    $0x4,%esp
80104557:	9c                   	pushf  
80104558:	5b                   	pop    %ebx
  asm volatile("cli");
80104559:	fa                   	cli    
  int eflags;

  eflags = readeflags();
  cli();
  if(mycpu()->ncli == 0)
8010455a:	e8 c1 f4 ff ff       	call   80103a20 <mycpu>
8010455f:	8b 80 a4 00 00 00    	mov    0xa4(%eax),%eax
80104565:	85 c0                	test   %eax,%eax
80104567:	75 11                	jne    8010457a <pushcli+0x2a>
    mycpu()->intena = eflags & FL_IF;
80104569:	81 e3 00 02 00 00    	and    $0x200,%ebx
8010456f:	e8 ac f4 ff ff       	call   80103a20 <mycpu>
80104574:	89 98 a8 00 00 00    	mov    %ebx,0xa8(%eax)
  mycpu()->ncli += 1;
8010457a:	e8 a1 f4 ff ff       	call   80103a20 <mycpu>
8010457f:	83 80 a4 00 00 00 01 	addl   $0x1,0xa4(%eax)
}
80104586:	83 c4 04             	add    $0x4,%esp
80104589:	5b                   	pop    %ebx
8010458a:	5d                   	pop    %ebp
8010458b:	c3                   	ret    
8010458c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80104590 <popcli>:

void
popcli(void)
{
80104590:	55                   	push   %ebp
80104591:	89 e5                	mov    %esp,%ebp
80104593:	83 ec 08             	sub    $0x8,%esp
  asm volatile("pushfl; popl %0" : "=r" (eflags));
80104596:	9c                   	pushf  
80104597:	58                   	pop    %eax
  if(readeflags()&FL_IF)
80104598:	f6 c4 02             	test   $0x2,%ah
8010459b:	75 35                	jne    801045d2 <popcli+0x42>
    panic("popcli - interruptible");
  if(--mycpu()->ncli < 0)
8010459d:	e8 7e f4 ff ff       	call   80103a20 <mycpu>
801045a2:	83 a8 a4 00 00 00 01 	subl   $0x1,0xa4(%eax)
801045a9:	78 34                	js     801045df <popcli+0x4f>
    panic("popcli");
  if(mycpu()->ncli == 0 && mycpu()->intena)
801045ab:	e8 70 f4 ff ff       	call   80103a20 <mycpu>
801045b0:	8b 90 a4 00 00 00    	mov    0xa4(%eax),%edx
801045b6:	85 d2                	test   %edx,%edx
801045b8:	74 06                	je     801045c0 <popcli+0x30>
    sti();
}
801045ba:	c9                   	leave  
801045bb:	c3                   	ret    
801045bc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  if(mycpu()->ncli == 0 && mycpu()->intena)
801045c0:	e8 5b f4 ff ff       	call   80103a20 <mycpu>
801045c5:	8b 80 a8 00 00 00    	mov    0xa8(%eax),%eax
801045cb:	85 c0                	test   %eax,%eax
801045cd:	74 eb                	je     801045ba <popcli+0x2a>
  asm volatile("sti");
801045cf:	fb                   	sti    
}
801045d0:	c9                   	leave  
801045d1:	c3                   	ret    
    panic("popcli - interruptible");
801045d2:	83 ec 0c             	sub    $0xc,%esp
801045d5:	68 23 78 10 80       	push   $0x80107823
801045da:	e8 b1 bd ff ff       	call   80100390 <panic>
    panic("popcli");
801045df:	83 ec 0c             	sub    $0xc,%esp
801045e2:	68 3a 78 10 80       	push   $0x8010783a
801045e7:	e8 a4 bd ff ff       	call   80100390 <panic>
801045ec:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

801045f0 <holding>:
{
801045f0:	55                   	push   %ebp
801045f1:	89 e5                	mov    %esp,%ebp
801045f3:	56                   	push   %esi
801045f4:	53                   	push   %ebx
801045f5:	8b 75 08             	mov    0x8(%ebp),%esi
801045f8:	31 db                	xor    %ebx,%ebx
  pushcli();
801045fa:	e8 51 ff ff ff       	call   80104550 <pushcli>
  r = lock->locked && lock->cpu == mycpu();
801045ff:	8b 06                	mov    (%esi),%eax
80104601:	85 c0                	test   %eax,%eax
80104603:	74 10                	je     80104615 <holding+0x25>
80104605:	8b 5e 08             	mov    0x8(%esi),%ebx
80104608:	e8 13 f4 ff ff       	call   80103a20 <mycpu>
8010460d:	39 c3                	cmp    %eax,%ebx
8010460f:	0f 94 c3             	sete   %bl
80104612:	0f b6 db             	movzbl %bl,%ebx
  popcli();
80104615:	e8 76 ff ff ff       	call   80104590 <popcli>
}
8010461a:	89 d8                	mov    %ebx,%eax
8010461c:	5b                   	pop    %ebx
8010461d:	5e                   	pop    %esi
8010461e:	5d                   	pop    %ebp
8010461f:	c3                   	ret    

80104620 <acquire>:
{
80104620:	55                   	push   %ebp
80104621:	89 e5                	mov    %esp,%ebp
80104623:	56                   	push   %esi
80104624:	53                   	push   %ebx
  pushcli(); // disable interrupts to avoid deadlock.
80104625:	e8 26 ff ff ff       	call   80104550 <pushcli>
  if(holding(lk))
8010462a:	8b 5d 08             	mov    0x8(%ebp),%ebx
8010462d:	83 ec 0c             	sub    $0xc,%esp
80104630:	53                   	push   %ebx
80104631:	e8 ba ff ff ff       	call   801045f0 <holding>
80104636:	83 c4 10             	add    $0x10,%esp
80104639:	85 c0                	test   %eax,%eax
8010463b:	0f 85 83 00 00 00    	jne    801046c4 <acquire+0xa4>
80104641:	89 c6                	mov    %eax,%esi
  asm volatile("lock; xchgl %0, %1" :
80104643:	ba 01 00 00 00       	mov    $0x1,%edx
80104648:	eb 09                	jmp    80104653 <acquire+0x33>
8010464a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80104650:	8b 5d 08             	mov    0x8(%ebp),%ebx
80104653:	89 d0                	mov    %edx,%eax
80104655:	f0 87 03             	lock xchg %eax,(%ebx)
  while(xchg(&lk->locked, 1) != 0)
80104658:	85 c0                	test   %eax,%eax
8010465a:	75 f4                	jne    80104650 <acquire+0x30>
  __sync_synchronize();
8010465c:	f0 83 0c 24 00       	lock orl $0x0,(%esp)
  lk->cpu = mycpu();
80104661:	8b 5d 08             	mov    0x8(%ebp),%ebx
80104664:	e8 b7 f3 ff ff       	call   80103a20 <mycpu>
  getcallerpcs(&lk, lk->pcs);
80104669:	8d 53 0c             	lea    0xc(%ebx),%edx
  lk->cpu = mycpu();
8010466c:	89 43 08             	mov    %eax,0x8(%ebx)
  ebp = (uint*)v - 2;
8010466f:	89 e8                	mov    %ebp,%eax
80104671:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    if(ebp == 0 || ebp < (uint*)KERNBASE || ebp == (uint*)0xffffffff)
80104678:	8d 88 00 00 00 80    	lea    -0x80000000(%eax),%ecx
8010467e:	81 f9 fe ff ff 7f    	cmp    $0x7ffffffe,%ecx
80104684:	77 1a                	ja     801046a0 <acquire+0x80>
    pcs[i] = ebp[1];     // saved %eip
80104686:	8b 48 04             	mov    0x4(%eax),%ecx
80104689:	89 0c b2             	mov    %ecx,(%edx,%esi,4)
  for(i = 0; i < 10; i++){
8010468c:	83 c6 01             	add    $0x1,%esi
    ebp = (uint*)ebp[0]; // saved %ebp
8010468f:	8b 00                	mov    (%eax),%eax
  for(i = 0; i < 10; i++){
80104691:	83 fe 0a             	cmp    $0xa,%esi
80104694:	75 e2                	jne    80104678 <acquire+0x58>
}
80104696:	8d 65 f8             	lea    -0x8(%ebp),%esp
80104699:	5b                   	pop    %ebx
8010469a:	5e                   	pop    %esi
8010469b:	5d                   	pop    %ebp
8010469c:	c3                   	ret    
8010469d:	8d 76 00             	lea    0x0(%esi),%esi
801046a0:	8d 04 b2             	lea    (%edx,%esi,4),%eax
801046a3:	83 c2 28             	add    $0x28,%edx
801046a6:	8d 76 00             	lea    0x0(%esi),%esi
801046a9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
    pcs[i] = 0;
801046b0:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
801046b6:	83 c0 04             	add    $0x4,%eax
  for(; i < 10; i++)
801046b9:	39 d0                	cmp    %edx,%eax
801046bb:	75 f3                	jne    801046b0 <acquire+0x90>
}
801046bd:	8d 65 f8             	lea    -0x8(%ebp),%esp
801046c0:	5b                   	pop    %ebx
801046c1:	5e                   	pop    %esi
801046c2:	5d                   	pop    %ebp
801046c3:	c3                   	ret    
    panic("acquire");
801046c4:	83 ec 0c             	sub    $0xc,%esp
801046c7:	68 41 78 10 80       	push   $0x80107841
801046cc:	e8 bf bc ff ff       	call   80100390 <panic>
801046d1:	eb 0d                	jmp    801046e0 <release>
801046d3:	90                   	nop
801046d4:	90                   	nop
801046d5:	90                   	nop
801046d6:	90                   	nop
801046d7:	90                   	nop
801046d8:	90                   	nop
801046d9:	90                   	nop
801046da:	90                   	nop
801046db:	90                   	nop
801046dc:	90                   	nop
801046dd:	90                   	nop
801046de:	90                   	nop
801046df:	90                   	nop

801046e0 <release>:
{
801046e0:	55                   	push   %ebp
801046e1:	89 e5                	mov    %esp,%ebp
801046e3:	53                   	push   %ebx
801046e4:	83 ec 10             	sub    $0x10,%esp
801046e7:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if(!holding(lk))
801046ea:	53                   	push   %ebx
801046eb:	e8 00 ff ff ff       	call   801045f0 <holding>
801046f0:	83 c4 10             	add    $0x10,%esp
801046f3:	85 c0                	test   %eax,%eax
801046f5:	74 22                	je     80104719 <release+0x39>
  lk->pcs[0] = 0;
801046f7:	c7 43 0c 00 00 00 00 	movl   $0x0,0xc(%ebx)
  lk->cpu = 0;
801046fe:	c7 43 08 00 00 00 00 	movl   $0x0,0x8(%ebx)
  __sync_synchronize();
80104705:	f0 83 0c 24 00       	lock orl $0x0,(%esp)
  asm volatile("movl $0, %0" : "+m" (lk->locked) : );
8010470a:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
}
80104710:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80104713:	c9                   	leave  
  popcli();
80104714:	e9 77 fe ff ff       	jmp    80104590 <popcli>
    panic("release");
80104719:	83 ec 0c             	sub    $0xc,%esp
8010471c:	68 49 78 10 80       	push   $0x80107849
80104721:	e8 6a bc ff ff       	call   80100390 <panic>
80104726:	66 90                	xchg   %ax,%ax
80104728:	66 90                	xchg   %ax,%ax
8010472a:	66 90                	xchg   %ax,%ax
8010472c:	66 90                	xchg   %ax,%ax
8010472e:	66 90                	xchg   %ax,%ax

80104730 <memset>:
80104730:	55                   	push   %ebp
80104731:	89 e5                	mov    %esp,%ebp
80104733:	57                   	push   %edi
80104734:	53                   	push   %ebx
80104735:	8b 55 08             	mov    0x8(%ebp),%edx
80104738:	8b 4d 10             	mov    0x10(%ebp),%ecx
8010473b:	f6 c2 03             	test   $0x3,%dl
8010473e:	75 05                	jne    80104745 <memset+0x15>
80104740:	f6 c1 03             	test   $0x3,%cl
80104743:	74 13                	je     80104758 <memset+0x28>
80104745:	89 d7                	mov    %edx,%edi
80104747:	8b 45 0c             	mov    0xc(%ebp),%eax
8010474a:	fc                   	cld    
8010474b:	f3 aa                	rep stos %al,%es:(%edi)
8010474d:	5b                   	pop    %ebx
8010474e:	89 d0                	mov    %edx,%eax
80104750:	5f                   	pop    %edi
80104751:	5d                   	pop    %ebp
80104752:	c3                   	ret    
80104753:	90                   	nop
80104754:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80104758:	0f b6 7d 0c          	movzbl 0xc(%ebp),%edi
8010475c:	c1 e9 02             	shr    $0x2,%ecx
8010475f:	89 f8                	mov    %edi,%eax
80104761:	89 fb                	mov    %edi,%ebx
80104763:	c1 e0 18             	shl    $0x18,%eax
80104766:	c1 e3 10             	shl    $0x10,%ebx
80104769:	09 d8                	or     %ebx,%eax
8010476b:	09 f8                	or     %edi,%eax
8010476d:	c1 e7 08             	shl    $0x8,%edi
80104770:	09 f8                	or     %edi,%eax
80104772:	89 d7                	mov    %edx,%edi
80104774:	fc                   	cld    
80104775:	f3 ab                	rep stos %eax,%es:(%edi)
80104777:	5b                   	pop    %ebx
80104778:	89 d0                	mov    %edx,%eax
8010477a:	5f                   	pop    %edi
8010477b:	5d                   	pop    %ebp
8010477c:	c3                   	ret    
8010477d:	8d 76 00             	lea    0x0(%esi),%esi

80104780 <memcmp>:
80104780:	55                   	push   %ebp
80104781:	89 e5                	mov    %esp,%ebp
80104783:	57                   	push   %edi
80104784:	56                   	push   %esi
80104785:	53                   	push   %ebx
80104786:	8b 5d 10             	mov    0x10(%ebp),%ebx
80104789:	8b 75 08             	mov    0x8(%ebp),%esi
8010478c:	8b 7d 0c             	mov    0xc(%ebp),%edi
8010478f:	85 db                	test   %ebx,%ebx
80104791:	74 29                	je     801047bc <memcmp+0x3c>
80104793:	0f b6 16             	movzbl (%esi),%edx
80104796:	0f b6 0f             	movzbl (%edi),%ecx
80104799:	38 d1                	cmp    %dl,%cl
8010479b:	75 2b                	jne    801047c8 <memcmp+0x48>
8010479d:	b8 01 00 00 00       	mov    $0x1,%eax
801047a2:	eb 14                	jmp    801047b8 <memcmp+0x38>
801047a4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801047a8:	0f b6 14 06          	movzbl (%esi,%eax,1),%edx
801047ac:	83 c0 01             	add    $0x1,%eax
801047af:	0f b6 4c 07 ff       	movzbl -0x1(%edi,%eax,1),%ecx
801047b4:	38 ca                	cmp    %cl,%dl
801047b6:	75 10                	jne    801047c8 <memcmp+0x48>
801047b8:	39 d8                	cmp    %ebx,%eax
801047ba:	75 ec                	jne    801047a8 <memcmp+0x28>
801047bc:	5b                   	pop    %ebx
801047bd:	31 c0                	xor    %eax,%eax
801047bf:	5e                   	pop    %esi
801047c0:	5f                   	pop    %edi
801047c1:	5d                   	pop    %ebp
801047c2:	c3                   	ret    
801047c3:	90                   	nop
801047c4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801047c8:	0f b6 c2             	movzbl %dl,%eax
801047cb:	5b                   	pop    %ebx
801047cc:	29 c8                	sub    %ecx,%eax
801047ce:	5e                   	pop    %esi
801047cf:	5f                   	pop    %edi
801047d0:	5d                   	pop    %ebp
801047d1:	c3                   	ret    
801047d2:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801047d9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

801047e0 <memmove>:
801047e0:	55                   	push   %ebp
801047e1:	89 e5                	mov    %esp,%ebp
801047e3:	56                   	push   %esi
801047e4:	53                   	push   %ebx
801047e5:	8b 45 08             	mov    0x8(%ebp),%eax
801047e8:	8b 5d 0c             	mov    0xc(%ebp),%ebx
801047eb:	8b 75 10             	mov    0x10(%ebp),%esi
801047ee:	39 c3                	cmp    %eax,%ebx
801047f0:	73 26                	jae    80104818 <memmove+0x38>
801047f2:	8d 0c 33             	lea    (%ebx,%esi,1),%ecx
801047f5:	39 c8                	cmp    %ecx,%eax
801047f7:	73 1f                	jae    80104818 <memmove+0x38>
801047f9:	85 f6                	test   %esi,%esi
801047fb:	8d 56 ff             	lea    -0x1(%esi),%edx
801047fe:	74 0f                	je     8010480f <memmove+0x2f>
80104800:	0f b6 0c 13          	movzbl (%ebx,%edx,1),%ecx
80104804:	88 0c 10             	mov    %cl,(%eax,%edx,1)
80104807:	83 ea 01             	sub    $0x1,%edx
8010480a:	83 fa ff             	cmp    $0xffffffff,%edx
8010480d:	75 f1                	jne    80104800 <memmove+0x20>
8010480f:	5b                   	pop    %ebx
80104810:	5e                   	pop    %esi
80104811:	5d                   	pop    %ebp
80104812:	c3                   	ret    
80104813:	90                   	nop
80104814:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80104818:	31 d2                	xor    %edx,%edx
8010481a:	85 f6                	test   %esi,%esi
8010481c:	74 f1                	je     8010480f <memmove+0x2f>
8010481e:	66 90                	xchg   %ax,%ax
80104820:	0f b6 0c 13          	movzbl (%ebx,%edx,1),%ecx
80104824:	88 0c 10             	mov    %cl,(%eax,%edx,1)
80104827:	83 c2 01             	add    $0x1,%edx
8010482a:	39 d6                	cmp    %edx,%esi
8010482c:	75 f2                	jne    80104820 <memmove+0x40>
8010482e:	5b                   	pop    %ebx
8010482f:	5e                   	pop    %esi
80104830:	5d                   	pop    %ebp
80104831:	c3                   	ret    
80104832:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104839:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80104840 <memcpy>:
80104840:	55                   	push   %ebp
80104841:	89 e5                	mov    %esp,%ebp
80104843:	5d                   	pop    %ebp
80104844:	eb 9a                	jmp    801047e0 <memmove>
80104846:	8d 76 00             	lea    0x0(%esi),%esi
80104849:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80104850 <strncmp>:
80104850:	55                   	push   %ebp
80104851:	89 e5                	mov    %esp,%ebp
80104853:	57                   	push   %edi
80104854:	56                   	push   %esi
80104855:	8b 7d 10             	mov    0x10(%ebp),%edi
80104858:	53                   	push   %ebx
80104859:	8b 4d 08             	mov    0x8(%ebp),%ecx
8010485c:	8b 75 0c             	mov    0xc(%ebp),%esi
8010485f:	85 ff                	test   %edi,%edi
80104861:	74 2f                	je     80104892 <strncmp+0x42>
80104863:	0f b6 01             	movzbl (%ecx),%eax
80104866:	0f b6 1e             	movzbl (%esi),%ebx
80104869:	84 c0                	test   %al,%al
8010486b:	74 37                	je     801048a4 <strncmp+0x54>
8010486d:	38 c3                	cmp    %al,%bl
8010486f:	75 33                	jne    801048a4 <strncmp+0x54>
80104871:	01 f7                	add    %esi,%edi
80104873:	eb 13                	jmp    80104888 <strncmp+0x38>
80104875:	8d 76 00             	lea    0x0(%esi),%esi
80104878:	0f b6 01             	movzbl (%ecx),%eax
8010487b:	84 c0                	test   %al,%al
8010487d:	74 21                	je     801048a0 <strncmp+0x50>
8010487f:	0f b6 1a             	movzbl (%edx),%ebx
80104882:	89 d6                	mov    %edx,%esi
80104884:	38 d8                	cmp    %bl,%al
80104886:	75 1c                	jne    801048a4 <strncmp+0x54>
80104888:	8d 56 01             	lea    0x1(%esi),%edx
8010488b:	83 c1 01             	add    $0x1,%ecx
8010488e:	39 fa                	cmp    %edi,%edx
80104890:	75 e6                	jne    80104878 <strncmp+0x28>
80104892:	5b                   	pop    %ebx
80104893:	31 c0                	xor    %eax,%eax
80104895:	5e                   	pop    %esi
80104896:	5f                   	pop    %edi
80104897:	5d                   	pop    %ebp
80104898:	c3                   	ret    
80104899:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801048a0:	0f b6 5e 01          	movzbl 0x1(%esi),%ebx
801048a4:	29 d8                	sub    %ebx,%eax
801048a6:	5b                   	pop    %ebx
801048a7:	5e                   	pop    %esi
801048a8:	5f                   	pop    %edi
801048a9:	5d                   	pop    %ebp
801048aa:	c3                   	ret    
801048ab:	90                   	nop
801048ac:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

801048b0 <strncpy>:
801048b0:	55                   	push   %ebp
801048b1:	89 e5                	mov    %esp,%ebp
801048b3:	56                   	push   %esi
801048b4:	53                   	push   %ebx
801048b5:	8b 45 08             	mov    0x8(%ebp),%eax
801048b8:	8b 5d 0c             	mov    0xc(%ebp),%ebx
801048bb:	8b 4d 10             	mov    0x10(%ebp),%ecx
801048be:	89 c2                	mov    %eax,%edx
801048c0:	eb 19                	jmp    801048db <strncpy+0x2b>
801048c2:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
801048c8:	83 c3 01             	add    $0x1,%ebx
801048cb:	0f b6 4b ff          	movzbl -0x1(%ebx),%ecx
801048cf:	83 c2 01             	add    $0x1,%edx
801048d2:	84 c9                	test   %cl,%cl
801048d4:	88 4a ff             	mov    %cl,-0x1(%edx)
801048d7:	74 09                	je     801048e2 <strncpy+0x32>
801048d9:	89 f1                	mov    %esi,%ecx
801048db:	85 c9                	test   %ecx,%ecx
801048dd:	8d 71 ff             	lea    -0x1(%ecx),%esi
801048e0:	7f e6                	jg     801048c8 <strncpy+0x18>
801048e2:	31 c9                	xor    %ecx,%ecx
801048e4:	85 f6                	test   %esi,%esi
801048e6:	7e 17                	jle    801048ff <strncpy+0x4f>
801048e8:	90                   	nop
801048e9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801048f0:	c6 04 0a 00          	movb   $0x0,(%edx,%ecx,1)
801048f4:	89 f3                	mov    %esi,%ebx
801048f6:	83 c1 01             	add    $0x1,%ecx
801048f9:	29 cb                	sub    %ecx,%ebx
801048fb:	85 db                	test   %ebx,%ebx
801048fd:	7f f1                	jg     801048f0 <strncpy+0x40>
801048ff:	5b                   	pop    %ebx
80104900:	5e                   	pop    %esi
80104901:	5d                   	pop    %ebp
80104902:	c3                   	ret    
80104903:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80104909:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80104910 <safestrcpy>:
80104910:	55                   	push   %ebp
80104911:	89 e5                	mov    %esp,%ebp
80104913:	56                   	push   %esi
80104914:	53                   	push   %ebx
80104915:	8b 4d 10             	mov    0x10(%ebp),%ecx
80104918:	8b 45 08             	mov    0x8(%ebp),%eax
8010491b:	8b 55 0c             	mov    0xc(%ebp),%edx
8010491e:	85 c9                	test   %ecx,%ecx
80104920:	7e 26                	jle    80104948 <safestrcpy+0x38>
80104922:	8d 74 0a ff          	lea    -0x1(%edx,%ecx,1),%esi
80104926:	89 c1                	mov    %eax,%ecx
80104928:	eb 17                	jmp    80104941 <safestrcpy+0x31>
8010492a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80104930:	83 c2 01             	add    $0x1,%edx
80104933:	0f b6 5a ff          	movzbl -0x1(%edx),%ebx
80104937:	83 c1 01             	add    $0x1,%ecx
8010493a:	84 db                	test   %bl,%bl
8010493c:	88 59 ff             	mov    %bl,-0x1(%ecx)
8010493f:	74 04                	je     80104945 <safestrcpy+0x35>
80104941:	39 f2                	cmp    %esi,%edx
80104943:	75 eb                	jne    80104930 <safestrcpy+0x20>
80104945:	c6 01 00             	movb   $0x0,(%ecx)
80104948:	5b                   	pop    %ebx
80104949:	5e                   	pop    %esi
8010494a:	5d                   	pop    %ebp
8010494b:	c3                   	ret    
8010494c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80104950 <strlen>:
80104950:	55                   	push   %ebp
80104951:	31 c0                	xor    %eax,%eax
80104953:	89 e5                	mov    %esp,%ebp
80104955:	8b 55 08             	mov    0x8(%ebp),%edx
80104958:	80 3a 00             	cmpb   $0x0,(%edx)
8010495b:	74 0c                	je     80104969 <strlen+0x19>
8010495d:	8d 76 00             	lea    0x0(%esi),%esi
80104960:	83 c0 01             	add    $0x1,%eax
80104963:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
80104967:	75 f7                	jne    80104960 <strlen+0x10>
80104969:	5d                   	pop    %ebp
8010496a:	c3                   	ret    

8010496b <swtch>:
8010496b:	8b 44 24 04          	mov    0x4(%esp),%eax
8010496f:	8b 54 24 08          	mov    0x8(%esp),%edx
80104973:	55                   	push   %ebp
80104974:	53                   	push   %ebx
80104975:	56                   	push   %esi
80104976:	57                   	push   %edi
80104977:	89 20                	mov    %esp,(%eax)
80104979:	89 d4                	mov    %edx,%esp
8010497b:	5f                   	pop    %edi
8010497c:	5e                   	pop    %esi
8010497d:	5b                   	pop    %ebx
8010497e:	5d                   	pop    %ebp
8010497f:	c3                   	ret    

80104980 <fetchint>:
80104980:	55                   	push   %ebp
80104981:	89 e5                	mov    %esp,%ebp
80104983:	53                   	push   %ebx
80104984:	83 ec 04             	sub    $0x4,%esp
80104987:	8b 5d 08             	mov    0x8(%ebp),%ebx
8010498a:	e8 31 f1 ff ff       	call   80103ac0 <myproc>
8010498f:	8b 00                	mov    (%eax),%eax
80104991:	39 d8                	cmp    %ebx,%eax
80104993:	76 1b                	jbe    801049b0 <fetchint+0x30>
80104995:	8d 53 04             	lea    0x4(%ebx),%edx
80104998:	39 d0                	cmp    %edx,%eax
8010499a:	72 14                	jb     801049b0 <fetchint+0x30>
8010499c:	8b 45 0c             	mov    0xc(%ebp),%eax
8010499f:	8b 13                	mov    (%ebx),%edx
801049a1:	89 10                	mov    %edx,(%eax)
801049a3:	31 c0                	xor    %eax,%eax
801049a5:	83 c4 04             	add    $0x4,%esp
801049a8:	5b                   	pop    %ebx
801049a9:	5d                   	pop    %ebp
801049aa:	c3                   	ret    
801049ab:	90                   	nop
801049ac:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801049b0:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801049b5:	eb ee                	jmp    801049a5 <fetchint+0x25>
801049b7:	89 f6                	mov    %esi,%esi
801049b9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

801049c0 <fetchstr>:
801049c0:	55                   	push   %ebp
801049c1:	89 e5                	mov    %esp,%ebp
801049c3:	53                   	push   %ebx
801049c4:	83 ec 04             	sub    $0x4,%esp
801049c7:	8b 5d 08             	mov    0x8(%ebp),%ebx
801049ca:	e8 f1 f0 ff ff       	call   80103ac0 <myproc>
801049cf:	39 18                	cmp    %ebx,(%eax)
801049d1:	76 29                	jbe    801049fc <fetchstr+0x3c>
801049d3:	8b 4d 0c             	mov    0xc(%ebp),%ecx
801049d6:	89 da                	mov    %ebx,%edx
801049d8:	89 19                	mov    %ebx,(%ecx)
801049da:	8b 00                	mov    (%eax),%eax
801049dc:	39 c3                	cmp    %eax,%ebx
801049de:	73 1c                	jae    801049fc <fetchstr+0x3c>
801049e0:	80 3b 00             	cmpb   $0x0,(%ebx)
801049e3:	75 10                	jne    801049f5 <fetchstr+0x35>
801049e5:	eb 39                	jmp    80104a20 <fetchstr+0x60>
801049e7:	89 f6                	mov    %esi,%esi
801049e9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
801049f0:	80 3a 00             	cmpb   $0x0,(%edx)
801049f3:	74 1b                	je     80104a10 <fetchstr+0x50>
801049f5:	83 c2 01             	add    $0x1,%edx
801049f8:	39 d0                	cmp    %edx,%eax
801049fa:	77 f4                	ja     801049f0 <fetchstr+0x30>
801049fc:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104a01:	83 c4 04             	add    $0x4,%esp
80104a04:	5b                   	pop    %ebx
80104a05:	5d                   	pop    %ebp
80104a06:	c3                   	ret    
80104a07:	89 f6                	mov    %esi,%esi
80104a09:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
80104a10:	83 c4 04             	add    $0x4,%esp
80104a13:	89 d0                	mov    %edx,%eax
80104a15:	29 d8                	sub    %ebx,%eax
80104a17:	5b                   	pop    %ebx
80104a18:	5d                   	pop    %ebp
80104a19:	c3                   	ret    
80104a1a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80104a20:	31 c0                	xor    %eax,%eax
80104a22:	eb dd                	jmp    80104a01 <fetchstr+0x41>
80104a24:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80104a2a:	8d bf 00 00 00 00    	lea    0x0(%edi),%edi

80104a30 <argint>:
80104a30:	55                   	push   %ebp
80104a31:	89 e5                	mov    %esp,%ebp
80104a33:	56                   	push   %esi
80104a34:	53                   	push   %ebx
80104a35:	e8 86 f0 ff ff       	call   80103ac0 <myproc>
80104a3a:	8b 40 18             	mov    0x18(%eax),%eax
80104a3d:	8b 55 08             	mov    0x8(%ebp),%edx
80104a40:	8b 40 44             	mov    0x44(%eax),%eax
80104a43:	8d 1c 90             	lea    (%eax,%edx,4),%ebx
80104a46:	e8 75 f0 ff ff       	call   80103ac0 <myproc>
80104a4b:	8b 00                	mov    (%eax),%eax
80104a4d:	8d 73 04             	lea    0x4(%ebx),%esi
80104a50:	39 c6                	cmp    %eax,%esi
80104a52:	73 1c                	jae    80104a70 <argint+0x40>
80104a54:	8d 53 08             	lea    0x8(%ebx),%edx
80104a57:	39 d0                	cmp    %edx,%eax
80104a59:	72 15                	jb     80104a70 <argint+0x40>
80104a5b:	8b 45 0c             	mov    0xc(%ebp),%eax
80104a5e:	8b 53 04             	mov    0x4(%ebx),%edx
80104a61:	89 10                	mov    %edx,(%eax)
80104a63:	31 c0                	xor    %eax,%eax
80104a65:	5b                   	pop    %ebx
80104a66:	5e                   	pop    %esi
80104a67:	5d                   	pop    %ebp
80104a68:	c3                   	ret    
80104a69:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104a70:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104a75:	eb ee                	jmp    80104a65 <argint+0x35>
80104a77:	89 f6                	mov    %esi,%esi
80104a79:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80104a80 <argptr>:
80104a80:	55                   	push   %ebp
80104a81:	89 e5                	mov    %esp,%ebp
80104a83:	56                   	push   %esi
80104a84:	53                   	push   %ebx
80104a85:	83 ec 10             	sub    $0x10,%esp
80104a88:	8b 5d 10             	mov    0x10(%ebp),%ebx
80104a8b:	e8 30 f0 ff ff       	call   80103ac0 <myproc>
80104a90:	89 c6                	mov    %eax,%esi
80104a92:	8d 45 f4             	lea    -0xc(%ebp),%eax
80104a95:	83 ec 08             	sub    $0x8,%esp
80104a98:	50                   	push   %eax
80104a99:	ff 75 08             	pushl  0x8(%ebp)
80104a9c:	e8 8f ff ff ff       	call   80104a30 <argint>
80104aa1:	83 c4 10             	add    $0x10,%esp
80104aa4:	85 c0                	test   %eax,%eax
80104aa6:	78 28                	js     80104ad0 <argptr+0x50>
80104aa8:	85 db                	test   %ebx,%ebx
80104aaa:	78 24                	js     80104ad0 <argptr+0x50>
80104aac:	8b 16                	mov    (%esi),%edx
80104aae:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104ab1:	39 c2                	cmp    %eax,%edx
80104ab3:	76 1b                	jbe    80104ad0 <argptr+0x50>
80104ab5:	01 c3                	add    %eax,%ebx
80104ab7:	39 da                	cmp    %ebx,%edx
80104ab9:	72 15                	jb     80104ad0 <argptr+0x50>
80104abb:	8b 55 0c             	mov    0xc(%ebp),%edx
80104abe:	89 02                	mov    %eax,(%edx)
80104ac0:	31 c0                	xor    %eax,%eax
80104ac2:	8d 65 f8             	lea    -0x8(%ebp),%esp
80104ac5:	5b                   	pop    %ebx
80104ac6:	5e                   	pop    %esi
80104ac7:	5d                   	pop    %ebp
80104ac8:	c3                   	ret    
80104ac9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104ad0:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104ad5:	eb eb                	jmp    80104ac2 <argptr+0x42>
80104ad7:	89 f6                	mov    %esi,%esi
80104ad9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80104ae0 <argstr>:
80104ae0:	55                   	push   %ebp
80104ae1:	89 e5                	mov    %esp,%ebp
80104ae3:	83 ec 20             	sub    $0x20,%esp
80104ae6:	8d 45 f4             	lea    -0xc(%ebp),%eax
80104ae9:	50                   	push   %eax
80104aea:	ff 75 08             	pushl  0x8(%ebp)
80104aed:	e8 3e ff ff ff       	call   80104a30 <argint>
80104af2:	83 c4 10             	add    $0x10,%esp
80104af5:	85 c0                	test   %eax,%eax
80104af7:	78 17                	js     80104b10 <argstr+0x30>
80104af9:	83 ec 08             	sub    $0x8,%esp
80104afc:	ff 75 0c             	pushl  0xc(%ebp)
80104aff:	ff 75 f4             	pushl  -0xc(%ebp)
80104b02:	e8 b9 fe ff ff       	call   801049c0 <fetchstr>
80104b07:	83 c4 10             	add    $0x10,%esp
80104b0a:	c9                   	leave  
80104b0b:	c3                   	ret    
80104b0c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80104b10:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104b15:	c9                   	leave  
80104b16:	c3                   	ret    
80104b17:	89 f6                	mov    %esi,%esi
80104b19:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80104b20 <syscall>:
80104b20:	55                   	push   %ebp
80104b21:	89 e5                	mov    %esp,%ebp
80104b23:	53                   	push   %ebx
80104b24:	83 ec 04             	sub    $0x4,%esp
80104b27:	e8 94 ef ff ff       	call   80103ac0 <myproc>
80104b2c:	89 c3                	mov    %eax,%ebx
80104b2e:	8b 40 18             	mov    0x18(%eax),%eax
80104b31:	8b 40 1c             	mov    0x1c(%eax),%eax
80104b34:	8d 50 ff             	lea    -0x1(%eax),%edx
80104b37:	83 fa 14             	cmp    $0x14,%edx
80104b3a:	77 1c                	ja     80104b58 <syscall+0x38>
80104b3c:	8b 14 85 80 78 10 80 	mov    -0x7fef8780(,%eax,4),%edx
80104b43:	85 d2                	test   %edx,%edx
80104b45:	74 11                	je     80104b58 <syscall+0x38>
80104b47:	ff d2                	call   *%edx
80104b49:	8b 53 18             	mov    0x18(%ebx),%edx
80104b4c:	89 42 1c             	mov    %eax,0x1c(%edx)
80104b4f:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80104b52:	c9                   	leave  
80104b53:	c3                   	ret    
80104b54:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80104b58:	50                   	push   %eax
80104b59:	8d 43 6c             	lea    0x6c(%ebx),%eax
80104b5c:	50                   	push   %eax
80104b5d:	ff 73 10             	pushl  0x10(%ebx)
80104b60:	68 51 78 10 80       	push   $0x80107851
80104b65:	e8 26 bc ff ff       	call   80100790 <cprintf>
80104b6a:	8b 43 18             	mov    0x18(%ebx),%eax
80104b6d:	83 c4 10             	add    $0x10,%esp
80104b70:	c7 40 1c ff ff ff ff 	movl   $0xffffffff,0x1c(%eax)
80104b77:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80104b7a:	c9                   	leave  
80104b7b:	c3                   	ret    
80104b7c:	66 90                	xchg   %ax,%ax
80104b7e:	66 90                	xchg   %ax,%ax

80104b80 <create>:
80104b80:	55                   	push   %ebp
80104b81:	89 e5                	mov    %esp,%ebp
80104b83:	57                   	push   %edi
80104b84:	56                   	push   %esi
80104b85:	53                   	push   %ebx
80104b86:	8d 75 da             	lea    -0x26(%ebp),%esi
80104b89:	83 ec 34             	sub    $0x34,%esp
80104b8c:	89 4d d0             	mov    %ecx,-0x30(%ebp)
80104b8f:	8b 4d 08             	mov    0x8(%ebp),%ecx
80104b92:	56                   	push   %esi
80104b93:	50                   	push   %eax
80104b94:	89 55 d4             	mov    %edx,-0x2c(%ebp)
80104b97:	89 4d cc             	mov    %ecx,-0x34(%ebp)
80104b9a:	e8 41 d6 ff ff       	call   801021e0 <nameiparent>
80104b9f:	83 c4 10             	add    $0x10,%esp
80104ba2:	85 c0                	test   %eax,%eax
80104ba4:	0f 84 46 01 00 00    	je     80104cf0 <create+0x170>
80104baa:	83 ec 0c             	sub    $0xc,%esp
80104bad:	89 c3                	mov    %eax,%ebx
80104baf:	50                   	push   %eax
80104bb0:	e8 ab cd ff ff       	call   80101960 <ilock>
80104bb5:	83 c4 0c             	add    $0xc,%esp
80104bb8:	6a 00                	push   $0x0
80104bba:	56                   	push   %esi
80104bbb:	53                   	push   %ebx
80104bbc:	e8 cf d2 ff ff       	call   80101e90 <dirlookup>
80104bc1:	83 c4 10             	add    $0x10,%esp
80104bc4:	85 c0                	test   %eax,%eax
80104bc6:	89 c7                	mov    %eax,%edi
80104bc8:	74 36                	je     80104c00 <create+0x80>
80104bca:	83 ec 0c             	sub    $0xc,%esp
80104bcd:	53                   	push   %ebx
80104bce:	e8 1d d0 ff ff       	call   80101bf0 <iunlockput>
80104bd3:	89 3c 24             	mov    %edi,(%esp)
80104bd6:	e8 85 cd ff ff       	call   80101960 <ilock>
80104bdb:	83 c4 10             	add    $0x10,%esp
80104bde:	66 83 7d d4 02       	cmpw   $0x2,-0x2c(%ebp)
80104be3:	0f 85 97 00 00 00    	jne    80104c80 <create+0x100>
80104be9:	66 83 7f 50 02       	cmpw   $0x2,0x50(%edi)
80104bee:	0f 85 8c 00 00 00    	jne    80104c80 <create+0x100>
80104bf4:	8d 65 f4             	lea    -0xc(%ebp),%esp
80104bf7:	89 f8                	mov    %edi,%eax
80104bf9:	5b                   	pop    %ebx
80104bfa:	5e                   	pop    %esi
80104bfb:	5f                   	pop    %edi
80104bfc:	5d                   	pop    %ebp
80104bfd:	c3                   	ret    
80104bfe:	66 90                	xchg   %ax,%ax
80104c00:	0f bf 45 d4          	movswl -0x2c(%ebp),%eax
80104c04:	83 ec 08             	sub    $0x8,%esp
80104c07:	50                   	push   %eax
80104c08:	ff 33                	pushl  (%ebx)
80104c0a:	e8 e1 cb ff ff       	call   801017f0 <ialloc>
80104c0f:	83 c4 10             	add    $0x10,%esp
80104c12:	85 c0                	test   %eax,%eax
80104c14:	89 c7                	mov    %eax,%edi
80104c16:	0f 84 e8 00 00 00    	je     80104d04 <create+0x184>
80104c1c:	83 ec 0c             	sub    $0xc,%esp
80104c1f:	50                   	push   %eax
80104c20:	e8 3b cd ff ff       	call   80101960 <ilock>
80104c25:	0f b7 45 d0          	movzwl -0x30(%ebp),%eax
80104c29:	66 89 47 52          	mov    %ax,0x52(%edi)
80104c2d:	0f b7 45 cc          	movzwl -0x34(%ebp),%eax
80104c31:	66 89 47 54          	mov    %ax,0x54(%edi)
80104c35:	b8 01 00 00 00       	mov    $0x1,%eax
80104c3a:	66 89 47 56          	mov    %ax,0x56(%edi)
80104c3e:	89 3c 24             	mov    %edi,(%esp)
80104c41:	e8 6a cc ff ff       	call   801018b0 <iupdate>
80104c46:	83 c4 10             	add    $0x10,%esp
80104c49:	66 83 7d d4 01       	cmpw   $0x1,-0x2c(%ebp)
80104c4e:	74 50                	je     80104ca0 <create+0x120>
80104c50:	83 ec 04             	sub    $0x4,%esp
80104c53:	ff 77 04             	pushl  0x4(%edi)
80104c56:	56                   	push   %esi
80104c57:	53                   	push   %ebx
80104c58:	e8 a3 d4 ff ff       	call   80102100 <dirlink>
80104c5d:	83 c4 10             	add    $0x10,%esp
80104c60:	85 c0                	test   %eax,%eax
80104c62:	0f 88 8f 00 00 00    	js     80104cf7 <create+0x177>
80104c68:	83 ec 0c             	sub    $0xc,%esp
80104c6b:	53                   	push   %ebx
80104c6c:	e8 7f cf ff ff       	call   80101bf0 <iunlockput>
80104c71:	83 c4 10             	add    $0x10,%esp
80104c74:	8d 65 f4             	lea    -0xc(%ebp),%esp
80104c77:	89 f8                	mov    %edi,%eax
80104c79:	5b                   	pop    %ebx
80104c7a:	5e                   	pop    %esi
80104c7b:	5f                   	pop    %edi
80104c7c:	5d                   	pop    %ebp
80104c7d:	c3                   	ret    
80104c7e:	66 90                	xchg   %ax,%ax
80104c80:	83 ec 0c             	sub    $0xc,%esp
80104c83:	57                   	push   %edi
80104c84:	31 ff                	xor    %edi,%edi
80104c86:	e8 65 cf ff ff       	call   80101bf0 <iunlockput>
80104c8b:	83 c4 10             	add    $0x10,%esp
80104c8e:	8d 65 f4             	lea    -0xc(%ebp),%esp
80104c91:	89 f8                	mov    %edi,%eax
80104c93:	5b                   	pop    %ebx
80104c94:	5e                   	pop    %esi
80104c95:	5f                   	pop    %edi
80104c96:	5d                   	pop    %ebp
80104c97:	c3                   	ret    
80104c98:	90                   	nop
80104c99:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104ca0:	66 83 43 56 01       	addw   $0x1,0x56(%ebx)
80104ca5:	83 ec 0c             	sub    $0xc,%esp
80104ca8:	53                   	push   %ebx
80104ca9:	e8 02 cc ff ff       	call   801018b0 <iupdate>
80104cae:	83 c4 0c             	add    $0xc,%esp
80104cb1:	ff 77 04             	pushl  0x4(%edi)
80104cb4:	68 f4 78 10 80       	push   $0x801078f4
80104cb9:	57                   	push   %edi
80104cba:	e8 41 d4 ff ff       	call   80102100 <dirlink>
80104cbf:	83 c4 10             	add    $0x10,%esp
80104cc2:	85 c0                	test   %eax,%eax
80104cc4:	78 1c                	js     80104ce2 <create+0x162>
80104cc6:	83 ec 04             	sub    $0x4,%esp
80104cc9:	ff 73 04             	pushl  0x4(%ebx)
80104ccc:	68 f3 78 10 80       	push   $0x801078f3
80104cd1:	57                   	push   %edi
80104cd2:	e8 29 d4 ff ff       	call   80102100 <dirlink>
80104cd7:	83 c4 10             	add    $0x10,%esp
80104cda:	85 c0                	test   %eax,%eax
80104cdc:	0f 89 6e ff ff ff    	jns    80104c50 <create+0xd0>
80104ce2:	83 ec 0c             	sub    $0xc,%esp
80104ce5:	68 e7 78 10 80       	push   $0x801078e7
80104cea:	e8 a1 b6 ff ff       	call   80100390 <panic>
80104cef:	90                   	nop
80104cf0:	31 ff                	xor    %edi,%edi
80104cf2:	e9 fd fe ff ff       	jmp    80104bf4 <create+0x74>
80104cf7:	83 ec 0c             	sub    $0xc,%esp
80104cfa:	68 f6 78 10 80       	push   $0x801078f6
80104cff:	e8 8c b6 ff ff       	call   80100390 <panic>
80104d04:	83 ec 0c             	sub    $0xc,%esp
80104d07:	68 d8 78 10 80       	push   $0x801078d8
80104d0c:	e8 7f b6 ff ff       	call   80100390 <panic>
80104d11:	eb 0d                	jmp    80104d20 <argfd.constprop.0>
80104d13:	90                   	nop
80104d14:	90                   	nop
80104d15:	90                   	nop
80104d16:	90                   	nop
80104d17:	90                   	nop
80104d18:	90                   	nop
80104d19:	90                   	nop
80104d1a:	90                   	nop
80104d1b:	90                   	nop
80104d1c:	90                   	nop
80104d1d:	90                   	nop
80104d1e:	90                   	nop
80104d1f:	90                   	nop

80104d20 <argfd.constprop.0>:
80104d20:	55                   	push   %ebp
80104d21:	89 e5                	mov    %esp,%ebp
80104d23:	56                   	push   %esi
80104d24:	53                   	push   %ebx
80104d25:	89 c3                	mov    %eax,%ebx
80104d27:	8d 45 f4             	lea    -0xc(%ebp),%eax
80104d2a:	89 d6                	mov    %edx,%esi
80104d2c:	83 ec 18             	sub    $0x18,%esp
80104d2f:	50                   	push   %eax
80104d30:	6a 00                	push   $0x0
80104d32:	e8 f9 fc ff ff       	call   80104a30 <argint>
80104d37:	83 c4 10             	add    $0x10,%esp
80104d3a:	85 c0                	test   %eax,%eax
80104d3c:	78 2a                	js     80104d68 <argfd.constprop.0+0x48>
80104d3e:	83 7d f4 0f          	cmpl   $0xf,-0xc(%ebp)
80104d42:	77 24                	ja     80104d68 <argfd.constprop.0+0x48>
80104d44:	e8 77 ed ff ff       	call   80103ac0 <myproc>
80104d49:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104d4c:	8b 44 90 28          	mov    0x28(%eax,%edx,4),%eax
80104d50:	85 c0                	test   %eax,%eax
80104d52:	74 14                	je     80104d68 <argfd.constprop.0+0x48>
80104d54:	85 db                	test   %ebx,%ebx
80104d56:	74 02                	je     80104d5a <argfd.constprop.0+0x3a>
80104d58:	89 13                	mov    %edx,(%ebx)
80104d5a:	89 06                	mov    %eax,(%esi)
80104d5c:	31 c0                	xor    %eax,%eax
80104d5e:	8d 65 f8             	lea    -0x8(%ebp),%esp
80104d61:	5b                   	pop    %ebx
80104d62:	5e                   	pop    %esi
80104d63:	5d                   	pop    %ebp
80104d64:	c3                   	ret    
80104d65:	8d 76 00             	lea    0x0(%esi),%esi
80104d68:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104d6d:	eb ef                	jmp    80104d5e <argfd.constprop.0+0x3e>
80104d6f:	90                   	nop

80104d70 <sys_dup>:
80104d70:	55                   	push   %ebp
80104d71:	31 c0                	xor    %eax,%eax
80104d73:	89 e5                	mov    %esp,%ebp
80104d75:	56                   	push   %esi
80104d76:	53                   	push   %ebx
80104d77:	8d 55 f4             	lea    -0xc(%ebp),%edx
80104d7a:	83 ec 10             	sub    $0x10,%esp
80104d7d:	e8 9e ff ff ff       	call   80104d20 <argfd.constprop.0>
80104d82:	85 c0                	test   %eax,%eax
80104d84:	78 42                	js     80104dc8 <sys_dup+0x58>
80104d86:	8b 75 f4             	mov    -0xc(%ebp),%esi
80104d89:	31 db                	xor    %ebx,%ebx
80104d8b:	e8 30 ed ff ff       	call   80103ac0 <myproc>
80104d90:	eb 0e                	jmp    80104da0 <sys_dup+0x30>
80104d92:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80104d98:	83 c3 01             	add    $0x1,%ebx
80104d9b:	83 fb 10             	cmp    $0x10,%ebx
80104d9e:	74 28                	je     80104dc8 <sys_dup+0x58>
80104da0:	8b 54 98 28          	mov    0x28(%eax,%ebx,4),%edx
80104da4:	85 d2                	test   %edx,%edx
80104da6:	75 f0                	jne    80104d98 <sys_dup+0x28>
80104da8:	89 74 98 28          	mov    %esi,0x28(%eax,%ebx,4)
80104dac:	83 ec 0c             	sub    $0xc,%esp
80104daf:	ff 75 f4             	pushl  -0xc(%ebp)
80104db2:	e8 19 c3 ff ff       	call   801010d0 <filedup>
80104db7:	83 c4 10             	add    $0x10,%esp
80104dba:	8d 65 f8             	lea    -0x8(%ebp),%esp
80104dbd:	89 d8                	mov    %ebx,%eax
80104dbf:	5b                   	pop    %ebx
80104dc0:	5e                   	pop    %esi
80104dc1:	5d                   	pop    %ebp
80104dc2:	c3                   	ret    
80104dc3:	90                   	nop
80104dc4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80104dc8:	8d 65 f8             	lea    -0x8(%ebp),%esp
80104dcb:	bb ff ff ff ff       	mov    $0xffffffff,%ebx
80104dd0:	89 d8                	mov    %ebx,%eax
80104dd2:	5b                   	pop    %ebx
80104dd3:	5e                   	pop    %esi
80104dd4:	5d                   	pop    %ebp
80104dd5:	c3                   	ret    
80104dd6:	8d 76 00             	lea    0x0(%esi),%esi
80104dd9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80104de0 <sys_read>:
80104de0:	55                   	push   %ebp
80104de1:	31 c0                	xor    %eax,%eax
80104de3:	89 e5                	mov    %esp,%ebp
80104de5:	83 ec 18             	sub    $0x18,%esp
80104de8:	8d 55 ec             	lea    -0x14(%ebp),%edx
80104deb:	e8 30 ff ff ff       	call   80104d20 <argfd.constprop.0>
80104df0:	85 c0                	test   %eax,%eax
80104df2:	78 4c                	js     80104e40 <sys_read+0x60>
80104df4:	8d 45 f0             	lea    -0x10(%ebp),%eax
80104df7:	83 ec 08             	sub    $0x8,%esp
80104dfa:	50                   	push   %eax
80104dfb:	6a 02                	push   $0x2
80104dfd:	e8 2e fc ff ff       	call   80104a30 <argint>
80104e02:	83 c4 10             	add    $0x10,%esp
80104e05:	85 c0                	test   %eax,%eax
80104e07:	78 37                	js     80104e40 <sys_read+0x60>
80104e09:	8d 45 f4             	lea    -0xc(%ebp),%eax
80104e0c:	83 ec 04             	sub    $0x4,%esp
80104e0f:	ff 75 f0             	pushl  -0x10(%ebp)
80104e12:	50                   	push   %eax
80104e13:	6a 01                	push   $0x1
80104e15:	e8 66 fc ff ff       	call   80104a80 <argptr>
80104e1a:	83 c4 10             	add    $0x10,%esp
80104e1d:	85 c0                	test   %eax,%eax
80104e1f:	78 1f                	js     80104e40 <sys_read+0x60>
80104e21:	83 ec 04             	sub    $0x4,%esp
80104e24:	ff 75 f0             	pushl  -0x10(%ebp)
80104e27:	ff 75 f4             	pushl  -0xc(%ebp)
80104e2a:	ff 75 ec             	pushl  -0x14(%ebp)
80104e2d:	e8 0e c4 ff ff       	call   80101240 <fileread>
80104e32:	83 c4 10             	add    $0x10,%esp
80104e35:	c9                   	leave  
80104e36:	c3                   	ret    
80104e37:	89 f6                	mov    %esi,%esi
80104e39:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
80104e40:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104e45:	c9                   	leave  
80104e46:	c3                   	ret    
80104e47:	89 f6                	mov    %esi,%esi
80104e49:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80104e50 <sys_write>:
80104e50:	55                   	push   %ebp
80104e51:	31 c0                	xor    %eax,%eax
80104e53:	89 e5                	mov    %esp,%ebp
80104e55:	83 ec 18             	sub    $0x18,%esp
80104e58:	8d 55 ec             	lea    -0x14(%ebp),%edx
80104e5b:	e8 c0 fe ff ff       	call   80104d20 <argfd.constprop.0>
80104e60:	85 c0                	test   %eax,%eax
80104e62:	78 4c                	js     80104eb0 <sys_write+0x60>
80104e64:	8d 45 f0             	lea    -0x10(%ebp),%eax
80104e67:	83 ec 08             	sub    $0x8,%esp
80104e6a:	50                   	push   %eax
80104e6b:	6a 02                	push   $0x2
80104e6d:	e8 be fb ff ff       	call   80104a30 <argint>
80104e72:	83 c4 10             	add    $0x10,%esp
80104e75:	85 c0                	test   %eax,%eax
80104e77:	78 37                	js     80104eb0 <sys_write+0x60>
80104e79:	8d 45 f4             	lea    -0xc(%ebp),%eax
80104e7c:	83 ec 04             	sub    $0x4,%esp
80104e7f:	ff 75 f0             	pushl  -0x10(%ebp)
80104e82:	50                   	push   %eax
80104e83:	6a 01                	push   $0x1
80104e85:	e8 f6 fb ff ff       	call   80104a80 <argptr>
80104e8a:	83 c4 10             	add    $0x10,%esp
80104e8d:	85 c0                	test   %eax,%eax
80104e8f:	78 1f                	js     80104eb0 <sys_write+0x60>
80104e91:	83 ec 04             	sub    $0x4,%esp
80104e94:	ff 75 f0             	pushl  -0x10(%ebp)
80104e97:	ff 75 f4             	pushl  -0xc(%ebp)
80104e9a:	ff 75 ec             	pushl  -0x14(%ebp)
80104e9d:	e8 2e c4 ff ff       	call   801012d0 <filewrite>
80104ea2:	83 c4 10             	add    $0x10,%esp
80104ea5:	c9                   	leave  
80104ea6:	c3                   	ret    
80104ea7:	89 f6                	mov    %esi,%esi
80104ea9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
80104eb0:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104eb5:	c9                   	leave  
80104eb6:	c3                   	ret    
80104eb7:	89 f6                	mov    %esi,%esi
80104eb9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80104ec0 <sys_close>:
80104ec0:	55                   	push   %ebp
80104ec1:	89 e5                	mov    %esp,%ebp
80104ec3:	83 ec 18             	sub    $0x18,%esp
80104ec6:	8d 55 f4             	lea    -0xc(%ebp),%edx
80104ec9:	8d 45 f0             	lea    -0x10(%ebp),%eax
80104ecc:	e8 4f fe ff ff       	call   80104d20 <argfd.constprop.0>
80104ed1:	85 c0                	test   %eax,%eax
80104ed3:	78 2b                	js     80104f00 <sys_close+0x40>
80104ed5:	e8 e6 eb ff ff       	call   80103ac0 <myproc>
80104eda:	8b 55 f0             	mov    -0x10(%ebp),%edx
80104edd:	83 ec 0c             	sub    $0xc,%esp
80104ee0:	c7 44 90 28 00 00 00 	movl   $0x0,0x28(%eax,%edx,4)
80104ee7:	00 
80104ee8:	ff 75 f4             	pushl  -0xc(%ebp)
80104eeb:	e8 30 c2 ff ff       	call   80101120 <fileclose>
80104ef0:	83 c4 10             	add    $0x10,%esp
80104ef3:	31 c0                	xor    %eax,%eax
80104ef5:	c9                   	leave  
80104ef6:	c3                   	ret    
80104ef7:	89 f6                	mov    %esi,%esi
80104ef9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
80104f00:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104f05:	c9                   	leave  
80104f06:	c3                   	ret    
80104f07:	89 f6                	mov    %esi,%esi
80104f09:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80104f10 <sys_fstat>:
80104f10:	55                   	push   %ebp
80104f11:	31 c0                	xor    %eax,%eax
80104f13:	89 e5                	mov    %esp,%ebp
80104f15:	83 ec 18             	sub    $0x18,%esp
80104f18:	8d 55 f0             	lea    -0x10(%ebp),%edx
80104f1b:	e8 00 fe ff ff       	call   80104d20 <argfd.constprop.0>
80104f20:	85 c0                	test   %eax,%eax
80104f22:	78 2c                	js     80104f50 <sys_fstat+0x40>
80104f24:	8d 45 f4             	lea    -0xc(%ebp),%eax
80104f27:	83 ec 04             	sub    $0x4,%esp
80104f2a:	6a 14                	push   $0x14
80104f2c:	50                   	push   %eax
80104f2d:	6a 01                	push   $0x1
80104f2f:	e8 4c fb ff ff       	call   80104a80 <argptr>
80104f34:	83 c4 10             	add    $0x10,%esp
80104f37:	85 c0                	test   %eax,%eax
80104f39:	78 15                	js     80104f50 <sys_fstat+0x40>
80104f3b:	83 ec 08             	sub    $0x8,%esp
80104f3e:	ff 75 f4             	pushl  -0xc(%ebp)
80104f41:	ff 75 f0             	pushl  -0x10(%ebp)
80104f44:	e8 a7 c2 ff ff       	call   801011f0 <filestat>
80104f49:	83 c4 10             	add    $0x10,%esp
80104f4c:	c9                   	leave  
80104f4d:	c3                   	ret    
80104f4e:	66 90                	xchg   %ax,%ax
80104f50:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104f55:	c9                   	leave  
80104f56:	c3                   	ret    
80104f57:	89 f6                	mov    %esi,%esi
80104f59:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80104f60 <sys_link>:
80104f60:	55                   	push   %ebp
80104f61:	89 e5                	mov    %esp,%ebp
80104f63:	57                   	push   %edi
80104f64:	56                   	push   %esi
80104f65:	53                   	push   %ebx
80104f66:	8d 45 d4             	lea    -0x2c(%ebp),%eax
80104f69:	83 ec 34             	sub    $0x34,%esp
80104f6c:	50                   	push   %eax
80104f6d:	6a 00                	push   $0x0
80104f6f:	e8 6c fb ff ff       	call   80104ae0 <argstr>
80104f74:	83 c4 10             	add    $0x10,%esp
80104f77:	85 c0                	test   %eax,%eax
80104f79:	0f 88 fb 00 00 00    	js     8010507a <sys_link+0x11a>
80104f7f:	8d 45 d0             	lea    -0x30(%ebp),%eax
80104f82:	83 ec 08             	sub    $0x8,%esp
80104f85:	50                   	push   %eax
80104f86:	6a 01                	push   $0x1
80104f88:	e8 53 fb ff ff       	call   80104ae0 <argstr>
80104f8d:	83 c4 10             	add    $0x10,%esp
80104f90:	85 c0                	test   %eax,%eax
80104f92:	0f 88 e2 00 00 00    	js     8010507a <sys_link+0x11a>
80104f98:	e8 e3 de ff ff       	call   80102e80 <begin_op>
80104f9d:	83 ec 0c             	sub    $0xc,%esp
80104fa0:	ff 75 d4             	pushl  -0x2c(%ebp)
80104fa3:	e8 18 d2 ff ff       	call   801021c0 <namei>
80104fa8:	83 c4 10             	add    $0x10,%esp
80104fab:	85 c0                	test   %eax,%eax
80104fad:	89 c3                	mov    %eax,%ebx
80104faf:	0f 84 ea 00 00 00    	je     8010509f <sys_link+0x13f>
80104fb5:	83 ec 0c             	sub    $0xc,%esp
80104fb8:	50                   	push   %eax
80104fb9:	e8 a2 c9 ff ff       	call   80101960 <ilock>
80104fbe:	83 c4 10             	add    $0x10,%esp
80104fc1:	66 83 7b 50 01       	cmpw   $0x1,0x50(%ebx)
80104fc6:	0f 84 bb 00 00 00    	je     80105087 <sys_link+0x127>
80104fcc:	66 83 43 56 01       	addw   $0x1,0x56(%ebx)
80104fd1:	83 ec 0c             	sub    $0xc,%esp
80104fd4:	8d 7d da             	lea    -0x26(%ebp),%edi
80104fd7:	53                   	push   %ebx
80104fd8:	e8 d3 c8 ff ff       	call   801018b0 <iupdate>
80104fdd:	89 1c 24             	mov    %ebx,(%esp)
80104fe0:	e8 5b ca ff ff       	call   80101a40 <iunlock>
80104fe5:	58                   	pop    %eax
80104fe6:	5a                   	pop    %edx
80104fe7:	57                   	push   %edi
80104fe8:	ff 75 d0             	pushl  -0x30(%ebp)
80104feb:	e8 f0 d1 ff ff       	call   801021e0 <nameiparent>
80104ff0:	83 c4 10             	add    $0x10,%esp
80104ff3:	85 c0                	test   %eax,%eax
80104ff5:	89 c6                	mov    %eax,%esi
80104ff7:	74 5b                	je     80105054 <sys_link+0xf4>
80104ff9:	83 ec 0c             	sub    $0xc,%esp
80104ffc:	50                   	push   %eax
80104ffd:	e8 5e c9 ff ff       	call   80101960 <ilock>
80105002:	83 c4 10             	add    $0x10,%esp
80105005:	8b 03                	mov    (%ebx),%eax
80105007:	39 06                	cmp    %eax,(%esi)
80105009:	75 3d                	jne    80105048 <sys_link+0xe8>
8010500b:	83 ec 04             	sub    $0x4,%esp
8010500e:	ff 73 04             	pushl  0x4(%ebx)
80105011:	57                   	push   %edi
80105012:	56                   	push   %esi
80105013:	e8 e8 d0 ff ff       	call   80102100 <dirlink>
80105018:	83 c4 10             	add    $0x10,%esp
8010501b:	85 c0                	test   %eax,%eax
8010501d:	78 29                	js     80105048 <sys_link+0xe8>
8010501f:	83 ec 0c             	sub    $0xc,%esp
80105022:	56                   	push   %esi
80105023:	e8 c8 cb ff ff       	call   80101bf0 <iunlockput>
80105028:	89 1c 24             	mov    %ebx,(%esp)
8010502b:	e8 60 ca ff ff       	call   80101a90 <iput>
80105030:	e8 bb de ff ff       	call   80102ef0 <end_op>
80105035:	83 c4 10             	add    $0x10,%esp
80105038:	31 c0                	xor    %eax,%eax
8010503a:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010503d:	5b                   	pop    %ebx
8010503e:	5e                   	pop    %esi
8010503f:	5f                   	pop    %edi
80105040:	5d                   	pop    %ebp
80105041:	c3                   	ret    
80105042:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80105048:	83 ec 0c             	sub    $0xc,%esp
8010504b:	56                   	push   %esi
8010504c:	e8 9f cb ff ff       	call   80101bf0 <iunlockput>
80105051:	83 c4 10             	add    $0x10,%esp
80105054:	83 ec 0c             	sub    $0xc,%esp
80105057:	53                   	push   %ebx
80105058:	e8 03 c9 ff ff       	call   80101960 <ilock>
8010505d:	66 83 6b 56 01       	subw   $0x1,0x56(%ebx)
80105062:	89 1c 24             	mov    %ebx,(%esp)
80105065:	e8 46 c8 ff ff       	call   801018b0 <iupdate>
8010506a:	89 1c 24             	mov    %ebx,(%esp)
8010506d:	e8 7e cb ff ff       	call   80101bf0 <iunlockput>
80105072:	e8 79 de ff ff       	call   80102ef0 <end_op>
80105077:	83 c4 10             	add    $0x10,%esp
8010507a:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010507d:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105082:	5b                   	pop    %ebx
80105083:	5e                   	pop    %esi
80105084:	5f                   	pop    %edi
80105085:	5d                   	pop    %ebp
80105086:	c3                   	ret    
80105087:	83 ec 0c             	sub    $0xc,%esp
8010508a:	53                   	push   %ebx
8010508b:	e8 60 cb ff ff       	call   80101bf0 <iunlockput>
80105090:	e8 5b de ff ff       	call   80102ef0 <end_op>
80105095:	83 c4 10             	add    $0x10,%esp
80105098:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010509d:	eb 9b                	jmp    8010503a <sys_link+0xda>
8010509f:	e8 4c de ff ff       	call   80102ef0 <end_op>
801050a4:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801050a9:	eb 8f                	jmp    8010503a <sys_link+0xda>
801050ab:	90                   	nop
801050ac:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

801050b0 <sys_unlink>:
801050b0:	55                   	push   %ebp
801050b1:	89 e5                	mov    %esp,%ebp
801050b3:	57                   	push   %edi
801050b4:	56                   	push   %esi
801050b5:	53                   	push   %ebx
801050b6:	8d 45 c0             	lea    -0x40(%ebp),%eax
801050b9:	83 ec 44             	sub    $0x44,%esp
801050bc:	50                   	push   %eax
801050bd:	6a 00                	push   $0x0
801050bf:	e8 1c fa ff ff       	call   80104ae0 <argstr>
801050c4:	83 c4 10             	add    $0x10,%esp
801050c7:	85 c0                	test   %eax,%eax
801050c9:	0f 88 77 01 00 00    	js     80105246 <sys_unlink+0x196>
801050cf:	8d 5d ca             	lea    -0x36(%ebp),%ebx
801050d2:	e8 a9 dd ff ff       	call   80102e80 <begin_op>
801050d7:	83 ec 08             	sub    $0x8,%esp
801050da:	53                   	push   %ebx
801050db:	ff 75 c0             	pushl  -0x40(%ebp)
801050de:	e8 fd d0 ff ff       	call   801021e0 <nameiparent>
801050e3:	83 c4 10             	add    $0x10,%esp
801050e6:	85 c0                	test   %eax,%eax
801050e8:	89 c6                	mov    %eax,%esi
801050ea:	0f 84 60 01 00 00    	je     80105250 <sys_unlink+0x1a0>
801050f0:	83 ec 0c             	sub    $0xc,%esp
801050f3:	50                   	push   %eax
801050f4:	e8 67 c8 ff ff       	call   80101960 <ilock>
801050f9:	58                   	pop    %eax
801050fa:	5a                   	pop    %edx
801050fb:	68 f4 78 10 80       	push   $0x801078f4
80105100:	53                   	push   %ebx
80105101:	e8 6a cd ff ff       	call   80101e70 <namecmp>
80105106:	83 c4 10             	add    $0x10,%esp
80105109:	85 c0                	test   %eax,%eax
8010510b:	0f 84 03 01 00 00    	je     80105214 <sys_unlink+0x164>
80105111:	83 ec 08             	sub    $0x8,%esp
80105114:	68 f3 78 10 80       	push   $0x801078f3
80105119:	53                   	push   %ebx
8010511a:	e8 51 cd ff ff       	call   80101e70 <namecmp>
8010511f:	83 c4 10             	add    $0x10,%esp
80105122:	85 c0                	test   %eax,%eax
80105124:	0f 84 ea 00 00 00    	je     80105214 <sys_unlink+0x164>
8010512a:	8d 45 c4             	lea    -0x3c(%ebp),%eax
8010512d:	83 ec 04             	sub    $0x4,%esp
80105130:	50                   	push   %eax
80105131:	53                   	push   %ebx
80105132:	56                   	push   %esi
80105133:	e8 58 cd ff ff       	call   80101e90 <dirlookup>
80105138:	83 c4 10             	add    $0x10,%esp
8010513b:	85 c0                	test   %eax,%eax
8010513d:	89 c3                	mov    %eax,%ebx
8010513f:	0f 84 cf 00 00 00    	je     80105214 <sys_unlink+0x164>
80105145:	83 ec 0c             	sub    $0xc,%esp
80105148:	50                   	push   %eax
80105149:	e8 12 c8 ff ff       	call   80101960 <ilock>
8010514e:	83 c4 10             	add    $0x10,%esp
80105151:	66 83 7b 56 00       	cmpw   $0x0,0x56(%ebx)
80105156:	0f 8e 10 01 00 00    	jle    8010526c <sys_unlink+0x1bc>
8010515c:	66 83 7b 50 01       	cmpw   $0x1,0x50(%ebx)
80105161:	74 6d                	je     801051d0 <sys_unlink+0x120>
80105163:	8d 45 d8             	lea    -0x28(%ebp),%eax
80105166:	83 ec 04             	sub    $0x4,%esp
80105169:	6a 10                	push   $0x10
8010516b:	6a 00                	push   $0x0
8010516d:	50                   	push   %eax
8010516e:	e8 bd f5 ff ff       	call   80104730 <memset>
80105173:	8d 45 d8             	lea    -0x28(%ebp),%eax
80105176:	6a 10                	push   $0x10
80105178:	ff 75 c4             	pushl  -0x3c(%ebp)
8010517b:	50                   	push   %eax
8010517c:	56                   	push   %esi
8010517d:	e8 be cb ff ff       	call   80101d40 <writei>
80105182:	83 c4 20             	add    $0x20,%esp
80105185:	83 f8 10             	cmp    $0x10,%eax
80105188:	0f 85 eb 00 00 00    	jne    80105279 <sys_unlink+0x1c9>
8010518e:	66 83 7b 50 01       	cmpw   $0x1,0x50(%ebx)
80105193:	0f 84 97 00 00 00    	je     80105230 <sys_unlink+0x180>
80105199:	83 ec 0c             	sub    $0xc,%esp
8010519c:	56                   	push   %esi
8010519d:	e8 4e ca ff ff       	call   80101bf0 <iunlockput>
801051a2:	66 83 6b 56 01       	subw   $0x1,0x56(%ebx)
801051a7:	89 1c 24             	mov    %ebx,(%esp)
801051aa:	e8 01 c7 ff ff       	call   801018b0 <iupdate>
801051af:	89 1c 24             	mov    %ebx,(%esp)
801051b2:	e8 39 ca ff ff       	call   80101bf0 <iunlockput>
801051b7:	e8 34 dd ff ff       	call   80102ef0 <end_op>
801051bc:	83 c4 10             	add    $0x10,%esp
801051bf:	31 c0                	xor    %eax,%eax
801051c1:	8d 65 f4             	lea    -0xc(%ebp),%esp
801051c4:	5b                   	pop    %ebx
801051c5:	5e                   	pop    %esi
801051c6:	5f                   	pop    %edi
801051c7:	5d                   	pop    %ebp
801051c8:	c3                   	ret    
801051c9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801051d0:	83 7b 58 20          	cmpl   $0x20,0x58(%ebx)
801051d4:	76 8d                	jbe    80105163 <sys_unlink+0xb3>
801051d6:	bf 20 00 00 00       	mov    $0x20,%edi
801051db:	eb 0f                	jmp    801051ec <sys_unlink+0x13c>
801051dd:	8d 76 00             	lea    0x0(%esi),%esi
801051e0:	83 c7 10             	add    $0x10,%edi
801051e3:	3b 7b 58             	cmp    0x58(%ebx),%edi
801051e6:	0f 83 77 ff ff ff    	jae    80105163 <sys_unlink+0xb3>
801051ec:	8d 45 d8             	lea    -0x28(%ebp),%eax
801051ef:	6a 10                	push   $0x10
801051f1:	57                   	push   %edi
801051f2:	50                   	push   %eax
801051f3:	53                   	push   %ebx
801051f4:	e8 47 ca ff ff       	call   80101c40 <readi>
801051f9:	83 c4 10             	add    $0x10,%esp
801051fc:	83 f8 10             	cmp    $0x10,%eax
801051ff:	75 5e                	jne    8010525f <sys_unlink+0x1af>
80105201:	66 83 7d d8 00       	cmpw   $0x0,-0x28(%ebp)
80105206:	74 d8                	je     801051e0 <sys_unlink+0x130>
80105208:	83 ec 0c             	sub    $0xc,%esp
8010520b:	53                   	push   %ebx
8010520c:	e8 df c9 ff ff       	call   80101bf0 <iunlockput>
80105211:	83 c4 10             	add    $0x10,%esp
80105214:	83 ec 0c             	sub    $0xc,%esp
80105217:	56                   	push   %esi
80105218:	e8 d3 c9 ff ff       	call   80101bf0 <iunlockput>
8010521d:	e8 ce dc ff ff       	call   80102ef0 <end_op>
80105222:	83 c4 10             	add    $0x10,%esp
80105225:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010522a:	eb 95                	jmp    801051c1 <sys_unlink+0x111>
8010522c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80105230:	66 83 6e 56 01       	subw   $0x1,0x56(%esi)
80105235:	83 ec 0c             	sub    $0xc,%esp
80105238:	56                   	push   %esi
80105239:	e8 72 c6 ff ff       	call   801018b0 <iupdate>
8010523e:	83 c4 10             	add    $0x10,%esp
80105241:	e9 53 ff ff ff       	jmp    80105199 <sys_unlink+0xe9>
80105246:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010524b:	e9 71 ff ff ff       	jmp    801051c1 <sys_unlink+0x111>
80105250:	e8 9b dc ff ff       	call   80102ef0 <end_op>
80105255:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010525a:	e9 62 ff ff ff       	jmp    801051c1 <sys_unlink+0x111>
8010525f:	83 ec 0c             	sub    $0xc,%esp
80105262:	68 18 79 10 80       	push   $0x80107918
80105267:	e8 24 b1 ff ff       	call   80100390 <panic>
8010526c:	83 ec 0c             	sub    $0xc,%esp
8010526f:	68 06 79 10 80       	push   $0x80107906
80105274:	e8 17 b1 ff ff       	call   80100390 <panic>
80105279:	83 ec 0c             	sub    $0xc,%esp
8010527c:	68 2a 79 10 80       	push   $0x8010792a
80105281:	e8 0a b1 ff ff       	call   80100390 <panic>
80105286:	8d 76 00             	lea    0x0(%esi),%esi
80105289:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80105290 <sys_open>:
80105290:	55                   	push   %ebp
80105291:	89 e5                	mov    %esp,%ebp
80105293:	57                   	push   %edi
80105294:	56                   	push   %esi
80105295:	53                   	push   %ebx
80105296:	8d 45 e0             	lea    -0x20(%ebp),%eax
80105299:	83 ec 24             	sub    $0x24,%esp
8010529c:	50                   	push   %eax
8010529d:	6a 00                	push   $0x0
8010529f:	e8 3c f8 ff ff       	call   80104ae0 <argstr>
801052a4:	83 c4 10             	add    $0x10,%esp
801052a7:	85 c0                	test   %eax,%eax
801052a9:	0f 88 1d 01 00 00    	js     801053cc <sys_open+0x13c>
801052af:	8d 45 e4             	lea    -0x1c(%ebp),%eax
801052b2:	83 ec 08             	sub    $0x8,%esp
801052b5:	50                   	push   %eax
801052b6:	6a 01                	push   $0x1
801052b8:	e8 73 f7 ff ff       	call   80104a30 <argint>
801052bd:	83 c4 10             	add    $0x10,%esp
801052c0:	85 c0                	test   %eax,%eax
801052c2:	0f 88 04 01 00 00    	js     801053cc <sys_open+0x13c>
801052c8:	e8 b3 db ff ff       	call   80102e80 <begin_op>
801052cd:	f6 45 e5 02          	testb  $0x2,-0x1b(%ebp)
801052d1:	0f 85 a9 00 00 00    	jne    80105380 <sys_open+0xf0>
801052d7:	83 ec 0c             	sub    $0xc,%esp
801052da:	ff 75 e0             	pushl  -0x20(%ebp)
801052dd:	e8 de ce ff ff       	call   801021c0 <namei>
801052e2:	83 c4 10             	add    $0x10,%esp
801052e5:	85 c0                	test   %eax,%eax
801052e7:	89 c6                	mov    %eax,%esi
801052e9:	0f 84 b2 00 00 00    	je     801053a1 <sys_open+0x111>
801052ef:	83 ec 0c             	sub    $0xc,%esp
801052f2:	50                   	push   %eax
801052f3:	e8 68 c6 ff ff       	call   80101960 <ilock>
801052f8:	83 c4 10             	add    $0x10,%esp
801052fb:	66 83 7e 50 01       	cmpw   $0x1,0x50(%esi)
80105300:	0f 84 aa 00 00 00    	je     801053b0 <sys_open+0x120>
80105306:	e8 55 bd ff ff       	call   80101060 <filealloc>
8010530b:	85 c0                	test   %eax,%eax
8010530d:	89 c7                	mov    %eax,%edi
8010530f:	0f 84 a6 00 00 00    	je     801053bb <sys_open+0x12b>
80105315:	e8 a6 e7 ff ff       	call   80103ac0 <myproc>
8010531a:	31 db                	xor    %ebx,%ebx
8010531c:	eb 0e                	jmp    8010532c <sys_open+0x9c>
8010531e:	66 90                	xchg   %ax,%ax
80105320:	83 c3 01             	add    $0x1,%ebx
80105323:	83 fb 10             	cmp    $0x10,%ebx
80105326:	0f 84 ac 00 00 00    	je     801053d8 <sys_open+0x148>
8010532c:	8b 54 98 28          	mov    0x28(%eax,%ebx,4),%edx
80105330:	85 d2                	test   %edx,%edx
80105332:	75 ec                	jne    80105320 <sys_open+0x90>
80105334:	83 ec 0c             	sub    $0xc,%esp
80105337:	89 7c 98 28          	mov    %edi,0x28(%eax,%ebx,4)
8010533b:	56                   	push   %esi
8010533c:	e8 ff c6 ff ff       	call   80101a40 <iunlock>
80105341:	e8 aa db ff ff       	call   80102ef0 <end_op>
80105346:	c7 07 02 00 00 00    	movl   $0x2,(%edi)
8010534c:	8b 55 e4             	mov    -0x1c(%ebp),%edx
8010534f:	83 c4 10             	add    $0x10,%esp
80105352:	89 77 10             	mov    %esi,0x10(%edi)
80105355:	c7 47 14 00 00 00 00 	movl   $0x0,0x14(%edi)
8010535c:	89 d0                	mov    %edx,%eax
8010535e:	f7 d0                	not    %eax
80105360:	83 e0 01             	and    $0x1,%eax
80105363:	83 e2 03             	and    $0x3,%edx
80105366:	88 47 08             	mov    %al,0x8(%edi)
80105369:	0f 95 47 09          	setne  0x9(%edi)
8010536d:	8d 65 f4             	lea    -0xc(%ebp),%esp
80105370:	89 d8                	mov    %ebx,%eax
80105372:	5b                   	pop    %ebx
80105373:	5e                   	pop    %esi
80105374:	5f                   	pop    %edi
80105375:	5d                   	pop    %ebp
80105376:	c3                   	ret    
80105377:	89 f6                	mov    %esi,%esi
80105379:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
80105380:	83 ec 0c             	sub    $0xc,%esp
80105383:	8b 45 e0             	mov    -0x20(%ebp),%eax
80105386:	31 c9                	xor    %ecx,%ecx
80105388:	6a 00                	push   $0x0
8010538a:	ba 02 00 00 00       	mov    $0x2,%edx
8010538f:	e8 ec f7 ff ff       	call   80104b80 <create>
80105394:	83 c4 10             	add    $0x10,%esp
80105397:	85 c0                	test   %eax,%eax
80105399:	89 c6                	mov    %eax,%esi
8010539b:	0f 85 65 ff ff ff    	jne    80105306 <sys_open+0x76>
801053a1:	e8 4a db ff ff       	call   80102ef0 <end_op>
801053a6:	bb ff ff ff ff       	mov    $0xffffffff,%ebx
801053ab:	eb c0                	jmp    8010536d <sys_open+0xdd>
801053ad:	8d 76 00             	lea    0x0(%esi),%esi
801053b0:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
801053b3:	85 c9                	test   %ecx,%ecx
801053b5:	0f 84 4b ff ff ff    	je     80105306 <sys_open+0x76>
801053bb:	83 ec 0c             	sub    $0xc,%esp
801053be:	56                   	push   %esi
801053bf:	e8 2c c8 ff ff       	call   80101bf0 <iunlockput>
801053c4:	e8 27 db ff ff       	call   80102ef0 <end_op>
801053c9:	83 c4 10             	add    $0x10,%esp
801053cc:	bb ff ff ff ff       	mov    $0xffffffff,%ebx
801053d1:	eb 9a                	jmp    8010536d <sys_open+0xdd>
801053d3:	90                   	nop
801053d4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801053d8:	83 ec 0c             	sub    $0xc,%esp
801053db:	57                   	push   %edi
801053dc:	e8 3f bd ff ff       	call   80101120 <fileclose>
801053e1:	83 c4 10             	add    $0x10,%esp
801053e4:	eb d5                	jmp    801053bb <sys_open+0x12b>
801053e6:	8d 76 00             	lea    0x0(%esi),%esi
801053e9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

801053f0 <sys_mkdir>:
801053f0:	55                   	push   %ebp
801053f1:	89 e5                	mov    %esp,%ebp
801053f3:	83 ec 18             	sub    $0x18,%esp
801053f6:	e8 85 da ff ff       	call   80102e80 <begin_op>
801053fb:	8d 45 f4             	lea    -0xc(%ebp),%eax
801053fe:	83 ec 08             	sub    $0x8,%esp
80105401:	50                   	push   %eax
80105402:	6a 00                	push   $0x0
80105404:	e8 d7 f6 ff ff       	call   80104ae0 <argstr>
80105409:	83 c4 10             	add    $0x10,%esp
8010540c:	85 c0                	test   %eax,%eax
8010540e:	78 30                	js     80105440 <sys_mkdir+0x50>
80105410:	83 ec 0c             	sub    $0xc,%esp
80105413:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105416:	31 c9                	xor    %ecx,%ecx
80105418:	6a 00                	push   $0x0
8010541a:	ba 01 00 00 00       	mov    $0x1,%edx
8010541f:	e8 5c f7 ff ff       	call   80104b80 <create>
80105424:	83 c4 10             	add    $0x10,%esp
80105427:	85 c0                	test   %eax,%eax
80105429:	74 15                	je     80105440 <sys_mkdir+0x50>
8010542b:	83 ec 0c             	sub    $0xc,%esp
8010542e:	50                   	push   %eax
8010542f:	e8 bc c7 ff ff       	call   80101bf0 <iunlockput>
80105434:	e8 b7 da ff ff       	call   80102ef0 <end_op>
80105439:	83 c4 10             	add    $0x10,%esp
8010543c:	31 c0                	xor    %eax,%eax
8010543e:	c9                   	leave  
8010543f:	c3                   	ret    
80105440:	e8 ab da ff ff       	call   80102ef0 <end_op>
80105445:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010544a:	c9                   	leave  
8010544b:	c3                   	ret    
8010544c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80105450 <sys_mknod>:
80105450:	55                   	push   %ebp
80105451:	89 e5                	mov    %esp,%ebp
80105453:	83 ec 18             	sub    $0x18,%esp
80105456:	e8 25 da ff ff       	call   80102e80 <begin_op>
8010545b:	8d 45 ec             	lea    -0x14(%ebp),%eax
8010545e:	83 ec 08             	sub    $0x8,%esp
80105461:	50                   	push   %eax
80105462:	6a 00                	push   $0x0
80105464:	e8 77 f6 ff ff       	call   80104ae0 <argstr>
80105469:	83 c4 10             	add    $0x10,%esp
8010546c:	85 c0                	test   %eax,%eax
8010546e:	78 60                	js     801054d0 <sys_mknod+0x80>
80105470:	8d 45 f0             	lea    -0x10(%ebp),%eax
80105473:	83 ec 08             	sub    $0x8,%esp
80105476:	50                   	push   %eax
80105477:	6a 01                	push   $0x1
80105479:	e8 b2 f5 ff ff       	call   80104a30 <argint>
8010547e:	83 c4 10             	add    $0x10,%esp
80105481:	85 c0                	test   %eax,%eax
80105483:	78 4b                	js     801054d0 <sys_mknod+0x80>
80105485:	8d 45 f4             	lea    -0xc(%ebp),%eax
80105488:	83 ec 08             	sub    $0x8,%esp
8010548b:	50                   	push   %eax
8010548c:	6a 02                	push   $0x2
8010548e:	e8 9d f5 ff ff       	call   80104a30 <argint>
80105493:	83 c4 10             	add    $0x10,%esp
80105496:	85 c0                	test   %eax,%eax
80105498:	78 36                	js     801054d0 <sys_mknod+0x80>
8010549a:	0f bf 45 f4          	movswl -0xc(%ebp),%eax
8010549e:	83 ec 0c             	sub    $0xc,%esp
801054a1:	0f bf 4d f0          	movswl -0x10(%ebp),%ecx
801054a5:	ba 03 00 00 00       	mov    $0x3,%edx
801054aa:	50                   	push   %eax
801054ab:	8b 45 ec             	mov    -0x14(%ebp),%eax
801054ae:	e8 cd f6 ff ff       	call   80104b80 <create>
801054b3:	83 c4 10             	add    $0x10,%esp
801054b6:	85 c0                	test   %eax,%eax
801054b8:	74 16                	je     801054d0 <sys_mknod+0x80>
801054ba:	83 ec 0c             	sub    $0xc,%esp
801054bd:	50                   	push   %eax
801054be:	e8 2d c7 ff ff       	call   80101bf0 <iunlockput>
801054c3:	e8 28 da ff ff       	call   80102ef0 <end_op>
801054c8:	83 c4 10             	add    $0x10,%esp
801054cb:	31 c0                	xor    %eax,%eax
801054cd:	c9                   	leave  
801054ce:	c3                   	ret    
801054cf:	90                   	nop
801054d0:	e8 1b da ff ff       	call   80102ef0 <end_op>
801054d5:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801054da:	c9                   	leave  
801054db:	c3                   	ret    
801054dc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

801054e0 <sys_chdir>:
801054e0:	55                   	push   %ebp
801054e1:	89 e5                	mov    %esp,%ebp
801054e3:	56                   	push   %esi
801054e4:	53                   	push   %ebx
801054e5:	83 ec 10             	sub    $0x10,%esp
801054e8:	e8 d3 e5 ff ff       	call   80103ac0 <myproc>
801054ed:	89 c6                	mov    %eax,%esi
801054ef:	e8 8c d9 ff ff       	call   80102e80 <begin_op>
801054f4:	8d 45 f4             	lea    -0xc(%ebp),%eax
801054f7:	83 ec 08             	sub    $0x8,%esp
801054fa:	50                   	push   %eax
801054fb:	6a 00                	push   $0x0
801054fd:	e8 de f5 ff ff       	call   80104ae0 <argstr>
80105502:	83 c4 10             	add    $0x10,%esp
80105505:	85 c0                	test   %eax,%eax
80105507:	78 77                	js     80105580 <sys_chdir+0xa0>
80105509:	83 ec 0c             	sub    $0xc,%esp
8010550c:	ff 75 f4             	pushl  -0xc(%ebp)
8010550f:	e8 ac cc ff ff       	call   801021c0 <namei>
80105514:	83 c4 10             	add    $0x10,%esp
80105517:	85 c0                	test   %eax,%eax
80105519:	89 c3                	mov    %eax,%ebx
8010551b:	74 63                	je     80105580 <sys_chdir+0xa0>
8010551d:	83 ec 0c             	sub    $0xc,%esp
80105520:	50                   	push   %eax
80105521:	e8 3a c4 ff ff       	call   80101960 <ilock>
80105526:	83 c4 10             	add    $0x10,%esp
80105529:	66 83 7b 50 01       	cmpw   $0x1,0x50(%ebx)
8010552e:	75 30                	jne    80105560 <sys_chdir+0x80>
80105530:	83 ec 0c             	sub    $0xc,%esp
80105533:	53                   	push   %ebx
80105534:	e8 07 c5 ff ff       	call   80101a40 <iunlock>
80105539:	58                   	pop    %eax
8010553a:	ff 76 68             	pushl  0x68(%esi)
8010553d:	e8 4e c5 ff ff       	call   80101a90 <iput>
80105542:	e8 a9 d9 ff ff       	call   80102ef0 <end_op>
80105547:	89 5e 68             	mov    %ebx,0x68(%esi)
8010554a:	83 c4 10             	add    $0x10,%esp
8010554d:	31 c0                	xor    %eax,%eax
8010554f:	8d 65 f8             	lea    -0x8(%ebp),%esp
80105552:	5b                   	pop    %ebx
80105553:	5e                   	pop    %esi
80105554:	5d                   	pop    %ebp
80105555:	c3                   	ret    
80105556:	8d 76 00             	lea    0x0(%esi),%esi
80105559:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
80105560:	83 ec 0c             	sub    $0xc,%esp
80105563:	53                   	push   %ebx
80105564:	e8 87 c6 ff ff       	call   80101bf0 <iunlockput>
80105569:	e8 82 d9 ff ff       	call   80102ef0 <end_op>
8010556e:	83 c4 10             	add    $0x10,%esp
80105571:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105576:	eb d7                	jmp    8010554f <sys_chdir+0x6f>
80105578:	90                   	nop
80105579:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80105580:	e8 6b d9 ff ff       	call   80102ef0 <end_op>
80105585:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010558a:	eb c3                	jmp    8010554f <sys_chdir+0x6f>
8010558c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80105590 <sys_exec>:
80105590:	55                   	push   %ebp
80105591:	89 e5                	mov    %esp,%ebp
80105593:	57                   	push   %edi
80105594:	56                   	push   %esi
80105595:	53                   	push   %ebx
80105596:	8d 85 5c ff ff ff    	lea    -0xa4(%ebp),%eax
8010559c:	81 ec a4 00 00 00    	sub    $0xa4,%esp
801055a2:	50                   	push   %eax
801055a3:	6a 00                	push   $0x0
801055a5:	e8 36 f5 ff ff       	call   80104ae0 <argstr>
801055aa:	83 c4 10             	add    $0x10,%esp
801055ad:	85 c0                	test   %eax,%eax
801055af:	0f 88 87 00 00 00    	js     8010563c <sys_exec+0xac>
801055b5:	8d 85 60 ff ff ff    	lea    -0xa0(%ebp),%eax
801055bb:	83 ec 08             	sub    $0x8,%esp
801055be:	50                   	push   %eax
801055bf:	6a 01                	push   $0x1
801055c1:	e8 6a f4 ff ff       	call   80104a30 <argint>
801055c6:	83 c4 10             	add    $0x10,%esp
801055c9:	85 c0                	test   %eax,%eax
801055cb:	78 6f                	js     8010563c <sys_exec+0xac>
801055cd:	8d 85 68 ff ff ff    	lea    -0x98(%ebp),%eax
801055d3:	83 ec 04             	sub    $0x4,%esp
801055d6:	31 db                	xor    %ebx,%ebx
801055d8:	68 80 00 00 00       	push   $0x80
801055dd:	6a 00                	push   $0x0
801055df:	8d bd 64 ff ff ff    	lea    -0x9c(%ebp),%edi
801055e5:	50                   	push   %eax
801055e6:	e8 45 f1 ff ff       	call   80104730 <memset>
801055eb:	83 c4 10             	add    $0x10,%esp
801055ee:	eb 2c                	jmp    8010561c <sys_exec+0x8c>
801055f0:	8b 85 64 ff ff ff    	mov    -0x9c(%ebp),%eax
801055f6:	85 c0                	test   %eax,%eax
801055f8:	74 56                	je     80105650 <sys_exec+0xc0>
801055fa:	8d 8d 68 ff ff ff    	lea    -0x98(%ebp),%ecx
80105600:	83 ec 08             	sub    $0x8,%esp
80105603:	8d 14 31             	lea    (%ecx,%esi,1),%edx
80105606:	52                   	push   %edx
80105607:	50                   	push   %eax
80105608:	e8 b3 f3 ff ff       	call   801049c0 <fetchstr>
8010560d:	83 c4 10             	add    $0x10,%esp
80105610:	85 c0                	test   %eax,%eax
80105612:	78 28                	js     8010563c <sys_exec+0xac>
80105614:	83 c3 01             	add    $0x1,%ebx
80105617:	83 fb 20             	cmp    $0x20,%ebx
8010561a:	74 20                	je     8010563c <sys_exec+0xac>
8010561c:	8b 85 60 ff ff ff    	mov    -0xa0(%ebp),%eax
80105622:	8d 34 9d 00 00 00 00 	lea    0x0(,%ebx,4),%esi
80105629:	83 ec 08             	sub    $0x8,%esp
8010562c:	57                   	push   %edi
8010562d:	01 f0                	add    %esi,%eax
8010562f:	50                   	push   %eax
80105630:	e8 4b f3 ff ff       	call   80104980 <fetchint>
80105635:	83 c4 10             	add    $0x10,%esp
80105638:	85 c0                	test   %eax,%eax
8010563a:	79 b4                	jns    801055f0 <sys_exec+0x60>
8010563c:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010563f:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105644:	5b                   	pop    %ebx
80105645:	5e                   	pop    %esi
80105646:	5f                   	pop    %edi
80105647:	5d                   	pop    %ebp
80105648:	c3                   	ret    
80105649:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80105650:	8d 85 68 ff ff ff    	lea    -0x98(%ebp),%eax
80105656:	83 ec 08             	sub    $0x8,%esp
80105659:	c7 84 9d 68 ff ff ff 	movl   $0x0,-0x98(%ebp,%ebx,4)
80105660:	00 00 00 00 
80105664:	50                   	push   %eax
80105665:	ff b5 5c ff ff ff    	pushl  -0xa4(%ebp)
8010566b:	e8 80 b6 ff ff       	call   80100cf0 <exec>
80105670:	83 c4 10             	add    $0x10,%esp
80105673:	8d 65 f4             	lea    -0xc(%ebp),%esp
80105676:	5b                   	pop    %ebx
80105677:	5e                   	pop    %esi
80105678:	5f                   	pop    %edi
80105679:	5d                   	pop    %ebp
8010567a:	c3                   	ret    
8010567b:	90                   	nop
8010567c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80105680 <sys_pipe>:
80105680:	55                   	push   %ebp
80105681:	89 e5                	mov    %esp,%ebp
80105683:	57                   	push   %edi
80105684:	56                   	push   %esi
80105685:	53                   	push   %ebx
80105686:	8d 45 dc             	lea    -0x24(%ebp),%eax
80105689:	83 ec 20             	sub    $0x20,%esp
8010568c:	6a 08                	push   $0x8
8010568e:	50                   	push   %eax
8010568f:	6a 00                	push   $0x0
80105691:	e8 ea f3 ff ff       	call   80104a80 <argptr>
80105696:	83 c4 10             	add    $0x10,%esp
80105699:	85 c0                	test   %eax,%eax
8010569b:	0f 88 ae 00 00 00    	js     8010574f <sys_pipe+0xcf>
801056a1:	8d 45 e4             	lea    -0x1c(%ebp),%eax
801056a4:	83 ec 08             	sub    $0x8,%esp
801056a7:	50                   	push   %eax
801056a8:	8d 45 e0             	lea    -0x20(%ebp),%eax
801056ab:	50                   	push   %eax
801056ac:	e8 6f de ff ff       	call   80103520 <pipealloc>
801056b1:	83 c4 10             	add    $0x10,%esp
801056b4:	85 c0                	test   %eax,%eax
801056b6:	0f 88 93 00 00 00    	js     8010574f <sys_pipe+0xcf>
801056bc:	8b 7d e0             	mov    -0x20(%ebp),%edi
801056bf:	31 db                	xor    %ebx,%ebx
801056c1:	e8 fa e3 ff ff       	call   80103ac0 <myproc>
801056c6:	eb 10                	jmp    801056d8 <sys_pipe+0x58>
801056c8:	90                   	nop
801056c9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801056d0:	83 c3 01             	add    $0x1,%ebx
801056d3:	83 fb 10             	cmp    $0x10,%ebx
801056d6:	74 60                	je     80105738 <sys_pipe+0xb8>
801056d8:	8b 74 98 28          	mov    0x28(%eax,%ebx,4),%esi
801056dc:	85 f6                	test   %esi,%esi
801056de:	75 f0                	jne    801056d0 <sys_pipe+0x50>
801056e0:	8d 73 08             	lea    0x8(%ebx),%esi
801056e3:	89 7c b0 08          	mov    %edi,0x8(%eax,%esi,4)
801056e7:	8b 7d e4             	mov    -0x1c(%ebp),%edi
801056ea:	e8 d1 e3 ff ff       	call   80103ac0 <myproc>
801056ef:	31 d2                	xor    %edx,%edx
801056f1:	eb 0d                	jmp    80105700 <sys_pipe+0x80>
801056f3:	90                   	nop
801056f4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801056f8:	83 c2 01             	add    $0x1,%edx
801056fb:	83 fa 10             	cmp    $0x10,%edx
801056fe:	74 28                	je     80105728 <sys_pipe+0xa8>
80105700:	8b 4c 90 28          	mov    0x28(%eax,%edx,4),%ecx
80105704:	85 c9                	test   %ecx,%ecx
80105706:	75 f0                	jne    801056f8 <sys_pipe+0x78>
80105708:	89 7c 90 28          	mov    %edi,0x28(%eax,%edx,4)
8010570c:	8b 45 dc             	mov    -0x24(%ebp),%eax
8010570f:	89 18                	mov    %ebx,(%eax)
80105711:	8b 45 dc             	mov    -0x24(%ebp),%eax
80105714:	89 50 04             	mov    %edx,0x4(%eax)
80105717:	31 c0                	xor    %eax,%eax
80105719:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010571c:	5b                   	pop    %ebx
8010571d:	5e                   	pop    %esi
8010571e:	5f                   	pop    %edi
8010571f:	5d                   	pop    %ebp
80105720:	c3                   	ret    
80105721:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80105728:	e8 93 e3 ff ff       	call   80103ac0 <myproc>
8010572d:	c7 44 b0 08 00 00 00 	movl   $0x0,0x8(%eax,%esi,4)
80105734:	00 
80105735:	8d 76 00             	lea    0x0(%esi),%esi
80105738:	83 ec 0c             	sub    $0xc,%esp
8010573b:	ff 75 e0             	pushl  -0x20(%ebp)
8010573e:	e8 dd b9 ff ff       	call   80101120 <fileclose>
80105743:	58                   	pop    %eax
80105744:	ff 75 e4             	pushl  -0x1c(%ebp)
80105747:	e8 d4 b9 ff ff       	call   80101120 <fileclose>
8010574c:	83 c4 10             	add    $0x10,%esp
8010574f:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105754:	eb c3                	jmp    80105719 <sys_pipe+0x99>
80105756:	66 90                	xchg   %ax,%ax
80105758:	66 90                	xchg   %ax,%ax
8010575a:	66 90                	xchg   %ax,%ax
8010575c:	66 90                	xchg   %ax,%ax
8010575e:	66 90                	xchg   %ax,%ax

80105760 <sys_fork>:
80105760:	55                   	push   %ebp
80105761:	89 e5                	mov    %esp,%ebp
80105763:	5d                   	pop    %ebp
80105764:	e9 f7 e4 ff ff       	jmp    80103c60 <fork>
80105769:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80105770 <sys_exit>:
80105770:	55                   	push   %ebp
80105771:	89 e5                	mov    %esp,%ebp
80105773:	83 ec 08             	sub    $0x8,%esp
80105776:	e8 65 e7 ff ff       	call   80103ee0 <exit>
8010577b:	31 c0                	xor    %eax,%eax
8010577d:	c9                   	leave  
8010577e:	c3                   	ret    
8010577f:	90                   	nop

80105780 <sys_wait>:
80105780:	55                   	push   %ebp
80105781:	89 e5                	mov    %esp,%ebp
80105783:	5d                   	pop    %ebp
80105784:	e9 97 e9 ff ff       	jmp    80104120 <wait>
80105789:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80105790 <sys_kill>:
80105790:	55                   	push   %ebp
80105791:	89 e5                	mov    %esp,%ebp
80105793:	83 ec 20             	sub    $0x20,%esp
80105796:	8d 45 f4             	lea    -0xc(%ebp),%eax
80105799:	50                   	push   %eax
8010579a:	6a 00                	push   $0x0
8010579c:	e8 8f f2 ff ff       	call   80104a30 <argint>
801057a1:	83 c4 10             	add    $0x10,%esp
801057a4:	85 c0                	test   %eax,%eax
801057a6:	78 18                	js     801057c0 <sys_kill+0x30>
801057a8:	83 ec 0c             	sub    $0xc,%esp
801057ab:	ff 75 f4             	pushl  -0xc(%ebp)
801057ae:	e8 bd ea ff ff       	call   80104270 <kill>
801057b3:	83 c4 10             	add    $0x10,%esp
801057b6:	c9                   	leave  
801057b7:	c3                   	ret    
801057b8:	90                   	nop
801057b9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801057c0:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801057c5:	c9                   	leave  
801057c6:	c3                   	ret    
801057c7:	89 f6                	mov    %esi,%esi
801057c9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

801057d0 <sys_getpid>:
801057d0:	55                   	push   %ebp
801057d1:	89 e5                	mov    %esp,%ebp
801057d3:	83 ec 08             	sub    $0x8,%esp
801057d6:	e8 e5 e2 ff ff       	call   80103ac0 <myproc>
801057db:	8b 40 10             	mov    0x10(%eax),%eax
801057de:	c9                   	leave  
801057df:	c3                   	ret    

801057e0 <sys_sbrk>:
801057e0:	55                   	push   %ebp
801057e1:	89 e5                	mov    %esp,%ebp
801057e3:	53                   	push   %ebx
801057e4:	8d 45 f4             	lea    -0xc(%ebp),%eax
801057e7:	83 ec 1c             	sub    $0x1c,%esp
801057ea:	50                   	push   %eax
801057eb:	6a 00                	push   $0x0
801057ed:	e8 3e f2 ff ff       	call   80104a30 <argint>
801057f2:	83 c4 10             	add    $0x10,%esp
801057f5:	85 c0                	test   %eax,%eax
801057f7:	78 27                	js     80105820 <sys_sbrk+0x40>
801057f9:	e8 c2 e2 ff ff       	call   80103ac0 <myproc>
801057fe:	83 ec 0c             	sub    $0xc,%esp
80105801:	8b 18                	mov    (%eax),%ebx
80105803:	ff 75 f4             	pushl  -0xc(%ebp)
80105806:	e8 d5 e3 ff ff       	call   80103be0 <growproc>
8010580b:	83 c4 10             	add    $0x10,%esp
8010580e:	85 c0                	test   %eax,%eax
80105810:	78 0e                	js     80105820 <sys_sbrk+0x40>
80105812:	89 d8                	mov    %ebx,%eax
80105814:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80105817:	c9                   	leave  
80105818:	c3                   	ret    
80105819:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80105820:	bb ff ff ff ff       	mov    $0xffffffff,%ebx
80105825:	eb eb                	jmp    80105812 <sys_sbrk+0x32>
80105827:	89 f6                	mov    %esi,%esi
80105829:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80105830 <sys_sleep>:
80105830:	55                   	push   %ebp
80105831:	89 e5                	mov    %esp,%ebp
80105833:	53                   	push   %ebx
80105834:	8d 45 f4             	lea    -0xc(%ebp),%eax
80105837:	83 ec 1c             	sub    $0x1c,%esp
8010583a:	50                   	push   %eax
8010583b:	6a 00                	push   $0x0
8010583d:	e8 ee f1 ff ff       	call   80104a30 <argint>
80105842:	83 c4 10             	add    $0x10,%esp
80105845:	85 c0                	test   %eax,%eax
80105847:	0f 88 8a 00 00 00    	js     801058d7 <sys_sleep+0xa7>
8010584d:	83 ec 0c             	sub    $0xc,%esp
80105850:	68 60 4c 11 80       	push   $0x80114c60
80105855:	e8 c6 ed ff ff       	call   80104620 <acquire>
8010585a:	8b 55 f4             	mov    -0xc(%ebp),%edx
8010585d:	83 c4 10             	add    $0x10,%esp
80105860:	8b 1d a0 54 11 80    	mov    0x801154a0,%ebx
80105866:	85 d2                	test   %edx,%edx
80105868:	75 27                	jne    80105891 <sys_sleep+0x61>
8010586a:	eb 54                	jmp    801058c0 <sys_sleep+0x90>
8010586c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80105870:	83 ec 08             	sub    $0x8,%esp
80105873:	68 60 4c 11 80       	push   $0x80114c60
80105878:	68 a0 54 11 80       	push   $0x801154a0
8010587d:	e8 de e7 ff ff       	call   80104060 <sleep>
80105882:	a1 a0 54 11 80       	mov    0x801154a0,%eax
80105887:	83 c4 10             	add    $0x10,%esp
8010588a:	29 d8                	sub    %ebx,%eax
8010588c:	3b 45 f4             	cmp    -0xc(%ebp),%eax
8010588f:	73 2f                	jae    801058c0 <sys_sleep+0x90>
80105891:	e8 2a e2 ff ff       	call   80103ac0 <myproc>
80105896:	8b 40 24             	mov    0x24(%eax),%eax
80105899:	85 c0                	test   %eax,%eax
8010589b:	74 d3                	je     80105870 <sys_sleep+0x40>
8010589d:	83 ec 0c             	sub    $0xc,%esp
801058a0:	68 60 4c 11 80       	push   $0x80114c60
801058a5:	e8 36 ee ff ff       	call   801046e0 <release>
801058aa:	83 c4 10             	add    $0x10,%esp
801058ad:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801058b2:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801058b5:	c9                   	leave  
801058b6:	c3                   	ret    
801058b7:	89 f6                	mov    %esi,%esi
801058b9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
801058c0:	83 ec 0c             	sub    $0xc,%esp
801058c3:	68 60 4c 11 80       	push   $0x80114c60
801058c8:	e8 13 ee ff ff       	call   801046e0 <release>
801058cd:	83 c4 10             	add    $0x10,%esp
801058d0:	31 c0                	xor    %eax,%eax
801058d2:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801058d5:	c9                   	leave  
801058d6:	c3                   	ret    
801058d7:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801058dc:	eb f4                	jmp    801058d2 <sys_sleep+0xa2>
801058de:	66 90                	xchg   %ax,%ax

801058e0 <sys_uptime>:
801058e0:	55                   	push   %ebp
801058e1:	89 e5                	mov    %esp,%ebp
801058e3:	53                   	push   %ebx
801058e4:	83 ec 10             	sub    $0x10,%esp
801058e7:	68 60 4c 11 80       	push   $0x80114c60
801058ec:	e8 2f ed ff ff       	call   80104620 <acquire>
801058f1:	8b 1d a0 54 11 80    	mov    0x801154a0,%ebx
801058f7:	c7 04 24 60 4c 11 80 	movl   $0x80114c60,(%esp)
801058fe:	e8 dd ed ff ff       	call   801046e0 <release>
80105903:	89 d8                	mov    %ebx,%eax
80105905:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80105908:	c9                   	leave  
80105909:	c3                   	ret    

8010590a <alltraps>:
8010590a:	1e                   	push   %ds
8010590b:	06                   	push   %es
8010590c:	0f a0                	push   %fs
8010590e:	0f a8                	push   %gs
80105910:	60                   	pusha  
80105911:	66 b8 10 00          	mov    $0x10,%ax
80105915:	8e d8                	mov    %eax,%ds
80105917:	8e c0                	mov    %eax,%es
80105919:	54                   	push   %esp
8010591a:	e8 c1 00 00 00       	call   801059e0 <trap>
8010591f:	83 c4 04             	add    $0x4,%esp

80105922 <trapret>:
80105922:	61                   	popa   
80105923:	0f a9                	pop    %gs
80105925:	0f a1                	pop    %fs
80105927:	07                   	pop    %es
80105928:	1f                   	pop    %ds
80105929:	83 c4 08             	add    $0x8,%esp
8010592c:	cf                   	iret   
8010592d:	66 90                	xchg   %ax,%ax
8010592f:	90                   	nop

80105930 <tvinit>:
80105930:	55                   	push   %ebp
80105931:	31 c0                	xor    %eax,%eax
80105933:	89 e5                	mov    %esp,%ebp
80105935:	83 ec 08             	sub    $0x8,%esp
80105938:	90                   	nop
80105939:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80105940:	8b 14 85 08 a0 10 80 	mov    -0x7fef5ff8(,%eax,4),%edx
80105947:	c7 04 c5 a2 4c 11 80 	movl   $0x8e000008,-0x7feeb35e(,%eax,8)
8010594e:	08 00 00 8e 
80105952:	66 89 14 c5 a0 4c 11 	mov    %dx,-0x7feeb360(,%eax,8)
80105959:	80 
8010595a:	c1 ea 10             	shr    $0x10,%edx
8010595d:	66 89 14 c5 a6 4c 11 	mov    %dx,-0x7feeb35a(,%eax,8)
80105964:	80 
80105965:	83 c0 01             	add    $0x1,%eax
80105968:	3d 00 01 00 00       	cmp    $0x100,%eax
8010596d:	75 d1                	jne    80105940 <tvinit+0x10>
8010596f:	a1 08 a1 10 80       	mov    0x8010a108,%eax
80105974:	83 ec 08             	sub    $0x8,%esp
80105977:	c7 05 a2 4e 11 80 08 	movl   $0xef000008,0x80114ea2
8010597e:	00 00 ef 
80105981:	68 39 79 10 80       	push   $0x80107939
80105986:	68 60 4c 11 80       	push   $0x80114c60
8010598b:	66 a3 a0 4e 11 80    	mov    %ax,0x80114ea0
80105991:	c1 e8 10             	shr    $0x10,%eax
80105994:	66 a3 a6 4e 11 80    	mov    %ax,0x80114ea6
8010599a:	e8 41 eb ff ff       	call   801044e0 <initlock>
8010599f:	83 c4 10             	add    $0x10,%esp
801059a2:	c9                   	leave  
801059a3:	c3                   	ret    
801059a4:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
801059aa:	8d bf 00 00 00 00    	lea    0x0(%edi),%edi

801059b0 <idtinit>:
801059b0:	55                   	push   %ebp
801059b1:	b8 ff 07 00 00       	mov    $0x7ff,%eax
801059b6:	89 e5                	mov    %esp,%ebp
801059b8:	83 ec 10             	sub    $0x10,%esp
801059bb:	66 89 45 fa          	mov    %ax,-0x6(%ebp)
801059bf:	b8 a0 4c 11 80       	mov    $0x80114ca0,%eax
801059c4:	66 89 45 fc          	mov    %ax,-0x4(%ebp)
801059c8:	c1 e8 10             	shr    $0x10,%eax
801059cb:	66 89 45 fe          	mov    %ax,-0x2(%ebp)
801059cf:	8d 45 fa             	lea    -0x6(%ebp),%eax
801059d2:	0f 01 18             	lidtl  (%eax)
801059d5:	c9                   	leave  
801059d6:	c3                   	ret    
801059d7:	89 f6                	mov    %esi,%esi
801059d9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

801059e0 <trap>:
801059e0:	55                   	push   %ebp
801059e1:	89 e5                	mov    %esp,%ebp
801059e3:	57                   	push   %edi
801059e4:	56                   	push   %esi
801059e5:	53                   	push   %ebx
801059e6:	83 ec 1c             	sub    $0x1c,%esp
801059e9:	8b 7d 08             	mov    0x8(%ebp),%edi
801059ec:	8b 47 30             	mov    0x30(%edi),%eax
801059ef:	83 f8 40             	cmp    $0x40,%eax
801059f2:	0f 84 f0 00 00 00    	je     80105ae8 <trap+0x108>
801059f8:	83 e8 20             	sub    $0x20,%eax
801059fb:	83 f8 1f             	cmp    $0x1f,%eax
801059fe:	77 10                	ja     80105a10 <trap+0x30>
80105a00:	ff 24 85 e0 79 10 80 	jmp    *-0x7fef8620(,%eax,4)
80105a07:	89 f6                	mov    %esi,%esi
80105a09:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
80105a10:	e8 ab e0 ff ff       	call   80103ac0 <myproc>
80105a15:	85 c0                	test   %eax,%eax
80105a17:	8b 5f 38             	mov    0x38(%edi),%ebx
80105a1a:	0f 84 14 02 00 00    	je     80105c34 <trap+0x254>
80105a20:	f6 47 3c 03          	testb  $0x3,0x3c(%edi)
80105a24:	0f 84 0a 02 00 00    	je     80105c34 <trap+0x254>
80105a2a:	0f 20 d1             	mov    %cr2,%ecx
80105a2d:	89 4d d8             	mov    %ecx,-0x28(%ebp)
80105a30:	e8 6b e0 ff ff       	call   80103aa0 <cpuid>
80105a35:	89 45 dc             	mov    %eax,-0x24(%ebp)
80105a38:	8b 47 34             	mov    0x34(%edi),%eax
80105a3b:	8b 77 30             	mov    0x30(%edi),%esi
80105a3e:	89 45 e4             	mov    %eax,-0x1c(%ebp)
80105a41:	e8 7a e0 ff ff       	call   80103ac0 <myproc>
80105a46:	89 45 e0             	mov    %eax,-0x20(%ebp)
80105a49:	e8 72 e0 ff ff       	call   80103ac0 <myproc>
80105a4e:	8b 4d d8             	mov    -0x28(%ebp),%ecx
80105a51:	8b 55 dc             	mov    -0x24(%ebp),%edx
80105a54:	51                   	push   %ecx
80105a55:	53                   	push   %ebx
80105a56:	52                   	push   %edx
80105a57:	8b 55 e0             	mov    -0x20(%ebp),%edx
80105a5a:	ff 75 e4             	pushl  -0x1c(%ebp)
80105a5d:	56                   	push   %esi
80105a5e:	83 c2 6c             	add    $0x6c,%edx
80105a61:	52                   	push   %edx
80105a62:	ff 70 10             	pushl  0x10(%eax)
80105a65:	68 9c 79 10 80       	push   $0x8010799c
80105a6a:	e8 21 ad ff ff       	call   80100790 <cprintf>
80105a6f:	83 c4 20             	add    $0x20,%esp
80105a72:	e8 49 e0 ff ff       	call   80103ac0 <myproc>
80105a77:	c7 40 24 01 00 00 00 	movl   $0x1,0x24(%eax)
80105a7e:	e8 3d e0 ff ff       	call   80103ac0 <myproc>
80105a83:	85 c0                	test   %eax,%eax
80105a85:	74 1d                	je     80105aa4 <trap+0xc4>
80105a87:	e8 34 e0 ff ff       	call   80103ac0 <myproc>
80105a8c:	8b 50 24             	mov    0x24(%eax),%edx
80105a8f:	85 d2                	test   %edx,%edx
80105a91:	74 11                	je     80105aa4 <trap+0xc4>
80105a93:	0f b7 47 3c          	movzwl 0x3c(%edi),%eax
80105a97:	83 e0 03             	and    $0x3,%eax
80105a9a:	66 83 f8 03          	cmp    $0x3,%ax
80105a9e:	0f 84 4c 01 00 00    	je     80105bf0 <trap+0x210>
80105aa4:	e8 17 e0 ff ff       	call   80103ac0 <myproc>
80105aa9:	85 c0                	test   %eax,%eax
80105aab:	74 0b                	je     80105ab8 <trap+0xd8>
80105aad:	e8 0e e0 ff ff       	call   80103ac0 <myproc>
80105ab2:	83 78 0c 04          	cmpl   $0x4,0xc(%eax)
80105ab6:	74 68                	je     80105b20 <trap+0x140>
80105ab8:	e8 03 e0 ff ff       	call   80103ac0 <myproc>
80105abd:	85 c0                	test   %eax,%eax
80105abf:	74 19                	je     80105ada <trap+0xfa>
80105ac1:	e8 fa df ff ff       	call   80103ac0 <myproc>
80105ac6:	8b 40 24             	mov    0x24(%eax),%eax
80105ac9:	85 c0                	test   %eax,%eax
80105acb:	74 0d                	je     80105ada <trap+0xfa>
80105acd:	0f b7 47 3c          	movzwl 0x3c(%edi),%eax
80105ad1:	83 e0 03             	and    $0x3,%eax
80105ad4:	66 83 f8 03          	cmp    $0x3,%ax
80105ad8:	74 37                	je     80105b11 <trap+0x131>
80105ada:	8d 65 f4             	lea    -0xc(%ebp),%esp
80105add:	5b                   	pop    %ebx
80105ade:	5e                   	pop    %esi
80105adf:	5f                   	pop    %edi
80105ae0:	5d                   	pop    %ebp
80105ae1:	c3                   	ret    
80105ae2:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80105ae8:	e8 d3 df ff ff       	call   80103ac0 <myproc>
80105aed:	8b 58 24             	mov    0x24(%eax),%ebx
80105af0:	85 db                	test   %ebx,%ebx
80105af2:	0f 85 e8 00 00 00    	jne    80105be0 <trap+0x200>
80105af8:	e8 c3 df ff ff       	call   80103ac0 <myproc>
80105afd:	89 78 18             	mov    %edi,0x18(%eax)
80105b00:	e8 1b f0 ff ff       	call   80104b20 <syscall>
80105b05:	e8 b6 df ff ff       	call   80103ac0 <myproc>
80105b0a:	8b 48 24             	mov    0x24(%eax),%ecx
80105b0d:	85 c9                	test   %ecx,%ecx
80105b0f:	74 c9                	je     80105ada <trap+0xfa>
80105b11:	8d 65 f4             	lea    -0xc(%ebp),%esp
80105b14:	5b                   	pop    %ebx
80105b15:	5e                   	pop    %esi
80105b16:	5f                   	pop    %edi
80105b17:	5d                   	pop    %ebp
80105b18:	e9 c3 e3 ff ff       	jmp    80103ee0 <exit>
80105b1d:	8d 76 00             	lea    0x0(%esi),%esi
80105b20:	83 7f 30 20          	cmpl   $0x20,0x30(%edi)
80105b24:	75 92                	jne    80105ab8 <trap+0xd8>
80105b26:	e8 e5 e4 ff ff       	call   80104010 <yield>
80105b2b:	eb 8b                	jmp    80105ab8 <trap+0xd8>
80105b2d:	8d 76 00             	lea    0x0(%esi),%esi
80105b30:	e8 6b df ff ff       	call   80103aa0 <cpuid>
80105b35:	85 c0                	test   %eax,%eax
80105b37:	0f 84 c3 00 00 00    	je     80105c00 <trap+0x220>
80105b3d:	e8 ee ce ff ff       	call   80102a30 <lapiceoi>
80105b42:	e8 79 df ff ff       	call   80103ac0 <myproc>
80105b47:	85 c0                	test   %eax,%eax
80105b49:	0f 85 38 ff ff ff    	jne    80105a87 <trap+0xa7>
80105b4f:	e9 50 ff ff ff       	jmp    80105aa4 <trap+0xc4>
80105b54:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80105b58:	e8 93 cd ff ff       	call   801028f0 <kbdintr>
80105b5d:	e8 ce ce ff ff       	call   80102a30 <lapiceoi>
80105b62:	e8 59 df ff ff       	call   80103ac0 <myproc>
80105b67:	85 c0                	test   %eax,%eax
80105b69:	0f 85 18 ff ff ff    	jne    80105a87 <trap+0xa7>
80105b6f:	e9 30 ff ff ff       	jmp    80105aa4 <trap+0xc4>
80105b74:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80105b78:	e8 53 02 00 00       	call   80105dd0 <uartintr>
80105b7d:	e8 ae ce ff ff       	call   80102a30 <lapiceoi>
80105b82:	e8 39 df ff ff       	call   80103ac0 <myproc>
80105b87:	85 c0                	test   %eax,%eax
80105b89:	0f 85 f8 fe ff ff    	jne    80105a87 <trap+0xa7>
80105b8f:	e9 10 ff ff ff       	jmp    80105aa4 <trap+0xc4>
80105b94:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80105b98:	0f b7 5f 3c          	movzwl 0x3c(%edi),%ebx
80105b9c:	8b 77 38             	mov    0x38(%edi),%esi
80105b9f:	e8 fc de ff ff       	call   80103aa0 <cpuid>
80105ba4:	56                   	push   %esi
80105ba5:	53                   	push   %ebx
80105ba6:	50                   	push   %eax
80105ba7:	68 44 79 10 80       	push   $0x80107944
80105bac:	e8 df ab ff ff       	call   80100790 <cprintf>
80105bb1:	e8 7a ce ff ff       	call   80102a30 <lapiceoi>
80105bb6:	83 c4 10             	add    $0x10,%esp
80105bb9:	e8 02 df ff ff       	call   80103ac0 <myproc>
80105bbe:	85 c0                	test   %eax,%eax
80105bc0:	0f 85 c1 fe ff ff    	jne    80105a87 <trap+0xa7>
80105bc6:	e9 d9 fe ff ff       	jmp    80105aa4 <trap+0xc4>
80105bcb:	90                   	nop
80105bcc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80105bd0:	e8 8b c7 ff ff       	call   80102360 <ideintr>
80105bd5:	e9 63 ff ff ff       	jmp    80105b3d <trap+0x15d>
80105bda:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80105be0:	e8 fb e2 ff ff       	call   80103ee0 <exit>
80105be5:	e9 0e ff ff ff       	jmp    80105af8 <trap+0x118>
80105bea:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80105bf0:	e8 eb e2 ff ff       	call   80103ee0 <exit>
80105bf5:	e9 aa fe ff ff       	jmp    80105aa4 <trap+0xc4>
80105bfa:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80105c00:	83 ec 0c             	sub    $0xc,%esp
80105c03:	68 60 4c 11 80       	push   $0x80114c60
80105c08:	e8 13 ea ff ff       	call   80104620 <acquire>
80105c0d:	c7 04 24 a0 54 11 80 	movl   $0x801154a0,(%esp)
80105c14:	83 05 a0 54 11 80 01 	addl   $0x1,0x801154a0
80105c1b:	e8 f0 e5 ff ff       	call   80104210 <wakeup>
80105c20:	c7 04 24 60 4c 11 80 	movl   $0x80114c60,(%esp)
80105c27:	e8 b4 ea ff ff       	call   801046e0 <release>
80105c2c:	83 c4 10             	add    $0x10,%esp
80105c2f:	e9 09 ff ff ff       	jmp    80105b3d <trap+0x15d>
80105c34:	0f 20 d6             	mov    %cr2,%esi
80105c37:	e8 64 de ff ff       	call   80103aa0 <cpuid>
80105c3c:	83 ec 0c             	sub    $0xc,%esp
80105c3f:	56                   	push   %esi
80105c40:	53                   	push   %ebx
80105c41:	50                   	push   %eax
80105c42:	ff 77 30             	pushl  0x30(%edi)
80105c45:	68 68 79 10 80       	push   $0x80107968
80105c4a:	e8 41 ab ff ff       	call   80100790 <cprintf>
80105c4f:	83 c4 14             	add    $0x14,%esp
80105c52:	68 3e 79 10 80       	push   $0x8010793e
80105c57:	e8 34 a7 ff ff       	call   80100390 <panic>
80105c5c:	66 90                	xchg   %ax,%ax
80105c5e:	66 90                	xchg   %ax,%ax

80105c60 <uartgetc>:
80105c60:	a1 bc a5 10 80       	mov    0x8010a5bc,%eax
80105c65:	55                   	push   %ebp
80105c66:	89 e5                	mov    %esp,%ebp
80105c68:	85 c0                	test   %eax,%eax
80105c6a:	74 1c                	je     80105c88 <uartgetc+0x28>
80105c6c:	ba fd 03 00 00       	mov    $0x3fd,%edx
80105c71:	ec                   	in     (%dx),%al
80105c72:	a8 01                	test   $0x1,%al
80105c74:	74 12                	je     80105c88 <uartgetc+0x28>
80105c76:	ba f8 03 00 00       	mov    $0x3f8,%edx
80105c7b:	ec                   	in     (%dx),%al
80105c7c:	0f b6 c0             	movzbl %al,%eax
80105c7f:	5d                   	pop    %ebp
80105c80:	c3                   	ret    
80105c81:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80105c88:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105c8d:	5d                   	pop    %ebp
80105c8e:	c3                   	ret    
80105c8f:	90                   	nop

80105c90 <uartputc.part.0>:
80105c90:	55                   	push   %ebp
80105c91:	89 e5                	mov    %esp,%ebp
80105c93:	57                   	push   %edi
80105c94:	56                   	push   %esi
80105c95:	53                   	push   %ebx
80105c96:	89 c7                	mov    %eax,%edi
80105c98:	bb 80 00 00 00       	mov    $0x80,%ebx
80105c9d:	be fd 03 00 00       	mov    $0x3fd,%esi
80105ca2:	83 ec 0c             	sub    $0xc,%esp
80105ca5:	eb 1b                	jmp    80105cc2 <uartputc.part.0+0x32>
80105ca7:	89 f6                	mov    %esi,%esi
80105ca9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
80105cb0:	83 ec 0c             	sub    $0xc,%esp
80105cb3:	6a 0a                	push   $0xa
80105cb5:	e8 96 cd ff ff       	call   80102a50 <microdelay>
80105cba:	83 c4 10             	add    $0x10,%esp
80105cbd:	83 eb 01             	sub    $0x1,%ebx
80105cc0:	74 07                	je     80105cc9 <uartputc.part.0+0x39>
80105cc2:	89 f2                	mov    %esi,%edx
80105cc4:	ec                   	in     (%dx),%al
80105cc5:	a8 20                	test   $0x20,%al
80105cc7:	74 e7                	je     80105cb0 <uartputc.part.0+0x20>
80105cc9:	ba f8 03 00 00       	mov    $0x3f8,%edx
80105cce:	89 f8                	mov    %edi,%eax
80105cd0:	ee                   	out    %al,(%dx)
80105cd1:	8d 65 f4             	lea    -0xc(%ebp),%esp
80105cd4:	5b                   	pop    %ebx
80105cd5:	5e                   	pop    %esi
80105cd6:	5f                   	pop    %edi
80105cd7:	5d                   	pop    %ebp
80105cd8:	c3                   	ret    
80105cd9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80105ce0 <uartinit>:
80105ce0:	55                   	push   %ebp
80105ce1:	31 c9                	xor    %ecx,%ecx
80105ce3:	89 c8                	mov    %ecx,%eax
80105ce5:	89 e5                	mov    %esp,%ebp
80105ce7:	57                   	push   %edi
80105ce8:	56                   	push   %esi
80105ce9:	53                   	push   %ebx
80105cea:	bb fa 03 00 00       	mov    $0x3fa,%ebx
80105cef:	89 da                	mov    %ebx,%edx
80105cf1:	83 ec 0c             	sub    $0xc,%esp
80105cf4:	ee                   	out    %al,(%dx)
80105cf5:	bf fb 03 00 00       	mov    $0x3fb,%edi
80105cfa:	b8 80 ff ff ff       	mov    $0xffffff80,%eax
80105cff:	89 fa                	mov    %edi,%edx
80105d01:	ee                   	out    %al,(%dx)
80105d02:	b8 0c 00 00 00       	mov    $0xc,%eax
80105d07:	ba f8 03 00 00       	mov    $0x3f8,%edx
80105d0c:	ee                   	out    %al,(%dx)
80105d0d:	be f9 03 00 00       	mov    $0x3f9,%esi
80105d12:	89 c8                	mov    %ecx,%eax
80105d14:	89 f2                	mov    %esi,%edx
80105d16:	ee                   	out    %al,(%dx)
80105d17:	b8 03 00 00 00       	mov    $0x3,%eax
80105d1c:	89 fa                	mov    %edi,%edx
80105d1e:	ee                   	out    %al,(%dx)
80105d1f:	ba fc 03 00 00       	mov    $0x3fc,%edx
80105d24:	89 c8                	mov    %ecx,%eax
80105d26:	ee                   	out    %al,(%dx)
80105d27:	b8 01 00 00 00       	mov    $0x1,%eax
80105d2c:	89 f2                	mov    %esi,%edx
80105d2e:	ee                   	out    %al,(%dx)
80105d2f:	ba fd 03 00 00       	mov    $0x3fd,%edx
80105d34:	ec                   	in     (%dx),%al
80105d35:	3c ff                	cmp    $0xff,%al
80105d37:	74 5a                	je     80105d93 <uartinit+0xb3>
80105d39:	c7 05 bc a5 10 80 01 	movl   $0x1,0x8010a5bc
80105d40:	00 00 00 
80105d43:	89 da                	mov    %ebx,%edx
80105d45:	ec                   	in     (%dx),%al
80105d46:	ba f8 03 00 00       	mov    $0x3f8,%edx
80105d4b:	ec                   	in     (%dx),%al
80105d4c:	83 ec 08             	sub    $0x8,%esp
80105d4f:	bb 60 7a 10 80       	mov    $0x80107a60,%ebx
80105d54:	6a 00                	push   $0x0
80105d56:	6a 04                	push   $0x4
80105d58:	e8 53 c8 ff ff       	call   801025b0 <ioapicenable>
80105d5d:	83 c4 10             	add    $0x10,%esp
80105d60:	b8 78 00 00 00       	mov    $0x78,%eax
80105d65:	eb 13                	jmp    80105d7a <uartinit+0x9a>
80105d67:	89 f6                	mov    %esi,%esi
80105d69:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
80105d70:	83 c3 01             	add    $0x1,%ebx
80105d73:	0f be 03             	movsbl (%ebx),%eax
80105d76:	84 c0                	test   %al,%al
80105d78:	74 19                	je     80105d93 <uartinit+0xb3>
80105d7a:	8b 15 bc a5 10 80    	mov    0x8010a5bc,%edx
80105d80:	85 d2                	test   %edx,%edx
80105d82:	74 ec                	je     80105d70 <uartinit+0x90>
80105d84:	83 c3 01             	add    $0x1,%ebx
80105d87:	e8 04 ff ff ff       	call   80105c90 <uartputc.part.0>
80105d8c:	0f be 03             	movsbl (%ebx),%eax
80105d8f:	84 c0                	test   %al,%al
80105d91:	75 e7                	jne    80105d7a <uartinit+0x9a>
80105d93:	8d 65 f4             	lea    -0xc(%ebp),%esp
80105d96:	5b                   	pop    %ebx
80105d97:	5e                   	pop    %esi
80105d98:	5f                   	pop    %edi
80105d99:	5d                   	pop    %ebp
80105d9a:	c3                   	ret    
80105d9b:	90                   	nop
80105d9c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80105da0 <uartputc>:
80105da0:	8b 15 bc a5 10 80    	mov    0x8010a5bc,%edx
80105da6:	55                   	push   %ebp
80105da7:	89 e5                	mov    %esp,%ebp
80105da9:	85 d2                	test   %edx,%edx
80105dab:	8b 45 08             	mov    0x8(%ebp),%eax
80105dae:	74 10                	je     80105dc0 <uartputc+0x20>
80105db0:	5d                   	pop    %ebp
80105db1:	e9 da fe ff ff       	jmp    80105c90 <uartputc.part.0>
80105db6:	8d 76 00             	lea    0x0(%esi),%esi
80105db9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
80105dc0:	5d                   	pop    %ebp
80105dc1:	c3                   	ret    
80105dc2:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80105dc9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80105dd0 <uartintr>:
80105dd0:	55                   	push   %ebp
80105dd1:	89 e5                	mov    %esp,%ebp
80105dd3:	83 ec 14             	sub    $0x14,%esp
80105dd6:	68 60 5c 10 80       	push   $0x80105c60
80105ddb:	e8 60 ab ff ff       	call   80100940 <consoleintr>
80105de0:	83 c4 10             	add    $0x10,%esp
80105de3:	c9                   	leave  
80105de4:	c3                   	ret    

80105de5 <vector0>:
80105de5:	6a 00                	push   $0x0
80105de7:	6a 00                	push   $0x0
80105de9:	e9 1c fb ff ff       	jmp    8010590a <alltraps>

80105dee <vector1>:
80105dee:	6a 00                	push   $0x0
80105df0:	6a 01                	push   $0x1
80105df2:	e9 13 fb ff ff       	jmp    8010590a <alltraps>

80105df7 <vector2>:
80105df7:	6a 00                	push   $0x0
80105df9:	6a 02                	push   $0x2
80105dfb:	e9 0a fb ff ff       	jmp    8010590a <alltraps>

80105e00 <vector3>:
80105e00:	6a 00                	push   $0x0
80105e02:	6a 03                	push   $0x3
80105e04:	e9 01 fb ff ff       	jmp    8010590a <alltraps>

80105e09 <vector4>:
80105e09:	6a 00                	push   $0x0
80105e0b:	6a 04                	push   $0x4
80105e0d:	e9 f8 fa ff ff       	jmp    8010590a <alltraps>

80105e12 <vector5>:
80105e12:	6a 00                	push   $0x0
80105e14:	6a 05                	push   $0x5
80105e16:	e9 ef fa ff ff       	jmp    8010590a <alltraps>

80105e1b <vector6>:
80105e1b:	6a 00                	push   $0x0
80105e1d:	6a 06                	push   $0x6
80105e1f:	e9 e6 fa ff ff       	jmp    8010590a <alltraps>

80105e24 <vector7>:
80105e24:	6a 00                	push   $0x0
80105e26:	6a 07                	push   $0x7
80105e28:	e9 dd fa ff ff       	jmp    8010590a <alltraps>

80105e2d <vector8>:
80105e2d:	6a 08                	push   $0x8
80105e2f:	e9 d6 fa ff ff       	jmp    8010590a <alltraps>

80105e34 <vector9>:
80105e34:	6a 00                	push   $0x0
80105e36:	6a 09                	push   $0x9
80105e38:	e9 cd fa ff ff       	jmp    8010590a <alltraps>

80105e3d <vector10>:
80105e3d:	6a 0a                	push   $0xa
80105e3f:	e9 c6 fa ff ff       	jmp    8010590a <alltraps>

80105e44 <vector11>:
80105e44:	6a 0b                	push   $0xb
80105e46:	e9 bf fa ff ff       	jmp    8010590a <alltraps>

80105e4b <vector12>:
80105e4b:	6a 0c                	push   $0xc
80105e4d:	e9 b8 fa ff ff       	jmp    8010590a <alltraps>

80105e52 <vector13>:
80105e52:	6a 0d                	push   $0xd
80105e54:	e9 b1 fa ff ff       	jmp    8010590a <alltraps>

80105e59 <vector14>:
80105e59:	6a 0e                	push   $0xe
80105e5b:	e9 aa fa ff ff       	jmp    8010590a <alltraps>

80105e60 <vector15>:
80105e60:	6a 00                	push   $0x0
80105e62:	6a 0f                	push   $0xf
80105e64:	e9 a1 fa ff ff       	jmp    8010590a <alltraps>

80105e69 <vector16>:
80105e69:	6a 00                	push   $0x0
80105e6b:	6a 10                	push   $0x10
80105e6d:	e9 98 fa ff ff       	jmp    8010590a <alltraps>

80105e72 <vector17>:
80105e72:	6a 11                	push   $0x11
80105e74:	e9 91 fa ff ff       	jmp    8010590a <alltraps>

80105e79 <vector18>:
80105e79:	6a 00                	push   $0x0
80105e7b:	6a 12                	push   $0x12
80105e7d:	e9 88 fa ff ff       	jmp    8010590a <alltraps>

80105e82 <vector19>:
80105e82:	6a 00                	push   $0x0
80105e84:	6a 13                	push   $0x13
80105e86:	e9 7f fa ff ff       	jmp    8010590a <alltraps>

80105e8b <vector20>:
80105e8b:	6a 00                	push   $0x0
80105e8d:	6a 14                	push   $0x14
80105e8f:	e9 76 fa ff ff       	jmp    8010590a <alltraps>

80105e94 <vector21>:
80105e94:	6a 00                	push   $0x0
80105e96:	6a 15                	push   $0x15
80105e98:	e9 6d fa ff ff       	jmp    8010590a <alltraps>

80105e9d <vector22>:
80105e9d:	6a 00                	push   $0x0
80105e9f:	6a 16                	push   $0x16
80105ea1:	e9 64 fa ff ff       	jmp    8010590a <alltraps>

80105ea6 <vector23>:
80105ea6:	6a 00                	push   $0x0
80105ea8:	6a 17                	push   $0x17
80105eaa:	e9 5b fa ff ff       	jmp    8010590a <alltraps>

80105eaf <vector24>:
80105eaf:	6a 00                	push   $0x0
80105eb1:	6a 18                	push   $0x18
80105eb3:	e9 52 fa ff ff       	jmp    8010590a <alltraps>

80105eb8 <vector25>:
80105eb8:	6a 00                	push   $0x0
80105eba:	6a 19                	push   $0x19
80105ebc:	e9 49 fa ff ff       	jmp    8010590a <alltraps>

80105ec1 <vector26>:
80105ec1:	6a 00                	push   $0x0
80105ec3:	6a 1a                	push   $0x1a
80105ec5:	e9 40 fa ff ff       	jmp    8010590a <alltraps>

80105eca <vector27>:
80105eca:	6a 00                	push   $0x0
80105ecc:	6a 1b                	push   $0x1b
80105ece:	e9 37 fa ff ff       	jmp    8010590a <alltraps>

80105ed3 <vector28>:
80105ed3:	6a 00                	push   $0x0
80105ed5:	6a 1c                	push   $0x1c
80105ed7:	e9 2e fa ff ff       	jmp    8010590a <alltraps>

80105edc <vector29>:
80105edc:	6a 00                	push   $0x0
80105ede:	6a 1d                	push   $0x1d
80105ee0:	e9 25 fa ff ff       	jmp    8010590a <alltraps>

80105ee5 <vector30>:
80105ee5:	6a 00                	push   $0x0
80105ee7:	6a 1e                	push   $0x1e
80105ee9:	e9 1c fa ff ff       	jmp    8010590a <alltraps>

80105eee <vector31>:
80105eee:	6a 00                	push   $0x0
80105ef0:	6a 1f                	push   $0x1f
80105ef2:	e9 13 fa ff ff       	jmp    8010590a <alltraps>

80105ef7 <vector32>:
80105ef7:	6a 00                	push   $0x0
80105ef9:	6a 20                	push   $0x20
80105efb:	e9 0a fa ff ff       	jmp    8010590a <alltraps>

80105f00 <vector33>:
80105f00:	6a 00                	push   $0x0
80105f02:	6a 21                	push   $0x21
80105f04:	e9 01 fa ff ff       	jmp    8010590a <alltraps>

80105f09 <vector34>:
80105f09:	6a 00                	push   $0x0
80105f0b:	6a 22                	push   $0x22
80105f0d:	e9 f8 f9 ff ff       	jmp    8010590a <alltraps>

80105f12 <vector35>:
80105f12:	6a 00                	push   $0x0
80105f14:	6a 23                	push   $0x23
80105f16:	e9 ef f9 ff ff       	jmp    8010590a <alltraps>

80105f1b <vector36>:
80105f1b:	6a 00                	push   $0x0
80105f1d:	6a 24                	push   $0x24
80105f1f:	e9 e6 f9 ff ff       	jmp    8010590a <alltraps>

80105f24 <vector37>:
80105f24:	6a 00                	push   $0x0
80105f26:	6a 25                	push   $0x25
80105f28:	e9 dd f9 ff ff       	jmp    8010590a <alltraps>

80105f2d <vector38>:
80105f2d:	6a 00                	push   $0x0
80105f2f:	6a 26                	push   $0x26
80105f31:	e9 d4 f9 ff ff       	jmp    8010590a <alltraps>

80105f36 <vector39>:
80105f36:	6a 00                	push   $0x0
80105f38:	6a 27                	push   $0x27
80105f3a:	e9 cb f9 ff ff       	jmp    8010590a <alltraps>

80105f3f <vector40>:
80105f3f:	6a 00                	push   $0x0
80105f41:	6a 28                	push   $0x28
80105f43:	e9 c2 f9 ff ff       	jmp    8010590a <alltraps>

80105f48 <vector41>:
80105f48:	6a 00                	push   $0x0
80105f4a:	6a 29                	push   $0x29
80105f4c:	e9 b9 f9 ff ff       	jmp    8010590a <alltraps>

80105f51 <vector42>:
80105f51:	6a 00                	push   $0x0
80105f53:	6a 2a                	push   $0x2a
80105f55:	e9 b0 f9 ff ff       	jmp    8010590a <alltraps>

80105f5a <vector43>:
80105f5a:	6a 00                	push   $0x0
80105f5c:	6a 2b                	push   $0x2b
80105f5e:	e9 a7 f9 ff ff       	jmp    8010590a <alltraps>

80105f63 <vector44>:
80105f63:	6a 00                	push   $0x0
80105f65:	6a 2c                	push   $0x2c
80105f67:	e9 9e f9 ff ff       	jmp    8010590a <alltraps>

80105f6c <vector45>:
80105f6c:	6a 00                	push   $0x0
80105f6e:	6a 2d                	push   $0x2d
80105f70:	e9 95 f9 ff ff       	jmp    8010590a <alltraps>

80105f75 <vector46>:
80105f75:	6a 00                	push   $0x0
80105f77:	6a 2e                	push   $0x2e
80105f79:	e9 8c f9 ff ff       	jmp    8010590a <alltraps>

80105f7e <vector47>:
80105f7e:	6a 00                	push   $0x0
80105f80:	6a 2f                	push   $0x2f
80105f82:	e9 83 f9 ff ff       	jmp    8010590a <alltraps>

80105f87 <vector48>:
80105f87:	6a 00                	push   $0x0
80105f89:	6a 30                	push   $0x30
80105f8b:	e9 7a f9 ff ff       	jmp    8010590a <alltraps>

80105f90 <vector49>:
80105f90:	6a 00                	push   $0x0
80105f92:	6a 31                	push   $0x31
80105f94:	e9 71 f9 ff ff       	jmp    8010590a <alltraps>

80105f99 <vector50>:
80105f99:	6a 00                	push   $0x0
80105f9b:	6a 32                	push   $0x32
80105f9d:	e9 68 f9 ff ff       	jmp    8010590a <alltraps>

80105fa2 <vector51>:
80105fa2:	6a 00                	push   $0x0
80105fa4:	6a 33                	push   $0x33
80105fa6:	e9 5f f9 ff ff       	jmp    8010590a <alltraps>

80105fab <vector52>:
80105fab:	6a 00                	push   $0x0
80105fad:	6a 34                	push   $0x34
80105faf:	e9 56 f9 ff ff       	jmp    8010590a <alltraps>

80105fb4 <vector53>:
80105fb4:	6a 00                	push   $0x0
80105fb6:	6a 35                	push   $0x35
80105fb8:	e9 4d f9 ff ff       	jmp    8010590a <alltraps>

80105fbd <vector54>:
80105fbd:	6a 00                	push   $0x0
80105fbf:	6a 36                	push   $0x36
80105fc1:	e9 44 f9 ff ff       	jmp    8010590a <alltraps>

80105fc6 <vector55>:
80105fc6:	6a 00                	push   $0x0
80105fc8:	6a 37                	push   $0x37
80105fca:	e9 3b f9 ff ff       	jmp    8010590a <alltraps>

80105fcf <vector56>:
80105fcf:	6a 00                	push   $0x0
80105fd1:	6a 38                	push   $0x38
80105fd3:	e9 32 f9 ff ff       	jmp    8010590a <alltraps>

80105fd8 <vector57>:
80105fd8:	6a 00                	push   $0x0
80105fda:	6a 39                	push   $0x39
80105fdc:	e9 29 f9 ff ff       	jmp    8010590a <alltraps>

80105fe1 <vector58>:
80105fe1:	6a 00                	push   $0x0
80105fe3:	6a 3a                	push   $0x3a
80105fe5:	e9 20 f9 ff ff       	jmp    8010590a <alltraps>

80105fea <vector59>:
80105fea:	6a 00                	push   $0x0
80105fec:	6a 3b                	push   $0x3b
80105fee:	e9 17 f9 ff ff       	jmp    8010590a <alltraps>

80105ff3 <vector60>:
80105ff3:	6a 00                	push   $0x0
80105ff5:	6a 3c                	push   $0x3c
80105ff7:	e9 0e f9 ff ff       	jmp    8010590a <alltraps>

80105ffc <vector61>:
80105ffc:	6a 00                	push   $0x0
80105ffe:	6a 3d                	push   $0x3d
80106000:	e9 05 f9 ff ff       	jmp    8010590a <alltraps>

80106005 <vector62>:
80106005:	6a 00                	push   $0x0
80106007:	6a 3e                	push   $0x3e
80106009:	e9 fc f8 ff ff       	jmp    8010590a <alltraps>

8010600e <vector63>:
8010600e:	6a 00                	push   $0x0
80106010:	6a 3f                	push   $0x3f
80106012:	e9 f3 f8 ff ff       	jmp    8010590a <alltraps>

80106017 <vector64>:
80106017:	6a 00                	push   $0x0
80106019:	6a 40                	push   $0x40
8010601b:	e9 ea f8 ff ff       	jmp    8010590a <alltraps>

80106020 <vector65>:
80106020:	6a 00                	push   $0x0
80106022:	6a 41                	push   $0x41
80106024:	e9 e1 f8 ff ff       	jmp    8010590a <alltraps>

80106029 <vector66>:
80106029:	6a 00                	push   $0x0
8010602b:	6a 42                	push   $0x42
8010602d:	e9 d8 f8 ff ff       	jmp    8010590a <alltraps>

80106032 <vector67>:
80106032:	6a 00                	push   $0x0
80106034:	6a 43                	push   $0x43
80106036:	e9 cf f8 ff ff       	jmp    8010590a <alltraps>

8010603b <vector68>:
8010603b:	6a 00                	push   $0x0
8010603d:	6a 44                	push   $0x44
8010603f:	e9 c6 f8 ff ff       	jmp    8010590a <alltraps>

80106044 <vector69>:
80106044:	6a 00                	push   $0x0
80106046:	6a 45                	push   $0x45
80106048:	e9 bd f8 ff ff       	jmp    8010590a <alltraps>

8010604d <vector70>:
8010604d:	6a 00                	push   $0x0
8010604f:	6a 46                	push   $0x46
80106051:	e9 b4 f8 ff ff       	jmp    8010590a <alltraps>

80106056 <vector71>:
80106056:	6a 00                	push   $0x0
80106058:	6a 47                	push   $0x47
8010605a:	e9 ab f8 ff ff       	jmp    8010590a <alltraps>

8010605f <vector72>:
8010605f:	6a 00                	push   $0x0
80106061:	6a 48                	push   $0x48
80106063:	e9 a2 f8 ff ff       	jmp    8010590a <alltraps>

80106068 <vector73>:
80106068:	6a 00                	push   $0x0
8010606a:	6a 49                	push   $0x49
8010606c:	e9 99 f8 ff ff       	jmp    8010590a <alltraps>

80106071 <vector74>:
80106071:	6a 00                	push   $0x0
80106073:	6a 4a                	push   $0x4a
80106075:	e9 90 f8 ff ff       	jmp    8010590a <alltraps>

8010607a <vector75>:
8010607a:	6a 00                	push   $0x0
8010607c:	6a 4b                	push   $0x4b
8010607e:	e9 87 f8 ff ff       	jmp    8010590a <alltraps>

80106083 <vector76>:
80106083:	6a 00                	push   $0x0
80106085:	6a 4c                	push   $0x4c
80106087:	e9 7e f8 ff ff       	jmp    8010590a <alltraps>

8010608c <vector77>:
8010608c:	6a 00                	push   $0x0
8010608e:	6a 4d                	push   $0x4d
80106090:	e9 75 f8 ff ff       	jmp    8010590a <alltraps>

80106095 <vector78>:
80106095:	6a 00                	push   $0x0
80106097:	6a 4e                	push   $0x4e
80106099:	e9 6c f8 ff ff       	jmp    8010590a <alltraps>

8010609e <vector79>:
8010609e:	6a 00                	push   $0x0
801060a0:	6a 4f                	push   $0x4f
801060a2:	e9 63 f8 ff ff       	jmp    8010590a <alltraps>

801060a7 <vector80>:
801060a7:	6a 00                	push   $0x0
801060a9:	6a 50                	push   $0x50
801060ab:	e9 5a f8 ff ff       	jmp    8010590a <alltraps>

801060b0 <vector81>:
801060b0:	6a 00                	push   $0x0
801060b2:	6a 51                	push   $0x51
801060b4:	e9 51 f8 ff ff       	jmp    8010590a <alltraps>

801060b9 <vector82>:
801060b9:	6a 00                	push   $0x0
801060bb:	6a 52                	push   $0x52
801060bd:	e9 48 f8 ff ff       	jmp    8010590a <alltraps>

801060c2 <vector83>:
801060c2:	6a 00                	push   $0x0
801060c4:	6a 53                	push   $0x53
801060c6:	e9 3f f8 ff ff       	jmp    8010590a <alltraps>

801060cb <vector84>:
801060cb:	6a 00                	push   $0x0
801060cd:	6a 54                	push   $0x54
801060cf:	e9 36 f8 ff ff       	jmp    8010590a <alltraps>

801060d4 <vector85>:
801060d4:	6a 00                	push   $0x0
801060d6:	6a 55                	push   $0x55
801060d8:	e9 2d f8 ff ff       	jmp    8010590a <alltraps>

801060dd <vector86>:
801060dd:	6a 00                	push   $0x0
801060df:	6a 56                	push   $0x56
801060e1:	e9 24 f8 ff ff       	jmp    8010590a <alltraps>

801060e6 <vector87>:
801060e6:	6a 00                	push   $0x0
801060e8:	6a 57                	push   $0x57
801060ea:	e9 1b f8 ff ff       	jmp    8010590a <alltraps>

801060ef <vector88>:
801060ef:	6a 00                	push   $0x0
801060f1:	6a 58                	push   $0x58
801060f3:	e9 12 f8 ff ff       	jmp    8010590a <alltraps>

801060f8 <vector89>:
801060f8:	6a 00                	push   $0x0
801060fa:	6a 59                	push   $0x59
801060fc:	e9 09 f8 ff ff       	jmp    8010590a <alltraps>

80106101 <vector90>:
80106101:	6a 00                	push   $0x0
80106103:	6a 5a                	push   $0x5a
80106105:	e9 00 f8 ff ff       	jmp    8010590a <alltraps>

8010610a <vector91>:
8010610a:	6a 00                	push   $0x0
8010610c:	6a 5b                	push   $0x5b
8010610e:	e9 f7 f7 ff ff       	jmp    8010590a <alltraps>

80106113 <vector92>:
80106113:	6a 00                	push   $0x0
80106115:	6a 5c                	push   $0x5c
80106117:	e9 ee f7 ff ff       	jmp    8010590a <alltraps>

8010611c <vector93>:
8010611c:	6a 00                	push   $0x0
8010611e:	6a 5d                	push   $0x5d
80106120:	e9 e5 f7 ff ff       	jmp    8010590a <alltraps>

80106125 <vector94>:
80106125:	6a 00                	push   $0x0
80106127:	6a 5e                	push   $0x5e
80106129:	e9 dc f7 ff ff       	jmp    8010590a <alltraps>

8010612e <vector95>:
8010612e:	6a 00                	push   $0x0
80106130:	6a 5f                	push   $0x5f
80106132:	e9 d3 f7 ff ff       	jmp    8010590a <alltraps>

80106137 <vector96>:
80106137:	6a 00                	push   $0x0
80106139:	6a 60                	push   $0x60
8010613b:	e9 ca f7 ff ff       	jmp    8010590a <alltraps>

80106140 <vector97>:
80106140:	6a 00                	push   $0x0
80106142:	6a 61                	push   $0x61
80106144:	e9 c1 f7 ff ff       	jmp    8010590a <alltraps>

80106149 <vector98>:
80106149:	6a 00                	push   $0x0
8010614b:	6a 62                	push   $0x62
8010614d:	e9 b8 f7 ff ff       	jmp    8010590a <alltraps>

80106152 <vector99>:
80106152:	6a 00                	push   $0x0
80106154:	6a 63                	push   $0x63
80106156:	e9 af f7 ff ff       	jmp    8010590a <alltraps>

8010615b <vector100>:
8010615b:	6a 00                	push   $0x0
8010615d:	6a 64                	push   $0x64
8010615f:	e9 a6 f7 ff ff       	jmp    8010590a <alltraps>

80106164 <vector101>:
80106164:	6a 00                	push   $0x0
80106166:	6a 65                	push   $0x65
80106168:	e9 9d f7 ff ff       	jmp    8010590a <alltraps>

8010616d <vector102>:
8010616d:	6a 00                	push   $0x0
8010616f:	6a 66                	push   $0x66
80106171:	e9 94 f7 ff ff       	jmp    8010590a <alltraps>

80106176 <vector103>:
80106176:	6a 00                	push   $0x0
80106178:	6a 67                	push   $0x67
8010617a:	e9 8b f7 ff ff       	jmp    8010590a <alltraps>

8010617f <vector104>:
8010617f:	6a 00                	push   $0x0
80106181:	6a 68                	push   $0x68
80106183:	e9 82 f7 ff ff       	jmp    8010590a <alltraps>

80106188 <vector105>:
80106188:	6a 00                	push   $0x0
8010618a:	6a 69                	push   $0x69
8010618c:	e9 79 f7 ff ff       	jmp    8010590a <alltraps>

80106191 <vector106>:
80106191:	6a 00                	push   $0x0
80106193:	6a 6a                	push   $0x6a
80106195:	e9 70 f7 ff ff       	jmp    8010590a <alltraps>

8010619a <vector107>:
8010619a:	6a 00                	push   $0x0
8010619c:	6a 6b                	push   $0x6b
8010619e:	e9 67 f7 ff ff       	jmp    8010590a <alltraps>

801061a3 <vector108>:
801061a3:	6a 00                	push   $0x0
801061a5:	6a 6c                	push   $0x6c
801061a7:	e9 5e f7 ff ff       	jmp    8010590a <alltraps>

801061ac <vector109>:
801061ac:	6a 00                	push   $0x0
801061ae:	6a 6d                	push   $0x6d
801061b0:	e9 55 f7 ff ff       	jmp    8010590a <alltraps>

801061b5 <vector110>:
801061b5:	6a 00                	push   $0x0
801061b7:	6a 6e                	push   $0x6e
801061b9:	e9 4c f7 ff ff       	jmp    8010590a <alltraps>

801061be <vector111>:
801061be:	6a 00                	push   $0x0
801061c0:	6a 6f                	push   $0x6f
801061c2:	e9 43 f7 ff ff       	jmp    8010590a <alltraps>

801061c7 <vector112>:
801061c7:	6a 00                	push   $0x0
801061c9:	6a 70                	push   $0x70
801061cb:	e9 3a f7 ff ff       	jmp    8010590a <alltraps>

801061d0 <vector113>:
801061d0:	6a 00                	push   $0x0
801061d2:	6a 71                	push   $0x71
801061d4:	e9 31 f7 ff ff       	jmp    8010590a <alltraps>

801061d9 <vector114>:
801061d9:	6a 00                	push   $0x0
801061db:	6a 72                	push   $0x72
801061dd:	e9 28 f7 ff ff       	jmp    8010590a <alltraps>

801061e2 <vector115>:
801061e2:	6a 00                	push   $0x0
801061e4:	6a 73                	push   $0x73
801061e6:	e9 1f f7 ff ff       	jmp    8010590a <alltraps>

801061eb <vector116>:
801061eb:	6a 00                	push   $0x0
801061ed:	6a 74                	push   $0x74
801061ef:	e9 16 f7 ff ff       	jmp    8010590a <alltraps>

801061f4 <vector117>:
801061f4:	6a 00                	push   $0x0
801061f6:	6a 75                	push   $0x75
801061f8:	e9 0d f7 ff ff       	jmp    8010590a <alltraps>

801061fd <vector118>:
801061fd:	6a 00                	push   $0x0
801061ff:	6a 76                	push   $0x76
80106201:	e9 04 f7 ff ff       	jmp    8010590a <alltraps>

80106206 <vector119>:
80106206:	6a 00                	push   $0x0
80106208:	6a 77                	push   $0x77
8010620a:	e9 fb f6 ff ff       	jmp    8010590a <alltraps>

8010620f <vector120>:
8010620f:	6a 00                	push   $0x0
80106211:	6a 78                	push   $0x78
80106213:	e9 f2 f6 ff ff       	jmp    8010590a <alltraps>

80106218 <vector121>:
80106218:	6a 00                	push   $0x0
8010621a:	6a 79                	push   $0x79
8010621c:	e9 e9 f6 ff ff       	jmp    8010590a <alltraps>

80106221 <vector122>:
80106221:	6a 00                	push   $0x0
80106223:	6a 7a                	push   $0x7a
80106225:	e9 e0 f6 ff ff       	jmp    8010590a <alltraps>

8010622a <vector123>:
8010622a:	6a 00                	push   $0x0
8010622c:	6a 7b                	push   $0x7b
8010622e:	e9 d7 f6 ff ff       	jmp    8010590a <alltraps>

80106233 <vector124>:
80106233:	6a 00                	push   $0x0
80106235:	6a 7c                	push   $0x7c
80106237:	e9 ce f6 ff ff       	jmp    8010590a <alltraps>

8010623c <vector125>:
8010623c:	6a 00                	push   $0x0
8010623e:	6a 7d                	push   $0x7d
80106240:	e9 c5 f6 ff ff       	jmp    8010590a <alltraps>

80106245 <vector126>:
80106245:	6a 00                	push   $0x0
80106247:	6a 7e                	push   $0x7e
80106249:	e9 bc f6 ff ff       	jmp    8010590a <alltraps>

8010624e <vector127>:
8010624e:	6a 00                	push   $0x0
80106250:	6a 7f                	push   $0x7f
80106252:	e9 b3 f6 ff ff       	jmp    8010590a <alltraps>

80106257 <vector128>:
80106257:	6a 00                	push   $0x0
80106259:	68 80 00 00 00       	push   $0x80
8010625e:	e9 a7 f6 ff ff       	jmp    8010590a <alltraps>

80106263 <vector129>:
80106263:	6a 00                	push   $0x0
80106265:	68 81 00 00 00       	push   $0x81
8010626a:	e9 9b f6 ff ff       	jmp    8010590a <alltraps>

8010626f <vector130>:
8010626f:	6a 00                	push   $0x0
80106271:	68 82 00 00 00       	push   $0x82
80106276:	e9 8f f6 ff ff       	jmp    8010590a <alltraps>

8010627b <vector131>:
8010627b:	6a 00                	push   $0x0
8010627d:	68 83 00 00 00       	push   $0x83
80106282:	e9 83 f6 ff ff       	jmp    8010590a <alltraps>

80106287 <vector132>:
80106287:	6a 00                	push   $0x0
80106289:	68 84 00 00 00       	push   $0x84
8010628e:	e9 77 f6 ff ff       	jmp    8010590a <alltraps>

80106293 <vector133>:
80106293:	6a 00                	push   $0x0
80106295:	68 85 00 00 00       	push   $0x85
8010629a:	e9 6b f6 ff ff       	jmp    8010590a <alltraps>

8010629f <vector134>:
8010629f:	6a 00                	push   $0x0
801062a1:	68 86 00 00 00       	push   $0x86
801062a6:	e9 5f f6 ff ff       	jmp    8010590a <alltraps>

801062ab <vector135>:
801062ab:	6a 00                	push   $0x0
801062ad:	68 87 00 00 00       	push   $0x87
801062b2:	e9 53 f6 ff ff       	jmp    8010590a <alltraps>

801062b7 <vector136>:
801062b7:	6a 00                	push   $0x0
801062b9:	68 88 00 00 00       	push   $0x88
801062be:	e9 47 f6 ff ff       	jmp    8010590a <alltraps>

801062c3 <vector137>:
801062c3:	6a 00                	push   $0x0
801062c5:	68 89 00 00 00       	push   $0x89
801062ca:	e9 3b f6 ff ff       	jmp    8010590a <alltraps>

801062cf <vector138>:
801062cf:	6a 00                	push   $0x0
801062d1:	68 8a 00 00 00       	push   $0x8a
801062d6:	e9 2f f6 ff ff       	jmp    8010590a <alltraps>

801062db <vector139>:
801062db:	6a 00                	push   $0x0
801062dd:	68 8b 00 00 00       	push   $0x8b
801062e2:	e9 23 f6 ff ff       	jmp    8010590a <alltraps>

801062e7 <vector140>:
801062e7:	6a 00                	push   $0x0
801062e9:	68 8c 00 00 00       	push   $0x8c
801062ee:	e9 17 f6 ff ff       	jmp    8010590a <alltraps>

801062f3 <vector141>:
801062f3:	6a 00                	push   $0x0
801062f5:	68 8d 00 00 00       	push   $0x8d
801062fa:	e9 0b f6 ff ff       	jmp    8010590a <alltraps>

801062ff <vector142>:
801062ff:	6a 00                	push   $0x0
80106301:	68 8e 00 00 00       	push   $0x8e
80106306:	e9 ff f5 ff ff       	jmp    8010590a <alltraps>

8010630b <vector143>:
8010630b:	6a 00                	push   $0x0
8010630d:	68 8f 00 00 00       	push   $0x8f
80106312:	e9 f3 f5 ff ff       	jmp    8010590a <alltraps>

80106317 <vector144>:
80106317:	6a 00                	push   $0x0
80106319:	68 90 00 00 00       	push   $0x90
8010631e:	e9 e7 f5 ff ff       	jmp    8010590a <alltraps>

80106323 <vector145>:
80106323:	6a 00                	push   $0x0
80106325:	68 91 00 00 00       	push   $0x91
8010632a:	e9 db f5 ff ff       	jmp    8010590a <alltraps>

8010632f <vector146>:
8010632f:	6a 00                	push   $0x0
80106331:	68 92 00 00 00       	push   $0x92
80106336:	e9 cf f5 ff ff       	jmp    8010590a <alltraps>

8010633b <vector147>:
8010633b:	6a 00                	push   $0x0
8010633d:	68 93 00 00 00       	push   $0x93
80106342:	e9 c3 f5 ff ff       	jmp    8010590a <alltraps>

80106347 <vector148>:
80106347:	6a 00                	push   $0x0
80106349:	68 94 00 00 00       	push   $0x94
8010634e:	e9 b7 f5 ff ff       	jmp    8010590a <alltraps>

80106353 <vector149>:
80106353:	6a 00                	push   $0x0
80106355:	68 95 00 00 00       	push   $0x95
8010635a:	e9 ab f5 ff ff       	jmp    8010590a <alltraps>

8010635f <vector150>:
8010635f:	6a 00                	push   $0x0
80106361:	68 96 00 00 00       	push   $0x96
80106366:	e9 9f f5 ff ff       	jmp    8010590a <alltraps>

8010636b <vector151>:
8010636b:	6a 00                	push   $0x0
8010636d:	68 97 00 00 00       	push   $0x97
80106372:	e9 93 f5 ff ff       	jmp    8010590a <alltraps>

80106377 <vector152>:
80106377:	6a 00                	push   $0x0
80106379:	68 98 00 00 00       	push   $0x98
8010637e:	e9 87 f5 ff ff       	jmp    8010590a <alltraps>

80106383 <vector153>:
80106383:	6a 00                	push   $0x0
80106385:	68 99 00 00 00       	push   $0x99
8010638a:	e9 7b f5 ff ff       	jmp    8010590a <alltraps>

8010638f <vector154>:
8010638f:	6a 00                	push   $0x0
80106391:	68 9a 00 00 00       	push   $0x9a
80106396:	e9 6f f5 ff ff       	jmp    8010590a <alltraps>

8010639b <vector155>:
8010639b:	6a 00                	push   $0x0
8010639d:	68 9b 00 00 00       	push   $0x9b
801063a2:	e9 63 f5 ff ff       	jmp    8010590a <alltraps>

801063a7 <vector156>:
801063a7:	6a 00                	push   $0x0
801063a9:	68 9c 00 00 00       	push   $0x9c
801063ae:	e9 57 f5 ff ff       	jmp    8010590a <alltraps>

801063b3 <vector157>:
801063b3:	6a 00                	push   $0x0
801063b5:	68 9d 00 00 00       	push   $0x9d
801063ba:	e9 4b f5 ff ff       	jmp    8010590a <alltraps>

801063bf <vector158>:
801063bf:	6a 00                	push   $0x0
801063c1:	68 9e 00 00 00       	push   $0x9e
801063c6:	e9 3f f5 ff ff       	jmp    8010590a <alltraps>

801063cb <vector159>:
801063cb:	6a 00                	push   $0x0
801063cd:	68 9f 00 00 00       	push   $0x9f
801063d2:	e9 33 f5 ff ff       	jmp    8010590a <alltraps>

801063d7 <vector160>:
801063d7:	6a 00                	push   $0x0
801063d9:	68 a0 00 00 00       	push   $0xa0
801063de:	e9 27 f5 ff ff       	jmp    8010590a <alltraps>

801063e3 <vector161>:
801063e3:	6a 00                	push   $0x0
801063e5:	68 a1 00 00 00       	push   $0xa1
801063ea:	e9 1b f5 ff ff       	jmp    8010590a <alltraps>

801063ef <vector162>:
801063ef:	6a 00                	push   $0x0
801063f1:	68 a2 00 00 00       	push   $0xa2
801063f6:	e9 0f f5 ff ff       	jmp    8010590a <alltraps>

801063fb <vector163>:
801063fb:	6a 00                	push   $0x0
801063fd:	68 a3 00 00 00       	push   $0xa3
80106402:	e9 03 f5 ff ff       	jmp    8010590a <alltraps>

80106407 <vector164>:
80106407:	6a 00                	push   $0x0
80106409:	68 a4 00 00 00       	push   $0xa4
8010640e:	e9 f7 f4 ff ff       	jmp    8010590a <alltraps>

80106413 <vector165>:
80106413:	6a 00                	push   $0x0
80106415:	68 a5 00 00 00       	push   $0xa5
8010641a:	e9 eb f4 ff ff       	jmp    8010590a <alltraps>

8010641f <vector166>:
8010641f:	6a 00                	push   $0x0
80106421:	68 a6 00 00 00       	push   $0xa6
80106426:	e9 df f4 ff ff       	jmp    8010590a <alltraps>

8010642b <vector167>:
8010642b:	6a 00                	push   $0x0
8010642d:	68 a7 00 00 00       	push   $0xa7
80106432:	e9 d3 f4 ff ff       	jmp    8010590a <alltraps>

80106437 <vector168>:
80106437:	6a 00                	push   $0x0
80106439:	68 a8 00 00 00       	push   $0xa8
8010643e:	e9 c7 f4 ff ff       	jmp    8010590a <alltraps>

80106443 <vector169>:
80106443:	6a 00                	push   $0x0
80106445:	68 a9 00 00 00       	push   $0xa9
8010644a:	e9 bb f4 ff ff       	jmp    8010590a <alltraps>

8010644f <vector170>:
8010644f:	6a 00                	push   $0x0
80106451:	68 aa 00 00 00       	push   $0xaa
80106456:	e9 af f4 ff ff       	jmp    8010590a <alltraps>

8010645b <vector171>:
8010645b:	6a 00                	push   $0x0
8010645d:	68 ab 00 00 00       	push   $0xab
80106462:	e9 a3 f4 ff ff       	jmp    8010590a <alltraps>

80106467 <vector172>:
80106467:	6a 00                	push   $0x0
80106469:	68 ac 00 00 00       	push   $0xac
8010646e:	e9 97 f4 ff ff       	jmp    8010590a <alltraps>

80106473 <vector173>:
80106473:	6a 00                	push   $0x0
80106475:	68 ad 00 00 00       	push   $0xad
8010647a:	e9 8b f4 ff ff       	jmp    8010590a <alltraps>

8010647f <vector174>:
8010647f:	6a 00                	push   $0x0
80106481:	68 ae 00 00 00       	push   $0xae
80106486:	e9 7f f4 ff ff       	jmp    8010590a <alltraps>

8010648b <vector175>:
8010648b:	6a 00                	push   $0x0
8010648d:	68 af 00 00 00       	push   $0xaf
80106492:	e9 73 f4 ff ff       	jmp    8010590a <alltraps>

80106497 <vector176>:
80106497:	6a 00                	push   $0x0
80106499:	68 b0 00 00 00       	push   $0xb0
8010649e:	e9 67 f4 ff ff       	jmp    8010590a <alltraps>

801064a3 <vector177>:
801064a3:	6a 00                	push   $0x0
801064a5:	68 b1 00 00 00       	push   $0xb1
801064aa:	e9 5b f4 ff ff       	jmp    8010590a <alltraps>

801064af <vector178>:
801064af:	6a 00                	push   $0x0
801064b1:	68 b2 00 00 00       	push   $0xb2
801064b6:	e9 4f f4 ff ff       	jmp    8010590a <alltraps>

801064bb <vector179>:
801064bb:	6a 00                	push   $0x0
801064bd:	68 b3 00 00 00       	push   $0xb3
801064c2:	e9 43 f4 ff ff       	jmp    8010590a <alltraps>

801064c7 <vector180>:
801064c7:	6a 00                	push   $0x0
801064c9:	68 b4 00 00 00       	push   $0xb4
801064ce:	e9 37 f4 ff ff       	jmp    8010590a <alltraps>

801064d3 <vector181>:
801064d3:	6a 00                	push   $0x0
801064d5:	68 b5 00 00 00       	push   $0xb5
801064da:	e9 2b f4 ff ff       	jmp    8010590a <alltraps>

801064df <vector182>:
801064df:	6a 00                	push   $0x0
801064e1:	68 b6 00 00 00       	push   $0xb6
801064e6:	e9 1f f4 ff ff       	jmp    8010590a <alltraps>

801064eb <vector183>:
801064eb:	6a 00                	push   $0x0
801064ed:	68 b7 00 00 00       	push   $0xb7
801064f2:	e9 13 f4 ff ff       	jmp    8010590a <alltraps>

801064f7 <vector184>:
801064f7:	6a 00                	push   $0x0
801064f9:	68 b8 00 00 00       	push   $0xb8
801064fe:	e9 07 f4 ff ff       	jmp    8010590a <alltraps>

80106503 <vector185>:
80106503:	6a 00                	push   $0x0
80106505:	68 b9 00 00 00       	push   $0xb9
8010650a:	e9 fb f3 ff ff       	jmp    8010590a <alltraps>

8010650f <vector186>:
8010650f:	6a 00                	push   $0x0
80106511:	68 ba 00 00 00       	push   $0xba
80106516:	e9 ef f3 ff ff       	jmp    8010590a <alltraps>

8010651b <vector187>:
8010651b:	6a 00                	push   $0x0
8010651d:	68 bb 00 00 00       	push   $0xbb
80106522:	e9 e3 f3 ff ff       	jmp    8010590a <alltraps>

80106527 <vector188>:
80106527:	6a 00                	push   $0x0
80106529:	68 bc 00 00 00       	push   $0xbc
8010652e:	e9 d7 f3 ff ff       	jmp    8010590a <alltraps>

80106533 <vector189>:
80106533:	6a 00                	push   $0x0
80106535:	68 bd 00 00 00       	push   $0xbd
8010653a:	e9 cb f3 ff ff       	jmp    8010590a <alltraps>

8010653f <vector190>:
8010653f:	6a 00                	push   $0x0
80106541:	68 be 00 00 00       	push   $0xbe
80106546:	e9 bf f3 ff ff       	jmp    8010590a <alltraps>

8010654b <vector191>:
8010654b:	6a 00                	push   $0x0
8010654d:	68 bf 00 00 00       	push   $0xbf
80106552:	e9 b3 f3 ff ff       	jmp    8010590a <alltraps>

80106557 <vector192>:
80106557:	6a 00                	push   $0x0
80106559:	68 c0 00 00 00       	push   $0xc0
8010655e:	e9 a7 f3 ff ff       	jmp    8010590a <alltraps>

80106563 <vector193>:
80106563:	6a 00                	push   $0x0
80106565:	68 c1 00 00 00       	push   $0xc1
8010656a:	e9 9b f3 ff ff       	jmp    8010590a <alltraps>

8010656f <vector194>:
8010656f:	6a 00                	push   $0x0
80106571:	68 c2 00 00 00       	push   $0xc2
80106576:	e9 8f f3 ff ff       	jmp    8010590a <alltraps>

8010657b <vector195>:
8010657b:	6a 00                	push   $0x0
8010657d:	68 c3 00 00 00       	push   $0xc3
80106582:	e9 83 f3 ff ff       	jmp    8010590a <alltraps>

80106587 <vector196>:
80106587:	6a 00                	push   $0x0
80106589:	68 c4 00 00 00       	push   $0xc4
8010658e:	e9 77 f3 ff ff       	jmp    8010590a <alltraps>

80106593 <vector197>:
80106593:	6a 00                	push   $0x0
80106595:	68 c5 00 00 00       	push   $0xc5
8010659a:	e9 6b f3 ff ff       	jmp    8010590a <alltraps>

8010659f <vector198>:
8010659f:	6a 00                	push   $0x0
801065a1:	68 c6 00 00 00       	push   $0xc6
801065a6:	e9 5f f3 ff ff       	jmp    8010590a <alltraps>

801065ab <vector199>:
801065ab:	6a 00                	push   $0x0
801065ad:	68 c7 00 00 00       	push   $0xc7
801065b2:	e9 53 f3 ff ff       	jmp    8010590a <alltraps>

801065b7 <vector200>:
801065b7:	6a 00                	push   $0x0
801065b9:	68 c8 00 00 00       	push   $0xc8
801065be:	e9 47 f3 ff ff       	jmp    8010590a <alltraps>

801065c3 <vector201>:
801065c3:	6a 00                	push   $0x0
801065c5:	68 c9 00 00 00       	push   $0xc9
801065ca:	e9 3b f3 ff ff       	jmp    8010590a <alltraps>

801065cf <vector202>:
801065cf:	6a 00                	push   $0x0
801065d1:	68 ca 00 00 00       	push   $0xca
801065d6:	e9 2f f3 ff ff       	jmp    8010590a <alltraps>

801065db <vector203>:
801065db:	6a 00                	push   $0x0
801065dd:	68 cb 00 00 00       	push   $0xcb
801065e2:	e9 23 f3 ff ff       	jmp    8010590a <alltraps>

801065e7 <vector204>:
801065e7:	6a 00                	push   $0x0
801065e9:	68 cc 00 00 00       	push   $0xcc
801065ee:	e9 17 f3 ff ff       	jmp    8010590a <alltraps>

801065f3 <vector205>:
801065f3:	6a 00                	push   $0x0
801065f5:	68 cd 00 00 00       	push   $0xcd
801065fa:	e9 0b f3 ff ff       	jmp    8010590a <alltraps>

801065ff <vector206>:
801065ff:	6a 00                	push   $0x0
80106601:	68 ce 00 00 00       	push   $0xce
80106606:	e9 ff f2 ff ff       	jmp    8010590a <alltraps>

8010660b <vector207>:
8010660b:	6a 00                	push   $0x0
8010660d:	68 cf 00 00 00       	push   $0xcf
80106612:	e9 f3 f2 ff ff       	jmp    8010590a <alltraps>

80106617 <vector208>:
80106617:	6a 00                	push   $0x0
80106619:	68 d0 00 00 00       	push   $0xd0
8010661e:	e9 e7 f2 ff ff       	jmp    8010590a <alltraps>

80106623 <vector209>:
80106623:	6a 00                	push   $0x0
80106625:	68 d1 00 00 00       	push   $0xd1
8010662a:	e9 db f2 ff ff       	jmp    8010590a <alltraps>

8010662f <vector210>:
8010662f:	6a 00                	push   $0x0
80106631:	68 d2 00 00 00       	push   $0xd2
80106636:	e9 cf f2 ff ff       	jmp    8010590a <alltraps>

8010663b <vector211>:
8010663b:	6a 00                	push   $0x0
8010663d:	68 d3 00 00 00       	push   $0xd3
80106642:	e9 c3 f2 ff ff       	jmp    8010590a <alltraps>

80106647 <vector212>:
80106647:	6a 00                	push   $0x0
80106649:	68 d4 00 00 00       	push   $0xd4
8010664e:	e9 b7 f2 ff ff       	jmp    8010590a <alltraps>

80106653 <vector213>:
80106653:	6a 00                	push   $0x0
80106655:	68 d5 00 00 00       	push   $0xd5
8010665a:	e9 ab f2 ff ff       	jmp    8010590a <alltraps>

8010665f <vector214>:
8010665f:	6a 00                	push   $0x0
80106661:	68 d6 00 00 00       	push   $0xd6
80106666:	e9 9f f2 ff ff       	jmp    8010590a <alltraps>

8010666b <vector215>:
8010666b:	6a 00                	push   $0x0
8010666d:	68 d7 00 00 00       	push   $0xd7
80106672:	e9 93 f2 ff ff       	jmp    8010590a <alltraps>

80106677 <vector216>:
80106677:	6a 00                	push   $0x0
80106679:	68 d8 00 00 00       	push   $0xd8
8010667e:	e9 87 f2 ff ff       	jmp    8010590a <alltraps>

80106683 <vector217>:
80106683:	6a 00                	push   $0x0
80106685:	68 d9 00 00 00       	push   $0xd9
8010668a:	e9 7b f2 ff ff       	jmp    8010590a <alltraps>

8010668f <vector218>:
8010668f:	6a 00                	push   $0x0
80106691:	68 da 00 00 00       	push   $0xda
80106696:	e9 6f f2 ff ff       	jmp    8010590a <alltraps>

8010669b <vector219>:
8010669b:	6a 00                	push   $0x0
8010669d:	68 db 00 00 00       	push   $0xdb
801066a2:	e9 63 f2 ff ff       	jmp    8010590a <alltraps>

801066a7 <vector220>:
801066a7:	6a 00                	push   $0x0
801066a9:	68 dc 00 00 00       	push   $0xdc
801066ae:	e9 57 f2 ff ff       	jmp    8010590a <alltraps>

801066b3 <vector221>:
801066b3:	6a 00                	push   $0x0
801066b5:	68 dd 00 00 00       	push   $0xdd
801066ba:	e9 4b f2 ff ff       	jmp    8010590a <alltraps>

801066bf <vector222>:
801066bf:	6a 00                	push   $0x0
801066c1:	68 de 00 00 00       	push   $0xde
801066c6:	e9 3f f2 ff ff       	jmp    8010590a <alltraps>

801066cb <vector223>:
801066cb:	6a 00                	push   $0x0
801066cd:	68 df 00 00 00       	push   $0xdf
801066d2:	e9 33 f2 ff ff       	jmp    8010590a <alltraps>

801066d7 <vector224>:
801066d7:	6a 00                	push   $0x0
801066d9:	68 e0 00 00 00       	push   $0xe0
801066de:	e9 27 f2 ff ff       	jmp    8010590a <alltraps>

801066e3 <vector225>:
801066e3:	6a 00                	push   $0x0
801066e5:	68 e1 00 00 00       	push   $0xe1
801066ea:	e9 1b f2 ff ff       	jmp    8010590a <alltraps>

801066ef <vector226>:
801066ef:	6a 00                	push   $0x0
801066f1:	68 e2 00 00 00       	push   $0xe2
801066f6:	e9 0f f2 ff ff       	jmp    8010590a <alltraps>

801066fb <vector227>:
801066fb:	6a 00                	push   $0x0
801066fd:	68 e3 00 00 00       	push   $0xe3
80106702:	e9 03 f2 ff ff       	jmp    8010590a <alltraps>

80106707 <vector228>:
80106707:	6a 00                	push   $0x0
80106709:	68 e4 00 00 00       	push   $0xe4
8010670e:	e9 f7 f1 ff ff       	jmp    8010590a <alltraps>

80106713 <vector229>:
80106713:	6a 00                	push   $0x0
80106715:	68 e5 00 00 00       	push   $0xe5
8010671a:	e9 eb f1 ff ff       	jmp    8010590a <alltraps>

8010671f <vector230>:
8010671f:	6a 00                	push   $0x0
80106721:	68 e6 00 00 00       	push   $0xe6
80106726:	e9 df f1 ff ff       	jmp    8010590a <alltraps>

8010672b <vector231>:
8010672b:	6a 00                	push   $0x0
8010672d:	68 e7 00 00 00       	push   $0xe7
80106732:	e9 d3 f1 ff ff       	jmp    8010590a <alltraps>

80106737 <vector232>:
80106737:	6a 00                	push   $0x0
80106739:	68 e8 00 00 00       	push   $0xe8
8010673e:	e9 c7 f1 ff ff       	jmp    8010590a <alltraps>

80106743 <vector233>:
80106743:	6a 00                	push   $0x0
80106745:	68 e9 00 00 00       	push   $0xe9
8010674a:	e9 bb f1 ff ff       	jmp    8010590a <alltraps>

8010674f <vector234>:
8010674f:	6a 00                	push   $0x0
80106751:	68 ea 00 00 00       	push   $0xea
80106756:	e9 af f1 ff ff       	jmp    8010590a <alltraps>

8010675b <vector235>:
8010675b:	6a 00                	push   $0x0
8010675d:	68 eb 00 00 00       	push   $0xeb
80106762:	e9 a3 f1 ff ff       	jmp    8010590a <alltraps>

80106767 <vector236>:
80106767:	6a 00                	push   $0x0
80106769:	68 ec 00 00 00       	push   $0xec
8010676e:	e9 97 f1 ff ff       	jmp    8010590a <alltraps>

80106773 <vector237>:
80106773:	6a 00                	push   $0x0
80106775:	68 ed 00 00 00       	push   $0xed
8010677a:	e9 8b f1 ff ff       	jmp    8010590a <alltraps>

8010677f <vector238>:
8010677f:	6a 00                	push   $0x0
80106781:	68 ee 00 00 00       	push   $0xee
80106786:	e9 7f f1 ff ff       	jmp    8010590a <alltraps>

8010678b <vector239>:
8010678b:	6a 00                	push   $0x0
8010678d:	68 ef 00 00 00       	push   $0xef
80106792:	e9 73 f1 ff ff       	jmp    8010590a <alltraps>

80106797 <vector240>:
80106797:	6a 00                	push   $0x0
80106799:	68 f0 00 00 00       	push   $0xf0
8010679e:	e9 67 f1 ff ff       	jmp    8010590a <alltraps>

801067a3 <vector241>:
801067a3:	6a 00                	push   $0x0
801067a5:	68 f1 00 00 00       	push   $0xf1
801067aa:	e9 5b f1 ff ff       	jmp    8010590a <alltraps>

801067af <vector242>:
801067af:	6a 00                	push   $0x0
801067b1:	68 f2 00 00 00       	push   $0xf2
801067b6:	e9 4f f1 ff ff       	jmp    8010590a <alltraps>

801067bb <vector243>:
801067bb:	6a 00                	push   $0x0
801067bd:	68 f3 00 00 00       	push   $0xf3
801067c2:	e9 43 f1 ff ff       	jmp    8010590a <alltraps>

801067c7 <vector244>:
801067c7:	6a 00                	push   $0x0
801067c9:	68 f4 00 00 00       	push   $0xf4
801067ce:	e9 37 f1 ff ff       	jmp    8010590a <alltraps>

801067d3 <vector245>:
801067d3:	6a 00                	push   $0x0
801067d5:	68 f5 00 00 00       	push   $0xf5
801067da:	e9 2b f1 ff ff       	jmp    8010590a <alltraps>

801067df <vector246>:
801067df:	6a 00                	push   $0x0
801067e1:	68 f6 00 00 00       	push   $0xf6
801067e6:	e9 1f f1 ff ff       	jmp    8010590a <alltraps>

801067eb <vector247>:
801067eb:	6a 00                	push   $0x0
801067ed:	68 f7 00 00 00       	push   $0xf7
801067f2:	e9 13 f1 ff ff       	jmp    8010590a <alltraps>

801067f7 <vector248>:
801067f7:	6a 00                	push   $0x0
801067f9:	68 f8 00 00 00       	push   $0xf8
801067fe:	e9 07 f1 ff ff       	jmp    8010590a <alltraps>

80106803 <vector249>:
80106803:	6a 00                	push   $0x0
80106805:	68 f9 00 00 00       	push   $0xf9
8010680a:	e9 fb f0 ff ff       	jmp    8010590a <alltraps>

8010680f <vector250>:
8010680f:	6a 00                	push   $0x0
80106811:	68 fa 00 00 00       	push   $0xfa
80106816:	e9 ef f0 ff ff       	jmp    8010590a <alltraps>

8010681b <vector251>:
8010681b:	6a 00                	push   $0x0
8010681d:	68 fb 00 00 00       	push   $0xfb
80106822:	e9 e3 f0 ff ff       	jmp    8010590a <alltraps>

80106827 <vector252>:
80106827:	6a 00                	push   $0x0
80106829:	68 fc 00 00 00       	push   $0xfc
8010682e:	e9 d7 f0 ff ff       	jmp    8010590a <alltraps>

80106833 <vector253>:
80106833:	6a 00                	push   $0x0
80106835:	68 fd 00 00 00       	push   $0xfd
8010683a:	e9 cb f0 ff ff       	jmp    8010590a <alltraps>

8010683f <vector254>:
8010683f:	6a 00                	push   $0x0
80106841:	68 fe 00 00 00       	push   $0xfe
80106846:	e9 bf f0 ff ff       	jmp    8010590a <alltraps>

8010684b <vector255>:
8010684b:	6a 00                	push   $0x0
8010684d:	68 ff 00 00 00       	push   $0xff
80106852:	e9 b3 f0 ff ff       	jmp    8010590a <alltraps>
80106857:	66 90                	xchg   %ax,%ax
80106859:	66 90                	xchg   %ax,%ax
8010685b:	66 90                	xchg   %ax,%ax
8010685d:	66 90                	xchg   %ax,%ax
8010685f:	90                   	nop

80106860 <walkpgdir>:
80106860:	55                   	push   %ebp
80106861:	89 e5                	mov    %esp,%ebp
80106863:	57                   	push   %edi
80106864:	56                   	push   %esi
80106865:	53                   	push   %ebx
80106866:	89 d3                	mov    %edx,%ebx
80106868:	89 d7                	mov    %edx,%edi
8010686a:	c1 eb 16             	shr    $0x16,%ebx
8010686d:	8d 34 98             	lea    (%eax,%ebx,4),%esi
80106870:	83 ec 0c             	sub    $0xc,%esp
80106873:	8b 06                	mov    (%esi),%eax
80106875:	a8 01                	test   $0x1,%al
80106877:	74 27                	je     801068a0 <walkpgdir+0x40>
80106879:	25 00 f0 ff ff       	and    $0xfffff000,%eax
8010687e:	8d 98 00 00 00 80    	lea    -0x80000000(%eax),%ebx
80106884:	c1 ef 0a             	shr    $0xa,%edi
80106887:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010688a:	89 fa                	mov    %edi,%edx
8010688c:	81 e2 fc 0f 00 00    	and    $0xffc,%edx
80106892:	8d 04 13             	lea    (%ebx,%edx,1),%eax
80106895:	5b                   	pop    %ebx
80106896:	5e                   	pop    %esi
80106897:	5f                   	pop    %edi
80106898:	5d                   	pop    %ebp
80106899:	c3                   	ret    
8010689a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
801068a0:	85 c9                	test   %ecx,%ecx
801068a2:	74 2c                	je     801068d0 <walkpgdir+0x70>
801068a4:	e8 f7 be ff ff       	call   801027a0 <kalloc>
801068a9:	85 c0                	test   %eax,%eax
801068ab:	89 c3                	mov    %eax,%ebx
801068ad:	74 21                	je     801068d0 <walkpgdir+0x70>
801068af:	83 ec 04             	sub    $0x4,%esp
801068b2:	68 00 10 00 00       	push   $0x1000
801068b7:	6a 00                	push   $0x0
801068b9:	50                   	push   %eax
801068ba:	e8 71 de ff ff       	call   80104730 <memset>
801068bf:	8d 83 00 00 00 80    	lea    -0x80000000(%ebx),%eax
801068c5:	83 c4 10             	add    $0x10,%esp
801068c8:	83 c8 07             	or     $0x7,%eax
801068cb:	89 06                	mov    %eax,(%esi)
801068cd:	eb b5                	jmp    80106884 <walkpgdir+0x24>
801068cf:	90                   	nop
801068d0:	8d 65 f4             	lea    -0xc(%ebp),%esp
801068d3:	31 c0                	xor    %eax,%eax
801068d5:	5b                   	pop    %ebx
801068d6:	5e                   	pop    %esi
801068d7:	5f                   	pop    %edi
801068d8:	5d                   	pop    %ebp
801068d9:	c3                   	ret    
801068da:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

801068e0 <mappages>:
801068e0:	55                   	push   %ebp
801068e1:	89 e5                	mov    %esp,%ebp
801068e3:	57                   	push   %edi
801068e4:	56                   	push   %esi
801068e5:	53                   	push   %ebx
801068e6:	89 d3                	mov    %edx,%ebx
801068e8:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
801068ee:	83 ec 1c             	sub    $0x1c,%esp
801068f1:	89 45 e4             	mov    %eax,-0x1c(%ebp)
801068f4:	8d 44 0a ff          	lea    -0x1(%edx,%ecx,1),%eax
801068f8:	8b 7d 08             	mov    0x8(%ebp),%edi
801068fb:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80106900:	89 45 e0             	mov    %eax,-0x20(%ebp)
80106903:	8b 45 0c             	mov    0xc(%ebp),%eax
80106906:	29 df                	sub    %ebx,%edi
80106908:	83 c8 01             	or     $0x1,%eax
8010690b:	89 45 dc             	mov    %eax,-0x24(%ebp)
8010690e:	eb 15                	jmp    80106925 <mappages+0x45>
80106910:	f6 00 01             	testb  $0x1,(%eax)
80106913:	75 45                	jne    8010695a <mappages+0x7a>
80106915:	0b 75 dc             	or     -0x24(%ebp),%esi
80106918:	3b 5d e0             	cmp    -0x20(%ebp),%ebx
8010691b:	89 30                	mov    %esi,(%eax)
8010691d:	74 31                	je     80106950 <mappages+0x70>
8010691f:	81 c3 00 10 00 00    	add    $0x1000,%ebx
80106925:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80106928:	b9 01 00 00 00       	mov    $0x1,%ecx
8010692d:	89 da                	mov    %ebx,%edx
8010692f:	8d 34 3b             	lea    (%ebx,%edi,1),%esi
80106932:	e8 29 ff ff ff       	call   80106860 <walkpgdir>
80106937:	85 c0                	test   %eax,%eax
80106939:	75 d5                	jne    80106910 <mappages+0x30>
8010693b:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010693e:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106943:	5b                   	pop    %ebx
80106944:	5e                   	pop    %esi
80106945:	5f                   	pop    %edi
80106946:	5d                   	pop    %ebp
80106947:	c3                   	ret    
80106948:	90                   	nop
80106949:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80106950:	8d 65 f4             	lea    -0xc(%ebp),%esp
80106953:	31 c0                	xor    %eax,%eax
80106955:	5b                   	pop    %ebx
80106956:	5e                   	pop    %esi
80106957:	5f                   	pop    %edi
80106958:	5d                   	pop    %ebp
80106959:	c3                   	ret    
8010695a:	83 ec 0c             	sub    $0xc,%esp
8010695d:	68 68 7a 10 80       	push   $0x80107a68
80106962:	e8 29 9a ff ff       	call   80100390 <panic>
80106967:	89 f6                	mov    %esi,%esi
80106969:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80106970 <deallocuvm.part.0>:
80106970:	55                   	push   %ebp
80106971:	89 e5                	mov    %esp,%ebp
80106973:	57                   	push   %edi
80106974:	56                   	push   %esi
80106975:	53                   	push   %ebx
80106976:	8d 99 ff 0f 00 00    	lea    0xfff(%ecx),%ebx
8010697c:	89 c7                	mov    %eax,%edi
8010697e:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
80106984:	83 ec 1c             	sub    $0x1c,%esp
80106987:	89 4d e0             	mov    %ecx,-0x20(%ebp)
8010698a:	39 d3                	cmp    %edx,%ebx
8010698c:	73 66                	jae    801069f4 <deallocuvm.part.0+0x84>
8010698e:	89 d6                	mov    %edx,%esi
80106990:	eb 3d                	jmp    801069cf <deallocuvm.part.0+0x5f>
80106992:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80106998:	8b 10                	mov    (%eax),%edx
8010699a:	f6 c2 01             	test   $0x1,%dl
8010699d:	74 26                	je     801069c5 <deallocuvm.part.0+0x55>
8010699f:	81 e2 00 f0 ff ff    	and    $0xfffff000,%edx
801069a5:	74 58                	je     801069ff <deallocuvm.part.0+0x8f>
801069a7:	83 ec 0c             	sub    $0xc,%esp
801069aa:	81 c2 00 00 00 80    	add    $0x80000000,%edx
801069b0:	89 45 e4             	mov    %eax,-0x1c(%ebp)
801069b3:	52                   	push   %edx
801069b4:	e8 37 bc ff ff       	call   801025f0 <kfree>
801069b9:	8b 45 e4             	mov    -0x1c(%ebp),%eax
801069bc:	83 c4 10             	add    $0x10,%esp
801069bf:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
801069c5:	81 c3 00 10 00 00    	add    $0x1000,%ebx
801069cb:	39 f3                	cmp    %esi,%ebx
801069cd:	73 25                	jae    801069f4 <deallocuvm.part.0+0x84>
801069cf:	31 c9                	xor    %ecx,%ecx
801069d1:	89 da                	mov    %ebx,%edx
801069d3:	89 f8                	mov    %edi,%eax
801069d5:	e8 86 fe ff ff       	call   80106860 <walkpgdir>
801069da:	85 c0                	test   %eax,%eax
801069dc:	75 ba                	jne    80106998 <deallocuvm.part.0+0x28>
801069de:	81 e3 00 00 c0 ff    	and    $0xffc00000,%ebx
801069e4:	81 c3 00 f0 3f 00    	add    $0x3ff000,%ebx
801069ea:	81 c3 00 10 00 00    	add    $0x1000,%ebx
801069f0:	39 f3                	cmp    %esi,%ebx
801069f2:	72 db                	jb     801069cf <deallocuvm.part.0+0x5f>
801069f4:	8b 45 e0             	mov    -0x20(%ebp),%eax
801069f7:	8d 65 f4             	lea    -0xc(%ebp),%esp
801069fa:	5b                   	pop    %ebx
801069fb:	5e                   	pop    %esi
801069fc:	5f                   	pop    %edi
801069fd:	5d                   	pop    %ebp
801069fe:	c3                   	ret    
801069ff:	83 ec 0c             	sub    $0xc,%esp
80106a02:	68 06 74 10 80       	push   $0x80107406
80106a07:	e8 84 99 ff ff       	call   80100390 <panic>
80106a0c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80106a10 <seginit>:
80106a10:	55                   	push   %ebp
80106a11:	89 e5                	mov    %esp,%ebp
80106a13:	83 ec 18             	sub    $0x18,%esp
80106a16:	e8 85 d0 ff ff       	call   80103aa0 <cpuid>
80106a1b:	69 c0 b0 00 00 00    	imul   $0xb0,%eax,%eax
80106a21:	ba 2f 00 00 00       	mov    $0x2f,%edx
80106a26:	66 89 55 f2          	mov    %dx,-0xe(%ebp)
80106a2a:	c7 80 f8 27 11 80 ff 	movl   $0xffff,-0x7feed808(%eax)
80106a31:	ff 00 00 
80106a34:	c7 80 fc 27 11 80 00 	movl   $0xcf9a00,-0x7feed804(%eax)
80106a3b:	9a cf 00 
80106a3e:	c7 80 00 28 11 80 ff 	movl   $0xffff,-0x7feed800(%eax)
80106a45:	ff 00 00 
80106a48:	c7 80 04 28 11 80 00 	movl   $0xcf9200,-0x7feed7fc(%eax)
80106a4f:	92 cf 00 
80106a52:	c7 80 08 28 11 80 ff 	movl   $0xffff,-0x7feed7f8(%eax)
80106a59:	ff 00 00 
80106a5c:	c7 80 0c 28 11 80 00 	movl   $0xcffa00,-0x7feed7f4(%eax)
80106a63:	fa cf 00 
80106a66:	c7 80 10 28 11 80 ff 	movl   $0xffff,-0x7feed7f0(%eax)
80106a6d:	ff 00 00 
80106a70:	c7 80 14 28 11 80 00 	movl   $0xcff200,-0x7feed7ec(%eax)
80106a77:	f2 cf 00 
80106a7a:	05 f0 27 11 80       	add    $0x801127f0,%eax
80106a7f:	66 89 45 f4          	mov    %ax,-0xc(%ebp)
80106a83:	c1 e8 10             	shr    $0x10,%eax
80106a86:	66 89 45 f6          	mov    %ax,-0xa(%ebp)
80106a8a:	8d 45 f2             	lea    -0xe(%ebp),%eax
80106a8d:	0f 01 10             	lgdtl  (%eax)
80106a90:	c9                   	leave  
80106a91:	c3                   	ret    
80106a92:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80106a99:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80106aa0 <switchkvm>:
80106aa0:	a1 a4 54 11 80       	mov    0x801154a4,%eax
80106aa5:	55                   	push   %ebp
80106aa6:	89 e5                	mov    %esp,%ebp
80106aa8:	05 00 00 00 80       	add    $0x80000000,%eax
80106aad:	0f 22 d8             	mov    %eax,%cr3
80106ab0:	5d                   	pop    %ebp
80106ab1:	c3                   	ret    
80106ab2:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80106ab9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80106ac0 <switchuvm>:
80106ac0:	55                   	push   %ebp
80106ac1:	89 e5                	mov    %esp,%ebp
80106ac3:	57                   	push   %edi
80106ac4:	56                   	push   %esi
80106ac5:	53                   	push   %ebx
80106ac6:	83 ec 1c             	sub    $0x1c,%esp
80106ac9:	8b 5d 08             	mov    0x8(%ebp),%ebx
80106acc:	85 db                	test   %ebx,%ebx
80106ace:	0f 84 cb 00 00 00    	je     80106b9f <switchuvm+0xdf>
80106ad4:	8b 43 08             	mov    0x8(%ebx),%eax
80106ad7:	85 c0                	test   %eax,%eax
80106ad9:	0f 84 da 00 00 00    	je     80106bb9 <switchuvm+0xf9>
80106adf:	8b 43 04             	mov    0x4(%ebx),%eax
80106ae2:	85 c0                	test   %eax,%eax
80106ae4:	0f 84 c2 00 00 00    	je     80106bac <switchuvm+0xec>
80106aea:	e8 61 da ff ff       	call   80104550 <pushcli>
80106aef:	e8 2c cf ff ff       	call   80103a20 <mycpu>
80106af4:	89 c6                	mov    %eax,%esi
80106af6:	e8 25 cf ff ff       	call   80103a20 <mycpu>
80106afb:	89 c7                	mov    %eax,%edi
80106afd:	e8 1e cf ff ff       	call   80103a20 <mycpu>
80106b02:	89 45 e4             	mov    %eax,-0x1c(%ebp)
80106b05:	83 c7 08             	add    $0x8,%edi
80106b08:	e8 13 cf ff ff       	call   80103a20 <mycpu>
80106b0d:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
80106b10:	83 c0 08             	add    $0x8,%eax
80106b13:	ba 67 00 00 00       	mov    $0x67,%edx
80106b18:	c1 e8 18             	shr    $0x18,%eax
80106b1b:	66 89 96 98 00 00 00 	mov    %dx,0x98(%esi)
80106b22:	66 89 be 9a 00 00 00 	mov    %di,0x9a(%esi)
80106b29:	88 86 9f 00 00 00    	mov    %al,0x9f(%esi)
80106b2f:	bf ff ff ff ff       	mov    $0xffffffff,%edi
80106b34:	83 c1 08             	add    $0x8,%ecx
80106b37:	c1 e9 10             	shr    $0x10,%ecx
80106b3a:	88 8e 9c 00 00 00    	mov    %cl,0x9c(%esi)
80106b40:	b9 99 40 00 00       	mov    $0x4099,%ecx
80106b45:	66 89 8e 9d 00 00 00 	mov    %cx,0x9d(%esi)
80106b4c:	be 10 00 00 00       	mov    $0x10,%esi
80106b51:	e8 ca ce ff ff       	call   80103a20 <mycpu>
80106b56:	80 a0 9d 00 00 00 ef 	andb   $0xef,0x9d(%eax)
80106b5d:	e8 be ce ff ff       	call   80103a20 <mycpu>
80106b62:	66 89 70 10          	mov    %si,0x10(%eax)
80106b66:	8b 73 08             	mov    0x8(%ebx),%esi
80106b69:	e8 b2 ce ff ff       	call   80103a20 <mycpu>
80106b6e:	81 c6 00 10 00 00    	add    $0x1000,%esi
80106b74:	89 70 0c             	mov    %esi,0xc(%eax)
80106b77:	e8 a4 ce ff ff       	call   80103a20 <mycpu>
80106b7c:	66 89 78 6e          	mov    %di,0x6e(%eax)
80106b80:	b8 28 00 00 00       	mov    $0x28,%eax
80106b85:	0f 00 d8             	ltr    %ax
80106b88:	8b 43 04             	mov    0x4(%ebx),%eax
80106b8b:	05 00 00 00 80       	add    $0x80000000,%eax
80106b90:	0f 22 d8             	mov    %eax,%cr3
80106b93:	8d 65 f4             	lea    -0xc(%ebp),%esp
80106b96:	5b                   	pop    %ebx
80106b97:	5e                   	pop    %esi
80106b98:	5f                   	pop    %edi
80106b99:	5d                   	pop    %ebp
80106b9a:	e9 f1 d9 ff ff       	jmp    80104590 <popcli>
80106b9f:	83 ec 0c             	sub    $0xc,%esp
80106ba2:	68 6e 7a 10 80       	push   $0x80107a6e
80106ba7:	e8 e4 97 ff ff       	call   80100390 <panic>
80106bac:	83 ec 0c             	sub    $0xc,%esp
80106baf:	68 99 7a 10 80       	push   $0x80107a99
80106bb4:	e8 d7 97 ff ff       	call   80100390 <panic>
80106bb9:	83 ec 0c             	sub    $0xc,%esp
80106bbc:	68 84 7a 10 80       	push   $0x80107a84
80106bc1:	e8 ca 97 ff ff       	call   80100390 <panic>
80106bc6:	8d 76 00             	lea    0x0(%esi),%esi
80106bc9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80106bd0 <inituvm>:
80106bd0:	55                   	push   %ebp
80106bd1:	89 e5                	mov    %esp,%ebp
80106bd3:	57                   	push   %edi
80106bd4:	56                   	push   %esi
80106bd5:	53                   	push   %ebx
80106bd6:	83 ec 1c             	sub    $0x1c,%esp
80106bd9:	8b 75 10             	mov    0x10(%ebp),%esi
80106bdc:	8b 45 08             	mov    0x8(%ebp),%eax
80106bdf:	8b 7d 0c             	mov    0xc(%ebp),%edi
80106be2:	81 fe ff 0f 00 00    	cmp    $0xfff,%esi
80106be8:	89 45 e4             	mov    %eax,-0x1c(%ebp)
80106beb:	77 49                	ja     80106c36 <inituvm+0x66>
80106bed:	e8 ae bb ff ff       	call   801027a0 <kalloc>
80106bf2:	83 ec 04             	sub    $0x4,%esp
80106bf5:	89 c3                	mov    %eax,%ebx
80106bf7:	68 00 10 00 00       	push   $0x1000
80106bfc:	6a 00                	push   $0x0
80106bfe:	50                   	push   %eax
80106bff:	e8 2c db ff ff       	call   80104730 <memset>
80106c04:	58                   	pop    %eax
80106c05:	8d 83 00 00 00 80    	lea    -0x80000000(%ebx),%eax
80106c0b:	b9 00 10 00 00       	mov    $0x1000,%ecx
80106c10:	5a                   	pop    %edx
80106c11:	6a 06                	push   $0x6
80106c13:	50                   	push   %eax
80106c14:	31 d2                	xor    %edx,%edx
80106c16:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80106c19:	e8 c2 fc ff ff       	call   801068e0 <mappages>
80106c1e:	89 75 10             	mov    %esi,0x10(%ebp)
80106c21:	89 7d 0c             	mov    %edi,0xc(%ebp)
80106c24:	83 c4 10             	add    $0x10,%esp
80106c27:	89 5d 08             	mov    %ebx,0x8(%ebp)
80106c2a:	8d 65 f4             	lea    -0xc(%ebp),%esp
80106c2d:	5b                   	pop    %ebx
80106c2e:	5e                   	pop    %esi
80106c2f:	5f                   	pop    %edi
80106c30:	5d                   	pop    %ebp
80106c31:	e9 aa db ff ff       	jmp    801047e0 <memmove>
80106c36:	83 ec 0c             	sub    $0xc,%esp
80106c39:	68 ad 7a 10 80       	push   $0x80107aad
80106c3e:	e8 4d 97 ff ff       	call   80100390 <panic>
80106c43:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80106c49:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80106c50 <loaduvm>:
80106c50:	55                   	push   %ebp
80106c51:	89 e5                	mov    %esp,%ebp
80106c53:	57                   	push   %edi
80106c54:	56                   	push   %esi
80106c55:	53                   	push   %ebx
80106c56:	83 ec 0c             	sub    $0xc,%esp
80106c59:	f7 45 0c ff 0f 00 00 	testl  $0xfff,0xc(%ebp)
80106c60:	0f 85 91 00 00 00    	jne    80106cf7 <loaduvm+0xa7>
80106c66:	8b 75 18             	mov    0x18(%ebp),%esi
80106c69:	31 db                	xor    %ebx,%ebx
80106c6b:	85 f6                	test   %esi,%esi
80106c6d:	75 1a                	jne    80106c89 <loaduvm+0x39>
80106c6f:	eb 6f                	jmp    80106ce0 <loaduvm+0x90>
80106c71:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80106c78:	81 c3 00 10 00 00    	add    $0x1000,%ebx
80106c7e:	81 ee 00 10 00 00    	sub    $0x1000,%esi
80106c84:	39 5d 18             	cmp    %ebx,0x18(%ebp)
80106c87:	76 57                	jbe    80106ce0 <loaduvm+0x90>
80106c89:	8b 55 0c             	mov    0xc(%ebp),%edx
80106c8c:	8b 45 08             	mov    0x8(%ebp),%eax
80106c8f:	31 c9                	xor    %ecx,%ecx
80106c91:	01 da                	add    %ebx,%edx
80106c93:	e8 c8 fb ff ff       	call   80106860 <walkpgdir>
80106c98:	85 c0                	test   %eax,%eax
80106c9a:	74 4e                	je     80106cea <loaduvm+0x9a>
80106c9c:	8b 00                	mov    (%eax),%eax
80106c9e:	8b 4d 14             	mov    0x14(%ebp),%ecx
80106ca1:	bf 00 10 00 00       	mov    $0x1000,%edi
80106ca6:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80106cab:	81 fe ff 0f 00 00    	cmp    $0xfff,%esi
80106cb1:	0f 46 fe             	cmovbe %esi,%edi
80106cb4:	01 d9                	add    %ebx,%ecx
80106cb6:	05 00 00 00 80       	add    $0x80000000,%eax
80106cbb:	57                   	push   %edi
80106cbc:	51                   	push   %ecx
80106cbd:	50                   	push   %eax
80106cbe:	ff 75 10             	pushl  0x10(%ebp)
80106cc1:	e8 7a af ff ff       	call   80101c40 <readi>
80106cc6:	83 c4 10             	add    $0x10,%esp
80106cc9:	39 f8                	cmp    %edi,%eax
80106ccb:	74 ab                	je     80106c78 <loaduvm+0x28>
80106ccd:	8d 65 f4             	lea    -0xc(%ebp),%esp
80106cd0:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106cd5:	5b                   	pop    %ebx
80106cd6:	5e                   	pop    %esi
80106cd7:	5f                   	pop    %edi
80106cd8:	5d                   	pop    %ebp
80106cd9:	c3                   	ret    
80106cda:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80106ce0:	8d 65 f4             	lea    -0xc(%ebp),%esp
80106ce3:	31 c0                	xor    %eax,%eax
80106ce5:	5b                   	pop    %ebx
80106ce6:	5e                   	pop    %esi
80106ce7:	5f                   	pop    %edi
80106ce8:	5d                   	pop    %ebp
80106ce9:	c3                   	ret    
80106cea:	83 ec 0c             	sub    $0xc,%esp
80106ced:	68 c7 7a 10 80       	push   $0x80107ac7
80106cf2:	e8 99 96 ff ff       	call   80100390 <panic>
80106cf7:	83 ec 0c             	sub    $0xc,%esp
80106cfa:	68 68 7b 10 80       	push   $0x80107b68
80106cff:	e8 8c 96 ff ff       	call   80100390 <panic>
80106d04:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80106d0a:	8d bf 00 00 00 00    	lea    0x0(%edi),%edi

80106d10 <allocuvm>:
80106d10:	55                   	push   %ebp
80106d11:	89 e5                	mov    %esp,%ebp
80106d13:	57                   	push   %edi
80106d14:	56                   	push   %esi
80106d15:	53                   	push   %ebx
80106d16:	83 ec 1c             	sub    $0x1c,%esp
80106d19:	8b 7d 10             	mov    0x10(%ebp),%edi
80106d1c:	85 ff                	test   %edi,%edi
80106d1e:	0f 88 8e 00 00 00    	js     80106db2 <allocuvm+0xa2>
80106d24:	3b 7d 0c             	cmp    0xc(%ebp),%edi
80106d27:	0f 82 93 00 00 00    	jb     80106dc0 <allocuvm+0xb0>
80106d2d:	8b 45 0c             	mov    0xc(%ebp),%eax
80106d30:	8d 98 ff 0f 00 00    	lea    0xfff(%eax),%ebx
80106d36:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
80106d3c:	39 5d 10             	cmp    %ebx,0x10(%ebp)
80106d3f:	0f 86 7e 00 00 00    	jbe    80106dc3 <allocuvm+0xb3>
80106d45:	89 7d e4             	mov    %edi,-0x1c(%ebp)
80106d48:	8b 7d 08             	mov    0x8(%ebp),%edi
80106d4b:	eb 42                	jmp    80106d8f <allocuvm+0x7f>
80106d4d:	8d 76 00             	lea    0x0(%esi),%esi
80106d50:	83 ec 04             	sub    $0x4,%esp
80106d53:	68 00 10 00 00       	push   $0x1000
80106d58:	6a 00                	push   $0x0
80106d5a:	50                   	push   %eax
80106d5b:	e8 d0 d9 ff ff       	call   80104730 <memset>
80106d60:	58                   	pop    %eax
80106d61:	8d 86 00 00 00 80    	lea    -0x80000000(%esi),%eax
80106d67:	b9 00 10 00 00       	mov    $0x1000,%ecx
80106d6c:	5a                   	pop    %edx
80106d6d:	6a 06                	push   $0x6
80106d6f:	50                   	push   %eax
80106d70:	89 da                	mov    %ebx,%edx
80106d72:	89 f8                	mov    %edi,%eax
80106d74:	e8 67 fb ff ff       	call   801068e0 <mappages>
80106d79:	83 c4 10             	add    $0x10,%esp
80106d7c:	85 c0                	test   %eax,%eax
80106d7e:	78 50                	js     80106dd0 <allocuvm+0xc0>
80106d80:	81 c3 00 10 00 00    	add    $0x1000,%ebx
80106d86:	39 5d 10             	cmp    %ebx,0x10(%ebp)
80106d89:	0f 86 81 00 00 00    	jbe    80106e10 <allocuvm+0x100>
80106d8f:	e8 0c ba ff ff       	call   801027a0 <kalloc>
80106d94:	85 c0                	test   %eax,%eax
80106d96:	89 c6                	mov    %eax,%esi
80106d98:	75 b6                	jne    80106d50 <allocuvm+0x40>
80106d9a:	83 ec 0c             	sub    $0xc,%esp
80106d9d:	68 e5 7a 10 80       	push   $0x80107ae5
80106da2:	e8 e9 99 ff ff       	call   80100790 <cprintf>
80106da7:	83 c4 10             	add    $0x10,%esp
80106daa:	8b 45 0c             	mov    0xc(%ebp),%eax
80106dad:	39 45 10             	cmp    %eax,0x10(%ebp)
80106db0:	77 6e                	ja     80106e20 <allocuvm+0x110>
80106db2:	8d 65 f4             	lea    -0xc(%ebp),%esp
80106db5:	31 ff                	xor    %edi,%edi
80106db7:	89 f8                	mov    %edi,%eax
80106db9:	5b                   	pop    %ebx
80106dba:	5e                   	pop    %esi
80106dbb:	5f                   	pop    %edi
80106dbc:	5d                   	pop    %ebp
80106dbd:	c3                   	ret    
80106dbe:	66 90                	xchg   %ax,%ax
80106dc0:	8b 7d 0c             	mov    0xc(%ebp),%edi
80106dc3:	8d 65 f4             	lea    -0xc(%ebp),%esp
80106dc6:	89 f8                	mov    %edi,%eax
80106dc8:	5b                   	pop    %ebx
80106dc9:	5e                   	pop    %esi
80106dca:	5f                   	pop    %edi
80106dcb:	5d                   	pop    %ebp
80106dcc:	c3                   	ret    
80106dcd:	8d 76 00             	lea    0x0(%esi),%esi
80106dd0:	83 ec 0c             	sub    $0xc,%esp
80106dd3:	68 fd 7a 10 80       	push   $0x80107afd
80106dd8:	e8 b3 99 ff ff       	call   80100790 <cprintf>
80106ddd:	83 c4 10             	add    $0x10,%esp
80106de0:	8b 45 0c             	mov    0xc(%ebp),%eax
80106de3:	39 45 10             	cmp    %eax,0x10(%ebp)
80106de6:	76 0d                	jbe    80106df5 <allocuvm+0xe5>
80106de8:	89 c1                	mov    %eax,%ecx
80106dea:	8b 55 10             	mov    0x10(%ebp),%edx
80106ded:	8b 45 08             	mov    0x8(%ebp),%eax
80106df0:	e8 7b fb ff ff       	call   80106970 <deallocuvm.part.0>
80106df5:	83 ec 0c             	sub    $0xc,%esp
80106df8:	31 ff                	xor    %edi,%edi
80106dfa:	56                   	push   %esi
80106dfb:	e8 f0 b7 ff ff       	call   801025f0 <kfree>
80106e00:	83 c4 10             	add    $0x10,%esp
80106e03:	8d 65 f4             	lea    -0xc(%ebp),%esp
80106e06:	89 f8                	mov    %edi,%eax
80106e08:	5b                   	pop    %ebx
80106e09:	5e                   	pop    %esi
80106e0a:	5f                   	pop    %edi
80106e0b:	5d                   	pop    %ebp
80106e0c:	c3                   	ret    
80106e0d:	8d 76 00             	lea    0x0(%esi),%esi
80106e10:	8b 7d e4             	mov    -0x1c(%ebp),%edi
80106e13:	8d 65 f4             	lea    -0xc(%ebp),%esp
80106e16:	5b                   	pop    %ebx
80106e17:	89 f8                	mov    %edi,%eax
80106e19:	5e                   	pop    %esi
80106e1a:	5f                   	pop    %edi
80106e1b:	5d                   	pop    %ebp
80106e1c:	c3                   	ret    
80106e1d:	8d 76 00             	lea    0x0(%esi),%esi
80106e20:	89 c1                	mov    %eax,%ecx
80106e22:	8b 55 10             	mov    0x10(%ebp),%edx
80106e25:	8b 45 08             	mov    0x8(%ebp),%eax
80106e28:	31 ff                	xor    %edi,%edi
80106e2a:	e8 41 fb ff ff       	call   80106970 <deallocuvm.part.0>
80106e2f:	eb 92                	jmp    80106dc3 <allocuvm+0xb3>
80106e31:	eb 0d                	jmp    80106e40 <deallocuvm>
80106e33:	90                   	nop
80106e34:	90                   	nop
80106e35:	90                   	nop
80106e36:	90                   	nop
80106e37:	90                   	nop
80106e38:	90                   	nop
80106e39:	90                   	nop
80106e3a:	90                   	nop
80106e3b:	90                   	nop
80106e3c:	90                   	nop
80106e3d:	90                   	nop
80106e3e:	90                   	nop
80106e3f:	90                   	nop

80106e40 <deallocuvm>:
80106e40:	55                   	push   %ebp
80106e41:	89 e5                	mov    %esp,%ebp
80106e43:	8b 55 0c             	mov    0xc(%ebp),%edx
80106e46:	8b 4d 10             	mov    0x10(%ebp),%ecx
80106e49:	8b 45 08             	mov    0x8(%ebp),%eax
80106e4c:	39 d1                	cmp    %edx,%ecx
80106e4e:	73 10                	jae    80106e60 <deallocuvm+0x20>
80106e50:	5d                   	pop    %ebp
80106e51:	e9 1a fb ff ff       	jmp    80106970 <deallocuvm.part.0>
80106e56:	8d 76 00             	lea    0x0(%esi),%esi
80106e59:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
80106e60:	89 d0                	mov    %edx,%eax
80106e62:	5d                   	pop    %ebp
80106e63:	c3                   	ret    
80106e64:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80106e6a:	8d bf 00 00 00 00    	lea    0x0(%edi),%edi

80106e70 <freevm>:
80106e70:	55                   	push   %ebp
80106e71:	89 e5                	mov    %esp,%ebp
80106e73:	57                   	push   %edi
80106e74:	56                   	push   %esi
80106e75:	53                   	push   %ebx
80106e76:	83 ec 0c             	sub    $0xc,%esp
80106e79:	8b 75 08             	mov    0x8(%ebp),%esi
80106e7c:	85 f6                	test   %esi,%esi
80106e7e:	74 59                	je     80106ed9 <freevm+0x69>
80106e80:	31 c9                	xor    %ecx,%ecx
80106e82:	ba 00 00 00 80       	mov    $0x80000000,%edx
80106e87:	89 f0                	mov    %esi,%eax
80106e89:	e8 e2 fa ff ff       	call   80106970 <deallocuvm.part.0>
80106e8e:	89 f3                	mov    %esi,%ebx
80106e90:	8d be 00 10 00 00    	lea    0x1000(%esi),%edi
80106e96:	eb 0f                	jmp    80106ea7 <freevm+0x37>
80106e98:	90                   	nop
80106e99:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80106ea0:	83 c3 04             	add    $0x4,%ebx
80106ea3:	39 fb                	cmp    %edi,%ebx
80106ea5:	74 23                	je     80106eca <freevm+0x5a>
80106ea7:	8b 03                	mov    (%ebx),%eax
80106ea9:	a8 01                	test   $0x1,%al
80106eab:	74 f3                	je     80106ea0 <freevm+0x30>
80106ead:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80106eb2:	83 ec 0c             	sub    $0xc,%esp
80106eb5:	83 c3 04             	add    $0x4,%ebx
80106eb8:	05 00 00 00 80       	add    $0x80000000,%eax
80106ebd:	50                   	push   %eax
80106ebe:	e8 2d b7 ff ff       	call   801025f0 <kfree>
80106ec3:	83 c4 10             	add    $0x10,%esp
80106ec6:	39 fb                	cmp    %edi,%ebx
80106ec8:	75 dd                	jne    80106ea7 <freevm+0x37>
80106eca:	89 75 08             	mov    %esi,0x8(%ebp)
80106ecd:	8d 65 f4             	lea    -0xc(%ebp),%esp
80106ed0:	5b                   	pop    %ebx
80106ed1:	5e                   	pop    %esi
80106ed2:	5f                   	pop    %edi
80106ed3:	5d                   	pop    %ebp
80106ed4:	e9 17 b7 ff ff       	jmp    801025f0 <kfree>
80106ed9:	83 ec 0c             	sub    $0xc,%esp
80106edc:	68 19 7b 10 80       	push   $0x80107b19
80106ee1:	e8 aa 94 ff ff       	call   80100390 <panic>
80106ee6:	8d 76 00             	lea    0x0(%esi),%esi
80106ee9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80106ef0 <setupkvm>:
80106ef0:	55                   	push   %ebp
80106ef1:	89 e5                	mov    %esp,%ebp
80106ef3:	56                   	push   %esi
80106ef4:	53                   	push   %ebx
80106ef5:	e8 a6 b8 ff ff       	call   801027a0 <kalloc>
80106efa:	85 c0                	test   %eax,%eax
80106efc:	89 c6                	mov    %eax,%esi
80106efe:	74 42                	je     80106f42 <setupkvm+0x52>
80106f00:	83 ec 04             	sub    $0x4,%esp
80106f03:	bb 20 a4 10 80       	mov    $0x8010a420,%ebx
80106f08:	68 00 10 00 00       	push   $0x1000
80106f0d:	6a 00                	push   $0x0
80106f0f:	50                   	push   %eax
80106f10:	e8 1b d8 ff ff       	call   80104730 <memset>
80106f15:	83 c4 10             	add    $0x10,%esp
80106f18:	8b 43 04             	mov    0x4(%ebx),%eax
80106f1b:	8b 4b 08             	mov    0x8(%ebx),%ecx
80106f1e:	83 ec 08             	sub    $0x8,%esp
80106f21:	8b 13                	mov    (%ebx),%edx
80106f23:	ff 73 0c             	pushl  0xc(%ebx)
80106f26:	50                   	push   %eax
80106f27:	29 c1                	sub    %eax,%ecx
80106f29:	89 f0                	mov    %esi,%eax
80106f2b:	e8 b0 f9 ff ff       	call   801068e0 <mappages>
80106f30:	83 c4 10             	add    $0x10,%esp
80106f33:	85 c0                	test   %eax,%eax
80106f35:	78 19                	js     80106f50 <setupkvm+0x60>
80106f37:	83 c3 10             	add    $0x10,%ebx
80106f3a:	81 fb 60 a4 10 80    	cmp    $0x8010a460,%ebx
80106f40:	75 d6                	jne    80106f18 <setupkvm+0x28>
80106f42:	8d 65 f8             	lea    -0x8(%ebp),%esp
80106f45:	89 f0                	mov    %esi,%eax
80106f47:	5b                   	pop    %ebx
80106f48:	5e                   	pop    %esi
80106f49:	5d                   	pop    %ebp
80106f4a:	c3                   	ret    
80106f4b:	90                   	nop
80106f4c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80106f50:	83 ec 0c             	sub    $0xc,%esp
80106f53:	56                   	push   %esi
80106f54:	31 f6                	xor    %esi,%esi
80106f56:	e8 15 ff ff ff       	call   80106e70 <freevm>
80106f5b:	83 c4 10             	add    $0x10,%esp
80106f5e:	8d 65 f8             	lea    -0x8(%ebp),%esp
80106f61:	89 f0                	mov    %esi,%eax
80106f63:	5b                   	pop    %ebx
80106f64:	5e                   	pop    %esi
80106f65:	5d                   	pop    %ebp
80106f66:	c3                   	ret    
80106f67:	89 f6                	mov    %esi,%esi
80106f69:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80106f70 <kvmalloc>:
80106f70:	55                   	push   %ebp
80106f71:	89 e5                	mov    %esp,%ebp
80106f73:	83 ec 08             	sub    $0x8,%esp
80106f76:	e8 75 ff ff ff       	call   80106ef0 <setupkvm>
80106f7b:	a3 a4 54 11 80       	mov    %eax,0x801154a4
80106f80:	05 00 00 00 80       	add    $0x80000000,%eax
80106f85:	0f 22 d8             	mov    %eax,%cr3
80106f88:	c9                   	leave  
80106f89:	c3                   	ret    
80106f8a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80106f90 <clearpteu>:
80106f90:	55                   	push   %ebp
80106f91:	31 c9                	xor    %ecx,%ecx
80106f93:	89 e5                	mov    %esp,%ebp
80106f95:	83 ec 08             	sub    $0x8,%esp
80106f98:	8b 55 0c             	mov    0xc(%ebp),%edx
80106f9b:	8b 45 08             	mov    0x8(%ebp),%eax
80106f9e:	e8 bd f8 ff ff       	call   80106860 <walkpgdir>
80106fa3:	85 c0                	test   %eax,%eax
80106fa5:	74 05                	je     80106fac <clearpteu+0x1c>
80106fa7:	83 20 fb             	andl   $0xfffffffb,(%eax)
80106faa:	c9                   	leave  
80106fab:	c3                   	ret    
80106fac:	83 ec 0c             	sub    $0xc,%esp
80106faf:	68 2a 7b 10 80       	push   $0x80107b2a
80106fb4:	e8 d7 93 ff ff       	call   80100390 <panic>
80106fb9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80106fc0 <copyuvm>:
80106fc0:	55                   	push   %ebp
80106fc1:	89 e5                	mov    %esp,%ebp
80106fc3:	57                   	push   %edi
80106fc4:	56                   	push   %esi
80106fc5:	53                   	push   %ebx
80106fc6:	83 ec 1c             	sub    $0x1c,%esp
80106fc9:	e8 22 ff ff ff       	call   80106ef0 <setupkvm>
80106fce:	85 c0                	test   %eax,%eax
80106fd0:	89 45 e0             	mov    %eax,-0x20(%ebp)
80106fd3:	0f 84 9f 00 00 00    	je     80107078 <copyuvm+0xb8>
80106fd9:	8b 4d 0c             	mov    0xc(%ebp),%ecx
80106fdc:	85 c9                	test   %ecx,%ecx
80106fde:	0f 84 94 00 00 00    	je     80107078 <copyuvm+0xb8>
80106fe4:	31 ff                	xor    %edi,%edi
80106fe6:	eb 4a                	jmp    80107032 <copyuvm+0x72>
80106fe8:	90                   	nop
80106fe9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80106ff0:	83 ec 04             	sub    $0x4,%esp
80106ff3:	81 c3 00 00 00 80    	add    $0x80000000,%ebx
80106ff9:	68 00 10 00 00       	push   $0x1000
80106ffe:	53                   	push   %ebx
80106fff:	50                   	push   %eax
80107000:	e8 db d7 ff ff       	call   801047e0 <memmove>
80107005:	58                   	pop    %eax
80107006:	8d 86 00 00 00 80    	lea    -0x80000000(%esi),%eax
8010700c:	b9 00 10 00 00       	mov    $0x1000,%ecx
80107011:	5a                   	pop    %edx
80107012:	ff 75 e4             	pushl  -0x1c(%ebp)
80107015:	50                   	push   %eax
80107016:	89 fa                	mov    %edi,%edx
80107018:	8b 45 e0             	mov    -0x20(%ebp),%eax
8010701b:	e8 c0 f8 ff ff       	call   801068e0 <mappages>
80107020:	83 c4 10             	add    $0x10,%esp
80107023:	85 c0                	test   %eax,%eax
80107025:	78 61                	js     80107088 <copyuvm+0xc8>
80107027:	81 c7 00 10 00 00    	add    $0x1000,%edi
8010702d:	39 7d 0c             	cmp    %edi,0xc(%ebp)
80107030:	76 46                	jbe    80107078 <copyuvm+0xb8>
80107032:	8b 45 08             	mov    0x8(%ebp),%eax
80107035:	31 c9                	xor    %ecx,%ecx
80107037:	89 fa                	mov    %edi,%edx
80107039:	e8 22 f8 ff ff       	call   80106860 <walkpgdir>
8010703e:	85 c0                	test   %eax,%eax
80107040:	74 61                	je     801070a3 <copyuvm+0xe3>
80107042:	8b 00                	mov    (%eax),%eax
80107044:	a8 01                	test   $0x1,%al
80107046:	74 4e                	je     80107096 <copyuvm+0xd6>
80107048:	89 c3                	mov    %eax,%ebx
8010704a:	25 ff 0f 00 00       	and    $0xfff,%eax
8010704f:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
80107055:	89 45 e4             	mov    %eax,-0x1c(%ebp)
80107058:	e8 43 b7 ff ff       	call   801027a0 <kalloc>
8010705d:	85 c0                	test   %eax,%eax
8010705f:	89 c6                	mov    %eax,%esi
80107061:	75 8d                	jne    80106ff0 <copyuvm+0x30>
80107063:	83 ec 0c             	sub    $0xc,%esp
80107066:	ff 75 e0             	pushl  -0x20(%ebp)
80107069:	e8 02 fe ff ff       	call   80106e70 <freevm>
8010706e:	83 c4 10             	add    $0x10,%esp
80107071:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)
80107078:	8b 45 e0             	mov    -0x20(%ebp),%eax
8010707b:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010707e:	5b                   	pop    %ebx
8010707f:	5e                   	pop    %esi
80107080:	5f                   	pop    %edi
80107081:	5d                   	pop    %ebp
80107082:	c3                   	ret    
80107083:	90                   	nop
80107084:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80107088:	83 ec 0c             	sub    $0xc,%esp
8010708b:	56                   	push   %esi
8010708c:	e8 5f b5 ff ff       	call   801025f0 <kfree>
80107091:	83 c4 10             	add    $0x10,%esp
80107094:	eb cd                	jmp    80107063 <copyuvm+0xa3>
80107096:	83 ec 0c             	sub    $0xc,%esp
80107099:	68 4e 7b 10 80       	push   $0x80107b4e
8010709e:	e8 ed 92 ff ff       	call   80100390 <panic>
801070a3:	83 ec 0c             	sub    $0xc,%esp
801070a6:	68 34 7b 10 80       	push   $0x80107b34
801070ab:	e8 e0 92 ff ff       	call   80100390 <panic>

801070b0 <uva2ka>:
801070b0:	55                   	push   %ebp
801070b1:	31 c9                	xor    %ecx,%ecx
801070b3:	89 e5                	mov    %esp,%ebp
801070b5:	83 ec 08             	sub    $0x8,%esp
801070b8:	8b 55 0c             	mov    0xc(%ebp),%edx
801070bb:	8b 45 08             	mov    0x8(%ebp),%eax
801070be:	e8 9d f7 ff ff       	call   80106860 <walkpgdir>
801070c3:	8b 00                	mov    (%eax),%eax
801070c5:	c9                   	leave  
801070c6:	89 c2                	mov    %eax,%edx
801070c8:	25 00 f0 ff ff       	and    $0xfffff000,%eax
801070cd:	83 e2 05             	and    $0x5,%edx
801070d0:	05 00 00 00 80       	add    $0x80000000,%eax
801070d5:	83 fa 05             	cmp    $0x5,%edx
801070d8:	ba 00 00 00 00       	mov    $0x0,%edx
801070dd:	0f 45 c2             	cmovne %edx,%eax
801070e0:	c3                   	ret    
801070e1:	eb 0d                	jmp    801070f0 <copyout>
801070e3:	90                   	nop
801070e4:	90                   	nop
801070e5:	90                   	nop
801070e6:	90                   	nop
801070e7:	90                   	nop
801070e8:	90                   	nop
801070e9:	90                   	nop
801070ea:	90                   	nop
801070eb:	90                   	nop
801070ec:	90                   	nop
801070ed:	90                   	nop
801070ee:	90                   	nop
801070ef:	90                   	nop

801070f0 <copyout>:
801070f0:	55                   	push   %ebp
801070f1:	89 e5                	mov    %esp,%ebp
801070f3:	57                   	push   %edi
801070f4:	56                   	push   %esi
801070f5:	53                   	push   %ebx
801070f6:	83 ec 1c             	sub    $0x1c,%esp
801070f9:	8b 5d 14             	mov    0x14(%ebp),%ebx
801070fc:	8b 55 0c             	mov    0xc(%ebp),%edx
801070ff:	8b 7d 10             	mov    0x10(%ebp),%edi
80107102:	85 db                	test   %ebx,%ebx
80107104:	75 40                	jne    80107146 <copyout+0x56>
80107106:	eb 70                	jmp    80107178 <copyout+0x88>
80107108:	90                   	nop
80107109:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80107110:	8b 55 e4             	mov    -0x1c(%ebp),%edx
80107113:	89 f1                	mov    %esi,%ecx
80107115:	29 d1                	sub    %edx,%ecx
80107117:	81 c1 00 10 00 00    	add    $0x1000,%ecx
8010711d:	39 d9                	cmp    %ebx,%ecx
8010711f:	0f 47 cb             	cmova  %ebx,%ecx
80107122:	29 f2                	sub    %esi,%edx
80107124:	83 ec 04             	sub    $0x4,%esp
80107127:	01 d0                	add    %edx,%eax
80107129:	51                   	push   %ecx
8010712a:	57                   	push   %edi
8010712b:	50                   	push   %eax
8010712c:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
8010712f:	e8 ac d6 ff ff       	call   801047e0 <memmove>
80107134:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
80107137:	83 c4 10             	add    $0x10,%esp
8010713a:	8d 96 00 10 00 00    	lea    0x1000(%esi),%edx
80107140:	01 cf                	add    %ecx,%edi
80107142:	29 cb                	sub    %ecx,%ebx
80107144:	74 32                	je     80107178 <copyout+0x88>
80107146:	89 d6                	mov    %edx,%esi
80107148:	83 ec 08             	sub    $0x8,%esp
8010714b:	89 55 e4             	mov    %edx,-0x1c(%ebp)
8010714e:	81 e6 00 f0 ff ff    	and    $0xfffff000,%esi
80107154:	56                   	push   %esi
80107155:	ff 75 08             	pushl  0x8(%ebp)
80107158:	e8 53 ff ff ff       	call   801070b0 <uva2ka>
8010715d:	83 c4 10             	add    $0x10,%esp
80107160:	85 c0                	test   %eax,%eax
80107162:	75 ac                	jne    80107110 <copyout+0x20>
80107164:	8d 65 f4             	lea    -0xc(%ebp),%esp
80107167:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010716c:	5b                   	pop    %ebx
8010716d:	5e                   	pop    %esi
8010716e:	5f                   	pop    %edi
8010716f:	5d                   	pop    %ebp
80107170:	c3                   	ret    
80107171:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80107178:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010717b:	31 c0                	xor    %eax,%eax
8010717d:	5b                   	pop    %ebx
8010717e:	5e                   	pop    %esi
8010717f:	5f                   	pop    %edi
80107180:	5d                   	pop    %ebp
80107181:	c3                   	ret    
